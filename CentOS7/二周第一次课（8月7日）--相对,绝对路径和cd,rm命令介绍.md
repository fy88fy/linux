
## 2.6 相对和绝对路径
### 相对路径
    相对当前目录位置的路径
    
```
[root@VM_46_188_centos /]# cd /usr/
[root@VM_46_188_centos usr]# ls
bin  etc  games  include  lib  lib64  libexec  local  sbin  share  src  tmp
[root@VM_46_188_centos usr]# cd local/php/
[root@VM_46_188_centos php]# ls
bin  etc  include  lib  php
[root@VM_46_188_centos php]# 
```

### 绝对路径
    从根开始的路径
    
```
[root@VM_46_188_centos fxq]# cd /etc/sysconfig/network-scripts/
[root@VM_46_188_centos network-scripts]# ls
ifcfg-eth0   ifdown-TeamPort  ifdown-ippp  ifdown-ppp     ifup           ifup-bnep  ifup-ipv6   ifup-post    ifup-tunnel        network-functions-ipv6
ifcfg-lo     ifdown-bnep      ifdown-ipv6  ifdown-routes  ifup-Team      ifup-eth   ifup-isdn   ifup-ppp     ifup-wireless
ifdown       ifdown-eth       ifdown-isdn  ifdown-sit     ifup-TeamPort  ifup-ib    ifup-plip   ifup-routes  init.ipv6-global
ifdown-Team  ifdown-ib        ifdown-post  ifdown-tunnel  ifup-aliases   ifup-ippp  ifup-plusb  ifup-sit     network-functions
[root@VM_46_188_centos network-scripts]#
```

## 2.7 cd命令
####    cd 进入目录
####    cd - 进入上一次所在的目录
    
```
[root@VM_46_188_centos usr]# cd local/php/
[root@VM_46_188_centos php]# ls
bin  etc  include  lib  php
[root@VM_46_188_centos php]# cd -
/usr
You have new mail in /var/spool/mail/root
[root@VM_46_188_centos usr]# pwd
/usr
[root@VM_46_188_centos usr]# cd -
/usr/local/php

```

####    cd ~ 进入用户家目录
    
```
[root@VM_46_188_centos ~]# cd ~
[root@VM_46_188_centos ~]# pwd
/root
[root@VM_46_188_centos ~]# cd 
[root@VM_46_188_centos ~]# pwd
/root
[root@VM_46_188_centos ~]# 

```

####    cd .. 进入到上一级目录
    
```
[root@VM_46_188_centos ~]# pwd
/root
[root@VM_46_188_centos ~]# cd ..
[root@VM_46_188_centos /]# pwd
/
[root@VM_46_188_centos /]# 
```

    
## 2.8 创建和删除目录mkdir/rmdir
#### mkdir 创建目录
    mkdir -p /tmp/fxq/1/2 一次创建一系列目录
    
```
[root@VM_46_188_centos /]# mkdir -p /tmp/fxq/1/2
[root@VM_46_188_centos /]# cd /tmp/fxq
[root@VM_46_188_centos fxq]# ls
1
[root@VM_46_188_centos fxq]# cd 1/
[root@VM_46_188_centos 1]# ls
2
[root@VM_46_188_centos 1]# tree /tmp/fxq
/tmp/fxq
`-- 1
    `-- 2

2 directories, 0 files

```

#### rmdir 删除空目录

rmdir -p /tmp/fxq/1/2 级联删除目录


## 2.9 rm命令
#### rm 删除文件

```
[root@VM_46_188_centos 1]# ls
1.txt  2  test1
[root@VM_46_188_centos 1]# rm 1.txt 
rm: remove regular empty file '1.txt'? y      
[root@VM_46_188_centos 1]# ls
2  test1
[root@VM_46_188_centos 1]#  

```

#### rm -r 删除目录


```
[root@VM_46_188_centos 1]# ls
2  test1
[root@VM_46_188_centos 1]# rm -r /tmp/fxq/1/2/
rm: remove directory '/tmp/fxq/1/2/'? y
[root@VM_46_188_centos 1]# ls
test1
[root@VM_46_188_centos 1]# 
```


#### rm -rf 强制删除文件或目录


```
[root@VM_46_188_centos fxq]# tree
.
|-- 1
|   |-- 1.txt
|   `-- test1
`-- 2

3 directories, 1 file
[root@VM_46_188_centos fxq]# rm -rfv /tmp/fxq/1/
removed directory: '/tmp/fxq/1/test1'
removed '/tmp/fxq/1/1.txt'
removed directory: '/tmp/fxq/1/'
[root@VM_46_188_centos fxq]# 

```
-f 加上-f选项删除不存在的文件或目录时不提示信息。


**history 查看命令历史**

**!tree 执行最近一次执行以tree 开头的命令**。
