#!/bin/bash
#date=2017-06-26
#Auther is fengxiaoqing
#
# Shell script scripts to delete user,and write to a file.
# -------------------------------------------------------------------------
# Copyright (c) 2017 fxq project <http://www.fy88fy.cc/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of fxq shell script collection (NSSC)
# Visit http://www.fy88fy.cc/ for more information.
# -------------------------------------------------------------------------
# 

while :
do
ps aux |grep sendmail |grep -v grep &>/dev/null
if [[ $? -ne 0 ]];then
	service sendmail restart
fi

ss -tunl |grep 25 |grep -v grep  &>/dev/null
if [[ $? -ne 0 ]]; then
	yum install sendmail -y && service sendmail start 

fi

number_disk=`df -h | grep "^/dev/" |awk '{print $(NF-1)}' | sed "s/%//"`
if [[ $number_disk -gt 3 ]]; then
	echo "The dev (`df -h | grep "^/dev/" |awk '{print $NF}'`) is leave a little . Please check.----" >/tmp/`date "+%F"`_disk_inode.log
	mail -s "The dev (`df -h | grep "^/dev/" |awk '{print $NF}'`) is leave a little . Please check."  18210031208@139.com < /tmp/`date "+%F"`_disk_inode.log
fi

number_inode=`df -i | grep "^/dev/" |awk '{print $(NF-1)}' | sed "s/%//"`
if [[ $number_inode -gt 3 ]]; then
	echo "The dev  (`df -i | grep "^/dev/" |awk '{print $NF}'`) inode is leave a little . Please check.----`date "+%F %T"`" >/tmp/`date "+%F"`_disk_inode.log
	mail -s "The dev (`df -i | grep "^/dev/" |awk '{print $NF}'`) inode is leave a little . Please check." 18210031208@163.com < /tmp/`date "+%F"`_disk_inode.log
fi 
sleep 30
done
