[toc]
## 2.14 文件和目录权限chmod

```
-rw-r--r--  1 root    root        222 Jul 16 20:55 baidu.png
drwxr-xr-x  2 root    root       4096 May 12 18:51 dir-2017-05-12

```
   rw-      r-x        r-x
   
(所有者) (所属组)   (其他人)

r:读 4    w:写  2    x：执行 1

rw-r-xr-x : 655 

### chmod 700 2.txt 更改文件和目录权限


权限后面的"." 是系统开启了selinux

setenforce 0

getenforce 0

vi /etc/selinux/config

### chmod -R 700 /tmp/test/ 级联更改权限
一次性更改子目录及内部文件的属性

chmod u=rwx,g=r--,o=r-- 2.txt

chmod a+x 2.txt  把所权限都加上执行权限。

chmod g+x 2.txt 给所组加上执行权限。








## 2.15 更改所有者和所属组chown
ls -l /tmp/2.txt

chown fxq /tmp/2.txt

chgrp fxq /tmp/2.txt

chown fxq:fxq /tmp/2.txt

chown :root /tmp/2.txt 更改所属组


chown -R fxq:fxq /tmp/



## 2.16 umask
默认：创建文件为644
    创建目录为755

umask

umask 002

目录没有x就无法进入目录

目录:777-022~755  
文件：666-022~644

(rw-rw-rw-)-(-----w-w-) =rw--r--r- 



## 2.17 隐藏权限lsattr/chattr

chattr +i 1.txt

chattr -i 1.txt

不能mv  不测写  不能touch  不能rm

lsattr 1.txt 查看文件隐藏权限

chattr +a 1.txt 只能追加内容.

不能删除 ,不能更改内容，

lsattr -d /tmp/ 查看目录本身隐藏权限

chattr +a /tmp/ 测追加 可以更改文件夹中文件。

lsattr -R  /tmp/ 查看tmp子目录所有文件的隐藏属性









