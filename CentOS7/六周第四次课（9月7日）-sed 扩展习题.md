
复习

扩展

1.打印某行到某行之间的内容http://ask.apelearn.com/question/559

```
例如：有个文件test的内容如下：
ert
fff
**
[abcfd]
123
324
444
[rty]
**
fgfgf

怎么能截取
[abcfd]
123
324
444
[rty]
这一部分出来呢？
```

    [root@0 sed]# sed -n '/\[abcfd\]/,/\[rty\]/p' 1.txt 
    [abcfd]
    123
    324
    444
    [rty]
    [root@0 sed]# 

**2.sed转换大小写** http://ask.apelearn.com/question/7758
        sed中，使用\u表示大写，\l表示小写
    
    1. 把每个单词的第一个小写字母变大写：
    sed 's/\b[a-z]/\u&/g' 1.txt
    
    2. 把所有小写变大写：
    sed 's/[a-z]/\u&/g' 1.txt
    
    3. 大写变小写：
    sed 's/[A-Z]/\l&/g' 1.txt

**3.sed在某一行最后添加一个数字**
http://ask.apelearn.com/question/288

    sed 's/\(^a.*\)/\1 12/' test
    
    #cat  test
    askdj
    aslkd aslkdjf3e
    skdjfsdfj
    sdkfjk
    fsdkfjksdjfkjsdf
    12sdfesdf
    aslkdjfkasdjf asdlfkjaskdfj
    
    #sed 's/\(^a.*\)/\1 12/' test

    askdj 12
    aslkd aslkdjf3e  12
    skdjfsdfj
    sdkfjk
    fsdkfjksdjfkjsdf
    12sdfesdf
    aslkdjfkasdjf asdlfkjaskdfj  12

**4 删除某行到最后一行**

http://ask.apelearn.com/question/213
```
    [root@test200 ~]# cat test
    a
    b
    c
    d
    e
    f
    [root@test200 ~]# sed '/c/{p;:a;N;$!ba;d}' test
    a
    b
    c
    
    定义一个标签a，匹配c，然后N把下一行加到模式空间里，匹配最后一行时，才退出标签循环，然后命令d，把这个模式空间里的内容全部清除。
    
    if 匹配"c"
    :a
    追加下一行
    if 不匹配"$"
    goto a
    最后退出循环，d命令删除。
    
    另一种写法：
    line=$((`grep -n "fxq" /etc/passwd| head -n1 | cut -d: -f1`+1)) &&  sed ''$line',$'d /etc/passwd && unset line
    
    line=$((`awk '/fxq/ {print NR}' /etc/passwd | head -n1`+1)) &&  sed ''$line',$'d /etc/passwd && unset line

6.打印1到100行含某个字符串的行 
这个需求，其实就是sed指定行范围匹配，较少见。实现：
    
    sed  -n '1,100{/abc/p}'  1.txt
```