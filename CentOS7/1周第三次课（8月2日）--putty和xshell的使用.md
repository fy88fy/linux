[toc]
## 1.9 使用PuTTY远程连接Linux
### 1.putty 介绍

> PuTTY是一个Telnet、SSH、rlogin、纯TCP以及串行接口连接软件。较早的版本仅支持Windows平台，在最近的版本中开始支持各类Unix平台，并打算移植至Mac OS X上。除了官方版本外，有许多第三方的团体或个人将PuTTY移植到其他平台上，像是以Symbian为基础的移动电话。PuTTY为一开放源代码软件，主要由Simon Tatham维护，使用MIT licence授权。随着Linux在服务器端应用的普及，Linux系统管理越来越依赖于远程。在各种远程登录工具中，Putty是出色的工具之一。Putty是一个免费的、Windows x86平台下的Telnet、SSH和rlogin客户端，但是功能丝毫不逊色于商业的Telnet类工具。

### 2.putty 下载及安装
- putty 官网：https://www.chiark.greenend.org.uk/~sgtatham/putty/

- putty 下载连接：https://the.earth.li/~sgtatham/putty/0.70/w32/putty-0.70-installer.msi

- 安装下进入目录：D:\Program Files (x86)\PuTTY。

- 有如下程序：putty、 puttygen、 pscp 、psftp、 plink。

- 双击putty.exe 进入putty界面。

![image](http://note.youdao.com/yws/api/personal/file/C9AE67504E414A8BB3A0EDC4D0CAF062?method=download&shareKey=1b0cc3efef7a0b065bdeca755e00f922)


### 3.putty 使用

#### (1) 输入相应linux IP,端口，sessions名称，再点save保存session。

![image](http://note.youdao.com/yws/api/personal/file/5B423051E91B42B1B4677F1815199725?method=download&shareKey=740db2c671ddd4d2eb45361f05ced201)

#### (2) 输入root及密码,连接linux成功。

![image](http://note.youdao.com/yws/api/personal/file/FAE0B8E03B0B4282B5459CA67917A910?method=download&shareKey=c0f939153db9c8f06aa193a0abc57a6b)

#### (3) 改变putty 内字体大小设置为12

![image](http://note.youdao.com/yws/api/personal/file/6FD851F09A024FD3A997762B8F02B8CB?method=download&shareKey=6b46604505c2adbaa11462369d65fcb5)

![image](http://note.youdao.com/yws/api/personal/file/89BBE735BD1241DD9F85E05D196C9A19?method=download&shareKey=ce5d6729395ad974d8bf4137bf160201)

#### (4) 改变鼠标滚轮上翻条目为9999

![image](http://note.youdao.com/yws/api/personal/file/C343D5E747604B03B4F773A6F8986D8A?method=download&shareKey=343b42341807ccefb14f9d7111e9a526)


## 1.10 使用Xshell连接Linux

### 1.Xshell 介绍

> Xshell 是一个强大的安全终端模拟软件，它支持SSH1, SSH2, 以及Microsoft Windows 平台的TELNET 协议。Xshell 通过互联网到远程主机的安全连接以及它创新性的设计和特色帮助用户在复杂的网络环境中享受他们的工作。
Xshell可以在Windows界面下用来访问远端不同系统下的服务器，从而比较好的达到远程控制终端的目的。

### 2.Xshell 下载及安装

- Xshell 官网https://www.netsarang.com/products/xsh_overview.html

- Xshell 下载连接：http://sw.bos.baidu.com/sw-search-sp/software/30b61b092de57/Xshell_5.0.1325.exe

- 安装后双击桌面Xshell图标, 进入Xshell界面。

![image](http://note.youdao.com/yws/api/personal/file/CC321759E0AA4AA2BB2CA7818F35274B?method=download&shareKey=e45e76131b403ed2dd8de478f0295d3a)


### 3.Xshell的使用

#### (1) 输入相应名称,主机IP，端口，再点“确定” 。

![image](http://note.youdao.com/yws/api/personal/file/41915A9933D040078FA05ED23381EB6C?method=download&shareKey=1565f1779dbea5d3b7cb2539deebb68b)

#### (2) 输入密码,连接linux成功。


```
WARNING! The remote SSH server rejected X11 forwarding request.
Last login: Wed Aug  2 21:17:28 2017 from 192.168.42.2
[root@localhost ~]#
```



#### (3) 改变Xshell 内字体大小设置为9

![image](http://note.youdao.com/yws/api/personal/file/043B6F406E5F454D8898ACC8C11B538E?method=download&shareKey=f82ab649e5984fbe10297a34ab7c6dd9)

![image](http://note.youdao.com/yws/api/personal/file/WEB179779fb1257ccf1fa99bc1f94433a8b?method=download&shareKey=7ce1bc74e899089b3538729a8d41c4c9)

#### (4) 其他一些设置

##### 1) 窗口排列方式：

![image](http://note.youdao.com/yws/api/personal/file/C0EBF7B7453847DCAF810B0769A8823C?method=download&shareKey=fd95f75a1d3f23dc06fb20feb1050d80)

##### 2) 右键快捷粘贴:

![image](http://note.youdao.com/yws/api/personal/file/E54B7D8E86BD467CBE10F42505233DFA?method=download&shareKey=41c0e0c19c386acd04c4f3913ad5a725)


## 1.11 PuTTY密钥认证

> ssh有密码登录和证书登录，初学者都喜欢用密码登录，甚至是root账户登录，密码是123456。但是在实际工作中，尤其是互联网公司，基本都是证书登录的。内网的机器有可能是通过密码登录的，但在外网的机器，如果是密码登录，很容易受到攻击，真正的生产环境中，ssh登录都是证书登录。

> 证书登录的步骤
1.客户端生成证书:私钥和公钥，然后私钥放在客户端，妥当保存，一般为了安全，访问有黑客拷贝客户端的私钥，客户端在生成私钥时，会设置一个密码，以后每次登录ssh服务器时，客户端都要输入密码解开私钥(如果工作中，你使用了一个没有密码的私钥，有一天服务器被黑了，你是跳到黄河都洗不清)。

> 2.服务器添加信用公钥：把客户端生成的公钥，上传到ssh服务器，添加到指定的文件中，这样，就完成ssh证书登录的配置了。

> 假设客户端想通过私钥要登录其他ssh服务器，同理，可以把公钥上传到其他ssh服务器。

下面是Putty实现密钥认证,使用Puttygen生成公钥和私钥

#### 1.选择Generate

![image](http://note.youdao.com/yws/api/personal/file/08AF5DD557E942BBB9BCDB2CA9E5FEBD?method=download&shareKey=a195df0d34cd65e372e7ff477edaf95a)

#### 2.在空白外不断移动鼠标

![image](http://note.youdao.com/yws/api/personal/file/1A57AF71F70744DF9518029B0178D7B3?method=download&shareKey=ad0fd4c1447f6db5995a3d393fb35a77)

#### 3.生成后保存公钥和私钥

![image](http://note.youdao.com/yws/api/personal/file/WEB5f1a18dfc3fc251fa33ddc6c6b503904?method=download&shareKey=58c46026952da956cc13a484f5a90057)

#### 4.进入linux服务器端执行下列操作：

```
mkdir /root/.ssh/
```

```
chmod 700 /root/.ssh/
```
```
vi /root/.ssh/authorized_keys
输入“i”
粘贴复制的公钥
输入“:wq”保存退出
```
#### 5.进入putty设置密钥的路径，并登录测试

![image](http://note.youdao.com/yws/api/personal/file/1048F2C8E2FF49AE83F6BEB640EA2BDF?method=download&shareKey=1e32b01229b7862fe848334328f65129)
出现下面界面，输入私钥的密码即可登录成功：

![image](http://note.youdao.com/yws/api/personal/file/3A4C44E00DBC4BF5A496D9B6DE1CF0B9?method=download&shareKey=e74a85d8e0085a4b0b5974a1beeb0a24)

## 1.12 xshell密钥认证

使用Xshell生成公钥和私钥

#### 1.选择菜单栏-新建用户密钥生成向导

![image](http://note.youdao.com/yws/api/personal/file/7D8306E74277469ABFDACCFF8D2867FA?method=download&shareKey=6674e9fadb034a051ce3db955dcdc6f6)

#### 2.保持默认，下一步

![image](http://note.youdao.com/yws/api/personal/file/CB1EA07CCB2945D0AEDA77A36C759526?method=download&shareKey=18a5b562a1cfce4e317f6ddbaf982d59)

#### 3.生成后保存公钥

![image](http://note.youdao.com/yws/api/personal/file/4014396ACC8246AA9B2BC18F5B4CDFCD?method=download&shareKey=8fc6e95230c9a72366d5a4ebc61dbf12)

私钥自动存储

![image](http://note.youdao.com/yws/api/personal/file/60697D4A58764F479DC653A0FE31DF89?method=download&shareKey=d6d7bd17db6c95b8e2cda4db53d3001a)

#### 4.进入linux服务器端执行下列操作：

```
mkdir /root/.ssh/
```

```
chmod 700 /root/.ssh/
```
```
vi /root/.ssh/authorized_keys
输入“i”

在最后，粘贴复制的公钥
输入“:wq”保存退出
```
#### 5.进入Xshell设置密钥登录测试

![image](http://note.youdao.com/yws/api/personal/file/07A218522CF449C38A1D6465D7F42AA8?method=download&shareKey=bf400a93f80c300dbddf81352a83b371)
出现下面界面，选择公钥认证，对应的私钥文件，输入私钥的密码即可登录成功：

![image](http://note.youdao.com/yws/api/personal/file/75391D2070CA44FAA177EE75FB286CCD?method=download&shareKey=f6a2ee18f23fb4e7afcf30185291b854)


```
Connecting to 192.168.42.180:22...
Connection established.
To escape to local shell, press 'Ctrl+Alt+]'.

WARNING! The remote SSH server rejected X11 forwarding request.
Last login: Wed Aug  2 21:18:05 2017 from 192.168.42.2
[root@localhost ~]# 

```


