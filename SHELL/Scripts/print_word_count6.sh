#!/bin/bash
#date=2017-06-25
#Auther is fengxiaoqing
#
# Shell script scripts to print word ,the word is small 6.
# -------------------------------------------------------------------------
# Copyright (c) 2017 fxq project <http://www.fy88fy.cc/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of fxq shell script collection (NSSC)
# Visit http://www.fy88fy.cc/ for more information.
# -------------------------------------------------------------------------

word=`echo "Bash also interprets a number of multi-character options." | sed 's/ /\n/g'` &>/dev/null
for i in $word
 do

	touch /tmp/i.txt
 	echo "$i" > /tmp/i.txt
 	number=`wc -c /tmp/i.txt|awk '{print $1}'`
	if [[ $number -lt 6 ]];then
	
		echo $i;

	fi
	rm -rf /tmp/i.txt
done
