[toc]
## 11.18 Apache用户认证
### (1)编辑配置文件
vim /usr/local/apache2.4/conf/extra/httpd-vhosts.conf 
```
如下内容
<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/www.123.com"
    ServerName www.123.com
    <Directory /data/wwwroot/www.123.com> //指定认证的目录
        AllowOverride AuthConfig //这个相当于打开认证的开关
        AuthName "123.com user auth" //自定义认证的名字，作用不大
        AuthType Basic //认证的类型，一般为Basic，其他类型阿铭没用过
        AuthUserFile /data/.htpasswd  //指定密码文件所在位置
        require valid-user //指定需要认证的用户为全部可用用户
    </Directory>
</VirtualHost>
```
### (2)生成密码文件
```
 /usr/local/apache2.4/bin/htpasswd -cm /data/.htpasswd aming 
```
### (3)重新加载配置文件
``` 
 重新加载配置-t , graceful
 /usr/local/apache2.4/bin/apachectl -t
 /usr/local/apache2.4/bin/apachectl graceful
```
### (4)测试
```
 绑定hosts，浏览器测试
 curl -x127.0.0.1:80 www.123.com //状态码为401
 curl -x127.0.0.1:80 -uaming:passwd www.123.com //状态码为200
```

```
<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/fxq.com"
    ServerName fxq.com
    ServerAlias www.fxq.com www.fff.com
    
################################################    
    <Directory /data/wwwroot/fxq.com>
        AllowOverride Authconfig
        AuthName "fxq.com user auth"
        AuthType Basic
        AuthUserFile /data/.htpasswd
        require valid-user
    </Directory>
##################################################
    
    ErrorLog "logs/fxq.com-error_log"
    CustomLog "logs/fxq.com-access_log" common
</VirtualHost>

```
/usr/local/apache2.4/bin/htpasswd -cm /data/.htpasswd fxq

建立第二个用户时不输入-c

```
[root@fxq-1 ~]# curl -x127.0.0.1:80   www.fxq.com -I
HTTP/1.1 401 Unauthorized
Date: Mon, 09 Oct 2017 13:14:33 GMT
Server: Apache/2.4.27 (Unix) PHP/7.1.6
WWW-Authenticate: Basic realm="fxq.com user auth"
Content-Type: text/html; charset=iso-8859-1

[root@fxq-1 ~]# curl -x127.0.0.1:80 -ufxq:qing123.  www.fxq.com -I
HTTP/1.1 200 OK
Date: Mon, 09 Oct 2017 13:15:10 GMT
Server: Apache/2.4.27 (Unix) PHP/7.1.6
X-Powered-By: PHP/7.1.6
Content-Type: text/html; charset=UTF-8

[root@fxq-1 ~]# 

```
**还可以针对单个文件进行认证**
```

<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/www.123.com"
    ServerName www.123.com
    <FilesMatch admin.php>
        AllowOverride AuthConfig
        AuthName "123.com user auth"
        AuthType Basic
        AuthUserFile /data/.htpasswd
        require valid-user
    </FilesMatch>
</VirtualHost>

```


## 11.19/11.20 域名跳转
### (1)需求
```
需求，把123.com域名跳转到www.123.com，

```
### (2)配置如下：
```

<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/www.123.com"
    ServerName www.123.com
    ServerAlias 123.com
    <IfModule mod_rewrite.c> //需要mod_rewrite模块支持
        RewriteEngine on  //打开rewrite功能
        RewriteCond %{HTTP_HOST} !^www.123.com$  //定义rewrite的条件，主机名（域名）不是www.123.com满足条件
        RewriteRule ^/(.*)$ http://www.123.com/$1 [R=301,L] //定义rewrite规则，当满足上面的条件时，这条规则才会执行
</IfModule>
</VirtualHost> 
 
```
### (3) 查看是否加载rewrite模块
```
 /usr/local/apache2/bin/apachectl -M|grep -i rewrite //若无该模块，需要编辑配置文件httpd.conf，删除rewrite_module (shared) 前面的#
  vim /usr/local/apache2.4/conf/httpd.conf
  再查看是否加载rewrite模块。
```
### (4)测试  
```
 curl -x127.0.0.1:80 -I 111.com //状态码为301

```
### (5)实例:访问fxq.com 跳转到111.com
```
<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/fxq.com"
    ServerName fxq.com
    ServerAlias www.fxq.com www.fff.com


########################################
    <IfModule mod_rewrite.c>
        RewriteEngine on
        RewriteCond %{HTTP_HOST} !^111.com$
        RewriteRule ^/(.*)$ http://www.111.com/$1 [R=301,L]
    </IfModule>
#######################################


   ErrorLog "logs/fxq.com-error_log"
    CustomLog "logs/fxq.com-access_log" combined
</VirtualHost>



[root@fxq-1 ~]# curl -x127.0.0.1:80 www.fxq.com -I
HTTP/1.1 301 Moved Permanently
Date: Mon, 09 Oct 2017 14:13:34 GMT
Server: Apache/2.4.27 (Unix) PHP/7.1.6
Location: http://www.111.com/
Content-Type: text/html; charset=iso-8859-1

[root@fxq-1 ~]#


```

## 11.21 Apache访问日志

**日志的定义格式在主配置文件中**

**有两种：combined  common**
```
 访问日志记录用户的每一个请求
 vim /usr/local/apache2.4/conf/httpd.conf //搜索LogFormat 
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %b" common 
```
### (1) 更改虚拟主机配置文件： 
``` 
 把虚拟主机配置文件改成如下： 
 <VirtualHost *:80>
    DocumentRoot "/data/wwwroot/www.fxq.com"
    ServerName www.fxq.com
    ServerAlias fxq.com
    CustomLog "logs/fxq.com-access_log" combined
</VirtualHost>
```
### (2)重新加载配置文件及测试
```
 /usr/local/apache2.4/bin/apachectl -t
 
 /usr/local/apache2.4/bin/apachectl graceful
 
 curl -x127.0.0.1:80 -I fxq.com 
 
 tail /usr/local/apache2.4/logs/fxq.com-access_log 
···






#######################################
   ErrorLog "logs/fxq.com-error_log"
   CustomLog "logs/fxq.com-access_log" combined

#######################################


<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/111.com"
    ServerName 111.com
    ServerAlias www.111.com
    ErrorLog "logs/111.com-error_log"
    CustomLog "logs/111.com-access_log" combined
</VirtualHost>






```
## 扩展 
apache虚拟主机开启php的短标签 http://ask.apelearn.com/question/5370

```
针对apache的虚拟主机开启php短标签

在对应的 虚拟主机 配置文件中加入


    ......
    ......
    ......
    php_admin_flag short_open_tag on
    
```