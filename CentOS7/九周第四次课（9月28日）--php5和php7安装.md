[toc]
## 11.10/11.11/11.12 安装PHP5
```
 PHP官网www.php.net
 当前主流版本为5.6/7.1
 cd /usr/local/src/ 
 wget http://cn2.php.net/distributions/php-5.6.30.tar.gz
 tar zxf php-5.6.30.tar.gz
 cd php-5.6.30
 yum install epel-release
 yum install -y libxml2-devel openssl-devel bzip2-devel libjpeg-devel  libpng-devel freetype-devel libmcrypt-devel
 
 无法安装epel：手动安装libmcrypt:
 wget https://nchc.dl.sourceforge.net/project/mcrypt/Libmcrypt/2.5.8/libmcrypt-2.5.8.tar.gz
tar -zxvf libmcrypt-2.5.8.tar.gz
cd libmcrypt-2.5.8
./configure
make && make install 
 
 ./configure --prefix=/usr/local/php --with-apxs2=/usr/local/apache2.4/bin/apxs --with-config-file-path=/usr/local/php/etc  --with-mysql=/usr/local/mysql --with-pdo-mysql=/usr/local/mysql --with-mysqli=/usr/local/mysql/bin/mysql_config --with-libxml-dir --with-gd --with-jpeg-dir --with-png-dir --with-freetype-dir --with-iconv-dir --with-zlib-dir --with-bz2 --with-openssl --with-mcrypt --enable-soap --enable-gd-native-ttf --enable-mbstring --enable-sockets --enable-exif 
 make && make install
 cp php.ini-production  /usr/local/php/etc/php.ini

/usr/local/php/bin/php -i 查看php编译参数等php信息
报错：
```
make时报错

/usr/local/src/php-5.6.30/ext/date/php_date.c:21:17: 致命错误：php.h：没有那个文件或目录  #include "php.h"         

解决办法:删除php目录重新./configure



## 11.13 安装PHP7

```
 cd /usr/local/src/ 
 wget http://cn2.php.net/distributions/php-7.1.6.tar.bz2
 tar zxf php-7.1.6.tar.bz2
 cd php-7.1.6
 ./configure --prefix=/usr/local/php7 --with-apxs2=/usr/local/apache2.4/bin/apxs --with-config-file-path=/usr/local/php7/etc  --with-pdo-mysql=/usr/local/mysql --with-mysqli=/usr/local/mysql/bin/mysql_config --with-libxml-dir --with-gd --with-jpeg-dir --with-png-dir --with-freetype-dir --with-iconv-dir --with-zlib-dir --with-bz2 --with-openssl --with-mcrypt --enable-soap --enable-gd-native-ttf --enable-mbstring --enable-sockets --enable-exif
 make && make install
 ls /usr/local/apache2.4/modules/libphp7.so
 cp php.ini-production  /usr/local/php7/etc/php.ini

```
## 扩展
php中mysql,mysqli,mysqlnd,pdo到底是什么 http://blog.csdn.net/u013785951/article/details/60876816
```
名词解释:

最开始的初学者，往往搞不清mysqli,mysqlnd,pdo到底是什么，下面先直接贴出最直观的名字吧。

MYSQL:This extension is deprecated as of PHP 5.5.0, and has been removed as of PHP 7.0.0. 
MYSQLI: MySQL Improved Extension 
MySQLND: MySQL Native Drive 
PDO:The PHP Data Objects。extension defines a lightweight, consistent interface for accessing databases in PHP。
以上摘自 PHP官方手册: http://php.net/manual/en/book.mysqli.php

用中文说: 
MYSQL 也叫 Original MySQL，PHP4版本的MYSQL扩展，从PHP5起已经被废弃，并别从PHP7开始已经被移除。

MYSQLI 叫做 “MySQL增强扩展”。

MYSQLND MYSQL NATIVE DIRVER 叫做MYSQL “官方驱动”或者更加直接点的叫做“原生驱动”

PDO PHP Data Objects PHP数据对象，是PHP应用中的一个数据库抽象层规范。

针对本篇文章

再补充几个名词解释：

1 什么是API？

一个应用程序接口（Application Programming Interface的缩写），定义了类，方法，函数，变量等等一切 你的应用程序中为了完成特定任务而需要调用的内容。在PHP应用程序需要和数据库进行交互的时候所需要的API 通常是通过PHP扩展暴露出来（给终端PHP程序员调用）。
上文所说的MYSQL 和MYSQLI扩展就提供了这样的API。

2什么是驱动？

驱动是一段设计用来于一种特定类型的数据库服务器进行交互的软件代码。驱动可能会调用一些库，比如MySQL客户端库或者MySQL Native驱动库。 这些库实现了用于和MySQL数据库服务器进行交互的底层协议。
在PHP拓展的角度上看，MYSQL和MYSQLi还是比较上层的拓展，依赖更底层的库去连接和访问数据库。 
上文所说的MYSQLND 就是所说的底层的数据库驱动。当然，还有一个驱动叫做libmysqlclient。至于如何选择使用这两种驱动的哪一种，请看这里选择哪一种底层数据库驱动。

总的来说:

从应用的层面上看，我们通过PHP 的MYSQL或者MYSQLi扩展提供的API去操作数据库。

从底层来看，MYSQLND提供了底层和数据库交互的支持(可以简单理解为和MySQL server进行网络协议交互)。

而PDO，则提供了一个统一的API接口，使得你的PHP应用不去关心具体要连接的数据库服务器系统类型。也就是说，如果你使用PDO的API，可以在任何需要的时候无缝切换数据库服务器。比如MYSQL,SQLITE任何数据库都行。

即从大部分功能上看，PDO提供的API接口和MYSQLI提供的接口对于普通的增删改查效果是一致的。

最后贴下代码：

MYSQL连接：

<?php

$conn = @ mysql_connect("localhost", "root", "") or die("数据库连接错误");

mysql_select_db("bbs", $conn);

mysql_query("set names 'utf8'");

echo "数据库连接成功";

?>

MYSQLI连接：

<?php

$conn = mysqli_connect('localhost', 'root', '', 'bbs');

if(!$conn){

die("数据库连接错误" . mysqli_connect_error());

}else{

echo"数据库连接成功";

}

?>

PDO连接：

<?php

try{

$pdo=new pdo("mysql:host=localhost;dbname=bbs","root","");

}catch(PDDException $e){

echo"数据库连接错误";

}

echo"数据库连接成功";

?>

想更多去了解他们的区别和联系，可以手动去编译一下PHP的源代码。注意参数

--enable-pdo 
--with-pdo-mysql
--enable-mysqlnd 
--with-mysqli
--with-mysql//php7的已经不再支持，此参数configure 的时候会报ERROR
```


查看编译参数 http://ask.apelearn.com/question/1295
```
转自 http://www.jzyuan.cn/?p=220
Linux下查看Nginx、Napache、MySQL、PHP的编译参数的命令如下：

1、nginx编译参数：
#/usr/local/nginx/sbin/nginx -V
2、apache编译参数：
# cat /usr/local/apache/build/config.nice
3、php编译参数：
# /usr/local/php/bin/php -i |grep configure
4、mysql编译参数：
# cat /usr/local/mysql/bin/mysqlbug|grep configure
```