[toc]
## 4.10/4.11/4.12 lvm讲解
LVM是 Logical Volume Manager(逻辑卷管理)的简写，它由Heinz Mauelshagen在Linux 2.4内核上实现。LVM将一个或多个硬盘的分区在逻辑上集合，相当于一个大硬盘来使用，当硬盘的空间不够使用的时候，可以继续将其它的硬盘的分区加入其中，这样可以实现磁盘空间的动态管理，相对于普通的磁盘分区有很大的灵活性。

与传统的磁盘与分区相比，LVM为计算机提供了更高层次的磁盘存储。它使系统管理员可以更方便的为应用与用户分配存储空间。在LVM管理下的存储卷可以按需要随时改变大小与移除(可能需对文件系统工具进行升级)。LVM也允许按用户组对存储卷进行管理，允许管理员用更直观的名称(如"sales'、 'development')代替物理磁盘名(如'sda'、'sdb')来标识存储卷。
fdisk /dev/sdb

![image](http://note.youdao.com/yws/api/personal/file/16A9B3C26D924E6A9C62FFE43D8174CC?method=download&shareKey=f1229cabe76e0478e7436880f45efd39)


==**以下实例：**==

==**将/dev/sdb划分三个物理分区sdb1、sdb2、sdb3，创建物理卷,创建逻辑卷,并对逻辑卷及卷组进行扩容，缩容。**==

### 1. fdisk /dev/sdb  n 建立三个分区，分别为1G,t 改变分区类型为8e


```
[root@fxq-1 ~]# fdisk /dev/sdb
欢迎使用 fdisk (util-linux 2.23.2)。

更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。


命令(输入 m 获取帮助)：m
命令操作
   a   toggle a bootable flag
   b   edit bsd disklabel
   c   toggle the dos compatibility flag
   d   delete a partition
   g   create a new empty GPT partition table
   G   create an IRIX (SGI) partition table
   l   list known partition types
   m   print this menu
   n   add a new partition
   o   create a new empty DOS partition table
   p   print the partition table
   q   quit without saving changes
   s   create a new empty Sun disklabel
   t   change a partition's system id
   u   change display/entry units
   v   verify the partition table
   w   write table to disk and exit
   x   extra functionality (experts only)

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): p
分区号 (1-4，默认 1)：1
起始 扇区 (2048-41943039，默认为 2048)：
将使用默认值 2048
Last 扇区, +扇区 or +size{K,M,G} (2048-41943039，默认为 41943039)：+1G
分区 1 已设置为 Linux 类型，大小设为 1 GiB

命令(输入 m 获取帮助)：t
已选择分区 1
Hex 代码(输入 L 列出所有代码)：8e      
已将分区“Linux”的类型更改为“Linux LVM”

```


```
[root@fxq-1 ~]# fdisk -l

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x000a46f8

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     2099199     1048576   8e  Linux LVM
/dev/sdb2         2099200     4196351     1048576   8e  Linux LVM
/dev/sdb3         4196352     6293503     1048576   8e  Linux LVM
[root@fxq-1 ~]# 

```
### 2. 创建物理卷

yum install -y lvm2 默认最小化安装没有安装lvm包

pvcreate /dev/sdb1

pvcreate /dev/sdb2

pvcreate /dev/sdb3

pvdisplay 列出当前的物理卷

pvremove /dev/sdb3 删除物理卷


查看yum 源里pvcreate 命令的安装包名字是什么

```
[root@fxq-1 ~]# yum provides "/*/pvcreate"
已加载插件：fastestmirror
Loading mirror speeds from cached hostfile
base/7/i386/filelists_db                                                          | 5.6 MB  00:00:40     
extras/7/i386/filelists_db                                                        |  586 B  00:00:00     
updates/7/i386/filelists_db                                                       | 3.2 MB  00:00:22     
1:bash-completion-2.1-6.el7.noarch : Programmable completion for Bash
源    ：base
匹配来源：
文件名    ：/usr/share/bash-completion/completions/pvcreate

7:lvm2-2.02.166-1.el7.i686 : Userland logical volume management tools
源    ：base
匹配来源：
文件名    ：/usr/sbin/pvcreate

7:lvm2-2.02.166-1.el7_3.1.i686 : Userland logical volume management tools
源    ：updates
匹配来源：
文件名    ：/usr/sbin/pvcreate

7:lvm2-2.02.166-1.el7_3.2.i686 : Userland logical volume management tools
源    ：updates
匹配来源：
文件名    ：/usr/sbin/pvcreate

7:lvm2-2.02.166-1.el7_3.3.i686 : Userland logical volume management tools
源    ：updates
匹配来源：
文件名    ：/usr/sbin/pvcreate

7:lvm2-2.02.166-1.el7_3.4.i686 : Userland logical volume management tools
源    ：updates
匹配来源：
文件名    ：/usr/sbin/pvcreate

7:lvm2-2.02.166-1.el7_3.5.i686 : Userland logical volume management tools
源    ：updates
匹配来源：
文件名    ：/usr/sbin/pvcreate

[root@fxq-1 ~]# 

```

**创建物理卷时发现分区文件不存在，执行命令：partprobe，自动生成分区文件.
然后pvcreate 创建后三个物理卷后，pvdisplay进行查看.**
```
[root@fxq-1 ~]# pvcreate /dev/sdb1
  Device /dev/sdb1 not found (or ignored by filtering).
[root@fxq-1 ~]# partprobe 
Error: Partition(s) 4 on /dev/sdb have been written, but we have been unable to inform the kernel of the 
change, probably because it/they are in use.  As a result, the old partition(s) will remain in use.  You should reboot now before making further changes.Warning: 无法以读写方式打开 /dev/sr0 (只读文件系统)。/dev/sr0 已按照只读方式打开。
[root@fxq-1 ~]# ls /dev/sdb1
/dev/sdb1
[root@fxq-1 ~]# ls /dev/sdb2
/dev/sdb2
[root@fxq-1 ~]# ls /dev/sdb3
/dev/sdb3
[root@fxq-1 ~]# pvcreate /dev/sdb1
WARNING: xfs signature detected on /dev/sdb1 at offset 0. Wipe it? [y/n]: y
  Wiping xfs signature on /dev/sdb1.
  Physical volume "/dev/sdb1" successfully created.
[root@fxq-1 ~]# pvcreate /dev/sdb2
  Physical volume "/dev/sdb2" successfully created.
[root@fxq-1 ~]# pvcreate /dev/sdb3
  Physical volume "/dev/sdb3" successfully created.
[root@fxq-1 ~]# pvdisplay 
  "/dev/sdb2" is a new physical volume of "1.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb2
  VG Name               
  PV Size               1.00 GiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               GSII2d-0LCa-KDPB-7sN2-inU2-Va8f-TJUC0c
   
  "/dev/sdb3" is a new physical volume of "1.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb3
  VG Name               
  PV Size               1.00 GiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               A5OfWM-eInj-nvkb-i35Z-KiXn-XQO5-I7zi10
   
  "/dev/sdb1" is a new physical volume of "1.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb1
  VG Name               
  PV Size               1.00 GiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               NLu5gC-zHT5-egJ4-Nvi9-6dsY-cQET-ah7qnC
   
[root@fxq-1 ~]# 
```

### 3. 创建逻辑卷

pvs  查看物理卷.

vgcreate vg1 /dev/sdb1 /dev/sdb2 

vgdisplay或vgs查看逻辑卷

lvcreate -L 100M -n lv1 vg1


```
[root@fxq-1 ~]# vgcreate vg1 /dev/sdb1 /dev/sdb2
  Volume group "vg1" successfully created
[root@fxq-1 ~]# vgdisplay 
  --- Volume group ---
  VG Name               vg1
  System ID             
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               1.99 GiB
  PE Size               4.00 MiB
  Total PE              510
  Alloc PE / Size       0 / 0   
  Free  PE / Size       510 / 1.99 GiB
  VG UUID               t5YfMC-DZ5b-5aYp-EEUW-V2We-MVNR-7
AK9Tw   
[root@fxq-1 ~]# vgs
  VG  #PV #LV #SN Attr   VSize VFree
  vg1   2   0   0 wz--n- 1.99g 1.99g
[root@fxq-1 ~]# lvcreate -L 100M -n lv1 vg1
  Logical volume "lv1" created.
[root@fxq-1 ~]# 
```
### 4. 格式化后挂载：


```
[root@fxq-1 ~]# mkfs.ext4 /dev/vg1/lv1
mke2fs 1.42.9 (28-Dec-2013)
文件系统标签=
OS type: Linux
块大小=1024 (log=0)
分块大小=1024 (log=0)
Stride=0 blocks, Stripe width=0 blocks
25688 inodes, 102400 blocks
5120 blocks (5.00%) reserved for the super user
第一个数据块=1
Maximum filesystem blocks=33685504
13 block groups
8192 blocks per group, 8192 fragments per group
1976 inodes per group
Superblock backups stored on blocks: 
	8193, 24577, 40961, 57345, 73729

Allocating group tables: 完成                            
正在写入inode表: 完成                            
Creating journal (4096 blocks): 完成
Writing superblocks and filesystem accounting information
: 完成 
[root@fxq-1 ~]# mount /dev/vg1/lv1 /mnt
[root@fxq-1 ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
/dev/sda3             28G  1.1G   27G    4% /
devtmpfs             240M     0  240M    0% /dev
tmpfs                248M     0  248M    0% /dev/shm
tmpfs                248M  4.7M  244M    2% /run
tmpfs                248M     0  248M    0% /sys/fs/cgrou
p/dev/sda1            197M  105M   93M   54% /boot
/dev/mapper/vg1-lv1   93M  1.6M   85M    2% /mnt
tmpfs                 50M     0   50M    0% /run/user/0
[root@fxq-1 ~]# ls /dev/mapper/vg1-lv1
/dev/mapper/vg1-lv1
[root@fxq-1 ~]# ls -l /dev/mapper/vg1-lv1 
lrwxrwxrwx 1 root root 7 8月  17 02:41 /dev/mapper/vg1-lv
1 -> ../dm-0[root@fxq-1 ~]# ls -l /dev/mapper/vg
ls: 无法访问/dev/mapper/vg: 没有那个文件或目录
[root@fxq-1 ~]# ls -l /dev/vg1/lv1
lrwxrwxrwx 1 root root 7 8月  17 02:41 /dev/vg1/lv1 -> ..
/dm-0[root@fxq-1 ~]#
```

### 5. 扩容逻辑卷:
**扩容：**

umount /mnt 

lvresize -L 300M /dev/vg1/lv1

resize2fs /dev/vg1/lv1

**减容：**

 e2fsck -f /dev/vg1/lv1
 lvresize -L 100M /dev/vg1/lv1
 vgs
 mount /dev/vg1/lv1/ /mnt
 df -h
 

```
[root@fxq-1 ~]# lvresize -L 300M /dev/vg1/lv1
  Size of logical volume vg1/lv1 changed from 100.00 MiB 
(25 extents) to 300.00 MiB (75 extents).  Logical volume vg1/lv1 successfully resized.
[root@fxq-1 ~]# e2fsck -f /dev/vg1/lv1
e2fsck 1.42.9 (28-Dec-2013)
/dev/vg1/lv1 is mounted.
e2fsck: 无法继续, 中止.

[root@fxq-1 ~]# resize2fs /dev/vg1/lv1
resize2fs 1.42.9 (28-Dec-2013)
Filesystem at /dev/vg1/lv1 is mounted on /mnt; on-line re
sizing requiredold_desc_blocks = 1, new_desc_blocks = 3
The filesystem on /dev/vg1/lv1 is now 307200 blocks long.

[root@fxq-1 ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
/dev/sda3             28G  1.1G   27G    4% /
devtmpfs             240M     0  240M    0% /dev
tmpfs                248M     0  248M    0% /dev/shm
tmpfs                248M  4.7M  244M    2% /run
tmpfs                248M     0  248M    0% /sys/fs/cgrou
p/dev/sda1            197M  105M   93M   54% /boot
tmpfs                 50M     0   50M    0% /run/user/0
/dev/mapper/vg1-lv1  287M  2.0M  270M    1% /mnt
[root@fxq-1 ~]# 

```


```
[root@fxq-1 ~]# umount /mnt
[root@fxq-1 ~]# e2fsck -f /dev/vg1/lv1
e2fsck 1.42.9 (28-Dec-2013)
第一步: 检查inode,块,和大小
第二步: 检查目录结构
第3步: 检查目录连接性
Pass 4: Checking reference counts
第5步: 检查簇概要信息
/dev/vg1/lv1: 13/75088 files (7.7% non-contiguous), 15639
/307200 blocks[root@fxq-1 ~]# resize2fs /dev/vg1/lv1 100M
resize2fs 1.42.9 (28-Dec-2013)
Resizing the filesystem on /dev/vg1/lv1 to 102400 (1k) bl
ocks.The filesystem on /dev/vg1/lv1 is now 102400 blocks long.

[root@fxq-1 ~]# lvresize -L 100M /dev/vg1/lv1
  WARNING: Reducing active logical volume to 100.00 MiB.
  THIS MAY DESTROY YOUR DATA (filesystem etc.)
Do you really want to reduce vg1/lv1? [y/n]: y
  Size of logical volume vg1/lv1 changed from 300.00 MiB 
(75 extents) to 100.00 MiB (25 extents).  Logical volume vg1/lv1 successfully resized.
[root@fxq-1 ~]# vgs
  VG  #PV #LV #SN Attr   VSize VFree
  vg1   2   1   0 wz--n- 1.99g 1.89g
[root@fxq-1 ~]# lvs
  LV   VG  Attr       LSize   Pool Origin Data%  Meta%  M
ove Log Cpy%Sync Convert  lv1  vg1 -wi-a----- 100.00m                            
[root@fxq-1 ~]# mount /dev/vg1/lv1/ /mnt
[root@fxq-1 ~]# df -h
文件系统             容量  已用  可用 已用% 挂载点
/dev/sda3             28G  1.1G   27G    4% /
devtmpfs             240M     0  240M    0% /dev
tmpfs                248M     0  248M    0% /dev/shm
tmpfs                248M  4.7M  244M    2% /run
tmpfs                248M     0  248M    0% /sys/fs/cgrou
p/dev/sda1            197M  105M   93M   54% /boot
tmpfs                 50M     0   50M    0% /run/user/0
/dev/mapper/vg1-lv1   93M  1.6M   86M    2% /mnt
[root@fxq-1 ~]# ls /mnt/
fxq  lost+found

```

### 6. xfs 扩容：

不用卸载


lvresize -L 300M /dev/vg1/lv1

xfs_growfs /dev/vg1/lv1


### 7. 扩展卷组：


fdisk /dev/sdb 新增/dev/sdb5（逻辑分区8e）2G
 
pvcreate /dev/sdb5

vgextend vg1 /dev/sdb5

lvresize -L 100M /dev/vg1/lv1 重新设置卷大小



```
[root@fxq-1 ~]# vgextend vg1 /dev/sdb3
  Volume group "vg1" successfully extended
[root@fxq-1 ~]# vgs
  VG  #PV #LV #SN Attr   VSize VFree
  vg1   3   1   0 wz--n- 2.99g 2.70g
[root@fxq-1 ~]# lvresize -L 3000M /dev/vg1/lv1
  Size of logical volume vg1/lv1 changed from 300.00 MiB 
(75 extents) to 2.93 GiB (750 extents).  Logical volume vg1/lv1 successfully resized.
[root@fxq-1 ~]# lvs
  LV   VG  Attr       LSize Pool Origin Data%  Meta%  Mov
e Log Cpy%Sync Convert  lv1  vg1 -wi-ao---- 2.93g                       
[root@fxq-1 ~]# umount /mnt
[root@fxq-1 ~]# e2fsck -f /dev/vg1/lv1
[root@fxq-1 ~]# resize2fs /dev/vg1/lv1
[root@fxq-1 ~]# mount /dev/vg1/lv1 /mnt

                      
```



## 4.13 磁盘故障小案例

编辑/etc/fstab 内容，出错后，开机有如下提示，输入root密码进入：

![image](http://note.youdao.com/yws/api/personal/file/85355926D11C44B380A6741EFED74E67?method=download&shareKey=a2b99d79d0d7c19816e95fb007c187ba)

