#!/bin/bash
#date=2017-06-25
#Auther is fengxiaoqing
#
# Shell script scripts to moniter lnmp 502 error. if 502 is comming,restart php-fpm.
# -------------------------------------------------------------------------
# Copyright (c) 2017 fxq project <http://www.fy88fy.cc/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of fxq shell script collection (NSSC)
# Visit http://www.fy88fy.cc/ for more information.
# -------------------------------------------------------------------------

while [[ true ]]; 
do
	log_file=access.log
	log_dir=/var/log/nginx/

	count_502=`cat $log_dir/$log_file| grep 502 |wc -l`

	if [[ $count_502 -ne 0 ]]; then
		/etc/init.d/php-fpm restart
		echo "restart php-fpm is successful! " >> /tmp/log/php-fpm_restart.log 
		echo "Restart time is `date`" >> /tmp/log/php-fpm_restart.log
	fi
	sleep 10
done
