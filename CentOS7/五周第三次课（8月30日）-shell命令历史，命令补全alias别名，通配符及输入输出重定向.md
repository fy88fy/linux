[toc]
## 8.1 shell介绍
-    shell是一个命令解释器，提供用户和机器的交互。
-    支持特定语法，比如逻辑判断，循环。
-    每个用户都有特定的shell
-    centos7默认shell 为bash(Bourne Agin Shell)
-    还用zsh,csh,ksh等
## 8.2 命令历史
- history 命令历史
- history -c 清空内存缓存命令。
- ~/.bash_history 命令历史的存储文件
- HISTSIZE=1000 默认命令历史记录1000条
- /etc/profile-----HISTORY=10000 
    
        source /etc/profile

- HISTTIMEFORMAT="%Y%m%d %H:%M:%S " 显示命令历史的执行时间
- chattr +a ~/.bash_history 永久保存命令历史，
- !! 上一条命令
- !1320 序号为1320条命令历史
- !mkdir 最近的以mkdir开关的命令
 

## 8.3 命令补全和别名
- tab键 敲一下，敲两下
- 参数补全，安装bash-completion
- alias 命令别名
    
        alias restartnet="systemctl restart network.service"

- unalias restartnet
- 用户自己的别名配置文件~/.bashrc
- ls /etc/profild.d/*.sh 也定义了一部分别名

## 8.4 通配符
    ls *.txt 匹配多个字符
    ls ?.txt 匹配单个字符
    ls [1-3].txt 匹配范围
    ls [0-9a-zA-Z].txt 匹配非特殊字符
    ls {1,3,4}.txt 匹配部分

```
    [root@0 ~]# ls *.txt
#fxq.txt  1.txt  111.txt  22.txt  3.txt  4.txt  \fxq.txt  ip.txt  sed.txt
[root@0 ~]# ls ?.txt
1.txt  3.txt  4.txt
[root@0 ~]# 

```


## 8.5 输入输出重定向
- cat 1.txt > 2.txt 重定向
- cat 1.txt >> 2.txt 追加重定向
- ls aaa.txt 2>error.txt错误重定向
- ls aaa.txt 2>>error.txt 错误追加重定向
- wc -l < 1.txt 输入重定向
- &> 结合了正确和错误
- ls 1.txt aaa.txt 1> 1.txt 2>error.txt
- ls 1.txt aaa.txt >1.txt 2>&1
