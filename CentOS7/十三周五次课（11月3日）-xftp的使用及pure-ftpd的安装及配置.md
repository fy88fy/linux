[toc]
## 15.4 xshell使用xftp传输文件
xftp下载地址：链接：http://pan.baidu.com/s/1eSMSLN0 密码：8os4

![image](http://note.youdao.com/yws/api/personal/file/B92476298DB14E85B1F981EB0F4A970D?method=download&shareKey=aa76061c827ea35e9d208e0d27c31baf)


快捷键：ctrl + alt + F：

## 15.5 使用pure-ftpd搭建ftp服务

### (1)pure-ftpd安装

```
 yum install -y epel-release
 yum install -y pure-ftpd
 
 ```
### (2)修改配置文件并启动服务
 ```
 vim /etc/pure-ftpd/pure-ftpd.conf//找到pureftpd.pdb这行，把行首的#删除
 systemctl stop vsftpd
 systemctl start pure-ftpd
 
 ```
### (3)建立ftp用户存放目录及虚拟用户
 ```
 mkdir /data/ftp
 useradd -u 1010 pure-ftp
 chown -R pure-ftp:pure-ftp /data/ftp
 
 ```
### (4)创建用户和密码
 ```
 pure-pw useradd testuser1 -u pure-ftp  -d /data/ftp
 pure-pw mkdb
 
其他：
 pure-pw list/userdel/usermod/passwd
```
### (5)测试

```
[root@fxq-1 ~]# lftp testuser1@192.168.103.132
口令: 
lftp testuser1@192.168.103.132:~> ls      
drwxr-xr-x    4 1010       pure-ftp           59 Nov  3 21:22 .
drwxr-xr-x    4 1010       pure-ftp           59 Nov  3 21:22 ..
drwxr-xr-x    2 1010       pure-ftp            6 Nov  3 21:22 123.tar.ga
-rw-r--r--    1 0          0                   0 Nov  3 21:17 123.txt
drwxr-xr-x    2 1010       pure-ftp            6 Nov  3 21:21 新文件夹
lftp testuser1@192.168.103.132:/> 

```

## 扩展
vsftp使用mysql存放虚拟用户并验证 http://www.aminglinux.com/bbs/thread-342-1-1.html

```
1  安装vsftpd

（1） yum install -y  vsftpd

（2）编辑vsftpd.conf

内容如下

listen=YES

connect_from_port_20=YES

pasv_enable=YES

tcp_wrappers=YES

local_enable=YES

chroot_local_user=yes

anonymous_enable=NO

guest_enable=YES

guest_username=vsftpdguest

user_config_dir=/etc/vsftpd/vsftpd_user_conf

pam_service_name=/etc/pam.d/vsftpd

dirmessage_enable=YES

idle_session_timeout=600

check_shell=NO

（3）创建一个虚拟用户映射系统用户    

useradd –s /sbin/nologin vsftpdguest

2 安装 mysql

具体步骤参考 http://www.lishiming.net/thread-7-1-2.html

3 安装 pam-mysql

wget  https://nchc.dl.sourceforge.net/project/pam-mysql/pam-mysql/0.7RC1/pam_mysql-0.7RC1.tar.gz

tar zxvf  pam_mysql-0.7RC1.tar.gz

cd pam_mysql-0.7RC1

./configure --with-mysql=/usr/local/mysql --with-pam=/usr --with-pam-mods-dir=/usr/lib

make && make install

4 创建vsftp 库和相关的表并授权

>create database vsftp;

>use vsftp ;

>create table users ( name char(16) binary ,passwd char(125) binary ) ;

>insert into users (name,passwd) values ('test001',password('123456'));

>insert into users (name,passwd) values ('test002',password('234567'));

>grant select on vsftp.users to vsftpdguest@localhost identified by 'vsftpdguest';

5 创建虚拟账户的配置文件

mkdir /etc/vsftpd/vsftpd_user_conf 

cd  /etc/vsftpd/vsftpd_user_conf

vim test001

内容如下

local_root=/ftp/        

write_enable=YES

virtual_use_local_privs=YES

chmod_enable=YES

6  编辑验证文件

vim  /etc/pam.d/vsftpd

内容如下

#%PAM-1.0

auth required /usr/lib/pam_mysql.so user=vsftpdguest passwd=vsftpdguest host=localhost db=vsftp table=users usercolumn=name passwdcolumn=passwd crypt=2

account required /usr/lib/pam_mysql.so user=vsftpdguest passwd=vsftpdguest host=localhost db=vsftp table=users usercolumn=name passwdcolumn=passwd crypt=2

如果不想使用mysql也可以使用文件的形式来搞虚拟账号，请参考  Centos5.5 配置vsftpd 虚拟账号
```



ftp的主动和被动模式 http://www.aminglinux.com/bbs/thread-961-1-1.html


```
FTP协议有两种工作方式：PORT方式和PASV方式，中文意思为主动模式和被动模式
一、什么是主动FTP  

        主动模式的FTP工作原理：客户端从一个任意的非特权端口N连接到FTP服务器的命令端口，也就是21端口。然后客户端开始监听端口N+1，并发送FTP命令“port N+1”到FTP服务器。接着服务器会从它自己的数据端口（20）连接到客户端指定的数据端口（N+1）。

    针对FTP服务器前面的防火墙来说，必须允许以下通讯才能支持主动方式FTP：    

    1、 任何大于1024的端口到FTP服务器的21端口。（客户端初始化的连接）  

    2、 FTP服务器的21端口到大于1024的端口。 （服务器响应客户端的控制端口） 

    3、 FTP服务器的20端口到大于1024的端口。（服务器端初始化数据连接到客户端的数据端口）

    4、 大于1024端口到FTP服务器的20端口（客户端发送ACK响应到服务器的数据端口） 

二、什么是被动FTP  

    为了解决服务器发起到客户的连接的问题，人们开发了一种不同的FTP连接方式。这就是所谓的被动方式，或者叫做PASV，当客户端通知服务器它处于被动模式时才启用。

   在被动方式FTP中，命令连接和数据连接都由客户端发起，这样就可以解决从服务器到客户端的数据端口的入方向连接被防火墙过滤掉的问题。

   当开启一个 FTP连接时，客户端打开两个任意的非特权本地端口（N > 1024和N+1）。第一个端口连接服务器的21端口，但与主动方式的FTP不同，客户端不会提交PORT命令并允许服务器来回连它的数据端口，而是提交 PASV命令。这样做的结果是服务器会开启一个任意的非特权端口（P > 1024），并发送PORT P命令给客户端。然后客户端发起从本地端口N+1到服务器的端口P的连接用来传送数据。  

       对于服务器端的防火墙来说，必须允许下面的通讯才能支持被动方式的FTP:     

    1、 从任何大于1024的端口到服务器的21端口（客户端初始化的连接）  

    2、 服务器的21端口到任何大于1024的端口（服务器响应到客户端的控制端口的连接） 

    3、 从任何大于1024端口到服务器的大于1024端口（客户端初始化数据连接到服务器指定的任意端口）

    4、 服务器的大于1024端口到远程的大于1024的端口（服务器发送ACK响应和数据到客户端的数据端口）

从上面可以看出，两种方式的命令链路连接方法是一样的，而数据链路的建立方法就完全不同，如下图：
FTP服务器的主动工作模式
```
![image](http://note.youdao.com/yws/api/personal/file/547664408B3E465FBB663014D3C04F09?method=download&shareKey=b94b3c6051f603c386506884b3984fd0)
```
FTP服务器的被动工作模式
```
![image](http://note.youdao.com/yws/api/personal/file/6C8195349A7A490789C99F136E1E8DEE?method=download&shareKey=532f80657b90382c97b01d94ff0533b8)
```

以上关于主动和被动FTP的解释，可以简单概括为以下两点：

    1、主动FTP：  

            命令连接：客户端 >1024端口 -> 服务器 21端口  

            数据连接：客户端 >1024端口 <- 服务器 20端口 

    2、被动FTP： 

            命令连接：客户端 >1024端口 -> 服务器 21端口 

            数据连接：客户端 >1024端口 -> 服务器 >1024端口

三、主动模式ftp与被动模式FTP优点和缺点：       

   主动FTP对FTP服务器的管理和安全很有利，但对客户端的管理不利。因为FTP服务器企图与客户端的高位随机端口建立连接，而这个端口很有可能被客户端的防火墙阻塞掉。被动FTP对FTP客户端的管理有利，但对服务器端的管理不利。因为客户端要与服务器端建立两个连接，其中一个连到一个高位随机端口，而这个端口很有可能被服务器端的防火墙阻塞掉。
```