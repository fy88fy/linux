[toc]
## 14.4 exportfs命令
### (1)常用选项
```
 -a 全部挂载或者全部卸载
 -r 重新挂载
 -u 卸载某一个目录
 -v 显示共享目录
``` 
### (2)以下操作在服务端上
```
vim /etc/exports //增加
/tmp/ 192.168.103.0/24(rw,sync,no_root_squash)
 exportfs -arv //不用重启nfs服务，配置文件就会生效
```


### (3)测试：



服务器端：

    vim /etc/exports 加入/tmp/ 192.168.103.0/24(rw,sync,no_root_squash)

```
[root@fxq-2 ~]# vim /etc/exports
[root@fxq-2 ~]# exportfs -arv
exporting 192.168.103.0/24:/tmp
exporting 192.168.103.0/24:/home/nfstestdir

```
**客户端挂载新建文件测试：**
```
[root@fxq-1 mnt]# showmount -e 192.168.103.132
Export list for 192.168.103.132:
/tmp             192.168.103.0/24
/home/nfstestdir 192.168.103.0/24
[root@fxq-1 mnt]# ls /tmp
easy_install-Zpiarp                                                       systemd-private-50416a0309c04609bb2a8a65ac5ec4e2-vmtoolsd.service-w5Ps3p
fxq.sock                                                                  systemd-private-7c4dfcbd242945bb9f45bf2d29875f8a-vmtoolsd.service-HQptxr
mysql.sock                                                                systemd-private-8409de14259f47d4ae0768c5279ae467-vmtoolsd.service-nwVjED
pear                                                                      systemd-private-8d83cc9575b24289985f5c186ebef16b-vmtoolsd.service-htGWgh
systemd-private-0b76ae2b7e5042a19cb38bee8ae0aaef-vmtoolsd.service-rjB47i  www.sock
systemd-private-28814affe88046fab641bee573ed5b3e-vmtoolsd.service-F9VK8X
[root@fxq-1 mnt]# mount -t nfs 192.168.103.132:/tmp/ /tmp/
[root@fxq-1 mnt]# df -h
文件系统                          容量  已用  可用 已用% 挂载点
/dev/mapper/cl-root                28G  5.3G   23G   19% /
devtmpfs                          237M     0  237M    0% /dev
tmpfs                             248M     0  248M    0% /dev/shm
tmpfs                             248M  4.7M  243M    2% /run
tmpfs                             248M     0  248M    0% /sys/fs/cgroup
/dev/sda1                        1014M  141M  874M   14% /boot
tmpfs                              50M     0   50M    0% /run/user/0
192.168.103.132:/home/nfstestdir   28G  3.9G   24G   14% /mnt
192.168.103.132:/tmp               28G  3.9G   24G   14% /tmp
[root@fxq-1 mnt]# ls /tmp
111.com.sock  pear                                                                      systemd-private-e2aa0c1565464e93a5bcd0cff4ffec5c-vmtoolsd.service-RgTyxT
db1.sql       systemd-private-6254010eaa994d1f99b3e3c138177d76-vmtoolsd.service-EKv0Po  test.com.sock
mysql.sock    systemd-private-bb35406284de4d429fa5cecf49ed7ee2-vmtoolsd.service-Hc6Eug

[root@fxq-1 mnt]# touch /tmp/aaaaaaaaa.txt
[root@fxq-1 mnt]# ls /tmp
111.com.sock   mysql.sock                                                                systemd-private-bb35406284de4d429fa5cecf49ed7ee2-vmtoolsd.service-Hc6Eug
aaaaaaaaa.txt  pear                                                                      systemd-private-e2aa0c1565464e93a5bcd0cff4ffec5c-vmtoolsd.service-RgTyxT
db1.sql        systemd-private-6254010eaa994d1f99b3e3c138177d76-vmtoolsd.service-EKv0Po  test.com.sock

[root@fxq-1 mnt]# umount /tmp
[root@fxq-1 mnt]# ls /tmp
easy_install-Zpiarp                                                       systemd-private-50416a0309c04609bb2a8a65ac5ec4e2-vmtoolsd.service-w5Ps3p
fxq.sock                                                                  systemd-private-7c4dfcbd242945bb9f45bf2d29875f8a-vmtoolsd.service-HQptxr
mysql.sock                                                                systemd-private-8409de14259f47d4ae0768c5279ae467-vmtoolsd.service-nwVjED
pear                                                                      systemd-private-8d83cc9575b24289985f5c186ebef16b-vmtoolsd.service-htGWgh
systemd-private-0b76ae2b7e5042a19cb38bee8ae0aaef-vmtoolsd.service-rjB47i  www.sock
systemd-private-28814affe88046fab641bee573ed5b3e-vmtoolsd.service-F9VK8X
[root@fxq-1 mnt]# 
```
**服务器端查看多了客户端创建的文件aaaaaaa.txt：**
```
[root@fxq-2 ~]# ls /tmp
111.com.sock   systemd-private-6254010eaa994d1f99b3e3c138177d76-vmtoolsd.service-EKv0Po
aaaaaaaaa.txt  systemd-private-bb35406284de4d429fa5cecf49ed7ee2-vmtoolsd.service-Hc6Eug
db1.sql        systemd-private-e2aa0c1565464e93a5bcd0cff4ffec5c-vmtoolsd.service-RgTyxT
mysql.sock     test.com.sock
pear
[root@fxq-2 ~]# 


```
## 14.5 NFS客户端问题
```
 NFS 4版本会有该问题
 客户端挂载共享目录后，不管是root用户还是普通用户，创建新文件时属主、属组为nobody
 客户端挂载时加上 -o nfsvers=3
 客户端和服务端都需要
 vim /etc/idmapd.conf //
 把“#Domain = local.domain.edu” 改为 “Domain = xxx.com” （这里的xxx.com,随意定义吧），然后再重启rpcidmapd服务


[root@fxq-1 /]# mount -t nfs -o nfsvers=3 192.168.103.132:/data /mnt
[root@fxq-1 /]# cd /mnt
[root@fxq-1 mnt]# mkdir 123.txt
[root@fxq-1 mnt]# ll
总用量 0
drwxr-xr-x 2 root    root      6 11月  2 22:00 123.txt
drwxr-xr-x 5 php-fpm php-fpm 159 11月  2 21:40 mysql
drwxr-xr-x 4 root    root     37 10月 28 12:16 wwwroot
drwxr-xr-x 2 root    root    187 10月 28 09:43 yum.bak
[root@fxq-1 mnt]# 


```



## 15.1 FTP介绍

 FTP是File Transfer Protocol（文件传输协议，简称文传协议）的英文简称，用于在Internet上控制文件的双向传输。
 FTP的主要作用就是让用户连接一个远程计算机（这些计算机上运行着FTP服务器程序），并查看远程计算机中的文件，然后把文件从远程计算机复制到本地计算机，或把本地计算机的文件传送到远程计算机。
 小公司用的多，大企业不用FTP，因为不安全

Git

## 15.2/15.3 使用vsftpd搭建ftp
### (1)安装vsftpd

```
 centos上自带vsftpd
 yum install -y vsftpd
 
 useradd -s /sbin/nologin virftp
 
 vim /etc/vsftpd/vsftpd_login //内容如下,奇数行为用户名，偶数行为密码，多个用户就写多行
 
    testuser1
    123456
    testuser2
    123456
    
 chmod 600 /etc/vsftpd/vsftpd_login
 
 生成机器可识别密码数据库
 db_load -T -t hash -f /etc/vsftpd/vsftpd_login /etc/vsftpd/vsftpd_login.db
```
### (2)编辑虚拟用户配置文件： 
```
 mkdir /etc/vsftpd/vsftpd_user_conf 
 cd /etc/vsftpd/vsftpd_user_conf
 
vim testuser1 //加入如下内容
    local_root=/home/virftp/testuser1
    anonymous_enable=NO
    write_enable=YES
    local_umask=022
    anon_upload_enable=NO
    anon_mkdir_write_enable=NO
    idle_session_timeout=600
    data_connection_timeout=120
    max_clients=10
```
### (3)创建虚拟用户目录
```
 mkdir /home/virftp/testuser1
 touch /home/virftp/testuser1/aming.txt
 chown -R virftp:virftp /home/virftp
```
### (4)编辑密码认证配置文件：
```
 vim /etc/pam.d/vsftpd //在最前面加上
    auth sufficient /lib64/security/pam_userdb.so db=/etc/vsftpd/vsftpd_login
    account sufficient /lib64/security/pam_userdb.so db=/etc/vsftpd/vsftpd_login
```
### (5)编辑vsftp主配置文件：
```
 vim /etc/vsftpd/vsftpd.conf
     将anonymous_enable=YES 改为 anonymous_enable=NO
     将#anon_upload_enable=YES 改为 anon_upload_enable=NO 
     将#anon_mkdir_write_enable=YES 改为 anon_mkdir_write_enable=NO
      再增加如下内容
    chroot_local_user=YES
    guest_enable=YES
    guest_username=virftp
    virtual_use_local_privs=YES
    user_config_dir=/etc/vsftpd/vsftpd_user_conf
    allow_writeable_chroot=YES
```
### (6)启动vsftpd服务
```
 systemctl start vsftpd 
```
### (7)客户端测试
```
 yum install -y lftp
 lftp ftpuser1@127.0.0.1
 执行命令ls，看是否正常输出
 若不正常查看日志/var/log/messages和/var/log/secure
 windows下安装filezilla客户端软件，进行测试


[root@fxq-1 mnt]# lftp testuser1@192.168.103.132
口令: 
lftp testuser1@192.168.103.132:~> ls      
-rw-r--r--    1 1004     1004            0 Nov 02 14:26 fxq.txt
drwxr-xr-x    2 1004     1004            6 Nov 02 14:40 新建文件夹
drwxr-xr-x    2 1004     1004            6 Nov 02 14:48 新文件夹
lftp testuser1@192.168.103.132:/>

xshell xftp 也可图形化测试


```