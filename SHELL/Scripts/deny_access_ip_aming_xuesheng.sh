
#!/bin/bash

logfile=/var/log/httpd/access_log
badip=/tmp/badip.txt
ipt=/sbin/iptables
##for ((i=30;i>=1;i--));do
##d=`date -R -d "-$i min"|awk '{print $2"/"$3"/"$4":"$5}'|awk -F ':' '{print $1":"$2":"$3":"}'`
d=`date -d "-1 minute" +%d/%b/%Y:%H:%M:`
d2=`date +%M`
#> $badip
block(){
	count_list=`cat /var/log/httpd/access_log |awk '{print $1}' | uniq -c |sort -nr | awk '{print $1}'`


        for count in $count_list; do

                if (( $count >= 4 )); then

             for ip in `cat /var/log/httpd/access_log |awk '{print $1}' | uniq -c |sort -nr | awk '{print $1,$2}' |grep "^$count " | awk '{print $2}'|uniq`; do
                        echo "$ip       `date "+%F %T"`"  >> /tmp/badip.log
                        iptables -A INPUT -p tcp -s $ip --dport 80 -j DROP
                      
                        done

                 fi
        done

}

unblock(){
for num in `awk '{print $1}' $badip` ;do
n=`$ipt -nvL --line-numbers|grep '0.0.0.0/0'|awk '$9=="'"$num"'" {print $1}'`
$ipt -D INPUT $n
$ipt -Z
done
}

while :;do
if (($d2 == "00" ||  $d2 == "30"))  ; then

    block
else
    block
fi
done
