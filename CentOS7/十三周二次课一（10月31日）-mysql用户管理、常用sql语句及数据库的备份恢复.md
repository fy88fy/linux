[toc]
## 13.4 mysql用户管理

### (1) 建立用户user1:

    grant all on *.* to 'user1' identified by 'passwd';
    
### (2) 建立用户user2并授权只能在一台主机登录：

     grant SELECT,UPDATE,INSERT on db1.* to 'user2'@'192.168.103.132' identified by 'passwd';
     
### (3)授权所有主机用户user3在所有主机登录：
 
     grant all on db1.* to 'user3'@'%' identified by 'passwd';
 
### (4)查看授权：

    show grants;
    show grants for user2@192.168.133.1;


## 13.5 常用sql语句
### (1) 增

    insert into db1.t1 values (1, 'abc');

### (2) 删

    delete form t1 where id=2;

### (3) 改
    
    update db1.t1 set name='aaa' where id=1;
### (4) 查

     select count(*) from mysql.user;
     select * from mysql.db;
     select db from mysql.db;
     select db,user from mysql.db;
     select * from mysql.db where host like '192.168.%';

### (5) 清空表，删除表，删除数据库
    
     truncate table db1.t1;
     drop table db1.t1;
     drop database db1;


## 13.6 mysql数据库备份恢复

mysql -u root -p123456 mysql 直接进入mysql数据库

### (1)备份库  

     mysqldump -uroot -p123456 mysql > /tmp/mysql.sql
     
### (2)恢复库

    mysql -uroot -p123456 mysql < /tmp/mysql.sql
    
### (3)备份表 

    mysqldump -uroot -p123456 mysql user > /tmp/user.sql
    
### (4)恢复表

    mysql -uroot -p123456 mysql < /tmp/user.sql
    
### (5)备份所有库

    mysqldump -uroot -p123456 -A >/tmp/all.sql

### (6)只备份表结构 
    
    mysqldump -uroot -p123456 -d mysql > /tmp/mysql.sql
    


## 扩展
SQL语句教程 http://blog.51cto.com/zt/206 

什么是事务？事务的特性有哪些？ http://blog.csdn.net/yenange/article/details/7556094

    
概念

    事务(Transaction)是访问并可能更新数据库中各种数据项的一个程序执行单元(unit)。事务通常由高级数据库操纵语言或编程语言（如SQL，C++或Java）书写的用户程序的执行所引起，并用形如begin transaction和end transaction语句（或函数调用）来界定。事务由事务开始(begin transaction)和事务结束(end transaction)之间执行的全体操作组成。
    例如：在关系数据库中，一个事务可以是一条SQL语句，一组SQL语句或整个程序。
    
特性

    　　事务是恢复和并发控制的基本单位。
    　　事务应该具有4个属性：原子性、一致性、隔离性、持续性。这四个属性通常称为ACID特性。
    　　原子性（atomicity）。一个事务是一个不可分割的工作单位，事务中包括的操作要么都做，要么都不做。
    　　一致性（consistency）。事务必须是使数据库从一个一致性状态变到另一个一致性状态。一致性与原子性是密切相关的。
    　　隔离性（isolation）。一个事务的执行不能被其他事务干扰。即一个事务内部的操作及使用的数据对并发的其他事务是隔离的，并发执行的各个事务之间不能互相干扰。
    　　持久性（durability）。持续性也称永久性（permanence），指一个事务一旦提交，它对数据库中数据的改变就应该是永久性的。接下来的其他操作或故障不应该对其有任何影响。


根据binlog恢复指定时间段的数据 http://www.centoscn.com/mysql/2015/0204/4630.html

```
如果不小心对数据库进行误操作，而又没有及时备份怎么办？这恐怕是广大的coder经常遇到的一类问题。
 我今天就因为不小心删除了某个数据库，但最后的备份是1个礼拜前的，唯一能解决的办法就是通过mysqlbinlog来恢复了。解决方案如下：

如果MySQL服务器启用了二进制日志，你可以使用mysqlbinlog工具来恢复从指定的时间点开始(例如，从你最后一次备份)直到现在或另一个指定的时间点的数据。
 关于启用二进制日志的信息，参见5.11.3节，“二进制日志”。对于mysqlbinlog的详细信息，参见mysql手册8.6节，“mysqlbinlog：用于处理二进制日志文件的实用工具”。
 要想从二进制日志恢复数据，你需要知道当前二进制日志文件的路径和文件名。
 一般可以从配置文件(一般情况，Linux下为my.cnf ，windows系统下为my.ini，取决于你的系统)中找到路径。如果未包含在选项文件中，当服务器启动时，可以在命令行中以选项的形式给出。
 启用二进制日志的选项为–log-bin。
 要想确定当前的二进制日志文件的文件名，输入下面的MySQL语句：
 SHOW BINLOG EVENTS \G;
 或者还可以从命令行输入下面的内容：
 mysql –user=root -pmypasswd -e ‘SHOW BINLOG EVENTS \G’ 将密码mypasswd替换为你的MySQL服务器的root密码。

比如得到的日志文件名为：
 mysql-bin.000001 1. 指定恢复时间 对于MySQL5.1.54，可以在mysqlbinlog语句中通过–start-date和–stop-date选项指定DATETIME格式的起止时间。

举例说明，比如在今天下午14:02(今天是2012年3月15日)，不小心执行SQL语句删除了一个数据表，但发现没有最新的备份（当然，这只是开发环境，并不是正式的生产环境，正式环境还得定时做数据备份）。要想恢复表和数据，可以通过mysqlbinlog恢复指定时间的备份，输入：
 mysqlbinlog –stop-date=”2012-03-15 14:02:00″ /data1/log/mysql/mysql-bin.000001  | mysql -u root -pmypasswd
 该命令将恢复截止到在–stop-date选项中以DATETIME格式给出的日期和时间的所有数据。

如果你没有检测到输入的错误的SQL语句，可能你想要恢复后面发生的数据库活动。
 根据这些，你可以用起使日期和时间再次运行mysqlbinlog：
 mysqlbinlog –start-date=”2012-03-15 00:01:00″ /data1/log/mysql/mysql-bin.000001  | mysql -u root -pmypasswd

在该行中，从今天凌晨0:01登录的SQL语句将运行，组合执行前夜的转储文件和mysqlbinlog的两行可以将所有数据恢复到今天凌晨0:01前一秒钟。
 你应检查日志以确保时间确切。下一节介绍如何实现。

2. 指定时间段恢复 通过mysqlbinlog –start-date 和–stop-date恢复指定时间段的数据库活动记录，如下：
 mysqlbinlog –start-date=”2012-03-09 02:00:00″ –stop-date=”2012-03-15 14:00:00″ /data1/log/mysql/mysql-bin.000001 > /tmp/mysql_restore_030915.sql
 通过这种方式，就能获取最后一个备份的文件时间2012-03-09 02:00:00到今天删除数据库之前2012-03-15 14:02这段时间的数据库活动事务操作

```


mysql字符集调整 http://xjsunjie.blog.51cto.com/999372/1355013

```
字符集是一套符号和编码的规则，不论是在oracle数据库还是在mysql数据库，都存在字符集的选择问题。对于数据库来说，字符集又是比较重要的，因为数据库存储的数据大部分都是各种文字，字符集对于数据库的存储、处理性能以及数据迁移都有重要的影响。



   如果在数据库创建阶段没有正确选择字符集，那么可能在后期需要更换字符集，而字符集的更换是代价比较高的操作，也存在一定的风险，所以我们建议在应用开始阶段，就按照需求正确的选择合适的字符集，尽量避免后期不必要的调整。





   mysql编译安装时，指定字符集的方法：

./configure  --with-charset=utf8




mysql的字符集有4个级别的默认设置：服务器级、数据库级、表级和字段级。分别在不同的地方设置，作用也不相同。
1、服务器字符集设定，在mysql服务启动的时候确定。
可以在my.cnf中设置：

[mysql]

### 默认字符集为utf8

default-character-set=utf8

[mysqld]
### 默认字符集为utf8

default-character-set=utf8

### （设定连接mysql数据库时使用utf8编码，以让mysql数据库为utf8运行）

init_connect='SET NAMES utf8'


或者在启动选项中指定：
mysqld --default-character-set=utf8

如果没有特别的指定服务器字符集，默认使用latin1(ISO-8859-1的别名)作为服务器字符集。上面三种设置的方式都只指定了字符集，没有去做校对，我们可以用show variables like 'char%';命令查询当前服务器的字符
集和校对规则。


mysql>show variables like 'char%';



   +--------------------------+----------------------------+




　　| Variable_name | Value |




　　+--------------------------+----------------------------+




　　| character_set_client | utf8 |




　　| character_set_connection | utf8 |




　　| character_set_database | utf8 |




　　| character_set_filesystem | binary |




　　| character_set_results | utf8 |




　　| character_set_server | utf8 |




　　| character_set_system | utf8 |




　　| character_sets_dir | /usr/share/mysql/charsets/ |




　　+--------------------------+----------------------------+




注：如果增加default-character-set=utf8后，MYSQL启动报错。可以用character_set_server=utf8来取代default-character-set=utf8，就能正常启动了。这是因为MYSQL不同版本识别的问题。





2、数据库级

创建数据库时指定字符集

mysql>CREATE DATABASE my_db default charset utf8 COLLATE utf8_general_ci;

#注意后面这句话 "COLLATE utf8_general_ci",大致意思是在排序时根据utf8编码格式来排序

如果指定了数据库编码，那么在这个数据库下创建的所有数据表的默认字符集都会是utf8了




修改MYSQL数据库编码，如果是MYSQL数据库编码不正确，可以在MYSQL执行如下命令:

ALTER DATABASE my_db DEFAULT CHARACTER SET utf8;   

以上命令就是将MYSQL的my_db数据库的编码设为utf8







3、 表级

创建表时指定字符集

mysql>create table my_table (name varchar(20) not null default '')type=myisam default charset utf8;

#这句话就是创建一个表,指定默认字符集为utf8




修改MYSQL表的编码：

ALTER TABLE my_table DEFAULT CHARACTER SET utf8; 

以上命令就是将一个表my_table的编码改为utf8




4、 字段级

alter table test add column address varchar(110) after stu_id;

在stu_id后增加一个字段address




alter table test add id int unsigned not Null auto_increment primary key;




修改字段的编码：

ALTER TABLE `test` CHANGE `name` `name` VARCHAR( 45 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL 

以上命令就是将MYSQL数据库test表中name的字段编码改为utf8




在命令行下插入汉字时如下代码：

set names utf8;有时候这一句很关键！

insert into charset values('王达');







注意：alter修改的方法不能更新已有记录的字符集，只是对新创建的表和记录生效。对已有记录字符集的调整，需要先将数据导出，经过适当调整后重新导入才可以完全修改编码。




导出导入的字符调整方法:

导出表结构

mysqldump -uroot -pmysql --default-character-set=latin1 -d  my_db> createtab.sql

手工修改createtab.sql表结构定义中的字符集为新的字符集

1、导出所有记录


mysqldump -uroot -pmysql --quick --no-create-info --extended-insert --default-character-set=latin1 --host=localhost  my_db> data.sql

2、打开data.sql，将set names latin1修改成set names utf8




:%s/latin1/utf8/g

全文替换

3、使用新的字符集创建新的数据库

create database  mydata  default charset utf8;

4、创建表，执行createtab.sql

mysql -uroot -pmysql mydata<creattab.sql

5、导入数据

mysql -uroot -pmysql mydata<data.sql




注意一点就是目标字符集要大于等于源字符集，否则会丢失一部分不支持的汉字数据。




附：旧数据升级办法

以原来的字符集为latin1为例，升级成为utf8的字符集。原来的表: old_table (default charset=latin1)，新表：new_table(default charset=utf8)。

第一步：导出旧数据

mysqldump --default-character-set=latin1 -hlocalhost -uroot -B my_db --tables old_table > old.sql

第二步：转换编码

iconv -t utf8  -f latin1 -c old.sql > new.sql

在这里，假定原来的数据默认是latin1编码。

第三步：导入

修改old.sql，增加一条sql语句： "SET NAMES utf8;"，保存。

mysql -hlocalhost -uroot my_db < new.sql

大功告成！










Mysql collate规则：

*_bin: 表示的是binary case sensitive collation，也就是说是区分大小写的
 *_cs: case sensitive collation，区分大小写
 *_ci: case insensitive collation，不区分大小写

```

使用xtrabackup备份innodb引擎的数据库 innobackupex 备份 Xtrabackup 增量备份
[http://zhangguangzhi.top/2017/08/23/innobackex%E5%B7%A5%E5%85%B7%E5%A4%87%E4%BB%BDmysql%E6%95%B0%E6%8D%AE/#%E4%B8%89%E3%80%81%E5%BC%80%E5%A7%8B%E6%81%A2%E5%A4%8Dmysql](http://zhangguangzhi.top/2017/08/23/innobackex%E5%B7%A5%E5%85%B7%E5%A4%87%E4%BB%BDmysql%E6%95%B0%E6%8D%AE/#%E4%B8%89%E3%80%81%E5%BC%80%E5%A7%8B%E6%81%A2%E5%A4%8Dmysql)

