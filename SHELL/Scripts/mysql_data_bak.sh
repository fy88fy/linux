#!/bin/bash
#
#to backup mysql databases;

PATH=PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/texlive/2012/bin/i386-linux:/usr/local/mysql/bin:/usr/local/mysql/bin:/root/bin

day=`date +%F`
day7=`date -d "8 days ago" +%y%m%d`
day30=`date -d "32 days ago" +%y%m%d`

passwd="qing123."

local_bak_dir=/backup/mysql/

remote_bak_dir=10.186.46.188:/backup/remote_dir

echo "mysql backup begin at `date "+%F %T"`" >> /var/log/mysqlbak.log 
exec 1>>/var/log/mysqlbak.log 2>>/var/log/mysqlbak.log

mysqldump -uroot -p$passwd --default-character-set=gbk discuz >$local_bak_dir/$day.sql

exec 1>>/var/log/mysqlbak.log 2>>/var/log/mysqlbak.log
rm -rf $local_bak_dir/$day7.sql

exec 1>>/var/log/mysqlbak.log 2>>/var/log/mysqlbak.log

rsync -a $local_bak_dir/$day.sql $remote_bak_dir/$day.sql

exec 1>>/var/log/mysqlbak.log 2>>/var/log/mysqlbak.log

echo "mysql backup end at `date "+%F %T"`" 

