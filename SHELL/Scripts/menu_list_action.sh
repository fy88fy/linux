#!/bin/bash
#date=2017-06-25
#Auther is fengxiaoqing
#
# Shell script scripts to print a menu,you can select 1,2,3,4,to exec a action.
# -------------------------------------------------------------------------
# Copyright (c) 2017 fxq project <http://www.fy88fy.cc/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of fxq shell script collection (NSSC)
# Visit http://www.fy88fy.cc/ for more information.
# -------------------------------------------------------------------------
# 
PS3="Please input a number(1|2|3|4|5):"
select name in "date" "ls" "who" "pwd" "quit"

do
case $name in
         date)
                date;
                ;;
  
        ls)
                ls;
                ;;
  
        who)
                who;
                ;;
        pwd)
                pwd;
                ;;


        quit)
                exit 0
                ;;


        *)
                echo -e "\033[32m------------------------------\033[0m"
                echo "1 date;"
                echo "2 ls;"
                echo "3 who;"
                echo "4 pwd"
                echo "5 quit"
                echo -e "\033[32m------------------------------\033[0m"
                echo -e "\033[31mPlease imput 1 or 2 or 3 or 4.\033[0m";
                ;;
esac
done
