[toc]
## 8.6 管道符和作业控制

### (1)管道|
**cat 1.txt | grep "root"**

```
[root@0 ~]# cat /etc/passwd | grep root
root:x:0:0:root:/root:/bin/bash
operator:x:11:0:operator:/root:/sbin/nologin
[root@0 ~]#
```

**find ./ -type f |wc -l: 查找当前目录有多少个文件**


```
[root@0 ~]# find ./ -type f |wc -l
171
[root@0 ~]#
```
### (2)作业控制

**vim /etc/passwd 打开文件进行编辑**

**ctrl + Z 暂停程序**

```
[root@0 ~]# vim /etc/passwd

[1]+  Stopped                 vim /etc/passwd
[root@0 ~]# 
```

**fg 调回前台**

**bg 放到后台运行**

**jobs 查看作业**


```
[root@0 ~]# jobs
[1]-  Stopped                 vim /etc/passwd
[2]+  Stopped                 vi /etc/fstab
[root@0 ~]#
[root@0 ~]# jobs
[1]-  Stopped                 vim /etc/passwd
[2]+  Stopped                 vi /etc/fstab
[root@0 ~]# fg 1

```


```
[root@0 ~]# sleep 1000
^Z
[4]+  Stopped                 sleep 1000
[root@0 ~]# jobs
[1]   Stopped                 vim /etc/passwd
[2]   Stopped                 vi /etc/fstab
[3]-  Stopped                 vmstat 1
[4]+  Stopped                 sleep 1000
[root@0 ~]# jobs 2000
-bash: jobs: 2000: no such job
[root@0 ~]# sleep 2000
^Z
[5]+  Stopped                 sleep 2000
[root@0 ~]# jobs
[1]   Stopped                 vim /etc/passwd
[2]   Stopped                 vi /etc/fstab
[3]   Stopped                 vmstat 1
[4]-  Stopped                 sleep 1000
[5]+  Stopped                 sleep 2000
[root@0 ~]# bg 5
[5]+ sleep 2000 &
[root@0 ~]# jobs
[1]   Stopped                 vim /etc/passwd
[2]   Stopped                 vi /etc/fstab
[3]-  Stopped                 vmstat 1
[4]+  Stopped                 sleep 1000
[5]   Running                 sleep 2000 &
[root@0 ~]# fg 1
vim /etc/passwd

[1]+  Stopped                 vim /etc/passwd
[root@0 ~]# sleep 3000 &
[6] 3723
[root@0 ~]# jobs
[1]+  Stopped                 vim /etc/passwd
[2]   Stopped                 vi /etc/fstab
[3]   Stopped                 vmstat 1
[4]-  Stopped                 sleep 1000
[5]   Running                 sleep 2000 &
[6]   Running                 sleep 3000 &
[root@0 ~]# 
```



## 8.7/8.8 shell变量
### （1）变量:

env 查看系统变量


```
[root@0 ~]# env
XDG_SESSION_ID=62201
HOSTNAME=0
TERM=xterm
SHELL=/bin/bash
HISTSIZE=6000
SSH_CLIENT=27.189.212.188 8616 22
SSH_TTY=/dev/pts/0
QT_GRAPHICSSYSTEM_CHECKED=1
USER=root
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=01;36:*.au=01;36:*.flac=01;36:*.mid=01;36:*.midi=01;36:*.mka=01;36:*.mp3=01;36:*.mpc=01;36:*.ogg=01;36:*.ra=01;36:*.wav=01;36:*.axa=01;36:*.oga=01;36:*.spx=01;36:*.xspf=01;36:
MAIL=/var/spool/mail/root
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/usr/local/mysql/bin/:/usr/local/apache2/bin/:/usr/local/php/bin/:/root/bin
PWD=/root
LANG=C
QT_GRAPHICSSYSTEM=native
SHLVL=1
HOME=/root
LOGNAME=root
SSH_CONNECTION=27.189.212.188 8616 10.186.46.188 22
LESSOPEN=||/usr/bin/lesspipe.sh %s
PROMPT_COMMAND=history -a
XDG_RUNTIME_DIR=/run/user/0
HISTTIMEFORMAT=%F %T 
_=/usr/bin/env
[root@0 ~]#
```


set 还可以查看用户自定义变量

a=1 自定义变量

变量名规则：不能以数字开头

变量里有特殊符号的话，得用''引起来


```
[root@0 ~]# a=111
[root@0 ~]# echo $a
111
[root@0 ~]# a='a$bc'
[root@0 ~]# echo $a
a$bc
[root@0 ~]# a="a$bc"
[root@0 ~]# echo $a
a
[root@0 ~]# 
```
变量的累加：


```
[root@0 ~]# a=1
[root@0 ~]# b=2
[root@0 ~]# echo $a$b
12
[root@0 ~]# a='a$bc'
[root@0 ~]# echo $a$b
a$bc2
[root@0 ~]# c="a$bc"
[root@0 ~]# echo $c
a
[root@0 ~]# c=a"$b"c
[root@0 ~]# echo $c
a2c
[root@0 ~]# 
```

全局变量：export b=222

w 查看登录终端：

echo $SSH_TTY


```
[root@0 ~]# w
 22:05:44 up 36 days,  4:57,  2 users,  load average: 0.00, 0.01, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
root     pts/1    27.189.212.188   21:37   28:38   0.03s  0.03s -bash
root     pts/0    27.189.212.188   21:22    0.00s  0.15s  0.00s w
[root@0 ~]# echo $SSH_TTY
/dev/pts/0
[root@0 ~]# 


[root@0 ~]# w
 22:06:03 up 36 days,  4:58,  2 users,  load average: 0.00, 0.01, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
root     pts/1    27.189.212.188   21:37    3.00s  0.03s  0.00s w
root     pts/0    27.189.212.188   21:22   11.00s  0.15s  0.11s -bash
You have new mail in /var/spool/mail/root
[root@0 ~]# echo $SSH_TTY
/dev/pts/1
[root@0 ~]# 




```

export 全局向下子shell 中生效，bash


unset a  删除自定义变量.



## 8.9 环境变量配置文件
### (1)系统层次：
/etc/porfile 用户环境变量，交互，登录才执行

/etc/bashrc 用户不用登录，执行shell


### (2)用户层次：
~/.proflie

~/.bashrc

. ~/.bash_profile

. ~/.bash_logout

. ~/.bash_history


### (3)PS1、PS2

PS1=[\u@\h \W]\$

PS2---->


```
<root@0>#for i in `seq 1 10`
> do
> echo $i
> done;
1
2
3
4
5
6
7
8
9
10
<root@0>#

```



## (4)扩展
bashrc和bash_profile的区别 http://ask.apelearn.com/question/7719


```
bash_profile和bashrc区别


【.bash_profile 与 .bashrc 的区别】
.bash_profile is executed for login shells, while .bashrc is executed for interactive non-login shells.


【login shell 与 non-login shell 的区别】
1、当你直接在机器login界面登陆、使用ssh登陆或者su切换用户登陆时，.bash_profile 会被调用来初始化shell环境
Note：.bash_profile文件默认调用.bashrc文件
.bash_profile中有如下内容
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi 
2、当你不登陆系统而使用ssh直接在远端执行命令，.bashrc 会被调用
3、当你已经登陆系统后，每打开一个新的Terminal时，.bashrc 都会被再次调用。


测试准备工作
hclient2主机hadoop用户家目录下执行
[hadoop@hclient2 ~]$ echo "invoke hclient2:~/.bashrc">>.bashrc
[hadoop@hclient2 ~]$ echo "invoke hclient2:~/.bash_profile">>.bash_profile


Login Shell
1、窗口登陆
Red Hat Enterprise Linux Server release 6.3 (Santiago)
Kernel 2.6.32-279.el6.x86_64 on an x86_64


hclient2 login: hadoop
Password:
Last login: Mon Feb 25 23:03:45 on tty1
invoke hclient2:~/.bashrc
invoke hclient2:~/.bash_profile

[hadoop@hclient2 ~]$
2、SSH 登陆
[hadoop@hserver ~]$ ssh hclient2
Last login: Mon Feb 25 22:42:19 2013 from hserver
invoke hclient2:~/.bashrc
invoke hclient2:~/.bash_profile
[hadoop@hclient2 ~]$
3、su 登陆
[root@hclient2 ~]# su - hadoop
invoke hclient2:~/.bashrc
invoke hclient2:~/.bash_profile


Non-login Shell:
Note: ssh ...[user@] hostname [command]
If command is specified, it is executed on the remote host instead of a login shell.
[hadoop@hserver ~]$ ssh hclient2 hostname
invoke hclient2:~/.bashrc
hclient2




【故】若要配置环境变量之类，最保险是写在 .bashrc 文件中。因为不管是登陆还是不登陆，该文件总会被调用！
```
