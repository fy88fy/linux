[toc]
## 12.13 Nginx防盗链
### (1)修改虚拟主机配置文件：
vim /usr/local/nginx/conf/vhost/test.com.conf 
加入下面配置：
```
location ~* ^.+\.(gif|jpg|png|swf|flv|rar|zip|doc|pdf|gz|bz2|jpeg|bmp|xls)$
        {
         expires 7d;
        valid_referers none blocked server_names  *.test.com ;
        if ($invalid_referer) {
                return 403;
        }
                access_log off;
        }

```

### （2）重启服务
/usr/local/nginx/sbin/nginx -t
/usr/local/nginx/sbin/nginx -s reload
### (3) 测试防盗链
```
[root@fxq-1 ~]# curl -x127.0.0.1:80 -e "http://www.test.com/index.php" www.test.com/1.jpg -I
HTTP/1.1 200 OK
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 11:36:13 GMT
Content-Type: image/jpeg
Content-Length: 0
Last-Modified: Mon, 23 Oct 2017 11:34:54 GMT
Connection: keep-alive
ETag: "59edd3de-0"
Expires: Mon, 30 Oct 2017 11:36:13 GMT
Cache-Control: max-age=604800
Accept-Ranges: bytes

[root@fxq-1 ~]# curl -x127.0.0.1:80 -e "http://www.baidu.com/index.php" www.test.com/1.jpg -I
HTTP/1.1 403 Forbidden
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 11:36:23 GMT
Content-Type: text/html
Content-Length: 169
Connection: keep-alive

[root@fxq-1 ~]#
```


## 12.14 Nginx访问控制

### 一、匹配目录

#### (1)修改虚拟主机配置文件：

vim /usr/local/nginx/conf/vhost/test.com.conf 

加入下面配置：

需求：访问/admin/目录的请求，只允许某几个IP访问，配置如下：

从上到下优先级逐级递减
```

location /admin/
{
    allow 192.168.103.1;
    allow 127.0.0.1;
    deny all;
}
```

 mkdir /data/wwwroot/test.com/admin/
 
 echo “test,test”>/data/wwwroot/test.com/admin/admin.html
 
#### （2）重启服务
/usr/local/nginx/sbin/nginx -t

/usr/local/nginx/sbin/nginx -s reload


#### (3)测试 
 curl -x127.0.0.1:80 www.test.com/admin/admin.html -I
 
 curl -x192.168.133.131:80 www.test.com/admin/admin.html -I
 
用谷歌浏览器打开：http://www.test.com/admin/admin.html 

```
[root@fxq-1 admin]# curl -x127.0.0.1:80 www.test.com/admin/admin.html -I
HTTP/1.1 200 OK
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 12:13:04 GMT
Content-Type: text/html
Content-Length: 10
Last-Modified: Thu, 19 Oct 2017 13:51:36 GMT
Connection: keep-alive
ETag: "59e8ade8-a"
Accept-Ranges: bytes

[root@fxq-1 admin]# curl -x192.168.103.131:80 www.test.com/admin/admin.html -I
HTTP/1.1 403 Forbidden
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 12:13:19 GMT
Content-Type: text/html
Content-Length: 169
Connection: keep-alive

[root@fxq-1 admin]#
```


### 二 、可以匹配正则

```



```
#### （1）做准备
```
 mkdir /data/wwwroot/test.com/upload/
 touch !$/1.php
 echo "111111" > /data/wwwroot/test.com/upload/1.php
 touch /data/wwwroot/test.com/upload/1.txt
``` 
#### (2) 配置文件中加入下面配置
``` 
location ~ .*(upload|image)/.*\.php$
{
        deny all;
}
```
#### (3)重新加载配置文件：
```
/usr/local/nginx/sbin/nginx -t

/usr/local/nginx/sbin/nginx -s reload
```
#### （4）测试：
```
[root@fxq-1 test.com]# curl -x127.0.0.1:80 www.test.com/upload/1.txt -I
HTTP/1.1 200 OK
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 12:36:46 GMT
Content-Type: text/plain
Content-Length: 0
Last-Modified: Mon, 23 Oct 2017 12:34:21 GMT
Connection: keep-alive
ETag: "59ede1cd-0"
Accept-Ranges: bytes

[root@fxq-1 test.com]# curl -x127.0.0.1:80 www.test.com/upload/1.php -I
HTTP/1.1 403 Forbidden
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 12:36:51 GMT
Content-Type: text/html
Content-Length: 169
Connection: keep-alive

```

### 三、根据user_agent限制

#### （1）修改配置文件：加入下面配置：

```
if ($http_user_agent ~ 'Spider/3.0|YoudaoBot|Tomato')
{
      return 403;
}


 deny all和return 403效果一样

```
#### （2）重启服务并测试：
```
/usr/local/nginx/sbin/nginx -t

/usr/local/nginx/sbin/nginx -s reload

[root@fxq-1 upload]# curl -x127.0.0.1:80 -A "Tomato" www.test.com -I
HTTP/1.1 403 Forbidden
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 12:45:31 GMT
Content-Type: text/html
Content-Length: 169
Connection: keep-alive

[root@fxq-1 upload]# curl -x127.0.0.1:80 -A "Tom2ato" www.test.com -I
HTTP/1.1 200 OK
Server: nginx/1.12.2
Date: Mon, 23 Oct 2017 12:45:35 GMT
Content-Type: text/html
Content-Length: 18
Last-Modified: Thu, 19 Oct 2017 13:37:42 GMT
Connection: keep-alive
ETag: "59e8aaa6-12"
Accept-Ranges: bytes

[root@fxq-1 upload]# 



```

## 12.15 Nginx解析php相关配置

```
 加入配置如下：
location ~ \.php$
    {
        include fastcgi_params;
        fastcgi_pass unix:/tmp/php-fcgi.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME /data/wwwroot/test.com$fastcgi_script_name;
    }
 
    
 "/data/wwwroot/test.com" 是网站根目录   
 ```
 ```
502：
php-fpm资源耗尽也会出现502
下面对应不上也会出现502

 fastcgi_pass 用来指定php-fpm监听的地址或者socket
 
 nginx配置中：
        # fastcgi_pass unix:/tmp/php-fcgi.sock;
        fastcgi_pass 127.0.0.1:9000;

php-fpm配置中：
    [www]
    #listen = /tmp/php-fcgi.sock
    listen = 127.0.0.1:9000



```

## 12.16 Nginx代理

用户------代理服务器--------服务器

```
```
### （1）编辑代理配置文件
```
加入如下内容：
vim /usr/local/nginx/conf/vhostproxy.conf
 
server
{
    listen 80;
    server_name ask.apelearn.com;

    location /
    {
        proxy_pass      http://121.201.9.155/;
        proxy_set_header Host   $host;
        proxy_set_header X-Real-IP      $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}

```
### （2）测试：
```
[root@fxq-1 vhost]# curl ask.apelearn.com/robots.txt
#
# robots.txt for MiWen
#

User-agent: *

Disallow: /?/admin/
Disallow: /?/people/
Disallow: /?/question/
Disallow: /account/
Disallow: /app/
Disallow: /cache/
Disallow: /install/
Disallow: /models/
Disallow: /crond/run/
Disallow: /search/
Disallow: /static/
Disallow: /setting/
Disallow: /system/
Disallow: /tmp/
Disallow: /themes/
Disallow: /uploads/
Disallow: /url-*
Disallow: /views/
Disallow: /*/ajax/
[root@fxq-1 vhost]# 





设置完代理后就可以通过本机访问robots.txt了。

[root@fxq-1 vhost]# curl -x127.0.0.1:80  ask.apelearn.com/robots.txt
#
# robots.txt for MiWen
#

User-agent: *

Disallow: /?/admin/
Disallow: /?/people/
Disallow: /?/question/
Disallow: /account/
Disallow: /app/
Disallow: /cache/
Disallow: /install/
Disallow: /models/
Disallow: /crond/run/
Disallow: /search/
Disallow: /static/
Disallow: /setting/
Disallow: /system/
Disallow: /tmp/
Disallow: /themes/
Disallow: /uploads/
Disallow: /url-*
Disallow: /views/
Disallow: /*/ajax/
[root@fxq-1 vhost]# 

```

## 扩展
502问题汇总 http://ask.apelearn.com/question/9109

```
常见的502错误
1.配置错误
因为nginx找不到php-fpm了，所以报错，一般是fastcgi_pass后面的路径配置错误了，后面可以是socket或者是ip:port


2.资源耗尽
lnmp架构在处理php时，nginx直接调取后端的php-fpm服务，如果nginx的请求量偏高，我们又没有给php-fpm配置足够的子进程，那么php-fpm就会资源耗尽，一旦资源耗尽nginx找不到php-fpm就会出现502错误，

解决方案
去调整php-fpm.conf中的pm.max_children数值，使其增加，但是也不能无限增加，毕竟资源有限，一般4G内存机器如果跑php-fpm和nginx，不跑mysql可以设置为150，8G为300以此类推


3.除了上面的两种错误还有其他的原因，很少有，我们可以借助nginx的错误日志来进行排查vim /usr/local/nginx/logs/nginx_error.log  我们也可以给日志定义级别vim/usr/local/nginx/conf/nginx.conf 找到error_log，默认是crit最严谨的就行，也可以改成debug显示的信息最全面，但是很容易撑爆我们的磁盘。



首先我们需要让浏览器进行访问
修改nginx的配置文件
[root@wqslinux ~]# vim/usr/local/nginx/conf/vhosts/111.conf

server
{
   listen 80;
   server_name www.111.com;       //域名地址
   index index.html index.htm index.php;
   root /data/www/;

   location ~ \.php$ {
       include fastcgi_params;
       fastcgi_pass unix:/tmp/www.sock;  //修改sock
      #fastcgi_pass 127.0.0.1:9000;
       fastcgi_index index.php;
       fastcgi_param SCRIPT_FILENAME /data/www$fastcgi_script_name;
    }

}



检查语法是否正常
[root@wqslinux ~]#/usr/local/nginx/sbin/nginx -t
重新加载配置文件
[root@wqslinux ~]# /usr/local/nginx/sbin/nginx-s reload
[root@wqslinux ~]# /etc/init.d/nginx reload

检查nginx是那个用户跑的
[root@wqslinux ~]# ps aux |grep nginx
编辑php-fpm文件
我们要在这个php-fpm文件里面设置nginx的用户主，跟组这样才不会显示502
[root@wqslinux ~]# vim/usr/local/php/etc/php-fpm.conf

[global]
pid = /usr/local/php/var/run/php-fpm.pid
error_log =/usr/local/php/var/log/php-fpm.log
[www]
listen = /tmp/www.sock
user = php-fpm
group = php-fpm
listen.owner = nobody    //定义属主
listen.group = nobody    //定义属组
pm = dynamic
pm.max_children = 50
pm.start_servers = 20
pm.min_spare_servers = 5
pm.max_spare_servers = 35
pm.max_requests = 500
rlimit_files = 1024

配置完之后重启php-fpm
[root@wqslinux ~]# /etc/init.d/php-fpm restart
ps： 再补充一个，是近期很多同学遇到的问题
这种情况下，使用的是socket，版本高于5.4（含5.4） 默认监听的socket文件权限是所有者只读，属组和其他用户没有任何权限。所以，nginx的启动用户（咱们配置的是nobody）就没有办法去读这个socket文件，最终导致502，这个问题可以在nginx的错误日志中发现。解决办法很简单，上面给出的配置文件中就有避免这个问题的配置。
listen.owner = nobody    //定义属主
listen.group = nobody    //定义属组
这两个配置就是定义socket的属主和属组是谁。除了这个还有一种方法
listen.mode = 777
这样nobody也可以有读取权限了。
```



location优先级 http://blog.lishiming.net/?p=100

```
在nginx配置文件中，location主要有这几种形式：

1. 正则匹配 location ~ /abc { }

2. 不区分大小写的正则匹配 location ~* /abc { }

3. 匹配路径的前缀，如果找到停止搜索 location ^~ /abc { }

4. 精确匹配 location = /abc { }

5.普通路径前缀匹配 location /abc { }

 

先说优先级

4 > 3 > 2 > 1 > 5

 

再来解释一下各个格式

location = / {
# 精确匹配 / ，主机名后面不能带任何字符串
[ configuration A ]
}

location / {
# 因为所有的地址都以 / 开头，所以这条规则将匹配到所有请求
# 但是正则和最长字符串会优先匹配
[ configuration B ]
}

location /documents/ {

# 匹配任何以 /documents/ 开头的地址，匹配符合以后，还要继续往下搜索
# 只有后面的正则表达式没有匹配到时，这一条才会采用这一条
[ configuration C ]
}

location ~ /documents/Abc {

# 匹配任何以 /documents/ 开头的地址，匹配符合以后，还要继续往下搜索
# 只有后面的正则表达式没有匹配到时，这一条才会采用这一条
[ configuration CC ]
}

location ^~ /images/ {

# 匹配任何以 /images/ 开头的地址，匹配符合以后，停止往下搜索正则，采用这一条。
[ configuration D ]
}

location ~* \.(gif|jpg|jpeg)$ {

# 匹配所有以 gif,jpg或jpeg 结尾的请求
# 然而，所有请求 /images/ 下的图片会被 config D 处理，因为 ^~ 到达不了这一条正则
[ configuration E ]
}

location /images/ {

# 字符匹配到 /images/，继续往下，会发现 ^~ 存在
[ configuration F ]
}

location /images/abc {

# 最长字符匹配到 /images/abc，继续往下，会发现 ^~ 存在
# F与G的放置顺序是没有关系的
[ configuration G ]
}

location ~ /images/abc/ {

# 只有去掉 config D 才有效：先最长匹配 config G 开头的地址，继续往下搜索，匹配到这一条正则，采用
[ configuration H ]
}​

 

再来分析一下A-H配置的执行顺序。

1. 下面2个配置同时存在时

location = / {
[ configuration A ]
}

location / {
[ configuration B ]
}

此时A生效，因为=/优先级高于/

 

2. 下面3个配置同时存在时

location  /documents/ {
[ configuration C ]
}

location ~ /documents/ {

[configuration CB]

}

location ~ /documents/abc {
[ configuration CC ]
}

当访问的url为/documents/abc/1.html，此时CC生效，首先CB优先级高于C，而CC更优先于CB

 

3. 下面4个配置同时存在时

location ^~ /images/ {
[ configuration D ]
}

location /images/ {
[ configuration F ]
}

location /images/abc {
[ configuration G ]
}

location ~ /images/abc/ {
[ configuration H ]
}​

当访问的链接为/images/abc/123.jpg时，此时D生效。虽然4个规则都能匹配到，但^~优先级是最高的。

若^~不存在时，H优先，因为~/images/ > /images/

而/images/和/images/abc同时存在时，/images/abc优先级更高，因为后者更加精准

 

4. 下面两个配置同时存在时

location ~* \.(gif|jpg|jpeg)$ {
[ configuration E ]
}

location ~ /images/abc/ {

[ configuration H ]
}​

当访问的链接为/images/abc/123.jpg时，E生效。因为上面的规则更加精准。
```