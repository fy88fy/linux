#!/bin/bash
#date=2017-06-21
#Auther is fengxiaoqing
#
# Shell script scripts to Xunjian
# -------------------------------------------------------------------------
# Copyright (c) 2017 fxq project <http://www.fy88fy.cc/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of fxq shell script collection (NSSC)
# Visit http://www.fy88fy.cc/ for more information.
# -------------------------------------------------------------------------

PS3="Please input a number(1|2|3|4|5):"
select name in "查看本机IP" "查看今天日期" "查看本机磁盘使用情况" "查看内存使用情况" "退出"

do
case $name in
	 查看本机IP)
        	ifconfig;
        	;;
     	
	查看今天日期)
        	date +%F;
        	;;
     	
	查看本机磁盘使用情况)
        	df -h;
        	;;
	查看内存使用情况)
			free;
			;;
	退出)
		exit 0
		;;
			
	
	*)
		echo -e "\033[32m------------------------------\033[0m"
		echo "1 查看本机IP;"
		echo "2 查看今天日期;"
		echo "3 查看本机磁盘使用情况;"
		echo "4 查看内存使用情况"
		echo "5 退出"	
		echo -e "\033[32m------------------------------\033[0m"
		echo -e "\033[31mPlease imput 1 or 2 or 3 or 4.\033[0m";
		;;
esac
done

