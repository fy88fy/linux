[toc]
## 6.1 压缩打包介绍
### 常见压缩文件
    windows .rar  .zip  .7z
    linux: .rar .zip .gz .bz2 .xz .tar.gz .tar.bz2  .tar.xz
## 6.2 gzip压缩工具
### gzip压缩文件:
    gzip 只能压缩文件，不能压缩目录.
    
    **gzip 1.txt 压缩完成原文件删除**
    
    生成1.txt文件:

    find /etc/ -type f -name  "*.conf" -exec cat {} >> /tmp/fxq/1.txt \;


```
# ls -lh /tmp/fxq/1.txt 
-rw-r--r-- 1 root root 2.3M Aug 23 22:01 /tmp/fxq/1.txt
```
### 1. gzip压缩文件：

    # gzip /tmp/fxq/1.txt

    # ls -lh
    total 620K
    -rw-r--r-- 1 root root 610K Aug 23 22:01 1.txt.gz


### 2. gzip解压缩：
    unzip 1.txt.gz
    gzip -d 1.txt.gz
```
# gzip -d 1.txt.gz 
# ll -h
total 2.3M
-rw-r--r-- 1 root root 2.3M Aug 23 22:01 1.txt
```
### 3. gzip指定压缩级别:

    默认是6级别
    # gzip -9 1.txt

### 4. zcat查看文件内容：

    # zcat 1.txt.gz 

### 5. gzip压缩后不删除原文件：

    # gzip -c 1.txt > /tmp/1.txt.gz 

### 6. gzip解压后改名并指定路径存放：

    # gzip -d -c /tmp/1.txt.gz > /tmp/2.txt.gz

### 7. gunzip解压缩后改名并指定路径存放：

    # gunzip -d -c /tmp/2.txt.gz > /tmp/fxq/3.txt

## 6.3 bzip2压缩工具

bzip2 比gzip 压缩能力更强

    # ll -hl
    total 292K
    -rw-r--r-- 1 root root 281K Aug 23 22:01 1.txt.bz2
    
### 1. bzip2压缩文件删除原文件：

    # bzip2 1.txt
    
### 2. bzip2压缩文件不删除原文件：

    # bzip2 -c 1.txt > /tmp/1.txt.bz2
    
### 3. bunzip2解压文件到指定路径:
    
    # bunzip2 -d -c /tmp/1.txt.bz2 > 3.txt

### 4. bzip2压缩级别:

    # bzip2 -9 1.txt
    默认压缩级别6.

### 5. bzcat查看文件内容：

    # bzcat 1.txt.bz2 

    # file 2.txt  查看文件类型。



## 6.4 xz压缩工具
xz压缩能力比gzip和bzip2都要强。使用方法基本上和gzip一样.
### 1. xz压缩文件：

    # xz  1.txt  

### 2. xz压缩级别:
    
    # xz -9 1.txt
    默认压缩级别9.

### 3. xz压缩不删除原文件：
    
    # xz -d 2.txt.xz 

### 4. unxz解压缩文件：
    
    # unxz 2.txt.xz
    
### 5. xz保留原文件,解压到指定路径：
    # xz -c 2.txt > /tmp/2.txt.xz

### 6. xz查看文件内容：
    
    # xzcat 1.txt.xz


