[toc]
## 10.19 iptables规则备份和恢复
### 保存iptables规则
    service iptables save //保存规则到/etc/sysconfig/iptables
### 备份iptables规则
    iptables-save >/tmp/myipt.txt
### 恢复iptables规则
    iptables-restore </tmp/myipt.txt

## 10.20 firewalld的9个zone

    打开firewalled
    systemctl disable iptables 停止ipbales
    systemctl stop iptables    停止ipbales
    systemctl enable firewalld  使能firewalld
    systemctl start firewalld    启用firewalld
    
    firewall-cmd --get-zones    查看从有的zone
    firewall-cmd --get-default-zone  查看默认的zone

```
[root@0 ~]# firewall-cmd --get-zones
block dmz drop external home internal public trusted work
[root@0 ~]#
```

```
[root@0 ~]# firewall-cmd --get-default-zone
public
[root@0 ~]#
```

默认9个zone
默认zone 为public

![image](http://note.youdao.com/yws/api/personal/file/FD3B7F3340AC4B68A8F736AC6BA602EE?method=download&shareKey=a4142dc1e0a3b7a3d41fea7e7690a3d4)



## 10.21 firewalld关于zone的操作
    firewall-cmd --set-default-zone=work 设定默认zone
    firewall-cmd --get-zone-of-interface=eth0 查指定网卡的zone
    firewall-cmd --zone=work --add-interface=eth0 给指定网卡添加zone
    firewall-cmd --zone=block --change-interface=eth0 更改指定网卡的zone
    firewall-cmd --zone=block --change-interface=eth0 删除指定网卡的zone
    firewall-cmd --get-active-zones  查看所有网卡所在的zone

```
[root@0 ~]# firewall-cmd --set-default-zone=work
success
[root@0 ~]# firewall-cmd --get-default-zone 
work
[root@0 ~]# 

```

```
[root@0 ~]# firewall-cmd --get-zones
block dmz drop external home internal public trusted work
[root@0 ~]# firewall-cmd --get-default-zone
public
[root@0 ~]# firewall-cmd --set-default-zone=work
success
[root@0 ~]# firewall-cmd --get-default-zone 
work
[root@0 ~]# firewall-cmd --get-zone-of-interface=eth0
no zone


[root@0 ~]# firewall-cmd --zone=work --add-interface=eth0
success
[root@0 ~]# firewall-cmd --get-default-zone 
work
[root@0 ~]# 
```

```
[root@0 ~]# firewall-cmd --zone=block --change-interface=eth0
success
[root@0 ~]# firewall-cmd --get-zone-of-interface=eth0 
block
[root@0 ~]# firewall-cmd --zone=block --remove-interface=eth0
success
[root@0 ~]# firewall-cmd --get-zone-of-interface=eth0 
no zone
[root@0 ~]# 


[root@0 ~]# firewall-cmd --get-active-zones 
block
  interfaces: eth0
[root@0 ~]# 

```




## 10.22 firewalld关于service的操作

    firewall-cmd --get-services 获取所有服务
    firewall-cmd --list-services 列出servers中的服务
    firewall-cmd --zone=public --add-service 临时增加服务
    firewall-cmd --zone=public --add-service=ftp --permanent 
    永久更改，进入配置文件:/etc/firewalld/zones

需求：ftp服务自定义端口1121 需要在work zone下面放行ftp

        cp /usr/lib/firewalld/services/ftp.xml /etc/firewalld/services/
    vi /etc/firewalld/services/ftp.xml 把21端口改为1121
    cp /usr/lib/firewalld/zones/work.xml /etc/firewalld/zones/
    vi /etc/firewalld/zones/work.xml 添加一行:"<service name="ftp"/>"
    firewall-cmd --reload  重新加载
    firewall-cmd --zone=work --list-services  

    
    模板：/usr/lib/firewalld/zones/
    
```
[root@0 ~]# firewall-cmd --zone=public --list-service
dhcpv6-client ssh
[root@0 ~]# firewall-cmd --zone=public --add-service=http
success
[root@0 ~]# firewall-cmd --zone=public --add-service=ftp 
success
[root@0 ~]# firewall-cmd --zone=public --list-services  
dhcpv6-client ftp http ssh
[root@0 ~]# 
```

```
[root@0 ~]# firewall-cmd --zone=public --add-service=http --permanent
success
[root@0 ~]# ls /etc/firewalld/zones/public.xml
/etc/firewalld/zones/public.xml
[root@0 ~]# cat /etc/firewalld/zones/public.xml
<?xml version="1.0" encoding="utf-8"?>
<zone>
  <short>Public</short>
  <description>For use in public areas. You do not trust the other computers on networks to not harm your computer. Only
 selected incoming connections are accepted.</description>  <service name="dhcpv6-client"/>
  <service name="http"/>
  <service name="ssh"/>
</zone>
[root@0 ~]# firewall-cmd --zone=public --add-service=ftp --permanent
success
[root@0 ~]# cat /etc/firewalld/zones/public.xml
<?xml version="1.0" encoding="utf-8"?>
<zone>
  <short>Public</short>
  <description>For use in public areas. You do not trust the other computers on networks to not harm your computer. Only
 selected incoming connections are accepted.</description>  <service name="ftp"/>
  <service name="dhcpv6-client"/>
  <service name="http"/>
  <service name="ssh"/>
</zone>
[root@0 ~]# 
```

```
[root@0 ~]# cat /etc/firewalld/services/ftp.xml
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>FTP</short>
  <description>FTP is a protocol used for remote file transfer. If you plan to make your FTP server publicly available, 
enable this option. You need the vsftpd package installed for this option to be useful.</description>  <port protocol="tcp" port="1121"/>
  <module name="nf_conntrack_ftp"/>
</service>
[root@0 ~]# 
```


```
[root@0 ~]# cat /etc/firewalld/zones/work.xml 
<?xml version="1.0" encoding="utf-8"?>
<zone>
  <short>Work</short>
  <description>For use in work areas. You mostly trust the other computers on networks to not harm your computer. Only s
elected incoming connections are accepted.</description>  <service name="ssh"/>
  <service name="ftp"/>
  <service name="ipp-client"/>
  <service name="dhcpv6-client"/>
</zone>
[root@0 ~]# 
```
