#!/bin/bash
#date 2017-07-05
#Authors is Fengxiaoqing
OS=`cat /etc/redhat-release |awk -F. '{print $1}' |awk '{print $NF}'`

if  (( $OS == 6  )) ; then
	tar -zcvf   ~/yum.repos.d.tar.gz.`date "+%F-%s"` /etc/yum.repos.d/*
	rm -rf /etc/yum.repos.d/* 
	rpm -e epel-release 
	wget -P /etc/yum.repos.d/ http://mirrors.aliyun.com/repo/epel-6.repo http://mirrors.aliyun.com/repo/Centos-6.repo
	yum repolist
	yum clean all
	yum makecache
fi


if (( $OS == 7  )) ; then
	tar -zcvf   ~/yum.repos.d.tar.gz.`date "+%F-%s"` /etc/yum.repos.d/*
	rm -rf /etc/yum.repos.d/* 
	rpm -e epel-release 
	wget -P /etc/yum.repos.d/ http://mirrors.aliyun.com/repo/epel-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
	yum repolist
	yum clean all
	yum makecache
fi

