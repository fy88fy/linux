[top]
## 9.1 正则介绍_grep上
### （1）什么是正则
    正则就是一串有规律的字符串。
    掌握好正则对于shell有很大帮助。
    各种语言都有正则，原理是一样的。
    主要命令有：grep/egrep sed awk。
### （2）grep
    grep 过滤指定关键字
    grep [-cinvrABC] 'word' filename
    grep -c 显示一共包含关键字的行数
    grep -n 显示一共包含关键字的行号
    grep -i 不区分大小写
    grep -v 取反
    grep -r 遍历所有子目录
    grep -A 后面根数字,过滤出符合要求的行以及下面的n行
    grep -B 后面根数字,过滤出符合要求的行以及上面的n行
    grep -C 后面根数字,过滤出符合要求的行以及上下面的n行

```
[root@0 ~]# grep -c root /etc/passwd
2
[root@0 ~]#


[root@0 ~]# grep -n root /etc/passwd
1:root:x:0:0:root:/root:/bin/bash
10:operator:x:11:0:operator:/root:/sbin/nologin
[root@0 ~]#

[root@0 ~]# grep -vn root /etc/passwd
2:bin:x:1:1:bin:/bin:/sbin/nologin
3:daemon:x:2:2:daemon:/sbin:/sbin/nologin
4:adm:x:3:4:adm:/var/adm:/sbin/nologin
5:lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
6:sync:x:5:0:sync:/sbin:/bin/sync
7:shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
8:halt:x:7:0:halt:/sbin:/sbin/halt
9:mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
11:games:x:12:100:games:/usr/games:/sbin/nologin
12:ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
13:nobody:x:99:99:Nobody:/:/sbin/nologin


[root@0 tmp]# grep -i root 1.txt 
1root:x:0:0:root:/root:/bin/bash
1ROOT:x:0:0:root:/root:/bin/bash
operator:x:11:0:operator:/root:/sbin/nologin
[root@0 tmp]#


[root@0 ~]# grep -r root /tmp/
/tmp/history_bak.log:      1 ./root/shell/mail.sh 111 111
/tmp/history_bak.log:      1 /bin/grep root /etc/passwd
/tmp/history_bak.log:      2 /usr/local/mysql/bin/mysql -u root -p
/tmp/history_bak.log:      1 /usr/local/mysql/bin/mysqldump -u root -p 
/tmp/history_bak.log:      1 /usr/local/mysql/bin/mysqldump -u root -p password "qing123."
/tmp/history_bak.log:      1 awk "/root/" 1.txt
/tmp/history_bak.log:      1 awk "/root|fxq/" 1.txt
/tmp/history_bak.log:      1 awk /root|fxq/ 1.txt
/tmp/history_bak.log:      1 cat /etc/passwd | grep root
/tmp/2.txt:1root:x:0:0:root:/root:/bin/bash
/tmp/2.txt:operator:x:11:0:operator:/root:/sbin/nologin
/tmp/1.txt:1root:x:0:0:root:/root:/bin/bash
/tmp/1.txt:operator:x:11:0:operator:/root:/sbin/nologin


[root@0 ~]# grep -nA2 root /etc/passwd
1:root:x:0:0:root:/root:/bin/bash
2-bin:x:1:1:bin:/bin:/sbin/nologin
3-daemon:x:2:2:daemon:/sbin:/sbin/nologin
--
10:operator:x:11:0:operator:/root:/sbin/nologin
11-games:x:12:100:games:/usr/games:/sbin/nologin
12-ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
[root@0 ~]# 


[root@0 tmp]# grep -nB2 root /etc/passwd
1:root:x:0:0:root:/root:/bin/bash
--
8-halt:x:7:0:halt:/sbin:/sbin/halt
9-mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
10:operator:x:11:0:operator:/root:/sbin/nologin
[root@0 tmp]# 


[root@0 tmp]# grep -nC2 root /etc/passwd
1:root:x:0:0:root:/root:/bin/bash
2-bin:x:1:1:bin:/bin:/sbin/nologin
3-daemon:x:2:2:daemon:/sbin:/sbin/nologin
--
8-halt:x:7:0:halt:/sbin:/sbin/halt
9-mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
10:operator:x:11:0:operator:/root:/sbin/nologin
11-games:x:12:100:games:/usr/games:/sbin/nologin
12-ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
[root@0 tmp]# 


```
    
## 9.2 grep中
    grep -n 'root' /etc/passwd
    grep -nv 'root' /etc/passwd
    grep '[0-9]' /etc/inittab    过滤带数字的行
    grep -v '[0-9]' /etc/inittab  过滤不包含数字化的行
    grep -v '^#' /etc/inittab  过滤以非#开头行
    grep '^[^a-zA-Z]' test.txt 过滤以非字母开头的行
    
```
[root@0 tmp]# grep '[0-9]' /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt

[root@0 tmp]# grep -vn '[0-9]' /etc/inittab 
1:# inittab is no longer used when using systemd.
2:#
3:# ADDING CONFIGURATION HERE WILL HAVE NO EFFECT ON YOUR SYSTEM.
4:#
5:# Ctrl-Alt-Delete is handled by /usr/lib/systemd/system/ctrl-alt-del.tar
get6:#
7:# systemd uses 'targets' instead of runlevels. By default, there are two
 main targets:8:#
11:#
12:# To view current default target, run:
13:# systemctl get-default
14:#
15:# To set a default target, run:
16:# systemctl set-default TARGET.target
17:#
[root@0 tmp]# 


[root@0 tmp]# grep -nv '^#' 1.txt
4:3333
5:1111
6:1root:x:0:0:root:/root:/bin/bash
7:1ROOT:x:0:0:root:/root:/bin/bash
8:bin:x:1:1:bin:/bin:/sbin/nologin




```

## 9.3 grep下
    grep 'r.o' test.txt .匹配任意一个字符
    grep 'oo*' test.txt * 匹配*前面的字符任意次 (>=0)
    grep '.*' test.txt   .* 匹配任意个任意字符
    grep 'o\{2\}' /etc/passwd 
    grep -E 'o{2}' /etc/passwd
    egrep 'o{2}' /etc/passwd
    egrep 'o+' /etc/passwd  + 匹配+前面的1次或多次 (>=1)
    egrep 'oo?' /etc/passwd ?匹配？前面字符0或者1次 (0或1)
    egrep 'root|nologin' /etc/passwd  匹配root或nologin
    egrep '(oo){2}' /etc/passwd  范围
```
[root@0 tmp]# egrep 'o{2}' /etc/passwd
root:x:0:0:root:/root:/bin/bash
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
mailnull:x:47:47::/var/spool/mqueue:/sbin/nologin
smmsp:x:51:51::/var/spool/mqueue:/sbin/nologin
[root@0 tmp]#
```

## 扩展
把一个目录下，过滤所有*.php文档中含有eval的行

    grep -r --include="*.php" 'eval' /data/
    
```
[root@0 fxq]# grep -r --include="*.php" 'eval' /   
/backup/hszd_zabbix/bak/var_www_html_zabbix/jsLoader.php:header('Cache-Control: public, must-revalidate');
/backup/hszd_zabbix/bak/var_www_html_zabbix/include/triggers.inc.php: * Substitute macros in the expression with the given values and evaluate its result.
/backup/hszd_zabbix/bak/var_www_html_zabbix/include/triggers.inc.php:function evalExpressionData($expression, $replaceFunctionMacros) {
/backup/hszd_zabbix/bak/var_www_html_zabbix/include/triggers.inc.php:	eval('$result = ('.trim($evStr).');');
/backup/hszd_zabbix/bak/var_www_html_zabbix/include/schema.inc.php:			'evaltype' => array(
/backup/hszd_zabbix/bak/var_www_html_zabbix/include/schema.inc.php:			'evaltype' => array(
/backup/hszd_zabbix/bak/var_www_html_zabbix/include/schema.inc.php:			'evaltype' => array(

```