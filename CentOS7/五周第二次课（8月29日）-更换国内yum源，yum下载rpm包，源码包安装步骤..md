[toc]
## 7.6 yum更换国内源
更换国内163yum源：

    (1)cd /etc/yum.repos.d/
    (2)rm -rf dvd.repo
    (3)wget httpd://mirrors.163.com/.help/CentOS7-Base-163.repo
    或curl -O http://mirrors.163.comm/.help/CentOS7-Base-163.repo
    (4)yum list
    
## 7.7 yum下载rpm包
安装扩展源epel

    yum install -y epel-release
    yum list | grep "epel"

yum 下载rpm包:

    yum install -y 包名字 --downloadonly
    
    ls /var/cache/yum/i386/7/base/packages/
    
    yum install -y 包名 --downloadonly --downloaddir= 路径
    
    yum reinstall -y 包名 --downloadonly
    
    yum reinstall -y 包名 --downloadonly --downloaddir= 路径
## 7.8/7.9 源码包安装
    cd /usr/local/src
    wget http://...httpd-2.4.6.tar.gz
    tar -xzvf httpd-2.4.6.tar.gz 
    cd /httpd-2.4.6
    ./confure --prefix=/usr/local/apach2
    echo $?
    看否为0
    make 
    make install
    

## 扩展
1. 配置yum源优先级 http://ask.apelearn.com/question/7168
```
    1.安装 yum-priorities
    
    yum install yum-priorities
    
    2.priorities的配置文件是/etc/yum/pluginconf.d/priorities.conf，确认其是否存在。
    其内容为:
    [main]
    enabled=1   # 0禁用 1启用
    
    3.编辑 /etc/yum.repos.d/目录下的*.repo 文件来设置优先级。
    
    参数为：
    
    priority=N   # N的值为1-99
    
    推荐的设置为：
    [base], [addons], [updates], [extras] … priority=1 
    [centosplus],[contrib] … priority=2
    Third Party Repos such as rpmforge … priority=N   (where N is > 10 and based on your preference)
    
    数字越大,优先级越低
```

2. 把源码包打包成rpm包 http://www.linuxidc.com/Linux/2012-09/70096.htm
 ```   
有好多朋友问到怎么制作rpm包，可不可把其它服务器上编译好的软件目录复杂到其它服务器上直接应用等等。。。这里做个简单的介绍，高级复杂的不会。

此方法是通过编写spec文件，使用rpmbuild来完成一个rpm的打包。

以nginx为例进行介绍

制作平台:CentOS 5.x X86_64

四步走：

第一步：建立目录结构

mkdir /usr/src/RedHat/{SOURCES,SPECS,BUILD,RPMS,SRPMS} -p

相关目录介绍：

/usr/src/redhat/SOURCES    #存放源代码、补丁等文件 
/usr/src/redhat/SPECS      #存放用于管理rpm制作进程的spec文件 
/usr/src/redhat/BUILD      #解压后的文件存放目录 
/usr/src/redhat/RPMS       #存放由rpmbuild制作好的二进制包 
/usr/src/redhat/SRPMS      #存放由rpmbuild制作好的源码包 
第二步：把源码包放在SOURCES目录下
cd /usr/src/redhat/SOURCES
wget http://nginx.org/download/nginx-1.2.0.tar.gz
 
第三步：生成nginx.spec文件
 
cd /usr/src/redhat/SPECS 
cat nginx.spec 
# 
# spec file for nginx 
# Build 2012-07-17 
# By opsren 
# 
Summary: High performance web server 
Name: Nginx 
Version: 1.2
Release: 0.el5.ngx 
License: 2-clause BSD-like license 
Group: Applications/Server 
Source: http://nginx.org/download/nginx-1.2.0.tar.gz 
URL: http://nginx.org 
Distribution: Centos/Redhat 
Packager: qiuzhijun <250621008@qq.com> 
  
%description 
Nginx ("engine x") is a high performance HTTP and reverse proxy server, as well as a mail(IMAP/POP3/SMTP) proxy server. 
%prep 
tar zxf $RPM_SOURCE_DIR/nginx-1.2.0.tar.gz 
%build 
cd nginx-1.2.0 
./configure --prefix=/usr/local/webserver/nginx --with-http_stub_status_module --with-http_ssl_module --with-pcre --lock-path=/var/run/nginx.lock --pid-path=/var/run/nginx.pid 
make 
%install 
cd nginx-1.2.0 
make install 
%preun 
if [ -z "`ps aux | grep nginx | grep -v grep`" ];then 
pkill nginx >/dev/null 
exit 0 
fi 
%files 
/usr/local/webserver/nginx 
 
第四步：RPM包制作
首先系统要安装好必备的制作工具：gcc、rpmbuild等
yum -y install gcc rpm-build pcre-devel 
cd /usr/src/redhat/SPECS/ 
rpmbuild -bb nginx.spec 
通过上面这条命令，会在/usr/src/redhat/RPMS/x86_64/下面生成nginx-1.2.0-1.el5.ngx.x86_64.rpm这个文件
 
-bb 这个选项就是制作二进制包（build binary package only from <specfile>）
 
对spec文件内容进行简单说明：
spec文件是制作rpm包的核心！
以#开头的是注释信息; 
Summary:对相关软件进行简单描述说明 
Name:定义rpm包的名称 
Version:定义软件的版本号 
Release:发行版本 
License:定义许可证 
Group:说明软件属于哪种应用类型 
Source:软件源码下载地址 
URL:软件相关官方站点 
Distribution: 发行版系列 
Packager: 制作人的简单信息 
  
%description:软件详细描述信息 
%prep:软件编译之前的处理 
%build:编译软件 
%install:安装软件 
%preun:定义卸载之前的动作 
%files:指定要打包的软件包，这里是/usr/local/webserver/nginx 
 
对于更详细的说明请参考官方资料：http://www.rpm.org/max-rpm/ch-rpm-inside.html
 
下面是apache的spec文件实例：
# 
# spec file for apache 
# Build 2012-07-17 
# By opsren 
# 
Summary: High stability web server 
Name: Apache 
Version: 2.2
Release: 22.el5
License: 2-clause BSD-like license 
Group: Applications/Server 
Source: http://apache.etoak.com/httpd/httpd-2.2.22.tar.gz 
URL: http://apache.org 
Distribution: Centos/Redhat 
Packager: qiuzhijun <250621008@qq.com> 
  
%description 
Apache is a first web server 
%prep 
tar zxf $RPM_SOURCE_DIR/httpd-2.2.22.tar.gz 
%build 
cd httpd-2.2.22 
./configure --prefix=/usr/local/webserver/apache --enable-so --enable-deflate --enable-headers --enable-mods-shared=all --enable-rewrite 
make 
%install 
cd httpd-2.2.22 
make install 
%preun 
if [ -z "`ps aux | grep httpd | grep -v grep`" ];then 
pkill httpd >/dev/null 
exit 0 
fi 
%files 
/usr/local/webserver/apache 
 以后对于相同或类似平台可以到其它服务器上进行rpm安装部署。
 
另外还有一种rpm打包的方法：rpm_create
这是一种新的打rpm的工具，不用spec语言，只需要会简单的shell命令，即可完成打包操作，非常方便，结合了spec语言和checkinstall，相比spec方法要简单很多！
 
官方站点：http://code.google.com/p/rpmcreate/
下载站点：wget http://rpmcreate.googlecode.com/files/rpm_create-1.7.5-9.x86_64.rpm
大家可以去官方站点参考！

```