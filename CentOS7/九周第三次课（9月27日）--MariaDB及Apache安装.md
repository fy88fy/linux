[toc]
## 11.6 MariaDB安装
    查看软件包的md5值:md5sum *.tar.gz
    http://mirrors.tuna.tsinghua.edu.cn/mariadb//mariadb-10.2.8/bintar-linux-x86_64/mariadb-10.2.8-linux-x86_64.tar.gz 
    
```
 cd /usr/local/src 
 wget https://downloads.mariadb.com/MariaDB/mariadb-10.2.6/bintar-linux-glibc_214-x86_64/mariadb-10.2.6-linux-glibc_214-x86_64.tar.gz
 
 tar zxvf mariadb-10.2.6-linux-glibc_214-x86_64.tar.gz
 mv mariadb-10.2.6-linux-glibc_214-x86_64 /usr/local/mariadb
 
 cd /usr/local/mariadb
 
 ./scripts/mysql_install_db --user=mysql --basedir=/usr/local/mariadb/ --datadir=/data/mariadb
 
 useradd mysql
 mkdir -p /data/mariadb
 
 cp support-files/my-small.cnf /usr/local/mariadb/my.cnf
 
 vi /usr/local/mariadb/my.cnf //定义basedir和datadir
 
 [mysqld]
basedir=/usr/local/mariadb
datadir=/data/mariadb
 
 
 
 cp support-files/mysql.server /etc/init.d/mariadb
 
 vim /etc/init.d/mariadb //定义basedir、datadir、conf以及启动参数
 
basedir=/usr/local/mariadb
datadir=/data/mariadb
conf=$basedir/my.cnf

 $bindir/mysqld_safe --defaults-file="$conf" --datadir="$datadir" --pid-file="$mysqld_pid_file_path" "$@" &

 
 /etc/init.d/mariadb start //启动

```



## 11.7/11.8/11.9 Apache安装
### (1)Apache下载地址：
```
2.2源码包： http://mirrors.cnnic.cn/apache/httpd/httpd-2.2.34.tar.gz

2.4源码包： http://mirrors.cnnic.cn/apache/httpd/httpd-2.4.27.tar.gz

apr: http://mirrors.hust.edu.cn/apache/apr/apr-1.6.2.tar.gz

apr-util: http://mirrors.hust.edu.cn/apache/apr/apr-util-1.6.0.tar.bz2 
```
### (2)安装软件

#### 1.解压
```
tar zxvf httpd-2.4.27.tar.gz
tar zxvf apr-util-1.5.4.tar.gz
tar zxvf apr-1.5.2.tar.gz
```   
#### 2.安装apr
```
cd /usr/local/src/apr-1.5.2
./configure --prefix=/usr/local/apr
make && make install
```
#### 3.安装apr-util

```
cd /usr/local/src/apr-util-1.5.4
./configure --prefix=/usr/local/apr-util --with-apr=/usr/local/apr
make && make install
```
#### 3.安装httpd
```
cd /usr/local/src/httpd-2.4.27

./configure \   
--prefix=/usr/local/apache2.4 \
--with-apr=/usr/local/apr \
--with-apr-util=/usr/local/apr-util \
--enable-so \
--enable-mods-shared=most

 make && make install
```
#### 4.安装完成,
```
ls /usr/local/apache2.4/modules
/usr/local/apache2.4/bin/httpd -M //查看加载的模块

```
#### 5.报错处理
```
报错1：
xml/apr_xml.c:35:19: 致命错误：expat.h：没有那个文件或目录  #include <expat.h>                    ^ 编译中断。 make[1]: 

安装：yum install expat-devel

报错2：
checking for pcre-config... false
configure: error: pcre-config for libpcre not found. PCRE is required and available from http://pcre.org/

wget https://downloads.sourceforge.net/pcre/pcre-8.41.tar.bz2

cd /usr/local/src/
tar -jxvf  pcre-8.41.tar.bz2
cd /usr/local/src/pcre-8.41
./configure --prefix=/usr/local/pcre
make && make install

报错4：
configure: error: APR not found. Please read the documentation.

tar -zxvf apr-1.6.2.tar.gz 
cd apr-1.6.2/
./configure --prefix=/usr/local/apr
make && make install 

tar -jxvf apr-util-1.6.0.tar.bz2 
cd apr-util-1.6.0/
./configure --prefix=/usr/local/apr-util --with-apr=/usr/local/apr
make && make install




报错5：
make[2]: *** [htpasswd] 错误 1
make[2]: 离开目录“/usr/local/src/httpd-2.4.27/support”
make[1]: *** [all-recursive] 错误 1
make[1]: 离开目录“/usr/local/src/httpd-2.4.27/support”
make: *** [all-recursive] 错误 1

cd /usr/local/src/

#cp -r apr-1.6.2  /usr/local/src/httpd-2.4.27/srclib/apr

#cp -r apr-util-1.6.0  /usr/local/src/httpd-2.4.27/srclib/apr-util

#./configure --with-included-apr --prefix=/usr/local/apache2.4 --with-apr=/usr/local/apr --with-apr-util=/usr/local/apr-util --with-pcre=/usr/local/pcre --enable-so
 --enable-mods-shared=most

#make &&make install


 
```




## 扩展
apache dso https://yq.aliyun.com/articles/6298
```
Apache HTTP服务器是一个模块化的软件，使管理者可以选择核心中包含的模块以裁剪功能。可以在编译时选择被静态包含进httpd二进制映象的模块，也可以编译成独立于主httpd二进制映象的动态共享对象DSO，DSO模块可以在编译服务器之后编译，也可以用Apache扩展工具(apxs)编译并增加。

本文阐述如何使用DSO模块及其工作原理。

工作原理

DSO是Dynamic Shared Objects（动态共享目标）的缩写，它是现代Unix派生出来的操作系统都存在着的一种动态连接机制。它提供了一种在运行时将特殊格式的代码，在程序运行需要时，将需要的部分从外存调入内存执行的方法。Apache在1.3以后的版本后开始支持它。因为Apache早就使用一个模块概念来扩展它的功能并且在内部使用一个基于调度的列表来链接扩展模块到Apache核心模块.所以,Apache早就注定要使用DSO来在运行时加载它的模块。
让我们先来看一下Apache本身的程序结构：这是一个很复杂的四层结构--每一层构建在下一层之上。
第四层是用Apache模块开发的第三方库--比如open ssl一般来说在Apache的官方发行版中这层是空的，但是在实际的Apache结构中这些库构成的层结构肯定是存在的。

第三层是一些可选的附加功能模块--如mod_ssl,mod_perl。这一层的每个模块通常实现的是Apache的一个独立的分离的功能而事实上这些模块没有一个是必须的，运行一个最小的Apache不需要任何一个此层的模块。

第二层是Apache的基本功能库-这也是Apache的核心本质层--这层包括Apache内核，http_core(Apache的核心模块）,它们实现了基本HTTP功能（比如资源处理（通过文件描述符和内存段等等），保持预生成（pre-forked)子进程模型，监听已配置的虚拟服务器的TCP/IP套接字，传输HTTP请求流到处理进程，处理HTTP协议状态，读写缓冲，此外还有附加的许多功能比如URL和MIME头的解析及DSO的装载等），也提供了Apache的应用程序接口（API)（其实Apache的真正功能还是包含在内部模块中的，为了允许这些模块完全控制Apache进程，内核必须提供API接口）,这层也包括了一般性的可用代码库（libap)和实现正则表达式匹配的库（libregex)还有就是一个小的操作系统的抽象库（libos)。

最低层是与OS相关的平台性应用函数，这些OS可以是不同现代UNIX的变种，win32,os/2,MacOS甚至只要是一个POSIX子系统。
apache模块功能分层
                                           图1 apache模块功能分层
在这个复杂的程序结构中有趣的部分是---事实上第三层和第四层与第二层之间是松散的连接，而另一方面第三层的模块间是互相依赖的--因这种结构造成的显著影响就是第三层和第四层的代码不能静态地连接到最低层平台级的代码上。因此DSO模式就成了解决它的一种手段。结合DSO功能，这个结构就变得很灵活了，可以让Apache内核（从技术上说应该是mod_so模块而不是内核）在启动时（而不是安装时）装载必要的部分以实现第三层和第四层的功能。
现代类Unix的系统都有一种叫动态共享对象(DSO)的动态连接/加载的巧妙的机制，从而可以在运行时刻，将编译成特殊格式的代码加载到一个可执行程序的地址空间。
加载的方法通常有两种：其一是，在可执行文件启动时由系统程序ld.so自动加载；其二是，在执行程序中手工地通过Unix加载器的系统接口执行系统调用dlopen()/dlsym()以实现加载。
按第一种方法，DSO通常被称为共享库(shared libraries)或者DSO库(DSO libraries)，使用libfoo.so或者libfoo.so.1.2的文件名，被存储在系统目录中(通常是/usr/lib)，并在编译安装时，使用连接器参数-lfoo建立了指向可执行程序的连接。通过设置连接器参数-R或者环境变量LD_LIBRARY_PATH，库中硬编码了可执行文件的路径，使Unix加载器能够定位到位于/usr/lib的libfoo.so，以解析可执行文件中尚未解析的位于DSO的符号。
通常，DSO不会引用可执行文件中的符号(因为它是通用代码的可重用库)，也不会有后继的解析动作。可执行文件无须自己作任何动作以使用DSO中的符号，而完全由Unix加载器代办(事实上，调用ld.so的代码是被连入每个可执行文件的非静态运行时刻启动代码的一部分)。动态加载公共库代码的优点是明显的：只需要在系统库libc.so中存储一个库代码，从而为每个程序节省了磁盘存储空间。

按第二种方法，DSO通常被称为共享对象(shared objects)或者DSO文件(DSO files)，可以使用任何文件名(但是规范的名称是foo.so)，被存储在程序特定的目录中，也不会自动建立指向其所用的可执行文件的连接，而由可执行文件在运行时自己调用dlopen()来加载DSO到其地址空间，同时也不会进行为可执行文件解析DSO中符号的操作。Unix加载器会根据可执行程序的输出符号表和已经加载的DSO库自动解析DSO中尚未解析的符号(尤其是无所不在的libc.so中的符号)，如此DSO就获得了可执行程序的符号信息，就好象是被静态连接的。

最后，为了利用DSO API的优点，执行程序必须用dlsym()解析DSO中的符号，以备稍后在诸如指派表等中使用。也就是说，执行程序必须自己解析其所需的符号。这种机制的优点是允许不加载可选的程序部件，直到程序需要的时候才被动态地加载(也就不需要内存开销)，以扩展程序的功能。

虽然这种DSO机制看似很直接，但至少有一个难点，就是在用DSO扩展程序功能(即第二种方法)时为DSO对可执行程序中符号的解析，这是因为，“反向解析”可执行程序中的DSO符号在所有标准平台上与库的设计都是矛盾的(库不会知道什么程序会使用它)。实际应用中，可执行文件中的全局符号通常不是重输出的，因此不能为DSO所用。所以在运行时刻用DSO来扩展程序功能，就必须找到强制连接器输出所有全局符号的方法。

共享库是一种典型的解决方法，因为它符合DSO机制，而且为操作系统所提供的几乎所有类型的库所使用。另一方面，使用共享对象并不是许多程序为扩展其功能所采用的方法。

截止到1998年，只有少数软件包使用DSO机制在运行时刻实际地扩展其功能，诸如Perl 5(通过其XS机制和DynaLoader模块), Netscape Server等。从1.3版本开始，Apache也加入此列，因为Apache已经用了基于指派表(dispatch-list-based)的方法来连接外部模块到Apache的核心。所以，Apache也就当然地在运行时刻用DSO来加载其模块。

优点和缺点
上述基于DSO的功能有如下优点：
由于服务器包的装配工作可以在运行时刻使用httpd.conf的配置命令LoadModule来进行，而不是在编译中使用configure来进行，因此显得更灵活。比如：只需要安装一个Apache，就可以运行多个不同的服务器实例(如标准&SSL版本，浓缩的&功能加强版本[mod_perl, PHP3]，等等)。
服务器包可以在安装后使用第三方模块被轻易地扩展。这至少对厂商发行包的维护者有巨大的好处，他可以建立一个Apache核心包，而为诸如PHP3, mod_perl, mod_fastcgi等扩展另建附加的包。
更简单的Apache模块原型。使用DSO配合apxs，可以脱离Apache源代码树，仅需要一个apxs -i和一个apachectl restart命令，把开发的模块新版本纳入运行中的Apache服务器。
DSO有如下缺点：
由于并不是所有的操作系统都支持动态加载代码到一个程序的地址空间，因此DSO机制并不能用于所有的平台。
由于Unix加载器有必须进行的符号解析的开销，服务器的启动会慢20%左右。
在某些平台上，独立位置代码(positon independent code[PIC])有时需要复杂的汇编语言技巧来实现相对寻址，而绝对寻址则不需要，因此服务器在运行时会慢5%左右。
由于DSO模块不能在所有平台上为其他基于DSO的库所连接(ld -lfoo)，比如，基于a.out的平台通常不提供此功能，而基于ELF的平台则提供，因此DSO机制并不能被用于所有类型的模块。或者可以这样说，编译为DSO文件的模块只能使用由Apache核心、C库(libc)和Apache核心所用的所有其他动态或静态的库、含有独立位置代码的静态库(libfoo.a)所提供的符号。而要使用其他代码，就只能确保Apache核心本身包含对此代码的引用，或者自己用dlopen()来加载此代码。
模块实现
相关模块
相关指令
mod_so
<IfModule>
LoadModule
Apache对独立模块的DSO支持是建立在被静态编译进Apache核心的mod_so模块基础上的，这是core以外唯一不能作为DSO存在的模块，而其他所有已发布的Apache模块，都可以通过安装文档中阐述的配置选项--enable-module=shared，被独立地编译成DSO并使之生效。一个被编译为mod_foo.so的DSO模块，可以在httpd.conf中使用mod_so的LoadModule指令，在服务器启动或重新启动时被加载。
用命令行参数-l可以查看已经编译到服务器中的模块。
新提供的支持程序apxs(APache eXtenSion)可以在Apache源代码树以外编译基于DSO的模块，从而简化Apache DSO模块的建立过程。其原理很简单：安装Apache时，配置命令make install会安装Apache C头文件，并把依赖于平台的编译器和连接器参数传给apxs程序，使用户可以脱离Apache的发布源代码树编译其模块源代码，而不改变支持DSO的编译器和连接器的参数。
用法概要
Apache 2.0的DSO功能简要说明：
编译并安装已发布的Apache模块，比如编译mod_foo.c为mod_foo.so的DSO模块：
$ ./configure --prefix=/path/to/install --enable-foo=shared
$ make install
编译并安装第三方Apache模块, 比如编译mod_foo.c为mod_foo.so的DSO模块：
$ ./configure --add-module=module_type:/path/to/3rdparty/mod_foo.c --enable-foo=shared
$ make install
配置Apache以便共享后续安装的模块：
$ ./configure --enable-so
$ make install
用apxs在Apache源代码树以外编译并安装第三方Apache模块，比如编译mod_foo.c为mod_foo.so的DSO模块：
$ cd /path/to/3rdparty
$ apxs -c mod_foo.c
$ apxs -i -a -n foo mod_foo.la
共享模块编译完毕以后，都必须在httpd.conf中用LoadModule指令使Apache激活该模块。
参考资料：
Ralf S. Engelschall--     Apache 1.3 Dynamic Shared Object (DSO) Support
Apache2.0 文档-- 动态共享对象
```
apache apxs http://man.chinaunix.net/newsoft/ApacheMenual_CN_2.2new/programs/apxs.html
```
apxs - Apache 扩展工具

apxs是一个为Apache HTTP服务器编译和安装扩展模块的工具，用于编译一个或多个源程序或目标代码文件为动态共享对象，使之可以用由mod_so提供的LoadModule指令在运行时加载到Apache服务器中。

因此，要使用这个扩展机制，你的平台必须支持DSO特性，而且Apache httpd必须内建了mod_so模块。apxs工具能自动探测是否具备这样的条件，你也可以自己用这个命令手动探测：

$ httpd -l
该命令的输出列表中应该有mod_so模块。如果所有这些条件均已具备，则可以很容易地借助apxs安装你自己的DSO模块以扩展Apache服务器的功能：

$ apxs -i -a -c mod_foo.c
gcc -fpic -DSHARED_MODULE -I/path/to/apache/include -c mod_foo.c
ld -Bshareable -o mod_foo.so mod_foo.o
cp mod_foo.so /path/to/apache/modules/mod_foo.so
chmod 755 /path/to/apache/modules/mod_foo.so
[activating module 'foo' in /path/to/apache/etc/httpd.conf]
$ apachectl restart
/path/to/apache/sbin/apachectl restart: httpd not running, trying to start
[Tue Mar 31 11:27:55 1998] [debug] mod_so.c(303): loaded module foo_module
/path/to/apache/sbin/apachectl restart: httpd started
$ _
其中的参数files可以是任何C源程序文件(.c)、目标代码文件(.o)、甚至是一个库(.a)。apxs工具会根据其后缀自动编译C源程序或者连接目标代码和库。但是，使用预编译的目标代码时，必须保证它们是地址独立代码(PIC)，使之能被动态地加载。如果使用GCC编译，则应该使用 -fpic 参数；如果使用其他C编译器，则应该查阅其手册，为apxs使用相应的编译参数。

有关Apache对DSO的支持的详细信息，可以阅读mod_so文档，或者直接阅读src/modules/standard/mod_so.c源程序。

top
语法

apxs -g [ -S name=value ] -n modname

apxs -q [ -S name=value ] query ...

apxs -c [ -S name=value ] [ -o dsofile ] [ -I incdir ] [ -D name=value ] [ -L libdir ] [ -l libname ] [ -Wc,compiler-flags ] [ -Wl,linker-flags ] files ...

apxs -i [ -S name=value ] [ -n modname ] [ -a ] [ -A ] dso-file ...

apxs -e [ -S name=value ] [ -n modname ] [ -a ] [ -A ] dso-file ...

top
选项

一般选项
-n modname
它明确设置了 -i(安装)和 -g(模板生成)选项的模块名称。对 -g 选项，它是必须的；对 -i 选项，apxs工具会根据源代码判断，或(在失败的情况下)按文件名推测出这个模块的名称。
查询选项
-q
查询某种apxs设置的信息。该选项的query参数可以是下列一个或多个字符串：CC, CFLAGS, CFLAGS_SHLIB, INCLUDEDIR, LD_SHLIB, LDFLAGS_SHLIB, LIBEXECDIR, LIBS_SHLIB, SBINDIR, SYSCONFDIR, TARGET 。
这个参数用于手动查询某些设置。比如，要手动处理Apache的C头文件，可以在Makefile中使用：

INC=-I`apxs -q INCLUDEDIR`
配置选项
-S name=value
此选项可以改变apxs的上述设置。
模板生成选项
-g
此选项生成一个名为name的子目录(见选项 -n)和其中的两个文件：一个是名为mod_name.c的样板模块源程序，可以用来建立你自己的模块，或是学习使用apxs机制的良好开端；另一个则是对应的Makefile ，用于编译和安装此模块。
DSO编译选项
-c
此选项表示需要执行编译操作。它首先会编译C源程序(.c)files为对应的目标代码文件(.o)，然后连接这些目标代码和files中其余的目标代码文件(.o和.a)，以生成动态共享对象dsofile 。如果没有指定 -o 选项，则此输出文件名由files中的第一个文件名推测得到，也就是默认为mod_name.so 。
-o dsofile
明确指定所建立的动态共享对象的文件名，它不能从files文件列表中推测得到。如果没有明确指定，则其文件名将为mod_unknown.so 。
-D name=value
此选项直接传递到给编译命令，用于增加自定义的编译变量。
-I incdir
此选项直接传递到给编译命令，用于增加自定义的包含目录。
-L libdir
此选项直接传递到给连接命令，用于增加自定义的库文件目录。
-l libname
此选项直接传递到给连接命令，用于增加自定义的库文件。
-Wc,compiler-flags
此选项用于向编译命令 libtool --mode=compile 中附加compiler-flags ，以增加编译器特有的选项。
-Wl,linker-flags
此选项用于向连接命令 libtool --mode=link 中附加linker-flags ，以增加连接器特有的选项。
DSO的安装和配置选项
-i
此选项表示需要执行安装操作，以安装一个或多个动态共享对象到服务器的modules目录中。
-a
此选项自动增加一个LoadModule行到httpd.conf文件中，以激活此模块，或者，如果此行已经存在，则启用之。
-A
与 -a 选项类似，但是它增加的LoadModule命令有一个井号前缀(#)，即此模块已经准备就绪但尚未启用。
-e
表示需要执行编辑操作，它可以与 -a 和 -A 选项配合使用，与 -i 操作类似，修改Apache的httpd.conf文件，但是并不安装此模块。
top
举例

假设有一个扩展Apache功能的模块mod_foo.c ，使用下列命令，可以将C源程序编译为共享模块，以在运行时加载到Apache服务器中：

$ apxs -c mod_foo.c
/path/to/libtool --mode=compile gcc ... -c mod_foo.c
/path/to/libtool --mode=link gcc ... -o mod_foo.la mod_foo.slo
$ _
然后，必须修改Apache的配置，以确保有一个LoadModule指令来加载此共享对象。为了简化这一步骤，apxs可以自动进行该操作，以安装此共享对象到"modules"目录，并更新httpd.conf文件，命令如下：

$ apxs -i -a mod_foo.la
/path/to/instdso.sh mod_foo.la /path/to/apache/modules
/path/to/libtool --mode=install cp mod_foo.la /path/to/apache/modules ... chmod 755 /path/to/apache/modules/mod_foo.so
[activating module 'foo' in /path/to/apache/conf/httpd.conf]
$ _
如果配置文件中尚不存在，会增加下列的行：

LoadModule foo_module modules/mod_foo.so
如果你希望默认禁用此模块，可以使用 -A 选项，即：

$ apxs -i -A mod_foo.c
要快速测试apxs机制，可以建立一个Apache模块样板及其对应的Makefile ：

$ apxs -g -n foo
Creating [DIR] foo
Creating [FILE] foo/Makefile
Creating [FILE] foo/modules.mk
Creating [FILE] foo/mod_foo.c
Creating [FILE] foo/.deps
$ _
然后，立即可以编译此样板模块为共享对象并加载到Apache服务器中：

$ cd foo
$ make all reload
apxs -c mod_foo.c
/path/to/libtool --mode=compile gcc ... -c mod_foo.c
/path/to/libtool --mode=link gcc ... -o mod_foo.la mod_foo.slo
apxs -i -a -n "foo" mod_foo.la
/path/to/instdso.sh mod_foo.la /path/to/apache/modules
/path/to/libtool --mode=install cp mod_foo.la /path/to/apache/modules ... chmod 755 /path/to/apache/modules/mod_foo.so
[activating module 'foo' in /path/to/apache/conf/httpd.conf]
apachectl restart
/path/to/apache/sbin/apachectl restart: httpd not running, trying to start
[Tue Mar 31 11:27:55 1998] [debug] mod_so.c(303): loaded module foo_module
/path/to/apache/sbin/apachectl restart: httpd started
$ _
```

apache工作模式 http://www.cnblogs.com/fnng/archive/2012/11/20/2779977.html

```
三种MPM介绍                                                                              

 

 

　　Apache 2.X  支持插入式并行处理模块，称为多路处理模块（MPM）。在编译apache时必须选择也只能选择一个MPM，对类UNIX系统，有几个不同的MPM可供选择，它们会影响到apache的速度和可伸缩性。 

　　Prefork MPM : 这个多路处理模块(MPM)实现了一个非线程型的、预派生的web服务器，它的工作方式类似于Apache 1.3。它适合于没有线程安全库，需要避免线程兼容性问题的系统。它是要求将每个请求相互独立的情况下最好的MPM，这样若一个请求出现问题就不会影响到其他请求。

　　这个MPM具有很强的自我调节能力，只需要很少的配置指令调整。最重要的是将MaxClients设置为一个足够大的数值以处理潜在的请求高峰，同时又不能太大，以致需要使用的内存超出物理内存的大小。

 

　　Worker MPM : 此多路处理模块(MPM)使网络服务器支持混合的多线程多进程。由于使用线程来处理请求，所以可以处理海量请求，而系统资源的开销小于基于进程的MPM。但是，它也使用了多进程，每个进程又有多个线程，以获得基于进程的MPM的稳定性。

　　每个进程可以拥有的线程数量是固定的。服务器会根据负载情况增加或减少进程数量。一个单独的控制进程(父进程)负责子进程的建立。每个子进程可以建立ThreadsPerChild数量的服务线程和一个监听线程，该监听线程监听接入请求并将其传递给服务线程处理和应答。

 

　　不管是Worker模式或是Prefork 模式，Apache总是试图保持一些备用的(spare)或者是空闲的子进程（空闲的服务线程池）用于迎接即将到来的请求。这样客户端就不需要在得到服务前等候子进程的产生。

 

　　Event MPM：以上两种稳定的MPM方式在非常繁忙的服务器应用下都有些不足。尽管HTTP的Keepalive方式能减少TCP连接数量和网络负载，但是 Keepalive需要和服务进程或者线程绑定，这就导致一个繁忙的服务器会耗光所有的线程。 Event MPM是解决这个问题的一种新模型，它把服务进程从连接中分离出来。在服务器处理速度很快，同时具有非常高的点击率时，可用的线程数量就是关键的资源限 制，此时Event MPM方式是最有效的。一个以Worker MPM方式工作的繁忙服务器能够承受每秒好几万次的访问量（例如在大型新闻服务站点的高峰时），而Event MPM可以用来处理更高负载。值得注意的是，Event MPM不能在安全HTTP（HTTPS）访问下工作。 

对于Event 模式，apache给出了以下警告：

This MPM is experimental, so it may or may not work as expected .

这种MPM目前处于试验状态，他可能不能按照预期的那样工作。

 

 

如何配置三种MPM                                                       

 

　　Prefork 是UNIX平台上默认的MPM，它所采用的预派生子进程方式也是apache 1.3中采用的模式。prefork 本身并没有使用到线程，2.0 版本使用它是为了与1.3版保持兼容性；另一方面，perfork用单独的子进程来处理不同的请示，之程之间是彼此独立的，这也使其成为最稳定的MPM之一 。

如何查看当前安装的Apache 的三种MPM。

复制代码
[root@localhost apache]# httpd -l
Compiled in modules:
  core.c
  prefork.c
  http_core.c
  mod_so.c
复制代码
如果你看到perfork.c 则表示当前为perfork MPM模式。worker.c 则表示为 worker MPM模式。

 

那么如何设置apache的MPM呢？

需要的apache 配置安装的时候需要指定模式：

　　[root@localhost httpd-2.4.1]# ./configure --prefix=/usr/local/apache2worker --enable-so --with-mpm=worker 
　　[root@localhost httpd-2.4.1]# make
　　[root@localhost httpd-2.4.1]# make install
指定--with-mpm=NAME 选项指定MPM，NAME就是你想使用的MPM的名称。不指定模式的话，默认为Prefork MPM。 

 

那么如何配置成Event MPM？ 

同我上面的方法一样，只需要在安装的时候加上以下参数： --enable-nonportable-atomics=yes 

需要注意的是Event MPM对于老的CPU可能是不支持的。

 

 

三种MPM参数分析                                                    

 

不管你安装的是apache哪种MPM

在安装完成之后打开.../apache/conf/extra/httpd-mpm.conf文件，找到如下配置：

 

# perfork MPM

复制代码
<IfModule mpm_prefork_module>
StartServers 5
MinSpareServers 5
MaxSpareServers 10
MaxRequestWorkers 250
MaxConnectionsPerChild 0
</IfModule> 
复制代码
# StartServers:　　数量的服务器进程开始

# MinSpareServers:　　最小数量的服务器进程,保存备用

# MaxSpareServers:　　最大数量的服务器进程,保存备用

# MaxRequestWorkers:　　最大数量的服务器进程允许开始

# MaxConnectionsPerChild:　　最大连接数的一个服务器进程服务

　　prefork 控制进程在最初建立“StartServers”个子进程后，为了满足MinSpareServers设置的需要创建一个进程，等待一秒钟，继续创建两 个，再等待一秒钟，继续创建四个……如此按指数级增加创建的进程数，最多达到每秒32个，直到满足MinSpareServers设置的值为止。这种模式 可以不必在请求到来时再产生新的进程，从而减小了系统开销以增加性能。MaxSpareServers设置了最大的空闲进程数，如果空闲进程数大于这个 值，Apache会自动kill掉一些多余进程。这个值不要设得过大，但如果设的值比MinSpareServers小，Apache会自动把其调整为 MinSpareServers+1。如果站点负载较大，可考虑同时加大MinSpareServers和MaxSpareServers。  

　　MaxRequestsPerChild设置的是每个子进程可处理的请求数。每个子进程在处理了“MaxRequestsPerChild”个请求后将自 动销毁。0意味着无限，即子进程永不销毁。虽然缺省设为0可以使每个子进程处理更多的请求，但如果设成非零值也有两点重要的好处：

1、可防止意外的内存泄 漏。2、在服务器负载下降的时侯会自动减少子进程数。

因此，可根据服务器的负载来调整这个值。

　　MaxRequestWorkers指令集同时将服务请求的数量上的限制。任何连接尝试在MaxRequestWorkerslimit将通常被排队，最多若干基于上ListenBacklog指令。 

在apache2.3.13以前的版本MaxRequestWorkers被称为MaxClients 。

（MaxClients是这些指令中最为重要的一个，设定的是 Apache可以同时处理的请求，是对Apache性能影响最大的参数。其缺省值150是远远不够的，如果请求总数已达到这个值（可通过ps -ef|grep http|wc -l来确认），那么后面的请求就要排队，直到某个已处理请求完毕。这就是系统资源还剩下很多而HTTP访问却很慢的主要原因。虽然理论上这个值越大，可以 处理的请求就越多，但Apache默认的限制不能大于256。）

 

# worker MPM 

复制代码
<IfModule mpm_worker_module>
StartServers 3
MinSpareThreads 75
MaxSpareThreads 250 
ThreadsPerChild 25
MaxRequestWorkers 400
MaxConnectionsPerChild 0
</IfModule>
复制代码
# StartServers:　　初始数量的服务器进程开始

# MinSpareThreads:　　最小数量的工作线程,保存备用

# MaxSpareThreads:　　最大数量的工作线程,保存备用

# ThreadsPerChild:　　固定数量的工作线程在每个服务器进程

# MaxRequestWorkers:　　最大数量的工作线程

# MaxConnectionsPerChild:　　最大连接数的一个服务器进程服务

　　Worker 由主控制进程生成“StartServers”个子进程，每个子进程中包含固定的ThreadsPerChild线程数，各个线程独立地处理请求。同样， 为了不在请求到来时再生成线程，MinSpareThreads和MaxSpareThreads设置了最少和最多的空闲线程数；

　　而MaxRequestWorkers 设置了同时连入的clients最大总数。如果现有子进程中的线程总数不能满足负载，控制进程将派生新的子进程 

　　MinSpareThreads和 MaxSpareThreads的最大缺省值分别是75和250。这两个参数对Apache的性能影响并不大，可以按照实际情况相应调节 。

　　ThreadsPerChild是worker MPM中与性能相关最密切的指令。ThreadsPerChild的最大缺省值是64，如果负载较大，64也是不够的。这时要显式使用 ThreadLimit指令，它的最大缺省值是20000。 

　　Worker模式下所能同时处理的请求总数是由子进程总数乘以ThreadsPerChild 值决定的，应该大于等于MaxRequestWorkers。如果负载很大，现有的子进程数不能满足时，控制进程会派生新的子进程。默认最大的子进程总数是16，加大时 也需要显式声明ServerLimit（最大值是20000）。需要注意的是，如果显式声明了ServerLimit，那么它乘以 ThreadsPerChild的值必须大于等于MaxRequestWorkers，而且MaxRequestWorkers必须是ThreadsPerChild的整数倍，否则 Apache将会自动调节到一个相应值。

 

# event MPM

复制代码
<IfModule mpm_event_module>
StartServers 3
MinSpareThreads 75
MaxSpareThreads 250
ThreadsPerChild 25
MaxRequestWorkers 400
MaxConnectionsPerChild 0
</IfModule> 
复制代码
# StartServers:初始数量的服务器进程开始

# MinSpareThreads:　　最小数量的工作线程,保存备用

# MaxSpareThreads:　　最大数量的工作线程,保存备用

# ThreadsPerChild:　　固定数量的工作线程在每个服务器进程

# MaxRequestWorkers:　　最大数量的工作线程

# MaxConnectionsPerChild:　　最大连接数的一个服务器进程服务

 

 ```
 