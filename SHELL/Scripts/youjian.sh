#!/bin/bash
SMTP_server='smtp.163.com'    # SMTP服务器
username='fy88fy@163.com'     # 用户名
password='Admin@123'             # 密码
from_email_address='fy88fy@163.com' # 发件人Email地址
to_email_address="$1"               # 收件人Email地址，zabbix传入的第一个参数
message_subject_utf8="$2"           # 邮件标题，zabbix传入的第二个参数
message_body_utf8="$3"              # 邮件内容，zabbix传入的第三个参数


DEBUG=0
if [ $DEBUG -gt 0 ]
then
        exec 2>>/tmp/mail-$(date +%F).log
        set -x
fi


# 转换邮件标题为GB2312，解决邮件标题含有中文，收到邮件显示乱码的问题。
message_subject_gb2312=`iconv -t GB2312 -f UTF-8 << EOF
$message_subject_utf8
EOF`
[ $? -eq 0 ] && message_subject="$message_subject_gb2312" || message_subject="$message_subject_utf8"

# 转换邮件内容为GB2312
message_body_gb2312=`iconv -t GB2312 -f UTF-8 << EOF
$message_body_utf8
EOF`
[ $? -eq 0 ] && message_body="$message_body_gb2312" || message_body="$message_body_utf8"

# 发送邮件
sendEmail='/usr/local/bin/sendEmail'
$sendEmail -s "$SMTP_server" -xu "$username" -xp "$password" -f "$from_email_address" -t "$to_email_address" -u "$message_subject" -m "$message_body" -o message-content-type=text -o message-charset=gb2312
