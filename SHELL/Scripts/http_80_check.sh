#!/bin/bash
#date 2017-06-24
#Authors=fengxiaoqing

service=nginx

while [[ true ]]; do
	ss -tunl | grep 80 &>/dev/null
	if [[ $? -ne 0 ]]; then
		service $service  restart
		echo "$service is restart finished"
	else 
		echo "$service is ok! "
	fi
	sleep 30
done

