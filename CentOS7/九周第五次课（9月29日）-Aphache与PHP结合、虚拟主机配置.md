[toc]
## 11.14/11.15 Apache和PHP结合
```
 httpd主配置文件/usr/local/apache2.4/conf/httpd.conf
 vim /usr/local/apache2.4/conf/httpd.conf //修改以下4个地方
```
**1.开启主机名去掉前面#**
```
    ServerName www.example.com:80 
```
**2.更改访问权限**

"Require all denied" 改为"Require all granted"
```    
     <Directory />
        AllowOverride none
        Require all granted
    </Directory>
```
**3.添加一行：**
```   
    AddType application/x-httpd-php .php
```
**4.更改默认索引页：**
```    
    DirectoryIndex index.html index.php
 ```
**5.测试语法**
 ```
 usr/local/apache2.4/bin/apachectl -t 
 
 ```
**6.重新加载配置文件**
 ```
 /usr/local/apache2.4/bin/apachectl graceful 
 
 或重新启动服务
 /usr/local/apache2.4/bin/apachectl restart
``` 
**7.查看端口：**
```
 netstat -lntp 
```
**8.编辑测试php文件:**
```
 vim /usr/local/apache2.4/htodcs/test.php //增加如下内容

<?php
echo 123;
?>
```
**9.curl测试:**
```
 curl localhost/test.php
```

**排查故障:**

```
1. 看是否加载php模块
    /usr/local/apache2.4/bin/apachectl -M
2. 看目录有无so模块文件 
    ls  /usr/local/apache2.4/modules/libphp5.so
3. 是否支持PHP解析，需增加：AddType application/x-httpd-php .php

 
 
 
```
## 11.16/11.17 Apache默认虚拟主机

```
 一台服务器可以访问多个网站，每个网站都是一个虚拟主机
 概念：域名（主机名）、DNS、解析域名、hosts
 任何一个域名解析到这台机器，都可以访问的虚拟主机就是默认虚拟主机
``` 
**1.在主配置文件中打开虚拟主机功能**
```
 vim /usr/local/apache2/conf/httpd.conf 
 搜索httpd-vhosts.conf，去掉#打开虚拟主机功能

    Include conf/extra/httpd-vhosts.conf
    
```
**2.加入两台虚拟主机:** 
```
 vim /usr/local/apache2/conf/extra/httpd-vhosts.conf 
 
<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/abc.com"
    ServerName abc.com
    ServerAlias www.abc.com
    ErrorLog "logs/abc.com-error_log"
    CustomLog "logs/abc.com-access_log" common
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "/data/wwwroot/111.com"
    ServerName 111.com
    ServerAlias www.111.com
    ErrorLog "logs/111.com-error_log"
    CustomLog "logs/111.com-access_log" common
</VirtualHost>
```
**3.测试并重新加载配置文件:**
```
 /usr/local/apache2/bin/apachectl –t
 /usr/local/apache2/bin/apachectl graceful
 ```
**4.curl 测试：**
 ```
 curl localhost
 
 curl -x192.168.42.180:80 111.com
 
    [root@fxq-0 logs]# curl -x192.168.42.180:80 www.111.com
    111.com[root@fxq-0 logs]# 
    
 curl -x192.168.42.180:80 abc.com
     
     [root@fxq-0 logs]# curl -x192.168.42.180:80 www.abc.com
    abc.com[root@fxq-0 logs]# 
 
 
 禁止从IP直接访问网站，加入下面一段在开头：
 
 <VirtualHost *:80>
    DocumentRoot "/data/wwwroot/1111.com"
    ServerName 1111.com
</VirtualHost>

 
```