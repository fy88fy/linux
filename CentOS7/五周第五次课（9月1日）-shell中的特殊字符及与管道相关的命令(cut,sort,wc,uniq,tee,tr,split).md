[toc]
## 8.10 shell特殊符号cut命令
### 1.特殊符号

```
* :任意个任意字符
? :任意单个字符
# :注释
\ ：转义字符
| ：管道符
```


### 2.几个和管道相关的命令
#### (1) cut
    


```
cut 把文件分段
cat /etc/passwd

cut -d: -f 3  /etc/passwd   
cut -d: -f 3,6,5  /etc/passwd
cut -d: -f 3-6  /etc/passwd
cut -c 10 /etc/passwd   取第十个字符
cut -c 5-10 /etc/passwd  区间


-d   分隔符 
－f  第几段   
－c  第几个字符
```
```
[root@0 ~]# cat /etc/passwd | head -2| cut -d: -f7
/bin/bash
/sbin/nologin

```


## 8.11 sort_wc_uniq命令

#### (1) sort

```
sort  排序
sort /etc/passwd  默认Assk 码
sort -t: -k3 /etc/passwd  :分隔   最三段
sort -t: -k3 -n /etc/passwd  n数字排序
sort -t: -k3,5 -n /etc/passwd 范围
sort -t: -k3 -n －r  /etc/passwd  倒序
sort -t: -k3 -n  －u  /etc/passwd  u 去重复
```
#### (2) wc
```
wc -l  行数
    wc -l 1.txt  2.txt
wc -w  单词
wc -m  字符
echo "12345" | wc -l   后面有个换行符 
```
cat -A 查看句尾隐藏的符号
#### (4) uniq
```
uniq -c

sort 1.txt | uniq -c 先排序再去重
```


## 8.12 tee_tr_split命令


#### (1) tee
和输出重定向挺像,不同的是显示在屏幕上。

cat 1.txt |  tee 2.txt

tee -a 是追加重定向
cat 1.txt |  tee -a 2.txt


#### (2) tr
 tr 'a-z' 'A-Z'
```
[root@0 tmp]# echo "fxqchina" | tr fxq FXQ
FXQchina
[root@0 tmp]# echo "fxqchina" | tr a-z A-Z
FXQCHINA
[root@0 tmp]# echo "fxqchina" | tr "a-z" "A-Z"
FXQCHINA
[root@0 tmp]# echo "fxqchina" | tr "b-z" "B-Z"
FXQCHINa


[root@0 tmp]# echo "fxqchina" | tr [a-z] 1
11111111
[root@0 tmp]# 
```
 

#### (3) split
切割文件

```
split  切割大文件
split -b 50m 1.txt   大小50M 一个
split -l 100 1.txt  100 行
```


split -b 50K 1.txt 

split -b 50K 1.txt new_

```
[root@0 fxq]# split -b 100k a.txt new_
[root@0 fxq]# ls
1.txt          a.txt       new_ab  new_ae  xab  xae
1.txt.tar.bz2  all.tar.gz  new_ac  new_af  xac  xaf
1.txt.tar.gz   new_aa      new_ad  xaa     xad
[root@0 fxq]# ll
total 5624
-rw-r--r-- 1 root root 2362256 Aug 24 23:29 1.txt
-rw-r--r-- 1 root root  288262 Aug 24 23:30 1.txt.tar.b
z2-rw-r--r-- 1 root root  624127 Aug 24 23:29 1.txt.tar.g
z-rw-r--r-- 1 root root  590532 Sep  1 21:12 a.txt
-rw-r--r-- 1 root root  624533 Aug 24 23:37 all.tar.gz
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_aa
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ab
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ac
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ad
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ae
-rw-r--r-- 1 root root   78532 Sep  1 21:13 new_af
-rw-r--r-- 1 root root  102400 Sep  1 21:12 xaa
-rw-r--r-- 1 root root  102400 Sep  1 21:12 xab
-rw-r--r-- 1 root root  102400 Sep  1 21:12 xac
-rw-r--r-- 1 root root  102400 Sep  1 21:12 xad
-rw-r--r-- 1 root root  102400 Sep  1 21:12 xae
-rw-r--r-- 1 root root   78532 Sep  1 21:12 xaf

```



split -l 500 1.txt


```
[root@0 fxq]# split -l 1000 a.txt 
[root@0 fxq]# ll
total 5632
-rw-r--r-- 1 root root 2362256 Aug 24 23:29 1.txt
-rw-r--r-- 1 root root  288262 Aug 24 23:30 1.txt.tar.b
z2-rw-r--r-- 1 root root  624127 Aug 24 23:29 1.txt.tar.g
z-rw-r--r-- 1 root root  590532 Sep  1 21:12 a.txt
-rw-r--r-- 1 root root  624533 Aug 24 23:37 all.tar.gz
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_aa
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ab
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ac
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ad
-rw-r--r-- 1 root root  102400 Sep  1 21:13 new_ae
-rw-r--r-- 1 root root   78532 Sep  1 21:13 new_af
-rw-r--r-- 1 root root   35609 Sep  1 21:14 xaa
-rw-r--r-- 1 root root   32772 Sep  1 21:14 xab
-rw-r--r-- 1 root root   35983 Sep  1 21:14 xac
-rw-r--r-- 1 root root   36528 Sep  1 21:14 xad
-rw-r--r-- 1 root root   41668 Sep  1 21:14 xae
-rw-r--r-- 1 root root   44310 Sep  1 21:14 xaf
-rw-r--r-- 1 root root   45098 Sep  1 21:14 xag
-rw-r--r-- 1 root root   34428 Sep  1 21:14 xah
-rw-r--r-- 1 root root   37206 Sep  1 21:14 xai
-rw-r--r-- 1 root root   30472 Sep  1 21:14 xaj
-rw-r--r-- 1 root root   37659 Sep  1 21:14 xak
-rw-r--r-- 1 root root   36961 Sep  1 21:14 xal
-rw-r--r-- 1 root root   44651 Sep  1 21:14 xam
-rw-r--r-- 1 root root   41979 Sep  1 21:14 xan
-rw-r--r-- 1 root root   44884 Sep  1 21:14 xao
-rw-r--r-- 1 root root   10324 Sep  1 21:14 xap
[root@0 fxq]# wc -l xaa
1000 xaa
[root@0 fxq]#
```

## 8.13 shell特殊符号下


```
$:变量前缀，!$  正则里表示行尾

~： 用户家目录

&: 把命令放到后台程序去

> >> 2> 2>>   &> 
     cat 2.txt &>> error.log
     cat 2.txt &> error.log

[]: 指定其中一个

&&  A 并且 B 左边执行成功才会执行右边面命令

||  A和 B  左边执行不成功才会执行右边命令

；  A或者B   左边 执行成功与否都会执行右边命令
```




相关测验题目：http://ask.apelearn.com/question/5437


```
作业题：

1. 设置环境变量 HISTSIZE , 使其能够保存10000条命令历史。

2. 为什么如果这样设置PS1 (PS1="[\u@\h \W]\$ ")  显示的结果和我们预想的不一样，那要如何设置才能恢复原来默认的？
    vim /etc/bashrc加入export PS1="[\u@\h \W]\\$ "

3. 想办法把当前目录下的文件的文件名中的小写字母全部替换为大写字母。

4. 使用sort以":"为分隔符，对/etc/passwd文件的第5段排序。

5. 使用cut以":"为分隔符，截出/etc/passwd的第三段字符。

6. 简述这几个文件的作用： /etc/profile, /etc/bashrc, .bashrc, .bash_profile.

7. export 的作用是什么？

8. linux下自定义变量要符合什么样的规则呢？  

9. 如何把要运行的命令丢到后台跑？又如何把后台跑的进程给调到前台？

10.  列出当前目录下以"test"开头的文件和目录。

11.  如何把一个命令的输出内容不仅打印到屏幕上而且还可以重定向到一个文件内？

12. 假如有个命令很长，我们如何使用一个简单的字符串代替这个复杂的命令呢？请举例说明。

13. 我如何实现这样的功能，把一条命令丢到后台运行，而且把其正确输出和错误输出同时重定向到一个文件内？

14. 如何按照大小（假如按照10M)分隔一个大文件，又如何按照行数(假如10000行)分隔？

15. 做实验，搞明白 ; && || 这三个符号的含义。

16. 如果只想让某个用户使用某个变量如何做？

17. 使用哪个命令会把系统当中所有的变量以及当前用户定义的自定义变量列出来？




扩展阅读：

Linux环境变量之“PS1" http://www.lishiming.net/thread-5364-1-1.html


```
    PS1（是数字1而不是字母l），每个版本bash的PS1变量内的特殊符号可能有些小的差异，你可以先man bash 一下。下面是FC4环境下默认的特殊符号所代表的意义：
    
    \d   ：代表日期，格式为weekday month date，例如："Mon Aug 1"
    
    \H ：完整的主机名称。例如：我的机器名称为：fc4.linux，则这个名称就是fc4.linux
    
    \h ：仅取主机的第一个名字，如上例，则为fc4，.linux则被省略
    
    \t ：显示时间为24小时格式，如：HH：MM：SS
    
    \T ：显示时间为12小时格式
    
    \A ：显示时间为24小时格式：HH：MM
    
    \u ：当前用户的账号名称
    
    \v ：BASH的版本信息
    
    \w ：完整的工作目录名称。家目录会以 ~代替
    
    \W ：利用basename取得工作目录名称，所以只会列出最后一个目录
    
    \\# ：下达的第几个命令
    
    \\$ ：提示字符，如果是root时，提示符为：#   ，普通用户则为：$
    
    
    默认的PS1内容为： '[\u@\h \W]\$ ' ，所以默认的提示符就是： [root@localhost ~]# 。
    
    但设置PS1的时候需要稍微处理一下


```

PS1="[\\u@\\h \\W]\\$ "  这样显示的结果才是正确的。

Linux支持中文 http://www.lishiming.net/thread-5360-1-1.html

    方法一：    修改/root/.bash_profile文件，增加export LANG=zh_CN.GB18030

对于其他用户，也必须相应修改该文件

    使用该方法时putty能显示中文，但桌面系统是英文，而且所有的网页中文显示还是乱码

    方法二：  
    引用:
    修改/etc/sysconfig/i18n文件
    #LANG="en_US.UTF-8"
    #SUPPORTED="en_US.UTF-8:en_US:en"
    #SYSFONT="latarcyrheb-sun16"
    改为
    LANG="zh_CN.GB18030"
    LANGUAGE="zh_CN.GB18030:zh_CN.GB2312:zh_CN"
    SUPPORTED="zh_CN.GB18030:zh_CN:zh"
    SYSFONT="lat0-sun16"
    SYSFONTACM="8859-15"

让命令历史永久保存并加时间戳 http://www.lishiming.net/thread-283-1-1.html

    #!/bin/sh
grep HISTTIMEFORMAT /etc/bashrc || echo 'export HISTTIMEFORMAT="%Y-%m-%d %H:%M:%S "' >>/etc/bashrc
for U in `grep -v shutdown /etc/passwd|awk -F: '$NF~/sh/&&$NF!~/no/{print $1}'`
do
    UHOME=`cat /etc/passwd|grep "^$U"|cut -d: -f6`
    [ ! -f $UHOME/.bash_history ] && touch $UHOME/.bash_history
    chattr +a $UHOME/.bash_history
done


linux 下/etc/profile、/etc/bashrc、~/.bash_profile、~/.bashrc 干啥的 http://www.lishiming.net/thread-909-1-1.html

    /etc/profile、/etc/bashrc、~/.bash_profile、~/.bashrc很容易混淆，他们之间有什么区别？它们的作用到底是什么？

    /etc/profile: 用来设置系统环境参数，比如$PATH. 这里面的环境变量是对系统内所有用户生效的。
    /etc/bashrc:  这个文件设置系统bash shell相关的东西，对系统内所有用户生效。只要用户运行bash命令，那么这里面的东西就在起作用。
    ~/.bash_profile: 用来设置一些环境变量，功能和/etc/profile 类似，但是这个是针对用户来设定的，也就是说，你在/home/user1/.bash_profile 中设定了环境变量，那么这个环境变量只针对 user1 这个用户生效.
    ~/.bashrc: 作用类似于/etc/bashrc, 只是针对用户自己而言，不对其他用户生效。
    
    另外/etc/profile中设定的变量(全局)的可以作用于任何用户,而~/.bashrc等中设定的变量(局部)只能继承/etc/profile中的变量,他们是"父子"关系.
    
    ~/.bash_profile 是交互式、login 方式进入 bash 运行的，意思是只有用户登录时才会生效。
    ~/.bashrc 是交互式 non-login 方式进入 bash 运行的，用户不一定登录，只要以该用户身份运行命令行就会读取该文件。

```

## 扩展
1. source exec 区别 http://alsww.blog.51cto.com/2001924/1113112
2. Linux特殊符号大全http://ask.apelearn.com/question/7720

    
```
    Linux特殊符号大全
在shell中常用的特殊符号罗列如下：

#   ;   ;;      .      ,       /       \       'string'|       !   $   ${}   $?      $$   $*  "string"*     **   ?   :   ^   $#   $@    `command`{}  []   [[]]   ()    (())  ||   &&       {xx,yy,zz,...}~   ~+   ~-    &   \<...\>   +       -        %=   ==   != 

# 井号 (comments)
这几乎是个满场都有的符号，除了先前已经提过的"第一行"
#!/bin/bash
井号也常出现在一行的开头，或者位于完整指令之后，这类情况表示符号后面的是注解文字，不会被执行。
# This line is comments.
echo "a = $a" # a = 0
由于这个特性，当临时不想执行某行指令时，只需在该行开头加上 # 就行了。这常用在撰写过程中。
#echo "a = $a" # a = 0
如果被用在指令中，或者引号双引号括住的话，或者在倒斜线的后面，那他就变成一般符号，不具上述的特殊功能。


~ 帐户的 home 目录
算是个常见的符号，代表使用者的 home 目录：cd ~；也可以直接在符号后加上某帐户的名称：cd ~user或者当成是路径的一部份：~/bin
~+ 当前的工作目录，这个符号代表当前的工作目录，她和内建指令 pwd的作用是相同的。
# echo ~+/var/log
~- 上次的工作目录，这个符号代表上次的工作目录。
# echo ~-/etc/httpd/logs


; 分号 (Command separator)
在 shell 中，担任"连续指令"功能的符号就是"分号"。譬如以下的例子：cd ~/backup ; mkdir startup ;cp ~/.* startup/.


;; 连续分号 (Terminator)
专用在 case 的选项，担任 Terminator 的角色。
case "$fop" inhelp) echo "Usage: Command -help -version filename";;version) echo "version 0.1" ;;esac


. 逗号 (dot,就是“点”)
在 shell 中，使用者应该都清楚，一个 dot 代表当前目录，两个 dot 代表上层目录。
CDPATH=.:~:/home:/home/web:/var:/usr/local
在上行 CDPATH 的设定中，等号后的 dot 代表的就是当前目录的意思。
如果档案名称以 dot 开头，该档案就属特殊档案，用 ls 指令必须加上 -a 选项才会显示。除此之外，在 regularexpression 中，一个 dot 代表匹配一个字元。


'string' 单引号 (single quote)
被单引号用括住的内容，将被视为单一字串。在引号内的代表变数的$符号，没有作用，也就是说，他被视为一般符号处理，防止任何变量替换。
heyyou=homeecho '$heyyou' # We get $heyyou


"string" 双引号 (double quote)
被双引号用括住的内容，将被视为单一字串。它防止通配符扩展，但允许变量扩展。这点与单引数的处理方式不同。
heyyou=homeecho "$heyyou" # We get home

`command` 倒引号 (backticks)
在前面的单双引号，括住的是字串，但如果该字串是一列命令列，会怎样？答案是不会执行。要处理这种情况，我们得用倒单引号来做。
fdv=`date +%F`echo "Today $fdv"
在倒引号内的 date +%F 会被视为指令，执行的结果会带入 fdv 变数中。


, 逗点 (comma，标点中的逗号)
这个符号常运用在运算当中当做"区隔"用途。如下例
#!/bin/bashlet "t1 = ((a = 5 + 3, b = 7 - 1, c = 15 / 3))"echo "t1= $t1, a = $a, b = $b"


/ 斜线 (forward slash)
在路径表示时，代表目录。
cd /etc/rc.dcd ../..cd /
通常单一的 / 代表 root 根目录的意思；在四则运算中，代表除法的符号。
let "num1 = ((a = 10 / 2, b = 25 / 5))"


\ 倒斜线
在交互模式下的escape 字元，有几个作用；放在指令前，有取消 aliases的作用；放在特殊符号前，则该特殊符号的作用消失；放在指令的最末端，表示指令连接下一行。
# type rmrm is aliased to `rm -i'# \rm ./*.log
上例，我在 rm 指令前加上 escape 字元，作用是暂时取消别名的功能，将 rm 指令还原。
# bkdir=/home# echo "Backup dir, \$bkdir = $bkdir"Backup dir,$bkdir = /home
上例 echo 内的 \$bkdir，escape 将 $ 变数的功能取消了，因此，会输出 $bkdir，而第二个 $bkdir则会输出变数的内容 /home。


| 管道 (pipeline)
pipeline 是 UNIX 系统，基础且重要的观念。连结上个指令的标准输出，做为下个指令的标准输入。
who | wc -l
善用这个观念，对精简 script 有相当的帮助。


! 惊叹号(negate or reverse)
通常它代表反逻辑的作用，譬如条件侦测中，用 != 来代表"不等于"
if [ "$?" != 0 ]thenecho "Executes error"exit 1fi
在规则表达式中她担任 "反逻辑" 的角色
ls a[!0-9]
上例，代表显示除了a0, a1 .... a9 这几个文件的其他文件。


: 冒号
在 bash 中，这是一个内建指令："什么事都不干"，但返回状态值 0。
:
echo $? # 回应为 0
: > f.
上面这一行，相当于cat/dev/null>f.

。不仅写法简短了，而且执行效率也好上许多。
有时，也会出现以下这类的用法
: ${HOSTNAME?} ${USER?} ${MAIL?}
这行的作用是，检查这些环境变数是否已设置，没有设置的将会以标准错误显示错误讯息。像这种检查如果使用类似 test 或 if这类的做法，基本上也可以处理，但都比不上上例的简洁与效率。
除了上述之外，还有一个地方必须使用冒号
PATH=$PATH:$HOME/fbin:$HOME/fperl:/usr/local/mozilla
在使用者自己的HOME 目录下的 .bash_profile或任何功能相似的档案中，设定关于"路径"的场合中，我们都使用冒号，来做区隔。


? 问号 (wild card)
在文件名扩展(Filename expansion)上扮演的角色是匹配一个任意的字元，但不包含 null 字元。
# ls a?a1
善用她的特点，可以做比较精确的档名匹配。


* 星号 (wild card)
相当常用的符号。在文件名扩展(Filename expansion)上，她用来代表任何字元，包含 null 字元。
# ls a*a a1 access_log
在运算时，它则代表 "乘法"。
let "fmult=2*3"
除了内建指令 let，还有一个关于运算的指令expr，星号在这里也担任"乘法"的角色。不过在使用上得小心，他的前面必须加上escape 字元。


** 次方运算
两个星号在运算时代表 "次方" 的意思。
let "sus=2**3"echo "sus = $sus" # sus = 8


$ 钱号(dollar sign)
变量替换(Variable Substitution)的代表符号。
vrs=123echo "vrs = $vrs" # vrs = 123
另外，在 Regular Expressions 里被定义为 "行" 的最末端 (end-of-line)。这个常用在grep、sed、awk 以及 vim(vi) 当中。


${} 变量的正规表达式
bash 对 ${} 定义了不少用法。以下是取自线上说明的表列
   ${parameter:-word}   ${parameter:=word}   ${parameter:?word}   ${parameter:+word}   ${parameter:offset}   ${parameter:offset:length}   ${!prefix*}   ${#parameter}   ${parameter#word}   ${parameter##word}   ${parameter%word}   ${parameter%%word}   ${parameter/pattern/string}   ${parameter//pattern/string}


$* 
$* 引用script的执行引用变量，引用参数的算法与一般指令相同，指令本身为0，其后为1，然后依此类推。引用变量的代表方式如下：
$0, $1, $2, $3, $4, $5, $6, $7, $8, $9, ${10}, ${11}.....
个位数的，可直接使用数字，但两位数以上，则必须使用 {} 符号来括住。
$* 则是代表所有引用变量的符号。使用时，得视情况加上双引号。
echo "$*"
还有一个与 $* 具有相同作用的符号，但效用与处理方式略为不同的符号。


$@
$@ 与 $* 具有相同作用的符号，不过她们两者有一个不同点。
符号 $* 将所有的引用变量视为一个整体。但符号 $@ 则仍旧保留每个引用变量的区段观念。

$#
这也是与引用变量相关的符号，她的作用是告诉你，引用变量的总数量是多少。
echo "$#"


$? 状态值 (status variable)
一般来说，UNIX(linux) 系统的进程以执行系统调用exit()来结束的。这个回传值就是status值。回传给父进程，用来检查子进程的执行状态。
一般指令程序倘若执行成功，其回传值为 0；失败为 1。
tar cvfz dfbackup.tar.gz /home/user > /dev/nullecho"$?"
由于进程的ID是唯一的，所以在同一个时间，不可能有重复性的PID。有时，script会需要产生临时文件，用来存放必要的资料。而此script亦有可能在同一时间被使用者们使用。在这种情况下，固定文件名在写法上就显的不可靠。唯有产生动态文件名，才能符合需要。符号

或许可以符合这种需求。它代表当前shell 的 PID。
echo "$HOSTNAME, $USER, $MAIL" > ftmp.$$
使用它来作为文件名的一部份，可以避免在同一时间，产生相同文件名的覆盖现象。
ps: 基本上，系统会回收执行完毕的 PID，然后再次依需要分配使用。所以 script 即使临时文件是使用动态档名的写法，如果script 执行完毕后仍不加以清除，会产生其他问题。

(   ) 指令群组 (command group)
用括号将一串连续指令括起来，这种用法对 shell 来说，称为指令群组。如下面的例子：(cd ~ ; vcgh=`pwd` ;echo $vcgh)，指令群组有一个特性，shell会以产生 subshell来执行这组指令。因此，在其中所定义的变数，仅作用于指令群组本身。我们来看个例子
# cat ftmp-01#!/bin/basha=fsh(a=incg ; echo -e "\n $a \n")echo $a#./ftmp-01incgfsh
除了上述的指令群组，括号也用在 array 变数的定义上；另外也应用在其他可能需要加上escape字元才能使用的场合，如运算式。


((  ))
这组符号的作用与 let 指令相似，用在算数运算上，是 bash 的内建功能。所以，在执行效率上会比使用 let指令要好许多。
#!/bin/bash(( a = 10 ))echo -e "inital value, a = $a\n"(( a++))echo "after a++, a = $a"

{  } 大括号 (Block of code)
有时候 script 当中会出现，大括号中会夹着一段或几段以"分号"做结尾的指令或变数设定。
# cat ftmp-02#!/bin/basha=fsh{a=inbc ; echo -e "\n $a \n"}echo $a#./ftmp-02inbcinbc
这种用法与上面介绍的指令群组非常相似，但有个不同点，它在当前的 shell 执行，不会产生 subshell。
大括号也被运用在 "函数" 的功能上。广义地说，单纯只使用大括号时，作用就像是个没有指定名称的函数一般。因此，这样写 script也是相当好的一件事。尤其对输出输入的重导向上，这个做法可精简 script 的复杂度。

此外，大括号还有另一种用法，如下
{xx,yy,zz,...}
这种大括号的组合，常用在字串的组合上，来看个例子
mkdir {userA,userB,userC}-{home,bin,data}
我们得到 userA-home, userA-bin, userA-data, userB-home, userB-bin,userB-data, userC-home, userC-bin,userC-data，这几个目录。这组符号在适用性上相当广泛。能加以善用的话，回报是精简与效率。像下面的例子
chown root /usr/{ucb/{ex,edit},lib/{ex?.?*,how_ex}}
如果不是因为支援这种用法，我们得写几行重复几次呀！


[   ] 中括号
常出现在流程控制中，扮演括住判断式的作用。if [ "$?" != 0 ]thenecho "Executes error"exit1fi
这个符号在正则表达式中担任类似 "范围" 或 "集合" 的角色
rm -r 200[1234]
上例，代表删除 2001, 2002, 2003, 2004 等目录的意思。


[[     ]]
这组符号与先前的 [] 符号，基本上作用相同，但她允许在其中直接使用 || 与&& 逻辑等符号。
#!/bin/bashread akif [[ $ak > 5 || $ak< 9 ]]thenecho $akfi


|| 逻辑符号
这个会时常看到，代表 or 逻辑的符号。


&& 逻辑符号
这个也会常看到，代表 and 逻辑的符号。


& 后台工作
单一个& 符号，且放在完整指令列的最后端，即表示将该指令列放入后台中工作。
tar cvfz data.tar.gz data > /dev/null&

\<...\> 单字边界
这组符号在规则表达式中，被定义为"边界"的意思。譬如，当我们想找寻 the 这个单字时，如果我们用
grep the FileA
你将会发现，像 there 这类的单字，也会被当成是匹配的单字。因为 the 正巧是 there的一部份。如果我们要必免这种情况，就得加上 "边界" 的符号
grep '\' FileA


+ 加号 (plus)
在运算式中，她用来表示 "加法"。
expr 1 + 2 + 3
此外在规则表达式中，用来表示"很多个"的前面字元的意思。
# grep '10\+9' fileB109100910000910000931010009#这个符号在使用时，前面必须加上escape 字元。


- 减号 (dash)
在运算式中，她用来表示 "减法"。
expr 10 - 2
此外也是系统指令的选项符号。
ls -expr 10 - 2
在 GNU 指令中，如果单独使用 - 符号，不加任何该加的文件名称时，代表"标准输入"的意思。这是 GNU指令的共通选项。譬如下例
tar xpvf -
这里的 - 符号，既代表从标准输入读取资料。
不过，在 cd 指令中则比较特别
cd -
这代表变更工作目录到"上一次"工作目录。


% 除法 (Modulo)
在运算式中，用来表示 "除法"。
expr 10 % 2
此外，也被运用在关于变量的规则表达式当中的下列
${parameter%word}${parameter%%word}
一个 % 表示最短的 word 匹配，两个表示最长的 word 匹配。


= 等号 (Equals)
常在设定变数时看到的符号。
vara=123echo " vara = $vara"
或者像是 PATH 的设定，甚至应用在运算或判断式等此类用途上。


== 等号 (Equals)
常在条件判断式中看到，代表 "等于" 的意思。
if [ $vara == $varb ]
...下略

!= 不等于
常在条件判断式中看到，代表 "不等于" 的意思。
if [ $vara != $varb ]
...下略


^
这个符号在规则表达式中，代表行的 "开头" 位置，在[]中也与"!"(叹号)一样表示“非”


输出/输入重导向
>      >>   <   <<   :>   &>   2&>   2<>>&   >&2   

文件描述符(File Descriptor)，用一个数字（通常为0-9）来表示一个文件。
常用的文件描述符如下：
文件描述符          名称         常用缩写     默认值
     0               标准输入      stdin            键盘
     1               标准输出      stdout         屏幕
     2            标准错误输出   stderr          屏幕
我们在简单地用<或>时，相当于使用 0< 或 1>（下面会详细介绍）。
* cmd > file
把cmd命令的输出重定向到文件file中。如果file已经存在，则清空原有文件，使用bash的noclobber选项可以防止复盖原有文件。
* cmd >> file
把cmd命令的输出重定向到文件file中，如果file已经存在，则把信息加在原有文件後面。
* cmd < file
使cmd命令从file读入
* cmd << text
从命令行读取输入，直到一个与text相同的行结束。除非使用引号把输入括起来，此模式将对输入内容进行shell变量替换。如果使用<<- ，则会忽略接下来输入行首的tab，结束行也可以是一堆tab再加上一个与text相同的内容，可以参考後面的例子。
* cmd <<< word
把word（而不是文件word）和後面的换行作为输入提供给cmd。
* cmd <> file
以读写模式把文件file重定向到输入，文件file不会被破坏。仅当应用程序利用了这一特性时，它才是有意义的。
* cmd >| file
功能同>，但即便在设置了noclobber时也会复盖file文件，注意用的是|而非一些书中说的!，目前仅在csh中仍沿用>!实现这一功能。
: > filename      把文件"filename"截断为0长度.# 如果文件不存在, 那么就创建一个0长度的文件(与'touch'的效果相同).
cmd >&n 把输出送到文件描述符n
cmd m>&n 把输出到文件符m的信息重定向到文件描述符n
cmd >&- 关闭标准输出
cmd <&n 输入来自文件描述符n
cmd m<&n m来自文件描述各个n
cmd <&- 关闭标准输入
cmd <&n- 移动输入文件描述符n而非复制它。
cmd >&n- 移动输出文件描述符 n而非复制它。
注意： >&实际上复制了文件描述符，这使得cmd > file 2>&1与cmd 2>&1 >file的效果不一样。
本文转载http://blog.csdn.net/xifeijian/article/details/9253011
```

3. sort并未按ASCII排序 http: //blog.csdn.net/zenghui08/article/details/7938975
```
        1. cat test.txt
    c
    b
    x
    d
    A
    E
    f
    a
    S
    u
    t
    T
    S
    s
    2. sort test.txt
    a
    A
    b
    c
    d
    E
    f
    s
    S
    S
    t
    T
    u
    x
    3. export LC_ALL=C
    4. sort test.txt
    A
    E
    S
    S
    T
    a
    b
    c
    d
    f
    s
    t
    u
    x
    Linux下的sort函数是通过locale来判断的，可以将LC_ALL设置为C，从而使得排序按照ASCII，这个问题没想到困扰了我很久。。

```

