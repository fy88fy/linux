[toc]
## 12.21 php-fpm的pool
### (1)修改配置文件php-fpm.conf
```

 vim /usr/local/php/etc/php-fpm.conf
 //在[global]部分增加下面一句，删除[www]之类的pool
 
 include = etc/php-fpm.d/*.conf
 
 
[root@fxq-1 etc]# cat /usr/local/php-fpm/etc/php-fpm.conf
    [global]
    pid = /usr/local/php-fpm/var/run/php-fpm.pid
    error_log = /usr/local/php-fpm/var/log/php-fpm.log
    include = etc/php-fpm.d/*.conf
[root@fxq-1 etc]# 
 
```
### (2)单独创建两个pool
```
 mkdir /usr/local/php/etc/php-fpm.d/
 cd /usr/local/php/etc/php-fpm.d/
 
 vim www.conf //内容如下
 
[www]
listen = /tmp/www.sock
listen.mode=666
user = php-fpm
group = php-fpm
pm = dynamic
pm.max_children = 50
pm.start_servers = 20
pm.min_spare_servers = 5
pm.max_spare_servers = 35
pm.max_requests = 500
rlimit_files = 1024


 继续编辑配置文件
 vim aming.conf //内容如下
 
[fxq]
listen = /tmp/fxq.sock
listen.mode=666
user = php-fpm
group = php-fpm
pm = dynamic
pm.max_children = 50
pm.start_servers = 20
pm.min_spare_servers = 5
pm.max_spare_servers = 35
pm.max_requests = 500
rlimit_files = 1024

```
### (3)重启服务并测试
```
 /usr/local/php/sbin/php-fpm –t
 /etc/init.d/php-fpm restart

cat /data/wwwroot/fxq.com/index.php
<?php
echo "this is fxq.com pages."

?>
[root@fxq-1 vhost]# /usr/local/nginx/sbin/nginx -t
nginx: the configuration file /usr/local/nginx/conf/nginx.conf syntax is ok
nginx: configuration file /usr/local/nginx/conf/nginx.conf test is successful
[root@fxq-1 vhost]# /usr/local/nginx/sbin/nginx -s reload
[root@fxq-1 vhost]# ps aux | grep php-fpm
root       3188  0.0  0.7 227252  4768 ?        Ss   21:10   0:00 php-fpm: master process (/usr/local/php-fpm/etc/php-fpm.conf)
php-fpm    3189  0.0  0.8 229276  4856 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3190  0.0  0.9 229276  5732 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3191  0.0  0.8 229276  4856 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3192  0.0  0.8 229276  4856 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3193  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3194  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3195  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3196  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3197  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3198  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3199  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3200  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3201  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3202  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3203  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3204  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3205  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3206  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3207  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3208  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool fxq
php-fpm    3209  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3210  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3211  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3212  0.0  0.8 229276  4860 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3213  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3214  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3215  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3216  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3217  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3218  0.0  0.8 229276  4864 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3219  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3220  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3221  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3222  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3223  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3224  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3225  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3226  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3227  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
php-fpm    3228  0.0  0.8 229276  4868 ?        S    21:10   0:00 php-fpm: pool www
root       3346  0.0  0.1 112672   972 pts/0    S+   21:20   0:00 grep --color=auto php-fpm
[root@fxq-1 vhost]# curl -x127.0.0.1:80 www.fxq.com 
this is fxq.com pages.[root@fxq-1 vhost]#


```
## 12.22 php-fpm慢执行日志
### (1)编辑配置文件pool
     vim /usr/local/php-fpm/etc/php-fpm.d/www.conf
     //加入如下内容
     
    request_slowlog_timeout = 1
    slowlog = /usr/local/php-fpm/var/log/www-slow.log
    
### (2)配置nginx的虚拟主机test.com.conf
     把unix:/tmp/php-fcgi.sock改为unix:/tmp/www.sock
     重新加载nginx服务
     
     vim /data/wwwroot/test.com/sleep.php
     //写入如下内容
     
     <?php echo "test slow log";sleep(2);echo "done";?>
     curl -x127.0.0.1:80 test.com/sleep.php 
     cat /usr/local/php-fpm/var/log/www-slow.log
### (3)重启服务并测试：

```
/etc/init.d/php-fpm restart
[root@fxq-1 log]# vim /data/wwwroot/test.com/sleep.php
[root@fxq-1 log]# curl -x127.0.0.1:80 www.test.com/sleep.php
test slow logdone[cat www-slow.log 

[25-Oct-2017 21:41:12]  [pool www] pid 3477
script_filename = /data/wwwroot/test.com/sleep.php
[0x00007f00c597c298] sleep() /data/wwwroot/test.com/sleep.php:3
[root@fxq-1 log]# 
```

## 12.23 open_basedir
### (1)编辑配置文件pool
     vim /usr/local/php-fpm/etc/php-fpm.d/fxq.conf
     //加入如下内容
     
    php_admin_value[open_basedir]=/data/wwwroot/fxq.com:/tmp/
     
     创建测试php脚本，进行测试
     再次更改fxq.conf，修改路径，再次测试
     
### (2)配置错误日志
     
     
    vim /usr/local/php-fpm/etc/php.ini
    
    log_errors = on
    error_log = /usr/local/php-fpm/var/log/php_errors.log
    error_reporting = E_ALL 
    
    touch /usr/local/php-fpm/var/log/php_errors.log
    chmod 777 /usr/local/php-fpm/var/log/php_errors.log
    
     
### (3)再次测试查看错误日志
```
[root@fxq-1 etc]# vim /usr/local/php-fpm/etc/php-fpm.d/fxq.conf 
[root@fxq-1 etc]# /etc/init.d/php-fpm restart
Gracefully shutting down php-fpm . done
Starting php-fpm  done
[root@fxq-1 etc]# curl -x127.0.0.1:80 www.fxq.com/index.php
No input file specified.
[root@fxq-1 etc]# cat /usr/local/php-fpm/var/log/php_errors.log 
[25-Oct-2017 14:45:21 UTC] PHP Warning:  Unknown: open_basedir restriction in effect. File(/data/wwwroot/fxq.com/index.php) is not within the allowed path(s): (/data/w
wwroot/fxq1.com:/tmp/) in Unknown on line 0[25-Oct-2017 14:45:21 UTC] PHP Warning:  Unknown: failed to open stream: Operation not permitted in Unknown on line 0
[root@fxq-1 etc]#
```
## 12.24 php-fpm进程管理

```
 pm = dynamic  //动态进程管理，也可以是static
 pm.max_children = 50 //最大子进程数，ps aux可以查看
 pm.start_servers = 20 //启动服务时会启动的进程数
 pm.min_spare_servers = 5 //定义在空闲时段，子进程数的最少数量，如果达到这个数值时，php-fpm服务会自动派生新的子进程。
 pm.max_spare_servers = 35 //定义在空闲时段，子进程数的最大值，如果高于这个数值就开始清理空闲的子进程。
 pm.max_requests = 500  //定义一个子进程最多处理的请求数，也就是说在一个php-fpm的子进程最多可以处理这么多请求，当达到这个数值时，它会自动退出。

```