[toc]

## 4.1 df命令
分区查看工具：

### 1. df
```
[fxq@vm_46_188_centos ~]$ df
Filesystem     1K-blocks    Used Available Use% Mounted on
/dev/vda1       20510332 4844740  14617076  25% /
devtmpfs          499348       0    499348   0% /dev
tmpfs             508452       0    508452   0% /dev/shm
tmpfs             508452   12660    495792   3% /run
tmpfs             508452       0    508452   0% /sys/fs/cgroup
tmpfs             101692       0    101692   0% /run/user/0
[fxq@vm_46_188_centos ~]$ 

```
### 2. df -h 
```
[fxq@vm_46_188_centos ~]$ df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/vda1        20G  4.7G   14G  25% /
devtmpfs        488M     0  488M   0% /dev
tmpfs           497M     0  497M   0% /dev/shm
tmpfs           497M   13M  485M   3% /run
tmpfs           497M     0  497M   0% /sys/fs/cgroup
tmpfs           100M     0  100M   0% /run/user/0
[fxq@vm_46_188_centos ~]$



```
### 3.df -i 

```
[fxq@vm_46_188_centos ~]$ df -i
Filesystem      Inodes  IUsed   IFree IUse% Mounted on
/dev/vda1      1310720 121406 1189314   10% /
devtmpfs        124837    298  124539    1% /dev
tmpfs           127113      1  127112    1% /dev/shm
tmpfs           127113    381  126732    1% /run
tmpfs           127113     13  127100    1% /sys/fs/cgroup
tmpfs           127113      1  127112    1% /run/user/0
[fxq@vm_46_188_centos ~]$ 
```

### 4. df -m


```
[fxq@vm_46_188_centos ~]$ df -m
Filesystem     1M-blocks  Used Available Use% Mounted on
/dev/vda1          20030  4732     14275  25% /
devtmpfs             488     0       488   0% /dev
tmpfs                497     0       497   0% /dev/shm
tmpfs                497    13       485   3% /run
tmpfs                497     0       497   0% /sys/fs/cgroup
tmpfs                100     0       100   0% /run/user/0

```



### 5. 单位换算：

10024Byte=1KB

1024KB =1MB 

1024MB=1GB

1024GB=1TB

/dev/shm 内存：

### 6.free 查看swap


```
[fxq@vm_46_188_centos ~]$ free
              total        used        free      shared  buff/cache  
 availableMem:        1016904      341924       83976       12736      591004  
    485408Swap:             0           0           0
[fxq@vm_46_188_centos ~]$ 
```




## 4.2 du命令

du -sh /etc/

du -sh /etc/passwd

ls -lh /etc/passwd

当文件小于4KB时，也会显示4KB

du -sb 可查看原大小 =ls -lh

du /root/


```
[root@vm_46_188_centos ~]# du -sh /root/
28M	/root/
[root@vm_46_188_centos ~]# du -s /root/
27768	/root/
[root@vm_46_188_centos ~]# du /root/
4	/root/fxq
4	/root/ffff
4	/root/234
8	/root/.ssh/fengxiaoqing/.git/logs/refs/remotes/origin
12	/root/.ssh/fengxiaoqing/.git/logs/refs/remotes
8	/root/.ssh/fengxiaoqing/.git/logs/refs/heads
24	/root/.ssh/fengxiaoqing/.git/logs/refs
32	/root/.ssh/fengxiaoqing/.git/logs
44	/root/.ssh/fengxiaoqing/.git/hooks
8	/root/.ssh/fengxiaoqing/.git/objects/5e
8	/root/.ssh/fengxiaoqing/.git/objects/46
8	/root/.ssh/fengxiaoqing/.git/objects/87
8	/root/.ssh/fengxiaoqing/.git/objects/f2
4	/root/.ssh/fengxiaoqing/.git/objects/pack
8	/root/.ssh/fengxiaoqing/.git/objects/02
4	/root/.ssh/fengxiaoqing/.git/objects/info
8	/root/.ssh/fengxiaoqing/.git/objects/9d
60	/root/.ssh/fengxiaoqing/.git/objects
4	/root/.ssh/fengxiaoqing/.git/branches
8	/root/.ssh/fengxiaoqing/.git/info
8	/root/.ssh/fengxiaoqing/.git/refs/remotes/origin
12	/root/.ssh/fengxiaoqing/.git/refs/remotes
8	/root/.ssh/fengxiaoqing/.git/refs/heads
4	/root/.ssh/fengxiaoqing/.git/refs/tags
28	/root/.ssh/fengxiaoqing/.git/refs
200	/root/.ssh/fengxiaoqing/.git
208	/root/.ssh/fengxiaoqing
228	/root/.ssh
4	/root/123/123
8	/root/123
192	/root/shell
4	/root/.pki/nssdb
8	/root/.pki
4	/root/dir-2017-05-12
27768	/root/
[root@vm_46_188_centos ~]# 

```


## 4.3/4.4 磁盘分区

fdisk -l 查看本机硬盘.


```
[root@vm_46_188_centos ~]# fdisk -l

Disk /dev/vda: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x0005fc9a

   Device Boot      Start         End      Blocks   Id  System
/dev/vda1   *        2048    41943039    20970496   83  Linux
[root@vm_46_188_centos ~]#

```
w 查看负载


```
[root@vm_46_188_centos ~]# w
 22:58:39 up 21 days,  5:50,  1 user,  load average: 3.59, 4.43, 4.76
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
root     pts/0    60.10.158.223    21:25    7.00s  0.16s  0.00s w
[root@vm_46_188_centos ~]#
```

mbr 最多有4个主分区, 最大2T


```
[root@fxq-1 ~]# fdisk -l

磁盘 /dev/sda：32.2 GB, 32212254720 字节，62914560 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x0004f0b3

   设备 Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      411647      204800   83  Linux
/dev/sda2          411648     4605951     2097152   82  Linux swap / Solaris
/dev/sda3         4605952    62914559    29154304   83  Linux

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节

[root@fxq-1 ~]# 

```


```
[root@fxq-1 ~]# fdisk -l

磁盘 /dev/sda：32.2 GB, 32212254720 字节，62914560 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x0004f0b3

   设备 Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      411647      204800   83  Linux
/dev/sda2          411648     4605951     2097152   82  Linux swap / Solaris
/dev/sda3         4605952    62914559    29154304   83  Linux

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节

[root@fxq-1 ~]# fdisk /dev/sdb
欢迎使用 fdisk (util-linux 2.23.2)。

更改将停留在内存中，直到您决定将更改写入磁盘。
使用写入命令前请三思。

Device does not contain a recognized partition table
使用磁盘标识符 0xc2b902ea 创建新的 DOS 磁盘标签。

命令(输入 m 获取帮助)：m
命令操作
   a   toggle a bootable flag
   b   edit bsd disklabel
   c   toggle the dos compatibility flag
   d   delete a partition
   g   create a new empty GPT partition table
   G   create an IRIX (SGI) partition table
   l   list known partition types
   m   print this menu
   n   add a new partition
   o   create a new empty DOS partition table
   p   print the partition table
   q   quit without saving changes
   s   create a new empty Sun disklabel
   t   change a partition's system id
   u   change display/entry units
   v   verify the partition table
   w   write table to disk and exit
   x   extra functionality (experts only)

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): p
分区号 (1-4，默认 1)：
起始 扇区 (2048-41943039，默认为 2048)：
将使用默认值 2048
Last 扇区, +扇区 or +size{K,M,G} (2048-41943039，默认为 41943039)：+2G
分区 1 已设置为 Linux 类型，大小设为 2 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (1 primary, 0 extended, 3 free)
   e   extended
Select (default p): p
分区号 (2-4，默认 2)：
起始 扇区 (4196352-41943039，默认为 4196352)：
将使用默认值 4196352
Last 扇区, +扇区 or +size{K,M,G} (4196352-41943039，默认为 41943039)：+5G
分区 2 已设置为 Linux 类型，大小设为 5 GiB

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (2 primary, 0 extended, 2 free)
   e   extended
Select (default p): p
分区号 (3,4，默认 3)：
起始 扇区 (14682112-41943039，默认为 14682112)：
将使用默认值 14682112
Last 扇区, +扇区 or +size{K,M,G} (14682112-41943039，默认为 41943039)：+8G        
分区 3 已设置为 Linux 类型，大小设为 8 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (3 primary, 0 extended, 1 free)
   e   extended
Select (default e): p
已选择分区 4
起始 扇区 (31459328-41943039，默认为 31459328)：
将使用默认值 31459328
Last 扇区, +扇区 or +size{K,M,G} (31459328-41943039，默认为 41943039)：
将使用默认值 41943039
分区 4 已设置为 Linux 类型，大小设为 5 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux
/dev/sdb4        31459328    41943039     5241856   83  Linux

命令(输入 m 获取帮助)：n
If you want to create more than four partitions, you must replace a
primary partition with an extended partition first.

命令(输入 m 获取帮助)：d
分区号 (1-4，默认 4)：4
分区 4 已删除

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (3 primary, 0 extended, 1 free)
   e   extended
Select (default e): e
已选择分区 4
起始 扇区 (31459328-41943039，默认为 31459328)：
将使用默认值 31459328
Last 扇区, +扇区 or +size{K,M,G} (31459328-41943039，默认为 41943039)：
将使用默认值 41943039
分区 4 已设置为 Extended 类型，大小设为 5 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux
/dev/sdb4        31459328    41943039     5241856    5  Extended

命令(输入 m 获取帮助)：n
All primary partitions are in use
添加逻辑分区 5
起始 扇区 (31461376-41943039，默认为 31461376)：   
将使用默认值 31461376
Last 扇区, +扇区 or +size{K,M,G} (31461376-41943039，默认为 41943039)：+4G
分区 5 已设置为 Linux 类型，大小设为 4 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux
/dev/sdb4        31459328    41943039     5241856    5  Extended
/dev/sdb5        31461376    39849983     4194304   83  Linux

命令(输入 m 获取帮助)：n
All primary partitions are in use
添加逻辑分区 6
起始 扇区 (39852032-41943039，默认为 39852032)：
将使用默认值 39852032
Last 扇区, +扇区 or +size{K,M,G} (39852032-41943039，默认为 41943039)：+1G
值超出范围。
Last 扇区, +扇区 or +size{K,M,G} (39852032-41943039，默认为 41943039)：+100M       
分区 6 已设置为 Linux 类型，大小设为 100 MiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux
/dev/sdb4        31459328    41943039     5241856    5  Extended
/dev/sdb5        31461376    39849983     4194304   83  Linux
/dev/sdb6        39852032    40056831      102400   83  Linux

```


```
   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048     4196351     2097152   83  Linux
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux
/dev/sdb4        31459328    41943039     5241856    5  Extended
/dev/sdb5        31461376    39849983     4194304   83  Linux
/dev/sdb6        39852032    40056831      102400   83  Linux

命令(输入 m 获取帮助)：d
分区号 (1-6，默认 6)：1
分区 1 已删除

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux
/dev/sdb4        31459328    41943039     5241856    5  Extended
/dev/sdb5        31461376    39849983     4194304   83  Linux
/dev/sdb6        39852032    40056831      102400   83  Linux

命令(输入 m 获取帮助)：d 
分区号 (2-6，默认 6)：5
分区 5 已删除

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0xc2b902ea

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb2         4196352    14682111     5242880   83  Linux
/dev/sdb3        14682112    31459327     8388608   83  Linux
/dev/sdb4        31459328    41943039     5241856    5  Extended
/dev/sdb5        39852032    40056831      102400   83  Linux

命令(输入 m 获取帮助)：
```



```
命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x21ba2d47

   设备 Boot      Start         End      Blocks   Id  System

命令(输入 m 获取帮助)：n     
Partition type:
   p   primary (0 primary, 0 extended, 4 free)
   e   extended
Select (default p): e
分区号 (1-4，默认 1)：
起始 扇区 (2048-41943039，默认为 2048)：
将使用默认值 2048
Last 扇区, +扇区 or +size{K,M,G} (2048-41943039，默认为 41943039)：+5
G 分区 1 已设置为 Extended 类型，大小设为 5 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x21ba2d47

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    10487807     5242880    5  Extended

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (0 primary, 1 extended, 3 free)
   l   logical (numbered from 5)
Select (default p): p
分区号 (2-4，默认 2)：3
起始 扇区 (10487808-41943039，默认为 10487808)：
将使用默认值 10487808
Last 扇区, +扇区 or +size{K,M,G} (10487808-41943039，默认为 41943039)
：+3G  分区 3 已设置为 Linux 类型，大小设为 3 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x21ba2d47

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    10487807     5242880    5  Extended
/dev/sdb3        10487808    16779263     3145728   83  Linux

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (1 primary, 1 extended, 2 free)
   l   logical (numbered from 5)
Select (default p): l
添加逻辑分区 5
起始 扇区 (4096-10487807，默认为 4096)：
将使用默认值 4096
Last 扇区, +扇区 or +size{K,M,G} (4096-10487807，默认为 10487807)：+1
G分区 5 已设置为 Linux 类型，大小设为 1 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x21ba2d47

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    10487807     5242880    5  Extended
/dev/sdb3        10487808    16779263     3145728   83  Linux
/dev/sdb5            4096     2101247     1048576   83  Linux

命令(输入 m 获取帮助)：n
Partition type:
   p   primary (1 primary, 1 extended, 2 free)
   l   logical (numbered from 5)
Select (default p): l
添加逻辑分区 6
起始 扇区 (2103296-10487807，默认为 2103296)：
将使用默认值 2103296
Last 扇区, +扇区 or +size{K,M,G} (2103296-10487807，默认为 10487807)
：+2G分区 6 已设置为 Linux 类型，大小设为 2 GiB

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x21ba2d47

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    10487807     5242880    5  Extended
/dev/sdb3        10487808    16779263     3145728   83  Linux
/dev/sdb5            4096     2101247     1048576   83  Linux
/dev/sdb6         2103296     6297599     2097152   83  Linux

命令(输入 m 获取帮助)：d  
分区号 (1,3,5,6，默认 6)：5
分区 5 已删除

命令(输入 m 获取帮助)：p

磁盘 /dev/sdb：21.5 GB, 21474836480 字节，41943040 个扇区
Units = 扇区 of 1 * 512 = 512 bytes
扇区大小(逻辑/物理)：512 字节 / 512 字节
I/O 大小(最小/最佳)：512 字节 / 512 字节
磁盘标签类型：dos
磁盘标识符：0x21ba2d47

   设备 Boot      Start         End      Blocks   Id  System
/dev/sdb1            2048    10487807     5242880    5  Extended
/dev/sdb3        10487808    16779263     3145728   83  Linux
/dev/sdb5         2103296     6297599     2097152   83  Linux


```
真正写数据的是逻辑分区，而不是扩分区

## 扩展学习 

parted分区gpt格式 http://www.apelearn.com/bbs/thread-7243-1-1.html


```
我们在课上讲的fdisk分区工具，它的分区格式为MBR，特点是，最多分4个主分区，磁盘大小不能超过2T。而GPT分区格式，突破了这些限制，它没有主分区、扩展分区、逻辑分区之分，在一块磁盘上最多可以分128个分区出来，支持大于2T的分区，最大卷可达18EB。 相信，随着存储级别的升级，将来的分区格式逐渐会淘汰MBR，而GPT成为主流。

parted 工具常用功能：
当在命令行输入parted后，进入parted命令的交互模式。输入help会显示帮助信息。下面就简单介绍一下常用的功能
1、check 简单检查文件系统。建议用其他命令检查文件系统，比如fsck
2、help 显示帮助信息
3、mklabel 创建分区表， 即是使用msdos（MBR）还是使用gpt，或者是其他方式分区表
4、 mkfs 创建文件系统。该命令不支持ext3 格式，因此建议不使用，最好是用parted分好区，然后退出parted交互模式，用其他命令进行分区，比如：mkfs.ext3
5、mkpart 创建新分区。
格式：mkpart PART-TYPE  [FS-TYPE]  START  END
PART-TYPE 类型主要有primary（主分区）, extended（扩展分区）, logical（逻辑区）. 扩展分区和逻辑分区只对msdos。
fs-type   文件系统类型，主要有fs32，NTFS，ext2，ext3等
start end 分区的起始和结束位置。
6、mkpartfs 建立分区及其文件系统。目前还不支持ext3文件系统，因此不建议使用该功能。最后是分好区后，退出parted，然后用其他命令建立文件系统。
7、print 输出分区信息。该功能有3个选项，
free 显示该盘的所有信息，并显示磁盘剩余空间
number 显示指定的分区的信息
all 显示所有磁盘信息
8、resize 调整指定的分区的大小。目前对ext3格式支持不是很好，所以不建议使用该功能。
9、rescue 恢复不小心删除的分区。如果不小心用parted的rm命令删除了一个分区，那么可以通过rescue功能进行恢复。恢复时需要给出分区的起始和结束的位置。然后parted就会在给定的范围内去寻找，并提示恢复分区。
10、rm 删除分区。命令格式 rm  number 。如：rm 3 就是将编号为3的分区删除
11、select 选择设备。当输入parted命令后直接回车进入交互模式是，如果有多块硬盘，需要用select 选择要操作的硬盘。如：select /dev/sdb
12、set 设置标记。更改指定分区编号的标志。标志通常有如下几种：boot  hidden   raid   lvm 等。
boot 为引导分区，hidden 为隐藏分区，raid 软raid，lvm 为逻辑分区。
如：set 3  boot  on   设置分区号3 为启动分区
注：以上内容为parted常用的功能，由于该工具目前对ext3支持得不是很好，因此有些功能无法应用，比如move（移动分区）和resize等。

parted分区功能事例。
1、用命令模式 为/dev/sdb创建gpt类型文件分区表,并分500G分区。然后为该分区创建ext3文件系统。并将该分区挂载在/test文件夹下。
#  parted  /dev/sdb  mklabel     —创建分区表
#  parted  /dev/sdb  mkpart  ext3  0  500000    —创建500G分区/dev/sdb1
# mkfs.ext3  /dev/sdb1      —-将分区/dev/sdb1格式化成ext3格式文件系统
# mount  /dev/sdb1 /test   —将/dev/sdb1 挂载在/test下
如果让系统自动挂载/dev/sdb1 需手工编辑/etc/fstab文件。并在文件末尾添加如下内容：
/dev/sdb1             /test                ext3    defaults        0 0
2、创建大小为4G的交互分区。由于已经创建了500G的/dev/sdb1 ,因此再创建的分区为/dev/sdb2
# parted /dev/sdb mkpart swap 500000 504000 —创建4G分区/dev/sdb2
# mkswap  /dev/sdb2   —-将/dev/sdb2创建为交换分区
# swapon /dev/sdb2   —-激活/dev/sdb2
如果让系统自动挂载/dev/sdb2这个交换分区，需手工编辑/etc/fstab文件。并在文件末尾添加如下内容：
/dev/sdb2             swap                swap    defaults        0 0
3、恢复被误删除的分区(也可以参考testdisk命令)。由于parted直接写磁盘，因此一旦不小心删除了某一分区，建议立即用rescue恢复。下面通过事例来理解恢复过程。
# parted /dev/sdb mkpart ext3 504000 514000 —-创建10G分区/dev/sdb3
# mkfs.ext3 /dev/sdb3  —将/dev/sdb3格式化成ext3文件系统。
# parted /dev/sdb rm 3 —-删除/dev/sdb3
# parted /dev/sdb rescue 504000 514000    —依照屏幕提示，输入yes即可恢复被误删除分区
```


## parted分区实例

命令分四个区并格式化挂载：

```
[root@fxq-1 ~]# 
[root@fxq-1 ~]# 
[root@fxq-1 ~]# 
[root@fxq-1 ~]# fdisk -l

Disk /dev/sda: 32.2 GB, 32212254720 bytes, 62914560 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x0004f0b3

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      411647      204800   83  Linux
/dev/sda2          411648     4605951     2097152   82  Linux swap / Solaris
/dev/sda3         4605952    62914559    29154304   83  Linux
WARNING: fdisk GPT support is currently new, and therefore in an experimental phase. Use at your own discretion.

Disk /dev/sdb: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: gpt


#         Start          End    Size  Type            Name



[root@fxq-1 ~]# parted /dev/sdb 
GNU Parted 3.1
Using /dev/sdb
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted)                                                                  
align-check  disk_toggle  mklabel      mktable      print        rescue       select       toggle       version      
disk_set     help         mkpart       name         quit         rm           set          unit         
(parted) mklabel gpt
Warning: The existing disk label on /dev/sdb will be destroyed and all data on this disk will be lost. Do you want to continue?
Yes/No? yes                                                               
(parted) print                                                            
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sdb: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags: 

Number  Start  End  Size  File system  Name  Flags

(parted) mkpart p1                                                        
File system type?  [ext2]? ext4                                           
Start? 1                                                                  
End? 5G                                                                   
(parted) print                                                            
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sdb: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags: 

Number  Start   End     Size    File system  Name  Flags
 1      1049kB  5000MB  4999MB  ext4         p1

(parted) mkpart p2                                                        
File system type?  [ext2]? ext4                                           
Start? 5G                                                                 
End? 8G                                                                   
(parted) mkpart p3                                                        
File system type?  [ext2]? ext4                                           
Start? 8G                                                                 
End? 10G                                                                  
(parted) print                                                            
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sdb: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags: 

Number  Start   End     Size    File system  Name  Flags
 1      1049kB  5000MB  4999MB  ext4         p1
 2      5000MB  8000MB  3000MB  ext4         p2
 3      8000MB  10.0GB  2001MB               p3

(parted) mkpart p4                                                        
File system type?  [ext2]? ext4                                           
Start? 10G                                                                
End? 100%                                                                 

(parted) print                                                            
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sdb: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags: 

Number  Start   End     Size    File system  Name  Flags
 1      1049kB  5000MB  4999MB  ext4         p1
 2      5000MB  8000MB  3000MB  ext4         p2
 3      8000MB  10.0GB  2001MB               p3
 4      10.0GB  21.5GB  11.5GB               p4

(parted) quit                                                            
Information: You may need to update /etc/fstab.




用fdisk命令查看：

[root@fxq-1 ~]# fdisk -l

Disk /dev/sda: 32.2 GB, 32212254720 bytes, 62914560 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: dos
Disk identifier: 0x0004f0b3

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1   *        2048      411647      204800   83  Linux
/dev/sda2          411648     4605951     2097152   82  Linux swap / Solaris
/dev/sda3         4605952    62914559    29154304   83  Linux
WARNING: fdisk GPT support is currently new, and therefore in an experimental phase. Use at your own discretion.

Disk /dev/sdb: 21.5 GB, 21474836480 bytes, 41943040 sectors
Units = sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disk label type: gpt


#         Start          End    Size  Type            Name
 1         2048      9764863    4.7G  Microsoft basic p1
 2      9764864     15624191    2.8G  Microsoft basic p2
 3     15624192     19531775    1.9G  Microsoft basic p3
 4     19531776     41940991   10.7G  Microsoft basic p4


格式化4个分区:

[root@fxq-1 ~]# mkfs.ext4 /dev/sdb1
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
305216 inodes, 1220352 blocks
61017 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=1249902592
38 block groups
32768 blocks per group, 32768 fragments per group
8032 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done 

[root@fxq-1 ~]# mkfs.ext4 /dev/sdb2
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
183264 inodes, 732416 blocks
36620 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=750780416
23 block groups
32768 blocks per group, 32768 fragments per group
7968 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
\Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done 

[root@fxq-1 ~]# mkfs.ext4 /dev/sdb3
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
122160 inodes, 488448 blocks
24422 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=501219328
15 block groups
32768 blocks per group, 32768 fragments per group
8144 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done 

[root@fxq-1 ~]# mkfs.ext4 /dev/sdb4
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
700384 inodes, 2801152 blocks
140057 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=2151677952
86 block groups
32768 blocks per group, 32768 fragments per group
8144 inodes per group
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done 



挂载：

[root@fxq-1 ~]# mount /dev/sdb4 /mnt
[root@fxq-1 ~]# df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda3        28G  960M   27G   4% /
devtmpfs        240M     0  240M   0% /dev
tmpfs           248M     0  248M   0% /dev/shm
tmpfs           248M  4.6M  244M   2% /run
tmpfs           248M     0  248M   0% /sys/fs/cgroup
/dev/sda1       197M  105M   93M  54% /boot
tmpfs            50M     0   50M   0% /run/user/0
/dev/sdb4        11G   41M  9.9G   1% /mnt
[root@fxq-1 ~]#
```
