[toc]
## 12.6 Nginx安装

```
    
安装过程：  
   33 cd /usr/local/src
   35  tar -zxvf nginx-1.12.2.tar.gz 
   36  cd nginx-1.12.2/
   43  cd /etc/yum.repos.d/
   47  mkdir /data/yum.bak
   48  mv * /data/yum.bak/
   50   wget -P /etc/yum.repos.d/ http://mirrors.aliyun.com/repo/epel-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
   53  yum install epel-release
   54  yum install pcre-devel
   55  cd /usr/local/src/nginx-1.12.2/
   57  ./configure --prefix=/usr/local/nginx/ --with-http_ssl_module
   58  yum install openssl-devel
   59  ./configure --prefix=/usr/local/nginx/ --with-http_ssl_module
   61  make && make install
   65  vim /etc/init.d/nginx
   66  cd /usr/local/nginx/conf/
   68  mv nginx.conf nginx.conf.bak
   69  vim nginx.conf
   72  chmod 755 /etc/init.d/nginx 
   73  chkconfig --add nginx
   74  chkconfig nginx on
   75  /usr/local/nginx/sbin/nginx -t
   76  /etc/init.d/nginx start
   77  netstat -lntp
   78  curl localhost
```




### （1）下载解压编译安装nginx:
```
 cd /usr/local/src
 wget http://nginx.org/download/nginx-1.12.2.tar.gz
 tar zxf nginx-1.12.2.tar.gz
 yum install pcre-devel openssl-devel
 ./configure --prefix=/usr/local/nginx --with-http_ssl_module
 make &&  make install
 
 --with-http_ssl_module :是开启nginx支持https
 ```
 ### (2) 编辑启动脚本：
 ```
 vim /etc/init.d/nginx //复制如下内容（参考https://coding.net/u/aminglinux/p/aminglinux-book/git/blob/master/D15Z/etc_init.d_nginx ）

```
如下脚本：
```
        #!/bin/bash
        # chkconfig: - 30 21
        # description: http service.
        # Source Function Library
        . /etc/init.d/functions
        # Nginx Settings
        NGINX_SBIN="/usr/local/nginx/sbin/nginx"
        NGINX_CONF="/usr/local/nginx/conf/nginx.conf"
        NGINX_PID="/usr/local/nginx/logs/nginx.pid"
        RETVAL=0
        prog="Nginx"
        start() 
        {
            echo -n $"Starting $prog: "
            mkdir -p /dev/shm/nginx_temp
            daemon $NGINX_SBIN -c $NGINX_CONF
            RETVAL=$?
            echo
            return $RETVAL
        }
        stop() 
        {
            echo -n $"Stopping $prog: "
            killproc -p $NGINX_PID $NGINX_SBIN -TERM
            rm -rf /dev/shm/nginx_temp
            RETVAL=$?
            echo
            return $RETVAL
        }
        reload()
        {
            echo -n $"Reloading $prog: "
            killproc -p $NGINX_PID $NGINX_SBIN -HUP
            RETVAL=$?
            echo
            return $RETVAL
        }
        restart()
        {
            stop
            start
        }
        configtest()
        {
            $NGINX_SBIN -c $NGINX_CONF -t
            return 0
        }
        case "$1" in
          start)
                start
                ;;
          stop)
                stop
                ;;
          reload)
                reload
                ;;
          restart)
                restart
                ;;
          configtest)
                configtest
                ;;
          *)
                echo $"Usage: $0 {start|stop|reload|restart|configtest}"
                RETVAL=1
        esac
        exit $RETVAL
```
### （3）编辑配置文件
```
 chmod 755 /etc/init.d/nginx
 chkconfig --add nginx 
 chkconfig nginx on 
 cd /usr/local/nginx/conf/; mv nginx.conf nginx.conf.bak
 vim nginx.conf //写入如下内容（参考https://coding.net/u/aminglinux/p/aminglinux-book/git/blob/master/D15Z/nginx.conf）

```

vim /usr/local/nginx/conf/nginx.conf

```
    user nobody nobody;
    worker_processes 2;
    error_log /usr/local/nginx/logs/nginx_error.log crit;
    pid /usr/local/nginx/logs/nginx.pid;
    worker_rlimit_nofile 51200;
    events
    {
        use epoll;
        worker_connections 6000;
    }
    http
    {
        include mime.types;
        default_type application/octet-stream;
        server_names_hash_bucket_size 3526;
        server_names_hash_max_size 4096;
        log_format combined_realip '$remote_addr $http_x_forwarded_for [$time_local]'
        ' $host "$request_uri" $status'
        ' "$http_referer" "$http_user_agent"';
        sendfile on;
        tcp_nopush on;
        keepalive_timeout 30;
        client_header_timeout 3m;
        client_body_timeout 3m;
        send_timeout 3m;
        connection_pool_size 256;
        client_header_buffer_size 1k;
        large_client_header_buffers 8 4k;
        request_pool_size 4k;
        output_buffers 4 32k;
        postpone_output 1460;
        client_max_body_size 10m;
        client_body_buffer_size 256k;
        client_body_temp_path /usr/local/nginx/client_body_temp;
        proxy_temp_path /usr/local/nginx/proxy_temp;
        fastcgi_temp_path /usr/local/nginx/fastcgi_temp;
        fastcgi_intercept_errors on;
        tcp_nodelay on;
        gzip on;
        gzip_min_length 1k;
        gzip_buffers 4 8k;
        gzip_comp_level 5;
        gzip_http_version 1.1;
        gzip_types text/plain application/x-javascript text/css text/htm 
        application/xml;
        server
        {
            listen 80;
            server_name localhost;
            index index.html index.htm index.php;
            root /usr/local/nginx/html;
            location ~ \.php$ 
            {
                include fastcgi_params;
                fastcgi_pass unix:/tmp/php-fcgi.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME /usr/local/nginx/html$fastcgi_script_name;
            }    
        }
    }
```

### （4）测试
```
/usr/local/nginx/sbin/nginx -t
 /etc/init.d/nginx  start

ps aux | grep nginx
netstat -lntp | grep 80

vim /usr/local/nginx/html/2.php
    <?php
    echo "This is Test Nginx Page!!"
    ?>


[root@fxq-1 nginx-1.12.2]# curl -x127.0.0.1:80 192.168.101.131/2.php
This is Test Nginx Page!![root@fxq-1 nginx-1.12.2]#
```
## 12.7 默认虚拟主机

### (1)修改主配置文件：nginx.conf

```

vim /usr/local/nginx/conf/nginx.conf //增加
 include vhost/*.conf
```
### (2) 修改配置文件：
```
 
 mkdir /usr/local/nginx/conf/vhost
 cd !$;  vim default.conf //加入如下内容
server
{
    listen 80 default_server;  // 有这个标记的就是默认虚拟主机
    server_name aaa.com;
    index index.html index.htm index.php;
    root /data/wwwroot/default;
}
 mkdir -p /data/wwwroot/default/
 echo “This is a default site.”>/data/wwwroot/default/index.html
 
 
 ```
 ### (3) 重启服务：
 ```
 /usr/local/nginx/sbin/nginx -t
 /usr/local/nginx/sbin/nginx -s reload
 
```
### (4) 测试
```
 
 curl localhost
 curl -x127.0.0.1:80 aaa.com

```

## 12.8 Nginx用户认证
```
 vim /usr/local/nginx/conf/vhost/test.com.conf//写入如下内容
1.全网站：
server
{
    listen 80;
    server_name www.test.com;
    index index.html index.htm index.php;
    root /data/wwwroot/test.com;
    
location  /
    {
        auth_basic              "Auth";
        auth_basic_user_file   /usr/local/nginx/conf/htpasswd;
}
}

2.admin目录：
server
{
    listen 80;
    server_name www.test.com;
    index index.html index.htm index.php;
    root /data/wwwroot/test.com;
    
location  /admin/
    {
        auth_basic              "Auth";
        auth_basic_user_file   /usr/local/nginx/conf/htpasswd;
}
}


3.admin.php文件
server
{
    listen 80;
    server_name www.test.com;
    index index.html index.htm index.php;
    root /data/wwwroot/test.com;
    
location  ~ admin.php
    {
        auth_basic              "Auth";
        auth_basic_user_file   /usr/local/nginx/conf/htpasswd;
}
}



 yum install -y httpd
 htpasswd -c /usr/local/nginx/conf/htpasswd aming
 -t &&  -s reload //测试配置并重新加载



mkdir /data/wwwroot/test.com
 echo “test.com”>/data/wwwroot/test.com/index.html
 curl -x127.0.0.1:80 test.com -I//状态码为401说明需要验证
 curl -uaming:passwd 访问状态码变为200
 编辑windows的hosts文件，然后在浏览器中访问test.com会有输入用户、密码的弹窗
 针对目录的用户认证
location  /admin/
    {
        auth_basic              "Auth";
        auth_basic_user_file   /usr/local/nginx/conf/htpasswd;

```

## 12.9 Nginx域名重定向

```
更改test.com.conf
server
{
    listen 80;
    server_name www.test.com www.test1.com www.test2.com;
    index index.html index.htm index.php;
    root /data/wwwroot/test.com;
    if ($host != 'www.test.com' ) {
        rewrite  ^/(.*)$  http://www.test.com/$1  permanent;
    }
}
 server_name后面支持写多个域名，这里要和httpd的做一个对比
 permanent为永久重定向，状态码为301，如果写redirect则为302

测试：

[root@fxq-1 vhost]# curl -x127.0.0.1:80 -I www.test1.com/
HTTP/1.1 301 Moved Permanently
Server: nginx/1.12.2
Date: Thu, 19 Oct 2017 14:18:59 GMT
Content-Type: text/html
Content-Length: 185
Connection: keep-alive
Location: http://test.com/

```


## 扩展
nginx.conf 配置详解 http://www.ha97.com/5194.html

```
        PS：Nginx使用有两三年了，现在经常碰到有新用户问一些很基本的问题，我也没时间一一回答，今天下午花了点时间，结合自己的使用经验，把Nginx的主要配置参数说明分享一下，也参考了一些网络的内容，这篇是目前最完整的Nginx配置参数中文说明了。更详细的模块参数请参考：http://wiki.nginx.org/Main
        
        
        #定义Nginx运行的用户和用户组
        user www www;
        
        #nginx进程数，建议设置为等于CPU总核心数。
        worker_processes 8;
        
        #全局错误日志定义类型，[ debug | info | notice | warn | error | crit ]
        error_log /var/log/nginx/error.log info;
        
        #进程文件
        pid /var/run/nginx.pid;
        
        #一个nginx进程打开的最多文件描述符数目，理论值应该是最多打开文件数（系统的值ulimit -n）与nginx进程数相除，但是nginx分配请求并不均匀，所以建议与ulimit -n的值保持一致。
        worker_rlimit_nofile 65535;
        
        #工作模式与连接数上限
        events
        {
        #参考事件模型，use [ kqueue | rtsig | epoll | /dev/poll | select | poll ]; epoll模型是Linux 2.6以上版本内核中的高性能网络I/O模型，如果跑在FreeBSD上面，就用kqueue模型。
        use epoll;
        #单个进程最大连接数（最大连接数=连接数*进程数）
        worker_connections 65535;
        }
        
        #设定http服务器
        http
        {
        include mime.types; #文件扩展名与文件类型映射表
        default_type application/octet-stream; #默认文件类型
        #charset utf-8; #默认编码
        server_names_hash_bucket_size 128; #服务器名字的hash表大小
        client_header_buffer_size 32k; #上传文件大小限制
        large_client_header_buffers 4 64k; #设定请求缓
        client_max_body_size 8m; #设定请求缓
        sendfile on; #开启高效文件传输模式，sendfile指令指定nginx是否调用sendfile函数来输出文件，对于普通应用设为 on，如果用来进行下载等应用磁盘IO重负载应用，可设置为off，以平衡磁盘与网络I/O处理速度，降低系统的负载。注意：如果图片显示不正常把这个改成off。
        autoindex on; #开启目录列表访问，合适下载服务器，默认关闭。
        tcp_nopush on; #防止网络阻塞
        tcp_nodelay on; #防止网络阻塞
        keepalive_timeout 120; #长连接超时时间，单位是秒
        
        #FastCGI相关参数是为了改善网站的性能：减少资源占用，提高访问速度。下面参数看字面意思都能理解。
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
        fastcgi_buffer_size 64k;
        fastcgi_buffers 4 64k;
        fastcgi_busy_buffers_size 128k;
        fastcgi_temp_file_write_size 128k;
        
        #gzip模块设置
        gzip on; #开启gzip压缩输出
        gzip_min_length 1k; #最小压缩文件大小
        gzip_buffers 4 16k; #压缩缓冲区
        gzip_http_version 1.0; #压缩版本（默认1.1，前端如果是squid2.5请使用1.0）
        gzip_comp_level 2; #压缩等级
        gzip_types text/plain application/x-javascript text/css application/xml;
        #压缩类型，默认就已经包含text/html，所以下面就不用再写了，写上去也不会有问题，但是会有一个warn。
        gzip_vary on;
        #limit_zone crawler $binary_remote_addr 10m; #开启限制IP连接数的时候需要使用
        
        upstream blog.ha97.com {
        #upstream的负载均衡，weight是权重，可以根据机器配置定义权重。weigth参数表示权值，权值越高被分配到的几率越大。
        server 192.168.80.121:80 weight=3;
        server 192.168.80.122:80 weight=2;
        server 192.168.80.123:80 weight=3;
        }
        
        #虚拟主机的配置
        server
        {
        #监听端口
        listen 80;
        #域名可以有多个，用空格隔开
        server_name www.ha97.com ha97.com;
        index index.html index.htm index.php;
        root /data/www/ha97;
        location ~ .*\.(php|php5)?$
        {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        include fastcgi.conf;
        }
        #图片缓存时间设置
        location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
        {
        expires 10d;
        }
        #JS和CSS缓存时间设置
        location ~ .*\.(js|css)?$
        {
        expires 1h;
        }
        #日志格式设定
        log_format access '$remote_addr - $remote_user [$time_local] "$request" '
        '$status $body_bytes_sent "$http_referer" '
        '"$http_user_agent" $http_x_forwarded_for';
        #定义本虚拟主机的访问日志
        access_log /var/log/nginx/ha97access.log access;
        
        #对 "/" 启用反向代理
        location / {
        proxy_pass http://127.0.0.1:88;
        proxy_redirect off;
        proxy_set_header X-Real-IP $remote_addr;
        #后端的Web服务器可以通过X-Forwarded-For获取用户真实IP
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        #以下是一些反向代理的配置，可选。
        proxy_set_header Host $host;
        client_max_body_size 10m; #允许客户端请求的最大单文件字节数
        client_body_buffer_size 128k; #缓冲区代理缓冲用户端请求的最大字节数，
        proxy_connect_timeout 90; #nginx跟后端服务器连接超时时间(代理连接超时)
        proxy_send_timeout 90; #后端服务器数据回传时间(代理发送超时)
        proxy_read_timeout 90; #连接成功后，后端服务器响应时间(代理接收超时)
        proxy_buffer_size 4k; #设置代理服务器（nginx）保存用户头信息的缓冲区大小
        proxy_buffers 4 32k; #proxy_buffers缓冲区，网页平均在32k以下的设置
        proxy_busy_buffers_size 64k; #高负荷下缓冲大小（proxy_buffers*2）
        proxy_temp_file_write_size 64k;
        #设定缓存文件夹大小，大于这个值，将从upstream服务器传
        }
        
        #设定查看Nginx状态的地址
        location /NginxStatus {
        stub_status on;
        access_log on;
        auth_basic "NginxStatus";
        auth_basic_user_file conf/htpasswd;
        #htpasswd文件的内容可以用apache提供的htpasswd工具来产生。
        }
        
        #本地动静分离反向代理配置
        #所有jsp的页面均交由tomcat或resin处理
        location ~ .(jsp|jspx|do)?$ {
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8080;
        }
        #所有静态文件由nginx直接读取不经过tomcat或resin
        location ~ .*.(htm|html|gif|jpg|jpeg|png|bmp|swf|ioc|rar|zip|txt|flv|mid|doc|ppt|pdf|xls|mp3|wma)$
        { expires 15d; }
        location ~ .*.(js|css)?$
        { expires 1h; }
        }
        }

```

http://my.oschina.net/duxuefeng/blog/34880

```
        Nginx 配置文件详解
        
        
        
        user nginx ;
        
        #用户
        
        
        
        worker_processes 8;
        
        #工作进程，根据硬件调整，大于等于cpu核数
        
        
        
        error_log logs/nginx_error.log crit;
        
        #错误日志
        
        
        
        pid logs/nginx.pid;
        
        #pid放置的位置
        
        
        
        worker_rlimit_nofile 204800;
        
        #指定进程可以打开的最大描述符
        
        这个指令是指当一个nginx进程打开的最多文件描述符数目，理论值应该是最多打开文
        
        件数（ulimit -n）与nginx进程数相除，但是nginx分配请求并不是那么均匀，所以最好与ulimit -n 的值保持一致。
        
        现在在linux 2.6内核下开启文件打开数为65535，worker_rlimit_nofile就相应应该填写65535。
        
        这是因为nginx调度时分配请求到进程并不是那么的均衡，所以假如填写10240，总并发量达到3-4万时就有进程可能超过10240了，这时会返回502错误。
        
        
        
        events
        
        
        
        {
        
        use epoll;
        
        #使用epoll的I/O 模型
        
        补充说明:
        
        与apache相类，nginx针对不同的操作系统，有不同的事件模型
        
        A）标准事件模型
        
        Select、poll属于标准事件模型，如果当前系统不存在更有效的方法，nginx会选择select或poll
        
        B）高效事件模型
        
        Kqueue：使用于FreeBSD 4.1+, OpenBSD 2.9+, NetBSD 2.0 和 MacOS X.使用双处理器的MacOS X系统使用kqueue可能会造成内核崩溃。
        
        Epoll:使用于Linux内核2.6版本及以后的系统。
        
        /dev/poll：使用于Solaris 7 11/99+, HP/UX 11.22+ (eventport), IRIX 6.5.15+ 和 Tru64 UNIX 5.1A+。
        
        Eventport：使用于Solaris 10. 为了防止出现内核崩溃的问题， 有必要安装安全补丁
        
        
        
        
        
        worker_connections 204800;
        
        #工作进程的最大连接数量，根据硬件调整，和前面工作进程配合起来用，尽量大，但是别把cpu跑到100%就行
        
        每个进程允许的最多连接数， 理论上每台nginx服务器的最大连接数为worker_processes*worker_connections
        
        
        
        keepalive_timeout 60;
        
        
        
        keepalive超时时间。
        
        
        
        client_header_buffer_size 4k;
        
        
        
        客户端请求头部的缓冲区大小，这个可以根据你的系统分页大小来设置，一般一个请求头的大小不会超过1k，不过由于一般系统分页都要大于1k，所以这里设置为分页大小。
        
        分页大小可以用命令getconf PAGESIZE 取得。
        
        [root@web001 ~]# getconf PAGESIZE
        
        4096
        
        但也有client_header_buffer_size超过4k的情况，但是client_header_buffer_size该值必须设置为“系统分页大小”的整倍数。
        
        
        
        open_file_cache max=65535 inactive=60s;
        
        
        
        这个将为打开文件指定缓存，默认是没有启用的，max指定缓存数量，建议和打开文件数一致，inactive是指经过多长时间文件没被请求后删除缓存。
        
        
        
        open_file_cache_valid 80s;
        
        
        
        这个是指多长时间检查一次缓存的有效信息。
        
        
        
        open_file_cache_min_uses 1;
        
        
        
        open_file_cache指令中的inactive参数时间内文件的最少使用次数，如果超过这个数字，文件描述符一直是在缓存中打开的，如上例，如果有一个文件在inactive时间内一次没被使用，它将被移除。
        
        
        
        
        
        }
        
        
        
        #设定http服务器，利用它的反向代理功能提供负载均衡支持
        
        http
        
        {
        
        include mime.types;
        
        #设定mime类型,类型由mime.type文件定义
        
        default_type application/octet-stream;
        
        log_format main '$host $status [$time_local] $remote_addr [$time_local] $request_uri '
        
        '"$http_referer" "$http_user_agent" "$http_x_forwarded_for" '
        
        '$bytes_sent $request_time $sent_http_x_cache_hit';
        
        log_format log404 '$status [$time_local] $remote_addr $host$request_uri $sent_http_location';
        
        $remote_addr与$http_x_forwarded_for用以记录客户端的ip地址；
        
        $remote_user：用来记录客户端用户名称；
        
        $time_local： 用来记录访问时间与时区；
        
        $request： 用来记录请求的url与http协议；
        
        $status： 用来记录请求状态；成功是200，
        
        $body_bytes_s ent ：记录发送给客户端文件主体内容大小；
        
        $http_referer：用来记录从那个页面链接访问过来的；
        
        $http_user_agent：记录客户毒啊浏览器的相关信息；
        
        通常web服务器放在反向代理的后面，这样就不能获取到客户的IP地址了，通过$remote_add拿到的IP地址是反向代理服务器的iP地址。反向代理服务器在转发请求的http头信息中，可以增加x_forwarded_for信息，用以记录原有客户端的IP地址和原来客户端的请求的服务器地址；
        
        access_log /dev/null;
        
        #用了log_format指令设置了日志格式之后，需要用access_log指令指定日志文件的存放路径；
        
        # access_log /usr/local/nginx/logs/access_log main;
        
        server_names_hash_bucket_size 128;
        
        #保存服务器名字的hash表是由指令server_names_hash_max_size 和server_names_hash_bucket_size所控制的。参数hash bucket size总是等于hash表的大小，并且是一路处理器缓存大小的倍数。在减少了在内存中的存取次数后，使在处理器中加速查找hash表键值成为可能。如果hash bucket size等于一路处理器缓存的大小，那么在查找键的时候，最坏的情况下在内存中查找的次数为2。第一次是确定存储单元的地址，第二次是在存储单元中查找键 值。因此，如果Nginx给出需要增大hash max size 或 hash bucket size的提示，那么首要的是增大前一个参数的大小.
        
        
        
        client_header_buffer_size 4k;
        
        客户端请求头部的缓冲区大小，这个可以根据你的系统分页大小来设置，一般一个请求的头部大小不会超过1k，不过由于一般系统分页都要大于1k，所以这里设置为分页大小。分页大小可以用命令getconf PAGESIZE取得。
        
        
        
        large_client_header_buffers 8 128k;
        
        客户请求头缓冲大小
        nginx默认会用client_header_buffer_size这个buffer来读取header值，如果
        
        header过大，它会使用large_client_header_buffers来读取
        如果设置过小HTTP头/Cookie过大 会报400 错误nginx 400 bad request
        求行如果超过buffer，就会报HTTP 414错误(URI Too Long)
        nginx接受最长的HTTP头部大小必须比其中一个buffer大，否则就会报400的
        
        HTTP错误(Bad Request)。
        
        open_file_cache max 102400
        
        使用字段:http, server, location 这个指令指定缓存是否启用,如果启用,将记录文件以下信息: ·打开的文件描述符,大小信息和修改时间. ·存在的目录信息. ·在搜索文件过程中的错误信息 --没有这个文件,无法正确读取,参考open_file_cache_errors指令选项:
        ·max -指定缓存的最大数目,如果缓存溢出,最长使用过的文件(LRU)将被移除
        例: open_file_cache max=1000 inactive=20s; open_file_cache_valid 30s; open_file_cache_min_uses 2; open_file_cache_errors on;
        
        open_file_cache_errors
        语法:open_file_cache_errors on | off 默认值:open_file_cache_errors off 使用字段:http, server, location 这个指令指定是否在搜索一个文件是记录cache错误.
        
        open_file_cache_min_uses
        
        语法:open_file_cache_min_uses number 默认值:open_file_cache_min_uses 1 使用字段:http, server, location 这个指令指定了在open_file_cache指令无效的参数中一定的时间范围内可以使用的最小文件数,如 果使用更大的值,文件描述符在cache中总是打开状态.
        open_file_cache_valid
        
        语法:open_file_cache_valid time 默认值:open_file_cache_valid 60 使用字段:http, server, location 这个指令指定了何时需要检查open_file_cache中缓存项目的有效信息.
        
        
        
        client_max_body_size 300m;
        
        设定通过nginx上传文件的大小
        
        
        
        sendfile on;
        
        #sendfile指令指定 nginx 是否调用sendfile 函数（zero copy 方式）来输出文件，
        对于普通应用，必须设为on。
        如果用来进行下载等应用磁盘IO重负载应用，可设置为off，以平衡磁盘与网络IO处理速度，降低系统uptime。
        
        tcp_nopush on;
        
        此选项允许或禁止使用socke的TCP_CORK的选项，此选项仅在使用sendfile的时候使用
        
        
        
        proxy_connect_timeout 90; 
        #后端服务器连接的超时时间_发起握手等候响应超时时间
        
        proxy_read_timeout 180;
        
        #连接成功后_等候后端服务器响应时间_其实已经进入后端的排队之中等候处理（也可以说是后端服务器处理请求的时间）
        
        proxy_send_timeout 180;
        
        #后端服务器数据回传时间_就是在规定时间之内后端服务器必须传完所有的数据
        
        proxy_buffer_size 256k;
        
        #设置从被代理服务器读取的第一部分应答的缓冲区大小，通常情况下这部分应答中包含一个小的应答头，默认情况下这个值的大小为指令proxy_buffers中指定的一个缓冲区的大小，不过可以将其设置为更小
        
        proxy_buffers 4 256k;
        
        #设置用于读取应答（来自被代理服务器）的缓冲区数目和大小，默认情况也为分页大小，根据操作系统的不同可能是4k或者8k
        
        proxy_busy_buffers_size 256k;
        
        
        
        proxy_temp_file_write_size 256k;
        
        #设置在写入proxy_temp_path时数据的大小，预防一个工作进程在传递文件时阻塞太长
        
        proxy_temp_path /data0/proxy_temp_dir;
        
        #proxy_temp_path和proxy_cache_path指定的路径必须在同一分区
        proxy_cache_path /data0/proxy_cache_dir levels=1:2 keys_zone=cache_one:200m inactive=1d max_size=30g;
        #设置内存缓存空间大小为200MB，1天没有被访问的内容自动清除，硬盘缓存空间大小为30GB。
        
        
        keepalive_timeout 120;
        
        keepalive超时时间。
        
        tcp_nodelay on;
        
        client_body_buffer_size 512k;
        如果把它设置为比较大的数值，例如256k，那么，无论使用firefox还是IE浏览器，来提交任意小于256k的图片，都很正常。如果注释该指令，使用默认的client_body_buffer_size设置，也就是操作系统页面大小的两倍，8k或者16k，问题就出现了。
        无论使用firefox4.0还是IE8.0，提交一个比较大，200k左右的图片，都返回500 Internal Server Error错误
        
        proxy_intercept_errors on;
        
        表示使nginx阻止HTTP应答代码为400或者更高的应答。
        
        
        
        upstream img_relay {
        
        server 127.0.0.1:8027;
        
        server 127.0.0.1:8028;
        
        server 127.0.0.1:8029;
        
        hash $request_uri;
        
        }
        
        nginx的upstream目前支持4种方式的分配
        
        1、轮询（默认）
        
        每个请求按时间顺序逐一分配到不同的后端服务器，如果后端服务器down掉，能自动剔除。
        
        2、weight
        指定轮询几率，weight和访问比率成正比，用于后端服务器性能不均的情况。
        例如：
        upstream bakend {
        server 192.168.0.14 weight=10;
        server 192.168.0.15 weight=10;
        }
        
        2、ip_hash
        每个请求按访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session的问题。
        例如：
        upstream bakend {
        ip_hash;
        server 192.168.0.14:88;
        server 192.168.0.15:80;
        }
        
        3、fair（第三方）
        按后端服务器的响应时间来分配请求，响应时间短的优先分配。
        upstream backend {
        server server1;
        server server2;
        fair;
        }
        
        4、url_hash（第三方）
        
        按访问url的hash结果来分配请求，使每个url定向到同一个后端服务器，后端服务器为缓存时比较有效。
        
        例：在upstream中加入hash语句，server语句中不能写入weight等其他的参数，hash_method是使用的hash算法
        
        upstream backend {
        server squid1:3128;
        server squid2:3128;
        hash $request_uri;
        hash_method crc32;
        }
        
        tips:
        
        upstream bakend{#定义负载均衡设备的Ip及设备状态
        ip_hash;
        server 127.0.0.1:9090 down;
        server 127.0.0.1:8080 weight=2;
        server 127.0.0.1:6060;
        server 127.0.0.1:7070 backup;
        }
        在需要使用负载均衡的server中增加
        proxy_pass http://bakend/;
        
        每个设备的状态设置为:
        1.down表示单前的server暂时不参与负载
        2.weight默认为1.weight越大，负载的权重就越大。
        3.max_fails：允许请求失败的次数默认为1.当超过最大次数时，返回proxy_next_upstream模块定义的错误
        4.fail_timeout:max_fails次失败后，暂停的时间。
        5.backup： 其它所有的非backup机器down或者忙的时候，请求backup机器。所以这台机器压力会最轻。
        
        nginx支持同时设置多组的负载均衡，用来给不用的server来使用。
        
        client_body_in_file_only设置为On 可以讲client post过来的数据记录到文件中用来做debug
        client_body_temp_path设置记录文件的目录 可以设置最多3层目录
        
        location对URL进行匹配.可以进行重定向或者进行新的代理 负载均衡
        
        
        
        server
        
        #配置虚拟机
        
        {
        
        listen 80;
        
        #配置监听端口
        
        server_name image.***.com;
        
        #配置访问域名
        
        location ~* \.(mp3|exe)$ {
        
        #对以“mp3或exe”结尾的地址进行负载均衡
        
        proxy_pass http://img_relay$request_uri;
        
        #设置被代理服务器的端口或套接字，以及URL
        
        proxy_set_header Host $host;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        #以上三行，目的是将代理服务器收到的用户的信息传到真实服务器上
        
        }
        
        location /face {
        
        if ($http_user_agent ~* "xnp") {
        
        rewrite ^(.*)$ http://211.151.188.190:8080/face.jpg redirect;
        
        }
        
        proxy_pass http://img_relay$request_uri;
        
        proxy_set_header Host $host;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        error_page 404 502 = @fetch;
        
        }
        
        location @fetch {
        
        access_log /data/logs/face.log log404;
        
        #设定本服务器的访问日志
        
        rewrite ^(.*)$ http://211.151.188.190:8080/face.jpg redirect;
        
        }
        
        
        
        location /image {
        
        if ($http_user_agent ~* "xnp") {
        
        rewrite ^(.*)$ http://211.151.188.190:8080/face.jpg redirect;
        
        }
        
        proxy_pass http://img_relay$request_uri;
        
        proxy_set_header Host $host;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        error_page 404 502 = @fetch;
        
        }
        
        location @fetch {
        
        access_log /data/logs/image.log log404;
        
        rewrite ^(.*)$ http://211.151.188.190:8080/face.jpg redirect;
        
        }
        
        }
        
        
        server
        
        {
        
        listen 80;
        
        server_name *.***.com *.***.cn;
        
        
        location ~* \.(mp3|exe)$ {
        
        proxy_pass http://img_relay$request_uri;
        
        proxy_set_header Host $host;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        }
        
        location / {
        
        if ($http_user_agent ~* "xnp") {
        
        rewrite ^(.*)$ http://i1.***img.com/help/noimg.gif redirect;
        
        }
        
        proxy_pass http://img_relay$request_uri;
        
        proxy_set_header Host $host;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        #error_page 404 http://i1.***img.com/help/noimg.gif;
        
        error_page 404 502 = @fetch;
        
        }
        
        location @fetch {
        
        access_log /data/logs/baijiaqi.log log404;
        
        rewrite ^(.*)$ http://i1.***img.com/help/noimg.gif redirect;
        
        }
        
        #access_log off;
        
        }
        
        
        
        server
        
        {
        
        listen 80;
        
        server_name *.***img.com;
        
        
        
        location ~* \.(mp3|exe)$ {
        
        proxy_pass http://img_relay$request_uri;
        
        proxy_set_header Host $host;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        }
        
        
        
        location / {
        
        if ($http_user_agent ~* "xnp") {
        
        rewrite ^(.*)$ http://i1.***img.com/help/noimg.gif;
        
        }
        
        proxy_pass http://img_relay$request_uri;
        
        proxy_set_header Host $host;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        #error_page 404 http://i1.***img.com/help/noimg.gif;
        
        error_page 404 = @fetch;
        
        }
        
        #access_log off;
        
        location @fetch {
        
        access_log /data/logs/baijiaqi.log log404;
        
        rewrite ^(.*)$ http://i1.***img.com/help/noimg.gif redirect;
        
        }
        
        }
        
        
        server
        
        {
        
        listen 8080;
        
        server_name ngx-ha.***img.com;
        
        location / {
        
        stub_status on;
        
        access_log off;
        
        }
        
        }
        
        server {
        
        listen 80;
        
        server_name imgsrc1.***.net;
        
        root html;
        
        }
        
        server {
        
        listen 80;
        
        server_name ***.com w.***.com;
        
        # access_log /usr/local/nginx/logs/access_log main;
        
        location / {
        
        rewrite ^(.*)$ http://www.***.com/ ;
        
        }
        
        }
        
        server {
        
        listen 80;
        
        server_name *******.com w.*******.com;
        
        # access_log /usr/local/nginx/logs/access_log main;
        
        location / {
        
        rewrite ^(.*)$ http://www.*******.com/;
        
        }
        
        }
        
        server {
        
        listen 80;
        
        server_name ******.com;
        
        # access_log /usr/local/nginx/logs/access_log main;
        
        location / {
        
        rewrite ^(.*)$ http://www.******.com/;
        
        }
        
        }
        
        location /NginxStatus {
        stub_status on;
        access_log on;
        auth_basic "NginxStatus";
        auth_basic_user_file conf/htpasswd;
        }
        
        #设定查看Nginx状态的地址
        
        
        
        location ~ /\.ht {
        deny all;
        }
        
        #禁止访问.htxxx文件
        
        
        }
        
        
        
        注释：变量
        
        Ngx_http_core_module模块支持内置变量，他们的名字和apache的内置变量是一致的。
        
        首先是说明客户请求title中的行，例如$http_user_agent,$http_cookie等等。
        
        此外还有其它的一些变量
        
        $args此变量与请求行中的参数相等
        
        $content_length等于请求行的“Content_Length”的值。
        
        $content_type等同与请求头部的”Content_Type”的值
        
        $document_root等同于当前请求的root指令指定的值
        
        $document_uri与$uri一样
        
        $host与请求头部中“Host”行指定的值或是request到达的server的名字（没有Host行）一样
        
        $limit_rate允许限制的连接速率
        
        $request_method等同于request的method，通常是“GET”或“POST”
        
        $remote_addr客户端ip
        
        $remote_port客户端port
        
        $remote_user等同于用户名，由ngx_http_auth_basic_module认证
        
        $request_filename当前请求的文件的路径名，由root或alias和URI request组合而成
        
        $request_body_file
        
        $request_uri含有参数的完整的初始URI
        
        $query_string与$args一样
        
        $sheeme http模式（http,https）尽在要求是评估例如
        
        Rewrite ^(.+)$ $sheme://example.com$; Redirect;
        
        $server_protocol等同于request的协议，使用“HTTP/或“HTTP/
        
        $server_addr request到达的server的ip，一般获得此变量的值的目的是进行系统调用。为了避免系统调用，有必要在listen指令中指明ip，并使用bind参数。
        
        $server_name请求到达的服务器名
        
        $server_port请求到达的服务器的端口号
        
        $uri等同于当前request中的URI，可不同于初始值，例如内部重定向时或使用index
```


nginx rewrite四种flag http://www.netingcn.com/nginx-rewrite-flag.html 



```
    利用nginx的rewrite的指令，可以实现url的转向，对于rewrtie有四种不同的flag，分别是redirect、permanent、break和last。其中前两种是跳转型的flag，后两种是代理型。跳转型是指有客户端浏览器重新对新地址进行请求，代理型是在WEB服务器内部实现跳转的。
    
    redirect：302跳转到rewrtie后面的地址。
    permanent：301永久调整到rewrtie后面的地址，即当前地址已经永久迁移到新地址，一般是为了对搜索引擎友好。
    last：将rewrite后的地址重新在server标签执行。
    break：将rewrite后地址重新在当前的location标签执行。
    使用root或proxy_pass指定源，last，break都可以，但是结果可能会有差别，后面用例子说明；使用alias指定源，必须使用last。假如有如下配置：
    
    	location / {
    	    root   /var/www/html;
    	    index  index.html index.htm;
    	    rewrite "/x/t.html" /y/t.html break;
    	}
    
    	location /y/ {
    	    root  /var/www/html/other;
    	}
    当请求/x/t.html,符合rewrite规则，所以进行调整，调整的地址为/y/t.html，由于使用的flag是break，所以在“location /”中进行跳转，结果是/var/www/html/y/t.html。但如果flag为last，那么/y/t.html将在server标签中重新执行，由于存在一个“location /y/”，所以跳转被重新指定到“location /y/”标签，所以这个时候的结果为/var/www/html/other/y/t.html。
    
    注意：使用last，如果配置不当，可能引起死循环。例如：
    
    	location /x/ {
    	    proxy_pass http://my_proxy;
    	    rewrite "^/x/(.*)\.html$" /x/1.html last;
    	}
```


http://unixman.blog.51cto.com/10163040/1711943



```
    一、last 和 break 总结如下：
    (1)、last 和 break 当出现在location 之外时，两者的作用是一致的没有任何差异。
    注意一点就是，他们会跳过所有的在他们之后的rewrite 模块中的指令，去选择自己匹配的location
    
    Example:
    1
    2
    3
    4
    5
    6
    7
    rewrite url1 url2 last; ①
    rewrite url3 url4 last; ②
    rewrite url5 url6 last; ③
     
    location ~  url2     ④
    location ~  url4     ⑤
    location ~  url6     ⑥
    
    当① 这条rewrite 规则生效后，它后面的②和③ 将被跳过不做判断，而去直接选择 后面的location。
    这里可能有一个疑问，那些指令输入rewrite 模块中的指令呢？ 若是使用nginx本身，你就要到官网上去查询了。
    但如果你使用的是tengine ，可以使用tengine -V 。会将你想要的信息列举出来。
    
    (1)、last 和 break 当出现在location 内部时，两者就存在了差异。
       last: 使用了last 指令，rewrite 后会跳出location 作用域，重新开始再走一次刚刚的行为
       break: 使用了break 指令，rewrite后不会跳出location 作用域。它的生命也在这个location中         终结。
    
    Example:
    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    11
    12
    13
    14
    15
    rewrite xxx1 yyy last; ⑦
    rewrite xxx2 yyy last; ⑧
    rewrite xxx3 yyy last; ⑨
    rewrite xxx4 yyy last; ⑩
     
    location ~  url1
    {
        rewrite url1 url2 last; ①
    }
     
    location ~  url2
    {
        rewrite url3 url4 break; ②
        fastcgi_pass 127.0.0.1:9000;
    }
    
    以上事例：
    第一个location 中的 rewrite 指令处理完成之后，会跳出location ，再重新判断rewrite 7 ~ 9 的规则。
    第二个location 中的 rewrite  指令处理完成之后，不会跳出location， 更不会重新判断rewrite 7 ~ 9 的规则。而只能将
    信息传递给后面的fastcgi_pass 或者proxy_pass 等指令
    
    二、permanent 和 redirect 总结如下：
    permanent: 大家公认的信息 ，永久性重定向。请求日志中的状态码为301
    redirect： 大家公认的信息 ，临时重定向。请求日志中的状态码为302
    
    从实现功能的角度上去看，permanent 和 redirect 是一样的。不存在哪里好，哪里坏。也不存在什么性能上的问题。
    但从SEO(或者是百度爬你的网站时)。 类似于这样的东西，会对你到底是永久性重定向还是临时重定向感兴趣。了解不到，需要深入，就google 吧。
    
    三、last 和 break VS permanent 和 redirect 
    在 permanent 和 redirect  中提到了 状态码 301 和 302。 那么last 和 break 想对于的访问日志的请求状态码又是多少呢？
    答案为： 200
    这两类关键字，我们能够眼睛看到的差异是什么呢？ 我举个例子说明吧：
    当你打开一个网页，同时打开debug 模式时，会发现301 和 302 时的行为是这样的。第一个请求301 或者 302 后，浏览器重新获取了一个新的URL ，然后会对这个新的URL 重新进行访问。所以当你配置的是permanent 和 redirect ,你对一个URL 的访问请求，落到服务器上至少为2次。
    
    而当你配置了last 或者是break 时，你最终的URL 确定下来后，不会将这个URL返回给浏览器，而是将其扔给了fastcgi_pass或者是proxy_pass指令去处理。请求一个URL ，落到服务器上的次数就为1次。
```