[toc]
## 10.28 rsync工具介绍

#### (1)同步到本机目录:
```
[root@fxq-0 ~]# rsync -av /etc/passwd /tmp/passwd
sending incremental file list
passwd

sent 1135 bytes  received 31 bytes  2332.00 bytes/sec
total size is 1061  speedup is 0.91
[root@fxq-0 ~]# 

```
#### (2)同步到其他主机：

(不写"user@"时，代表就是本机登录用户,rsync -av /tmp/passwd root@192.168.42.181: /tmp/passwd2017.txt)
```
[root@fxq-0 ~]# rsync -av /tmp/passwd 192.168.42.181:/tmp/passwd2017.txt
sending incremental file list
passwd

sent 1135 bytes  received 31 bytes  2332.00 bytes/sec
total size is 1061  speedup is 0.91

```


## 10.29/10.30 rsync常用选项
```
rsync常用选项:
    -a -rtplgoD
    -r同步目录时要加上，类似cp的-r 选项
    -v可视化
    -l 保留软连接
    -L 加上所同步软连接时候会把源文件也同步
    -p 保持文件的权限管理属性
    -O 保持文件的属主
    -g 保持文件的属组
    -D 保持设备文件信息
    -t 保持文件的时间属性。
    --delete 
    --exclude 过滤指定文件,如果：--exclude “logs” 把文件名所括logs的文件或者过滤掉， 不同步。
    -P 显示同步过程，比如速率，比-v更加详细设计。
    -u 加上该选项后，如果dest中的文件比src新，则不同步。
    -z 传输更快，节省带宽。
```

## 10.31 rsync通过ssh同步
    rsync -av /tmp/111/ /tmp/111_dest/
    rsync -avL /tmp/111/ /tmp/111_dest/
    rsync -avL --delete /root/111/ /tmp/111_dest/
    rsync -avL --exclude "*.log" --exclude "*.txt"  /root/111/ /tmp/111_dest/
    rsync -avP /tmp/111/ /tmp/111_dest/ 
    rsync -avPu /tmp/111/ /tmp/111_dest/
    rsync -avPuz /tmp/111/ /tmp/111_dest/
 实例：
 
 -av   
```
[root@fxq-0 tmp]# ls -l /tmp
总用量 4
drwxr-xr-x 2 root root  50 9月  19 22:53 111
-rw-r--r-- 1 root root 501 9月  19 22:52 fstab
[root@fxq-0 tmp]# ls /tmp/111/
2.txt  fstab.txt  passwd
[root@fxq-0 tmp]# rsync -av /tmp/111/ /tmp/111_dest/
sending incremental file list
created directory /tmp/111_dest
./
2.txt
fstab.txt -> /etc/fstab
passwd

sent 2302 bytes  received 56 bytes  4716.00 bytes/sec
total size is 2132  speedup is 0.90
[root@fxq-0 tmp]# ls /tmp/111_dest/
2.txt  fstab.txt  passwd
[root@fxq-0 tmp]# 

```
-avL

```
[root@fxq-0 tmp]# rsync -avL /tmp/111/ /tmp/111_dest/
sending incremental file list
fstab.txt

sent 617 bytes  received 31 bytes  1296.00 bytes/sec
total size is 2623  speedup is 4.05
[root@fxq-0 tmp]# ls /tmp/111_dest/
2.txt  fstab.txt  passwd
[root@fxq-0 tmp]# ls -l /tmp/111_dest/
总用量 12
-rw-r--r-- 1 root root 1061 8月   4 04:42 2.txt
-rw-r--r-- 1 root root  501 8月   1 06:43 fstab.txt
-rw-r--r-- 1 root root 1061 8月   4 04:42 passwd
[root@fxq-0 tmp]#
```

--delete 
```
[root@fxq-0 tmp]# ls -l /tmp/111_dest/
总用量 12
-rw-r--r-- 1 root root 1061 8月   4 04:42 2.txt
-rw-r--r-- 1 root root  501 8月   1 06:43 fstab.txt
-rw-r--r-- 1 root root 1061 8月   4 04:42 passwd
[root@fxq-0 tmp]# touch /tmp/111_dest/new.txt
[root@fxq-0 tmp]# ls /tmp/111_dest/
2.txt  fstab.txt  new.txt  passwd
[root@fxq-0 tmp]# rsync -avPuzL --delete /tmp/111/ /tmp/111_dest/
sending incremental file list
deleting new.txt

sent 73 bytes  received 12 bytes  56.67 bytes/sec
total size is 2623  speedup is 30.86
[root@fxq-0 tmp]# ls /tmp/111_dest/
2.txt  fstab.txt  passwd
[root@fxq-0 tmp]# 

```

--exclude
```
[root@fxq-0 tmp]# ls /tmp/111_dest/
passwd
[root@fxq-0 tmp]# rsync -avLPuz --exclude "*.txt" /tmp/111/ /tmp/111_dest/
sending incremental file list
./

sent 48 bytes  received 15 bytes  126.00 bytes/sec
total size is 1061  speedup is 16.84
[root@fxq-0 tmp]# ls /tmp/111_dest/
passwd
[root@fxq-0 tmp]# rsync -avLPuz --exclude "2*.txt" /tmp/111/ /tmp/111_dest/
sending incremental file list
fstab.txt
         501 100%    0.00kB/s    0:00:00 (xfer#1, to-check=1/3)

sent 393 bytes  received 31 bytes  848.00 bytes/sec
total size is 1562  speedup is 3.68
[root@fxq-0 tmp]# ls /tmp/111_dest/
fstab.txt  passwd
[root@fxq-0 tmp]# 
```
