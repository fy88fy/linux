[toc]
## 10.32/10.33 rsync通过服务同步
### 1. rsync通过ssh方式同步

#### (1)从本地机器---远程机器:

    rsync -avP /etc/passwd 192.168.42.181:/tmp/1.txt
```
[root@fxq-0 ~]# rsync -avP /etc/passwd 192.168.42.181:/tmp/testPasswd.txt
sending incremental file list
passwd
        1061 100%    0.00kB/s    0:00:00 (xfer#1, to-check=0/1)

sent 1135 bytes  received 31 bytes  777.33 bytes/sec
total size is 1061  speedup is 0.91

[root@fxq-1 ~]# ls /tmp
2.txt  ks-script-a0okBe  passwd  passwd2017.txt  testPasswd.txt  yum.log

```
#### (2)从远程机器---本地机器:

    rsync -avP 192.168.42.181:/tmp/1.txt /tmp/2.txt
```
[root@fxq-0 ~]# ls /tmp
111  111_dest  fstab
[root@fxq-0 ~]# rsync -avP 192.168.42.181:/tmp/testPasswd.txt /tmp/
receiving incremental file list
testPasswd.txt
        1061 100%    1.01MB/s    0:00:00 (xfer#1, to-check=0/1)

sent 30 bytes  received 1148 bytes  2356.00 bytes/sec
total size is 1061  speedup is 0.90
[root@fxq-0 ~]# ls !$
ls /tmp/
111  111_dest  fstab  testPasswd.txt
[root@fxq-0 ~]# ls

```
    
    
#### (3) ssh端口不是22时：

    rsync -avP -e "ssh -p 22" /etc/passwd 192.168.42.181:/tmp/1.txt
    ssh -p 22 192.168.42.181

```
[root@fxq-0 ~]# rsync -avP -e "ssh -p 22" /etc/passwd 192.168.42.181:/tmp/1.txt
sending incremental file list
passwd
        1061 100%    0.00kB/s    0:00:00 (xfer#1, to-check=0/1)

sent 1135 bytes  received 31 bytes  2332.00 bytes/sec
total size is 1061  speedup is 0.91

```
#### (4) ssh 端口不是22的机器:

        ssh -p 22 192.168.42.181

```
[root@fxq-0 ~]# ssh -p 22 192.168.42.181
Last login: Wed Sep 20 21:52:52 2017 from 192.168.42.2
[root@fxq-1 ~]# ls
anaconda-ks.cfg
[root@fxq-1 ~]# hostname
fxq-1
[root@fxq-1 ~]#
```
### 2. rsync通过服务方式同步：

#### (1)编辑配置文件：/etc/rsyncd.conf


```
port=873
log file=/var/log/rsync.log
pid file=/var/run/rsyncd.pid
address=192.168.42.180
[test]
path=/tmp/rsync
use chroot=false
max connections=4
read only=no
list=true
uid=root
gid=root
#auth users=test
#secrets file=/etc/rsyncd.passwd
hosts allow=192.168.42.181

```

```
 rsyncd.conf配置文件详解 
 port：指定在哪个端口启动rsyncd服务，默认是873端口。
 log file：指定日志文件。
 pid file：指定pid文件，这个文件的作用涉及服务的启动、停止等进程管理操作。
 address：指定启动rsyncd服务的IP。假如你的机器有多个IP，就可以指定由其中一个启动rsyncd服务，如果不指定该参数，默认是在全部IP上启动。
 []：指定模块名，里面内容自定义。
 path：指定数据存放的路径。
 use chroot true|false：表示在传输文件前首先chroot到path参数所指定的目录下。这样做的原因是实现额外的安全防护，但缺点是需要以roots权限，并且不能备份指向外部的符号连接所指向的目录文件。默认情况下chroot值为true，如果你的数据当中有软连接文件，阿铭建议你设置成false。

```

#### (2) 启动服务:rsync --daemon


```
[root@fxq-0 ~]# rsync --daemon
[root@fxq-0 ~]# ps aux | grep 873
root      2099  0.0  0.2   6644   900 pts/0    R+   22:15   0:00 grep --color=auto 873
[root@fxq-0 ~]# ps aux | grep rsync
root      2097  0.0  0.1   6560   512 ?        Ss   22:15   0:00 rsync --daemon
root      2101  0.0  0.2   6644   904 pts/0    S+   22:15   0:00 grep --color=auto rsync
[root@fxq-0 ~]# ss -an | grep 873
tcp    LISTEN     0      5      192.168.42.180:873                   *:*            
```



### (3) 服务方式同步：

格式: rsync -avP /tmp/ 192.168.42.180::test/


```
[root@fxq-1 ~]# rsync -avP /tmp/ 192.168.42.180::test/
sending incremental file list
./
1.txt
        1061 100%    0.00kB/s    0:00:00 (xfer#1, to-check=8/10)
afxq.txt
        1061 100%    1.01MB/s    0:00:00 (xfer#2, to-check=7/10)
testPasswd.txt
        1061 100%    1.01MB/s    0:00:00 (xfer#3, to-check=6/10)
.ICE-unix/
.Test-unix/
.X11-unix/
.XIM-unix/
.font-unix/
test/

sent 3525 bytes  received 92 bytes  2411.33 bytes/sec
total size is 3183  speedup is 0.88

[root@fxq-0 rsync]# ls /tmp/
111  111_dest  fstab  rsync  testPasswd.txt
[root@fxq-0 rsync]# 

```
#### 1) 如果端口为非873:
    
    rsync -avP --port 8730 /tmp/ 192.168.42.180::test/


```
 max connections：指定最大的连接数，默认是0，即没有限制。
 read only ture|false：如果为true，则不能上传到该模块指定的路径下。
 list：表示当用户查询该服务器上的可用模块时，该模块是否被列出，设定为true则列出，false则隐藏。
 uid/gid：指定传输文件时以哪个用户/组的身份传输。
 auth users：指定传输时要使用的用户名。
 secrets file：指定密码文件，该参数连同上面的参数如果不指定，则不使用密码验证。注意该密码文件的权限一定要是600。格式：用户名:密码
 hosts allow：表示被允许连接该模块的主机，可以是IP或者网段，如果是多个，中间用空格隔开。 
 当设置了auth users和secrets file后，客户端连服务端也需要用用户名密码了，若想在命令行中带上密码，可以设定一个密码文件
 rsync -avL test@192.168.133.130::test/test1/  /tmp/test8/ --password-file=/etc/pass 
 其中/etc/pass内容就是一个密码，权限要改为600

```

同步:
```
[root@fxq-1 ~]# rsync -avP --port=873 192.168.42.180::test
receiving incremental file list
drwxrwxrwt         165 2017/09/20 22:53:39 .
-rw-r--r--        1061 2017/08/04 04:42:20 1.txt
-rw-r--r--        1061 2017/08/04 04:42:20 afxq.txt
-rw-r--r--        1061 2017/08/04 04:42:20 testPasswd.txt
drwxrwxrwt           6 2017/08/01 06:48:11 .ICE-unix
drwxrwxrwt           6 2017/08/01 06:48:11 .Test-unix
drwxrwxrwt           6 2017/08/01 06:48:11 .X11-unix
drwxrwxrwt           6 2017/08/01 06:48:11 .XIM-unix
drwxrwxrwt           6 2017/08/01 06:48:11 .font-unix
drwxrwxrwt         156 2017/09/20 22:47:04 1
-rw-r--r--        1061 2017/08/04 04:42:20 1/1.txt
-rw-r--r--        1061 2017/08/04 04:42:20 1/afxq.txt
-rw-r--r--        1061 2017/08/04 04:42:20 1/testPasswd.txt
drwxrwxrwt           6 2017/08/01 06:48:11 1/.ICE-unix
drwxrwxrwt           6 2017/08/01 06:48:11 1/.Test-unix
drwxrwxrwt           6 2017/08/01 06:48:11 1/.X11-unix
drwxrwxrwt           6 2017/08/01 06:48:11 1/.XIM-unix
drwxrwxrwt           6 2017/08/01 06:48:11 1/.font-unix
drwxr-xr-x           6 2017/09/20 22:47:04 1/test
drwxr-xr-x           6 2017/09/20 22:47:04 test

sent 39 bytes  received 417 bytes  912.00 bytes/sec
total size is 6366  speedup is 13.96

```

#### 2) 使用账户和密码登录:
**在服务器端，编辑配置文件：**

    vim /etc/rsync.conf
    
    auth users=test
    secrets file=/etc/rsyncd.passwd

**编辑密码配置文件：**

    密码文件格式：
    test:123456

**在客户端执行：**

    rsync -avP /tmp/ test@192.168.42.180::test/

```
[root@fxq-1 ~]# rsync -avP /tmp/ test@192.168.42.180::test/
Password: 
sending incremental file list

sent 382 bytes  received 21 bytes  89.56 bytes/sec
total size is 6366  speedup is 15.80
[root@fxq-1 ~]# touch /tmp/aaaa.txt
[root@fxq-1 ~]# rsync -avP /tmp/ test@192.168.42.180::test/
Password: 
sending incremental file list
./
aaaa.txt
           0 100%    0.00kB/s    0:00:00 (xfer#1, to-check=18/21)

sent 449 bytes  received 43 bytes  140.57 bytes/sec
total size is 6366  speedup is 12.94

```
#### 如果不想手动输入密码：

**在客户端设置密码文件：**

    vi /etc/pass

**密码文件格式:**

    123456

同步：

    rsync -avL test@192.168.42.180::test/test1/  /tmp/test8/ --password-file=/etc/pass 


```
[root@fxq-1 ~]# chmod 600 /etc/pass

[root@fxq-1 ~]# touch /tmp/3333.txt
[root@fxq-1 ~]# rsync -avP /tmp/ test@192.168.42.180::test/ --password-file=/etc/pass
sending incremental file list
./
3333.txt
           0 100%    0.00kB/s    0:00:00 (xfer#1, to-check=19/22)

sent 466 bytes  received 43 bytes  1018.00 bytes/sec
total size is 6366  speedup is 12.51
[root@fxq-1 ~]#
```


## 10.34 linux系统日志
### /var/log/messages
    总日志systemlog
### logrotate

 ### /etc/logrotate.conf 日志切割配置文件
 
 参考https://my.oschina.net/u/2000675/blog/908189
 
 dmesg命令
 保存在内存中。
 /var/log/dmesg 日志  系统启动日志
 
 last命令，调用的文件/var/log/wtmp
 
 lastb命令查看登录失败的用户，对应的文件时/var/log/btmp
 
 /var/log/secure 系统安全日志

## 10.35 screen工具

 为了不让一个任务意外中断
 nohup command &
 screen是一个虚拟终端
 yum install -y screen
 screen直接回车就进入了虚拟终端
 ctral a组合键再按d退出虚拟终端，但不是结束
 screen -ls 查看虚拟终端列表
 screen -r id 进入指定的终端
 screen -S aming
 screen -r aming



```
[root@fxq-0 rsync]# ls
1  1.txt  3333.txt  aaaa.txt  afxq.txt  nohup.out  test  testPasswd.txt
[root@fxq-0 rsync]# screen -l
[screen is terminating]
[root@fxq-0 rsync]# screen -ls
No Sockets found in /var/run/screen/S-root.

[root@fxq-0 rsync]# screen
[detached from 2388.pts-1.fxq-0]
[root@fxq-0 rsync]# screnn -ls
-bash: screnn: 未找到命令
[root@fxq-0 rsync]# screen -ls
There is a screen on:
	2388.pts-1.fxq-0	(Detached)
1 Socket in /var/run/screen/S-root.

[root@fxq-0 rsync]# screen -S "testScreen"
[detached from 2403.testScreen]
[root@fxq-0 rsync]# screen -ls
There are screens on:
	2403.testScreen	(Detached)
	2388.pts-1.fxq-0	(Detached)
2 Sockets in /var/run/screen/S-root.

[root@fxq-0 rsync]#
```

## 扩展
1. Linux日志文件总管logrotate http://linux.cn/article-4126-1.html


2. xargs用法详解 http://blog.csdn.net/zhangfn2011/article/details/6776925