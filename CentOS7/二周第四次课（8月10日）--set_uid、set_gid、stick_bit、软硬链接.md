### 2.18 特殊权限set_uid

**which 查看文件路径：**
```
[root@VM_46_188_centos ~]# which passwd 
/usr/bin/passwd
[root@VM_46_188_centos ~]# ls -l /usr/bin/passwd
-rwsr-xr-x. 1 root root 27832 Jun 10  2014 /usr/bin/passwd
```
rws ：set_uid

linux用户密码存放文件的路径：

```
[root@VM_46_188_centos ~]# ls -l /etc/shadow
---------- 1 root root 965 Jul 26 14:32 /etc/shadow
```
> 普通用户要修改密码，一定要改shadow文件，这样就用到了set_uid ，加上s权限后就可以实现相应的功能。
> 当然其他用户执行带有set_uid 权限的二进制可执行文件时，可临时拥有该文件所有者的权限。

**给文件加上set_uid权限:**
> chmod u=rws /usr/bin/ls
> 
> chmod u+x /usr/bin/ls
> 
> S:大写s时，为没有x权限
> 
> 目录也可以设置，但是是没有意义的。
> 
> chmod 4755 /usr/bin/ls

**把ls 命令设置set_uid 就可以查看/root/内容：**
```
[root@VM_46_188_centos ~]# su - fxq
Last login: Sun Jul 16 13:32:41 CST 2017 on pts/1
[fxq@VM_46_188_centos ~]$ whoami
fxq
[fxq@VM_46_188_centos ~]$ ls /root/
ls: cannot open directory /root/: Permission denied
[fxq@VM_46_188_centos ~]$ exit
logout
[root@VM_46_188_centos ~]# chmod u+s /usr/bin/ls
ls        lsblk     lsinitrd  lslogins  lsmd      
lsattr    lscpu     lslocks   lsmcli    lsscsi    
[root@VM_46_188_centos ~]# chmod u+s /usr/bin/ls
[root@VM_46_188_centos ~]# ll /usr/bin/ls
-rwsr-xr-x. 1 root root 117616 Nov 20  2015 /usr/bin/ls
[root@VM_46_188_centos ~]# su - fxq
Last login: Tue Aug  8 21:45:22 CST 2017 on pts/0
[fxq@VM_46_188_centos ~]$ ls /root/
#fxq.txt  1.cap          1.log.tar.gz   1.txt    123.zip  3.txt      a.out                 dir-2017-05-12  httpd_process_check.sh  ping_host_alive.sh  wordpress-4.7.4-zh_CN.zip
,         1.ipt          1.log.tar1.xz  111.txt  2        4.txt      anaconda-ks.cfg       ffff            ip.txt                  sed.txt             youjian.sh
,.pub     1.log.tar      1.log.xz       12       2.cap    [1-3].log  auto_install_lamp.sh  gdlogo.png      iptables.rules          shell               ~iptables_rules
1         1.log.tar.bz2  1.log.zip      123      2.txt    \fxq.txt   baidu.png             gdlogo.png.1    null                    weixin
[fxq@VM_46_188_centos ~]$ 

```


### 2.19 特殊权限set_gid

> chmod g+s /root/234  给目录234加上set_gid
> 
> chmod g-s /root/234 给目录234删除set_gid
> 
> chmod 2755 /root/234 给目录234加上set_gid

作用在文件和目录时：

**当作用在文件上时**，和set_uid 类似，执行该文件的用户，会临时拥有文件所属组的权限。


**当然作用在目录上时**，在该目录中创建文件时，文件的所属组和父目录所属组保持一致。




```
[root@VM_46_188_centos ~]# ll /usr/bin/ls
-rwxr-sr-x. 1 root root 117616 Nov 20  2015 /usr/bin/ls
[root@VM_46_188_centos ~]# su - fxq
Last login: Tue Aug  8 21:47:24 CST 2017 on pts/0
[fxq@VM_46_188_centos ~]$ ls /root
#fxq.txt       123                   gdlogo.png
,              123.zip               gdlogo.png.1
,.pub          2                     httpd_process_check.sh
1              2.cap                 ip.txt
1.cap          2.txt                 iptables.rules
1.ipt          3.txt                 null
1.log.tar      4.txt                 ping_host_alive.sh
1.log.tar.bz2  [1-3].log             sed.txt
1.log.tar.gz   \fxq.txt              shell
1.log.tar1.xz  a.out                 weixin
1.log.xz       anaconda-ks.cfg       wordpress-4.7.4-zh_CN.zip
1.log.zip      auto_install_lamp.sh  youjian.sh
1.txt          baidu.png             ~iptables_rules
111.txt        dir-2017-05-12
12             ffff
[fxq@VM_46_188_centos ~]$ 

```


```
[root@VM_46_188_centos ~]# ll -d 234
drwxr-xr-x 2 root root 4096 Aug  8 21:58 234
[root@VM_46_188_centos ~]# chgrp fxq 234
[root@VM_46_188_centos ~]# ll -d 234
drwxr-xr-x 2 root fxq 4096 Aug  8 21:58 234
[root@VM_46_188_centos ~]# chmod g+s 234
[root@VM_46_188_centos ~]# ll -d 234
drwxr-sr-x 2 root fxq 4096 Aug  8 21:58 234
[root@VM_46_188_centos ~]# touch 234/test.txt
You have new mail in /var/spool/mail/root
[root@VM_46_188_centos ~]# ll 234/test.txt
-rw-r--r-- 1 root fxq 0 Aug  8 22:01 234/test.txt
[root@VM_46_188_centos ~]# chmod g-s 234
[root@VM_46_188_centos ~]# ll -d 234
drwxr-xr-x 2 root fxq 4096 Aug  8 22:01 234
[root@VM_46_188_centos ~]# touch 234/test1.txt
[root@VM_46_188_centos ~]# ll 234/test1.txt
-rw-r--r-- 1 root root 0 Aug  8 22:01 234/test1.txt
[root@VM_46_188_centos ~]# 
```


### 2.20 特殊权限stick_bit
防删除位

**功能：** 防止别人删除自己的文件。

chmod 1755 /test

```
[root@VM_46_188_centos ~]# ll /tmp -d
drwxrwxrwt. 8 root root 4096 Aug  8 21:59 /tmp

```

```
[root@VM_46_188_centos ~]# mkdir /test
[root@VM_46_188_centos ~]# chmod 1777 /test
[root@VM_46_188_centos ~]# ll -d /test
drwxrwxrwt 2 root root 4096 Aug  8 22:16 /test
[root@VM_46_188_centos ~]# su - fxq
Last login: Tue Aug  8 22:15:34 CST 2017 on pts/0
[fxq@VM_46_188_centos ~]$ touch /test/fxq.txt
[fxq@VM_46_188_centos ~]$ echo "111111111" >/test/fxq.txt
[fxq@VM_46_188_centos ~]$ cat /test/fxq.txt
111111111
[fxq@VM_46_188_centos ~]$ su - fxq1 
Password: 
[fxq1@VM_46_188_centos ~]$ echo "2222222" > /test/fxq.txt 
-bash: /test/fxq.txt: Permission denied
[fxq1@VM_46_188_centos ~]$ ll /test/
total 4
-rw-rw-r-- 1 fxq fxq 10 Aug  8 22:17 fxq.txt
[fxq1@VM_46_188_centos ~]$ su - fxq
Password: 
Last login: Tue Aug  8 22:16:55 CST 2017 on pts/0
[fxq@VM_46_188_centos ~]$ chmod 777 /test/fxq.txt
[fxq@VM_46_188_centos ~]$ su - fxq1
Password: 
Last login: Tue Aug  8 22:17:50 CST 2017 on pts/0
[fxq1@VM_46_188_centos ~]$ echo "2222222" > /test/fxq.txt 
[fxq1@VM_46_188_centos ~]$ cat /test/fxq.txt
2222222
[fxq1@VM_46_188_centos ~]$ rm -f /test/fxq.txt
rm: cannot remove '/test/fxq.txt': Operation not permitted
[fxq1@VM_46_188_centos ~]$ 

```


### 2.21 软链接文件
#### 软链接:
类似于wodows的快捷方式，路径越长，链接文件越大。可用于文件或目录，可跨分区.

```
[fxq1@VM_46_188_centos ~]$ ls -l /bin
lrwxrwxrwx. 1 root root 7 Apr 21  2016 /bin -> usr/bin
```
**创建软链接文件：**

ln -s /usr/bin/ls /home/fxq/ls

做软链接使用绝对路径。

> df -h
> 
> 如果log文件太大，不断的在变大，分区快满了。这时可以把log文件移动到大分区后，生成软链接文件就可以了。



### 2.22 硬连接文件
#### 硬链接：
只支持给文件做硬链接。

> 书写格式：
> 
> ln gdlogo.png gdlogo.png.hard
> 
```
[root@VM_46_188_centos ~]# ln gdlogo.png gdlogo.png.hard
[root@VM_46_188_centos ~]# ll
total 1724
-rwxr-xr-x  1 root    root     118464 Aug  4 21:49 a.out
-rw-------. 1 root    root       2523 Apr 21  2016 anaconda-ks.cfg
-rw-r--r--  1 root    root       6434 Jul 19 22:33 auto_install_lamp.
sh-rw-r--r--  1 root    root        222 Jul 16 20:55 baidu.png
drwxr-xr-x  2 root    root       4096 May 12 18:51 dir-2017-05-12
drwxr-xr-x  2 root    root       4096 Jul  7 22:45 ffff
drwxrwxrwt  2 root    root       4096 Aug  8 22:13 fxq
-rw-r--r--  2 root    root        222 Jul 16 20:54 gdlogo.png
-rw-r--r--  1 root    root      11217 Feb 29  2016 gdlogo.png.1
-rw-r--r--  2 root    root        222 Jul 16 20:54 gdlogo.png.hard
-rw-r--r--  1 root    root       2706 Jun 27 00:23 httpd_process_chec
k.sh-rwxr-xr-t  1 root    root         48 Jul  8 08:25 ip.txt
-rw-r--r--  1 root    root       3277 Jul 18 11:48 iptables.rules
-rw-r--r--  1 root    root        142 Jun 23 16:26 null
-rw-r--r--  1 root    root        124 Jun 27 10:45 ping_host_alive.sh
-rw-r--r--  1 root    root        108 Jun 22 21:26 sed.txt
drwxr-xr-x  2 root    root       4096 Jul 23 17:52 shell
drwxr-xr-x  5 root    root       4096 Jun 29 00:10 weixin
-rw-r--r--  1 root    root    1270021 Jun  2 23:22 wordpress-4.7.4-zh
_CN.zip-rw-r--r--  1 fxq     user1      1299 Jun 30 17:07 youjian.sh
-rw-r--r--  1 root    root       3274 Jul 18 11:44 ~iptables_rules
[root@VM_46_188_centos ~]# ln -s /root/gdlogo.png /root/gdlogo.png.so
ft
[root@VM_46_188_centos ~]# ll
drwxrwxrwt  2 root    root       4096 Aug  8 22:13 fxq
-rw-r--r--  2 root    root        222 Jul 16 20:54 gdlogo.png
-rw-r--r--  1 root    root      11217 Feb 29  2016 gdlogo.png.1
-rw-r--r--  2 root    root        222 Jul 16 20:54 gdlogo.png.hard
lrwxrwxrwx  1 root    root         16 Aug  8 22:46 gdlogo.png.soft ->
 /root/gdlogo.png-rw-r--r--  1 root    root       2706 Jun 27 00:23 httpd_process_chec
k.sh-rwxr-xr-t  1 root    root         48 Jul  8 08:25 ip.txt
-rw-r--r--  1 root    root       3277 Jul 18 11:48 iptables.rules
-rw-r--r--  1 root    root        142 Jun 23 16:26 null
-rw-r--r--  1 root    root        124 Jun 27 10:45 ping_host_alive.sh
-rw-r--r--  1 root    root        108 Jun 22 21:26 sed.txt
drwxr-xr-x  2 root    root       4096 Jul 23 17:52 shell
drwxr-xr-x  5 root    root       4096 Jun 29 00:10 weixin
-rw-r--r--  1 root    root    1270021 Jun  2 23:22 wordpress-4.7.4-zh
_CN.zip-rw-r--r--  1 fxq     user1      1299 Jun 30 17:07 youjian.sh
-rw-r--r--  1 root    root   
```

**把源文件删除，软链接文件不可用，硬链接文件可用,和原文件一样。硬链接不会占用多余空间。硬链接不可对目录做，不能跨分区。**


```
[root@VM_46_188_centos ~]# ll -i 
total 1724
131226 -rw-r--r--  1 root    root       6434 Jul 19 22:33 auto_install_lamp.sh
131221 -rw-r--r--  1 root    root        222 Jul 16 20:55 baidu.png
131084 drwxr-xr-x  2 root    root       4096 May 12 18:51 dir-2017-05-12
131099 drwxr-xr-x  2 root    root       4096 Jul  7 22:45 ffff
215075 drwxrwxrwt  2 root    root       4096 Aug  8 22:13 fxq
131219 -rw-r--r--  2 root    root        222 Jul 16 20:54 gdlogo.png
131223 -rw-r--r--  1 root    root      11217 Feb 29  2016 gdlogo.png.1
131219 -rw-r--r--  2 root    root       222 Jul 16 20:54 gdlogo.png.hard
131229 lrwxrwxrwx  1 root    root     16 Aug  8 22:46 gdlogo.png.soft -> /root/gdlogo.png
131139 -rw-r--r--  1 root    root       2706 Jun 27 00:23 
[root@VM_46_188_centos ~]# 

```

