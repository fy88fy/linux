[toc]
## 10.1 使用w查看系统负载
监控系统状态
### 1.w/uptime 查看系统负载

```
[root@0 ~]# w
 21:48:00 up 7 days,  7:15,  1 user,  load average: 0.00, 0.02, 0.05
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
root     pts/1    124.238.165.42   21:47    0.00s  0.04s  0.00s w
[root@0 ~]#
```
    load average: 1分钟 5分钟  15分钟  单位时间内保用cpu 活动的进程有多少个
    

### 2.cat /proc/cpuinfo 查看cpu核数
    cat /proc/cpuinfo | grep "processor" | wc -l



## 10.2 vmstat命令

### 1. vmstat 监控系统状态
    vmstat 
    vmstat 1
    vmstat 1 5 


```
[root@0 ~]# vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 2  0      0 172524 187064 439320    0    0     0    39   26   19 69  0 31  0  0

```
    r: 有多少个进程run使用cpu
    b: 有多少个进程被组断
    swpd：内存不够时会占用
    si : 有多少个kb数据从swap 进入内存中。(参照物:内存)
    so :有多少个kb数据从内存进入swap 
    bi: 从磁盘进入到内存
    bo：从内存中写入到磁盘
       如果bi bo  大了。 b也是大
    us:用户占用cpu比例
    fy：系统本身的进程占用cpu比例
    id:系统剩余cpu百分比
    wa:进程等待cpu百分比
    
    
    用法 vmstat 1
    关键几列：r,b,swpd,si,so
## 10.3 top命令
    3s 显示1次
    zombie 主进程被意外中止，留下子进程没有人管
    %CPU st:被偷走了的cpu  vmware
    us大负载会高，负载高不一定us大
    默认cpu使用率排序
    M：使用内存排序
    P:cpu排序
    1:显示所有cpu
    
    RES： 进程使用物理内存大小 


```
top - 22:34:23 up 7 days,  8:02,  1 user,  load average: 0.17, 0.06, 0.06Tasks:  79 total,   3 running,  76 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us,  0.7 sy,  0.0 ni, 99.0 id,  0.3 wa,  0.0 hi,  0.0 si,  0.0 stKiB Mem :  1016904 total,   171640 free,   218672 used,   626592 buff/cache
KiB Swap:        0 total,        0 free,        0 used.   626804 avail Mem 
  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND                                                                                                        
  979 root      20   0   37864  21572    980 S  0.3  2.1   6:42.33 secu-tcs-agent                                                                                                 
 2987 root      20   0   39064  22984   5576 R  0.3  2.3   5:20.08 sap1009                                                                                                        
    1 root      20   0   41040   3304   2240 S  0.0  0.3   1:22.94 systemd                                                                                                        
    2 root      20   0       0      0      0 S  0.0  0.0   0:00.02 kthreadd                
```

### 1. top -c 显示进程中的路径

```
top - 22:37:12 up 7 days,  8:05,  1 user,  load average: 0.07, 0.06, 0.05
Tasks:  79 total,   4 running,  75 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.3 us,  0.7 sy,  0.0 ni, 99.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  1016904 total,   171508 free,   218800 used,   626596 buff/cache
KiB Swap:        0 total,        0 free,        0 used.   626684 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND                                                                                                        
 2987 root      20   0   39064  22984   5576 R  0.3  2.3   5:20.55 /usr/local/sa/agent/plugins/sap1009                                                                            
    1 root      20   0   41040   3304   2240 S  0.0  0.3   1:22.96 /usr/lib/systemd/systemd --system --deserialize 41                                                             
    2 root      20   0       0      0      0 S  0.0  0.0   0:00.02 [kthreadd]                                                                                                     
    3 root      20   0       0      0      0 S  0.0  0.0   0:01.13 [ksoftirqd/0]                    
```

top -bn1 一次性显示所有进程的cpu及内存占用情况. 运用于脚本中.


## 10.4 sar命令
    安装包:yum install -y sysstat
    sar -n DEV 1 10 网卡流量
    rxpck/s  接收到数据量  如果查是几千正常，过高就不正常。、
    
    txpck/s  发送
    sar -n DEV -f /var/log/sa/sa11
        /var/log/sa/sa11  目录下最多保留一个月
### 1.sar -q 查看负载
    sar -q 1 10 

```
[root@0 ~]# sar -q 
Linux 3.10.0-327.36.3.el7.x86_64 (0) 	09/11/17 	_x86_64_	(1 CPU)

00:00:01      runq-sz  plist-sz   ldavg-1   ldavg-5  ldavg-15   blocked
00:10:01            3       110      0.01      0.04      0.05         0
00:20:01            1       113      0.01      0.04      0.05         1
00:30:01            2       110      0.00      0.01      0.05         0
00:40:01            2       113      0.18      0.09      0.07         0
00:50:01            3       110      0.00      0.03      0.05         0
01:00:01            4       115      0.00      0.01      0.05         1

```

### 2. sar -b 查看磁盘
sar -b 1 5


```
[root@0 ~]# sar -b 
Linux 3.10.0-327.36.3.el7.x86_64 (0) 	09/11/17 	_x86_64_	(1 CPU)

00:00:01          tps      rtps      wtps   bread/s   bwrtn/s
00:10:01         1.39      0.00      1.39      0.00     91.46
00:20:01         1.21      0.00      1.21      0.00     78.44
00:30:01         1.31      0.00      1.31      0.00     79.93

```


## 10.5 nload命令
    安装所需要工具:nload:
    yum install epel -y && yumm install nload -y 
    输入:nload 会动态显示网卡流量
![image](http://note.youdao.com/yws/api/personal/file/3A2525A554024E0296DC4F563211976B?method=download&shareKey=a78dca4dcab9f1f83099a425577d892b)

