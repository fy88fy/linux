[toc]
## 7.1 安装软件包的三种方法
yum ---python
1. rpm工具
1. yum工具
1. 源码包

## 7.2 rpm包介绍
设置光驱并挂载：

```
[root@fxq-0 ~]# mount /dev/cdrom /mnt
mount: /dev/sr0 写保护，将以只读方式挂载
[root@fxq-0 ~]# ls /mnt
EULA    isolinux  repodata                      TRANS.TBL
GPL     LiveOS    RPM-GPG-KEY-CentOS-7
images  Packages  RPM-GPG-KEY-CentOS-Testing-7
[root@fxq-0 ~]#cd /mnt/Packages/

```
postfix-2.10.1-6.el7.i686.rpm
包名-主版本-次版本-发布版本号-平台-扩展名

## 7.3 rpm工具用法

### (1) 安装

    rpm -ivh  postfix-2.10.1-6.el7.i686.rpm
### (2) 升级
    rpm -Uvh  postfix-2.10.1-6.el7.i686.rpm

### (3)卸载
    rpm -e postfix

### (4)  查询安装过的包
    rpm -qa postfix

### (5) 查询包是否安装
     rpm -q postfix 

### (6) 查看包信息
     rpm -qi postfix

### (7) 查看安装了哪些文件
    rpm -ql postfix 

### (8) 查看文件来源的包
    rpm -qf fstab 
    rpm -qf `which cd`
    


## 7.4 yum工具用法
### （1）列出可安装包.
    yum list 
###  (2) 列出可安装包组.
    yum grouplist 
### (3) 查询安装包
    yum search all network
### (4) 查询安装包
    yum list | grep 'vim'
### (5) 安装：
    yum install -y postfix
### (6) 卸货
    yum remove postfix
### (7) 升级
    yum update postfix
### (8)升级系统及其rpm包
    yum update 
### (9) 查询vim命令所依赖的包。
    yum provides "/*/vim" 


## 7.5 yum搭建本地仓库


```
mount /dev/cdrom /mnt
cp -r /etc/yum.repos.d/ /etc/yum.repos.d.bak
rm -rf /etc/yum.repos.d/*
vim /etc/yum.repos.d/dvd.repo 
[dvd]
name=install dvd
baseurl=file:///mnt
enable=1
gpgcheck=0

yum clean all 
yum list
```




## 扩展
**1. yum保留已经安装过的包**

```
可以设置使yum保留已经下载的rpm包，供以后升级或重新安装时使用。
修改/etc/yum.conf即可：

[main]
cachedir=/home/soft1/yumcache
keepcache=1
debuglevel=2

chchedir是放置下载的包的地方，可以修改为自己想放置的位置。
keepcache为1时表示保存已经下载的rpm包。
```
**2. 搭建局域网yum源** 
```
yum局域网软件源搭建


1、搭建Apache服务器或ftp服务器
yum安装或二进制包安装

2、准备RPM包把CentOS的DVD1和DVD2.iso都下载下来，把DVD1.iso里的所有内容解压出来，放到/var/www/html/centos-6目录下，然后把DVD2.iso解压出来的Packages目录下的rpm包复制到/var/html/centos-6/Packages目录下，这样/var/html/centos-6/Packages里面就有了6000多个rpm包。

3、创建yum仓库
准备createrepo：yum -y install createrepo
创建repository：createrepo /var/www/html/centos-6/
创建完成之后，会在/var/www/html/centos-6/repodata下生成一些文件。


4、使用软件源

在其他centos机器上试试软件源能不能用。

首先修改机器上软件源配置文件：

# cd /etc/yum.repos.d/
# mkdir bk
# mv *.repo bk/
# cp bk/CentOS-Base.repo ./
# vi CentOS-Base.repo

CentOS-Base.repo文件修改之后如下：

[base]
name=CentOS-$releasever - Base
baseurl=http://*.*.*.*/centos-6/
gpgcheck=1(改成0下面那行就不用设置了)
gpgkey=http:///*.*.*.*/centos-6/RPM-GPG-KEY-CentOS-6
enabled=1
#released updates 
#[updates]
#name=CentOS-$releasever - Updates
#baseurl=http:///*.*.*.*/centos-6/
#gpgcheck=1
#gpgkey=http:///*.*.*.*/centos-6/RPM-GPG-KEY-CentOS-6
#enabled = 1

保存之后，就可以使用局域网的软件源了：

# yum update


原地址：http://www.linuxidc.com/Linux/2013-07/87315.htm
```





