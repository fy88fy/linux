#!/bin/bash
#date=2017-06-21
#Auther is fengxiaoqing
#
# Shell script scripts to install LAMP
# -------------------------------------------------------------------------
# Copyright (c) 2017 fxq project <http://www.fy88fy.cc/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of fxq shell script collection (NSSC)
# Visit http://www.fy88fy.cc/ for more information.
# -------------------------------------------------------------------------

PS3="Please input a number(1|2|3|4):"
select name in "apache" "mysql" "php" "quit" 

do
case $name in
	 apache)
        	echo "apache install";
        	;;
     	
	mysql)
        	echo "mysql install";
        	;;
     	
	php)
        	echo "php install";
        	;;
	
	quit)
		exit 0
		;;
	
	*)
		echo -e "\033[32m------------------------------\033[0m"
		echo "1 apache install;"
		echo "2 mysql install;"
		echo "3 php install;"
		echo "4 quit;"
		echo -e "\033[32m------------------------------\033[0m"
		echo -e "\033[31mPlease imput 1 or 2 or 3 or 4.\033[0m";
		;;
esac
done
