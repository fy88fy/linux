[toc]
## 12.1 LNMP架构介绍
- 和LAMP不同的，提供web服务的是Nginx
- 并且php是作为一个独立服务存在的，这个服务叫php-fpm
- Nginx直接处理静态请求，动态请求会转发给php-fpm
- nginx 处理静态请求能力很强，比apache强，图片类的静态网站并发可以达到好几万.
![image](http://note.youdao.com/yws/api/personal/file/896D4A1B313F46D28E8D66BA9BA239B3?method=download&shareKey=bd4cab549faf85c1ddd5740d1d861ce3)
## 12.2 MySQL安装


```
mysql安装步骤：  
   53 tar -zxvf mysql-5.6.35-linux-glibc2.5-x86_64.tar.gz 
   54  mv mysql-5.6.35-linux-glibc2.5-x86_64 /usr/local/mysql
   55  cd /usr/local/mysql
   59  useradd -M -s /sbin/nologin mysql
   60  mkdir -p /data/mysql
   61  chown mysql.mysql /data/mysql
   63  rm -rf /etc/my.cnf
   65  ./scripts/mysql_install_db --user=mysql --datadir=/data/mysql/
   67  cd support-files/
   69  cp mysql.server /etc/init.d/mysqld
   70  vim /etc/init.d/mysqld
   73  cp ../my.cnf /etc/
   74  vim /etc/my.cnf
   75  setenforce 0
   76  vim /etc/selinux/config 
   77  systemctl disable firewalld.service 
   78  systemctl stop firewalld.service 
   81  /etc/init.d/mysqld start
   
   
   
 To start mysqld at boot time you have to copy
support-files/mysql.server to the right place for your system

PLEASE REMEMBER TO SET A PASSWORD FOR THE MySQL root USER !
To do so, start the server, then issue the following commands:

  ./bin/mysqladmin -u root password 'new-password'
  ./bin/mysqladmin -u root -h fxq-2 password 'new-password'

Alternatively you can run:

  ./bin/mysql_secure_installation

which will also give you the option of removing the test
databases and anonymous user created by default.  This is
strongly recommended for production servers.

See the manual for more instructions.

You can start the MySQL daemon with:

  cd . ; ./bin/mysqld_safe &

You can test the MySQL daemon with mysql-test-run.pl

  cd mysql-test ; perl mysql-test-run.pl

Please report any problems at http://bugs.mysql.com/

The latest information about MySQL is available on the web at

  http://www.mysql.com

Support MySQL by buying support/licenses at http://shop.mysql.com

New default config file was created as ./my.cnf and
will be used by default by the server when you start it.
You may edit this file to change server settings
  
   

```

### 1.安装包类型
    （1）rpm 
    （2）源码
    （3）二进制免编译
### 2.安装
    (1) cd /usr/local/src/
    
    (2) wget 对应的包
        5.6_32位二进制包：wget http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.35-linux-glibc2.5-i686.tar.gz
        5.6_64位二进制包：wget http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.35-linux-glibc2.5-x86_64.tar.gz
    
    (3) tar -zxvf mysql-5.6.35-linux-glibc2.5-i686.tar.gz
    
    (4) cp -R  mysql-5.6.35-linux-glibc2.5-i686 /usr/local/mysql
    
    (5) cd /usr/local/mysql/
    
    (6) useradd mysql
    
    (7) mkdir /data/
    
    (8) ./scripts/mysql_install_db --user=mysql --datadir=/data/mysql
    
        查询提示少的包，通过yum进行查找:yum list | grep -i perl | grep -i dumper
    
    (9) cp support-files/my-default.cnf /etc/my.cnf
    
    (10) vi /etc/my.cnf
        改为:
        datadir=/data/mysql
        socket=/tmp/mysql.sock




**安装报错1：**

./scripts/mysql_install_db --user=mysql --datadir=/data/mysql
        后报错：
    
```
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# ./scripts/mysql_install_db --user=mysql --da
tadir=/data/mysql/FATAL ERROR: please install the following Perl modules before executing ./scripts/mysql_ins
tall_db:Data::Dumper

```
     解决方法：yum install perl-Data-Dumper
    
**安装报错2：**
```
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# ./scripts/mysql_install_db --user=mysql --da
tadir=/data/mysql/Installing MySQL system tables..../bin/mysqld: error while loading shared libraries: libaio
.so.1: cannot open shared object file: No such file or directory[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# 

```
    解决方法: yum install libaio
  
```
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# ls /etc/my.cnf
/etc/my.cnf
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# rpm -qf /etc/my.cnf
mariadb-libs-5.5.52-1.el7.i686
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# 

```
    (11) cp support-files/mysql.server /etc/init.d/mysqld
    
    (12) vi /etc/init.d/mysqld
        
        加入下面两行的配置:
        basedir=/usr/local/mysql
        datadir=/data/mysql
    
    (13) service mysqld start
        /etc/init.d/mysqld start
        /usr/local/mysql/bin/mysqld_safe --datadir=/data/mysql --pid-file=/data/mysql/fxq-0.pid &
        /usr/local/mysql/bin/mysqld_safe --defaults-file=/etc/my.cnf --user=mysql --datadir=/data/mysql &
    
**启动报错：**
```
报错一：
[root@fxq-2 mysql]# /etc/init.d/mysqld start
Starting MySQL.171027 22:41:11 mysqld_safe error: log-error set to '/data/mysql//fxq-2.err', however file don't exists. Create writable for user 'my
 ERROR! The server quit without updating PID file (/data/mysql//fxq-2.pid).


解决办法：
系统自动安装了my.cnf
yum remove mariadb-libs-5.5.52-1.el7.x86_64 
然后再安装启动就不正常了




报错二：

service mysqld start
/etc/init.d/mysqld: line 256: my_print_defaults: command not found
/etc/init.d/mysqld: line 276: cd: /usr/local/mysql: No such file or directory
Starting MySQL ERROR! Couldn't find MySQL server (/usr/local/mysql/bin/mysqld_safe)
```
    (没有执行：cp -R  mysql-5.6.35-linux-glibc2.5-i686 /usr/local/mysql)

**杀死进程经验**：

    killall 先停止读写，比kill -9 靠谱
    killall之后，如果等了一段时间进程还在。说明还有数据库在写入硬盘，不可强行杀死，否则可能会造成数据丢失。


## 12.3/12.4 PHP安装

```
安装步骤：
  159  cd /usr/local/src/
  160  tar -jxvf php-5.6.30.tar.bz2 
  161  cd php-5.6.30/
  163  useradd -M -s /sbin/nologin php-fpm
  164  yum install -y libxml2-devel curl-devel libjpeg-devel libpng-devel freetype-devel libmcrypt-devel openssl-devel
  175  ./configure --prefix=/usr/local/php-fpm --with-config-file-path=/usr/local/php-fpm/etc --enable-fpm --with-fpm-user=php-fpm --with-fpm-group=php-fpm --with-mysql=/usr/local/mysql --with-mysqli=/usr/local/mysql/bin/mysql_config --with-pdo-mysql=/usr/local/mysql --with-mysql-sock=/tmp/mysql.sock --with-libxml-dir --with-gd --with-jpeg-dir --with-png-dir --with-freetype-dir --with-iconv-dir --with-zlib-dir --with-mcrypt --enable-soap --enable-gd-native-ttf --enable-ftp --enable-mbstring --enable-exif --with-pear --with-curl  --with-openssl
  177  make && make install 
  185  cp php.ini-production /usr/local/php-fpm/etc/php.ini
  190  vim /usr/local/php-fpm/etc/php-fpm.conf
  198  cp sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm
  200  chmod 755 /etc/init.d/php-fpm
  202  chkconfig --add php-fpm
  203  chkconfig php-fpm on
  206  service php-fpm start
  207  ps aux | grep php-fpm

```


```

 和LAMP安装PHP方法有差别，需要开启php-fpm服务
 cd /usr/local/src/
 wget http://cn2.php.net/distributions/php-5.6.30.tar.gz
 tar zxf php-5.6.30.tar.gz
 useradd -s /sbin/nologin php-fpm
 yum install -y libxml2-devel curl-devel libjpeg-devel libpng-devel freetype-devel libmcrypt-devel openssl-devel
 cd php-5.6.30
 ./configure --prefix=/usr/local/php-fpm --with-config-file-path=/usr/local/php-fpm/etc --enable-fpm --with-fpm-user=php-fpm --with-fpm-group=php-fpm --with-mysql=/usr/local/mysql --with-mysqli=/usr/local/mysql/bin/mysql_config --with-pdo-mysql=/usr/local/mysql --with-mysql-sock=/tmp/mysql.sock --with-libxml-dir --with-gd --with-jpeg-dir --with-png-dir --with-freetype-dir --with-iconv-dir --with-zlib-dir --with-mcrypt --enable-soap --enable-gd-native-ttf --enable-ftp --enable-mbstring --enable-exif --with-pear --with-curl  --with-openssl

```

**报错1：**

checking for xml2-config path... 
configure: error: xml2-config not found. Please check your libxml2 installation.

[root@fxq-1 php-5.6.30]# yum install libxml2-devel


**报错2：**

configure: error: Cannot find OpenSSL's <evp.h>

[root@fxq-1 php-5.6.30]# yum install openssl-devel

**报错3：**

checking for cURL support... yes
checking for cURL in default path... not found
configure: error: Please reinstall the libcurl distribution -
    easy.h should be in <curl-dir>/include/curl/
    
[root@fxq-1 php-5.6.30]# yum install curl-devel


**报错4：**

configure: error: jpeglib.h not found.

[root@fxq-1 php-5.6.30]# yum install libjpeg-turbo-devel

**报错5：**

configure: error: png.h not found.

yum install libpng-devel

**报错6：**

If configure fails try --with-xpm-dir=<DIR>
configure: error: freetype-config not found.

[root@fxq-1 php-5.6.30]# yum install freetype-devel


**报错7：**

configure: error: mcrypt.h not found. Please reinstall libmcrypt.

yum install epel-release
yum install libmcrypt-devel

```

 make && make install
 
 
 
 /usr/local/php-fpm/sbin/php-fpm -m
 /usr/local/php-fpm/sbin/php-fpm -i
 /usr/local/php-fpm/sbin/php-fpm -t

 cp php.ini-production /usr/local/php-fpm/etc/php.ini
 vi /usr/local/php/etc/php-fpm.conf //写入如下内容（参考https://coding.net/u/aminglinux/p/aminglinux-book/git/blob/master/D15Z/php-fpm.conf）
 
[global]
pid = /usr/local/php-fpm/var/run/php-fpm.pid
error_log = /usr/local/php-fpm/var/log/php-fpm.log
[www]
listen = /tmp/php-fcgi.sock
listen.mode = 666
user = php-fpm
group = php-fpm
pm = dynamic
pm.max_children = 50
pm.start_servers = 20
pm.min_spare_servers = 5
pm.max_spare_servers = 35
pm.max_requests = 500
rlimit_files = 1024
 
 
 cp sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm
 chmod 755 /etc/init.d/php-fpm

 chkconfig --add php-fpm
 chkconfig php-fpm on
 或
 systemctl enable php-fpm 
 
 
 service php-fpm start

 ps aux |grep php-fpm
 
 
 [root@fxq-1 php-5.6.30]# ls -l /tmp/php-fcgi.sock 
srw-rw-rw- 1 root root 0 10月 18 23:30 /tmp/php-fcgi.sock
[root@fxq-1 php-5.6.30]#
```
book.aminglinux.com

## 12.5 Nginx介绍
```
 Nginx官网 nginx.org，最新版1.13，最新稳定版1.12 
 Nginx应用场景：web服务、反向代理、负载均衡
 Nginx著名分支，淘宝基于Nginx开发的Tengine，使用上和Nginx一致，服务名，配置文件名都一样，和Nginx的最大区别在于Tenging增加了一些定制化模块，在安全限速方面表现突出，另外它支持对js，css合并
 Nginx核心+lua相关的组件和模块组成了一个支持lua的高性能web容器openresty，参考http://jinnianshilongnian.iteye.com/blog/2280928

```
最新稳定版本：stable 稳定版nginx-1.12.2 

## 扩展
Nginx为什么比Apache Httpd高效：原理篇 http://www.toxingwang.com/linux-unix/linux-basic/1712.html

apache和nginx工作原理比较 http://www.server110.com/nginx/201402/6543.html

mod_php 和 mod_fastcgi以及php-fpm的比较 http://dwz.cn/1lwMSd

概念了解：CGI，FastCGI，PHP-CGI与PHP-FPM http://www.nowamagic.net/librarys/veda/detail/1319/ https://www.


aw


aimai.com/371.html