#!/bin/bash
#date=2017-06-26
#Auther is fengxiaoqing
#
# Shell script scripts to delete user,and write to a file.
# -------------------------------------------------------------------------
# Copyright (c) 2017 fxq project <http://www.fy88fy.cc/>
# This script is licensed under GNU GPL version 2.0 or above
# -------------------------------------------------------------------------
# This script is part of fxq shell script collection (NSSC)
# Visit http://www.fy88fy.cc/ for more information.
# -------------------------------------------------------------------------
# 
check_disk()
{
number_disk=`df -h | grep "^/dev/" |awk '{print $(NF-1)}' | sed "s/%//"`
part_disk=`df -h | grep "^/dev/" |awk '{print $NF}'`
log_disk=/tmp/`date "+%F"`_disk.log

if [[ $number_disk -gt 3 ]]; then
        echo "The dev ($part_disk) is leave a little . Please check.----" > $log_disk
        mail -s "The dev ($part_disk) is leave a little . Please check."  18210031208@163.com < $log_disk
fi
}

check_disk_inode()
{
number_inode=`df -i | grep "^/dev/" |awk '{print $(NF-1)}' | sed "s/%//"`
inode_disk_part=`df -i | grep "^/dev/" |awk '{print $NF}'`
log_disk_inode=/tmp/`date "+%F"`_disk_inode.log

if [[ $number_inode -gt 3 ]]; then
        echo "The dev  ($inode_disk_part) inode is leave a little . Please check.----`date "+%F %T"`" >$log_disk_inode
        mail -s "The dev ($inode_disk_part)  inode is leave a little . Please check." 18210031208@163.com < $log_disk_inode
fi
}




while :
do
ps aux |grep sendmail |grep -v grep &>/dev/null

if [[ $? -ne 0 ]];then
	service sendmail restart
fi

ss -tunl |grep 25 |grep -v grep  &>/dev/null

if [[ $? -ne 0 ]]; then
	yum install sendmail -y && service sendmail start 
fi


check_disk
check_disk_inode

sleep 30
done
