[toc]
## 16.1 Tomcat介绍
```
 Tomcat是Apache软件基金会（Apache Software Foundation）的Jakarta项目中的一个核心项目，由Apache、Sun和其他一些公司及个人共同开发而成。
 java程序写的网站用tomcat+jdk来运行
 tomcat是一个中间件，真正起作用的，解析java脚本的是jdk
 jdk（java development kit）是整个java的核心，它包含了java运行环境和一堆java相关的工具以及java基础库。
 最主流的jdk为sun公司发布的jdk，除此之外，其实IBM公司也有发布JDK，CentOS上也可以用yum安装openjdk

```
## 16.2 安装jdk
```
 jdk版本1.6，1.7，1.8
 官网下载地址 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
 下载jdk8，放到/usr/local/src/目录下
 tar zxvf jdk-8u144-linux-x64.tar.gz
 mv jdk1.8.0_144 /usr/local/jdk1.8
 vi /etc/profile //最后面增加
JAVA_HOME=/usr/local/jdk1.8/
JAVA_BIN=/usr/local/jdk1.8/bin
JRE_HOME=/usr/local/jdk1.8/jre
PATH=$PATH:/usr/local/jdk1.8/bin:/usr/local/jdk1.8/jre/bin
CLASSPATH=/usr/local/jdk1.8/jre/lib:/usr/local/jdk1.8/lib:/usr/local/jdk1.8/jre/lib/charsets.jar 
 source /etc/profile
 java -version


[root@fxq-1 jdk1.8]# java -version
java version "1.8.0_151"
Java(TM) SE Runtime Environment (build 1.8.0_151-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.151-b12, mixed mode)


```
## 16.3 安装Tomcat
```
 cd /usr/local/src
 wget https://mirrors.cnnic.cn/apache/tomcat/tomcat-8/v8.5.23/bin/apache-tomcat-8.5.23.tar.gz
 tar zxvf apache-tomcat-8.5.23.tar.gz
 mv apache-tomcat-8.5.23 /usr/local/tomcat
 /usr/local/tomcat/bin/startup.sh
 ps aux|grep tomcat
 netstat -lntp |grep java
 三个端口8080为提供web服务的端口，8005为管理端口，8009端口为第三方服务调用的端口，比如httpd和Tomcat结合时会用到



[root@fxq-2 bin]# ps aux | grep java
root       3284  5.0 18.0 2170800 73084 pts/1   Sl   23:49   0:07 /usr/local/jdk1.8/bin/java -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -classpath /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar -Dcatalina.base=/usr/local/tomcat -Dcatalina.home=/usr/local/tomcat -Djava.io.tmpdir=/usr/local/tomcat/temp org.apache.catalina.startup.Bootstrap start
root       3350  0.0  0.2 112668   972 pts/1    S+   23:52   0:00 grep --color=auto java
[root@fxq-2 bin]# 



[root@fxq-2 bin]# netstat -lntp | grep java
tcp6       0      0 :::8080                 :::*                    LISTEN      3284/java           
tcp6       0      0 127.0.0.1:8005          :::*                    LISTEN      3284/java           
tcp6       0      0 :::8009                 :::*                    LISTEN      3284/java           
[root@fxq-2 bin]# 




[root@fxq-2 bin]# ./shutdown.sh 
Using CATALINA_BASE:   /usr/local/tomcat
Using CATALINA_HOME:   /usr/local/tomcat
Using CATALINA_TMPDIR: /usr/local/tomcat/temp
Using JRE_HOME:        /usr/local/jdk1.8
Using CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar

[root@fxq-2 bin]# 
[root@fxq-2 bin]# ps aux | grep tomcat
root       3381  1.0  0.2 112668   968 pts/1    R+   23:52   0:00 grep --color=auto tomcat
[root@fxq-2 bin]# ./startup.sh 
Using CATALINA_BASE:   /usr/local/tomcat
Using CATALINA_HOME:   /usr/local/tomcat
Using CATALINA_TMPDIR: /usr/local/tomcat/temp
Using JRE_HOME:        /usr/local/jdk1.8
Using CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar
Tomcat started.
[root@fxq-2 bin]# ps aux | grep tomcat
root       3405 25.3  7.2 2118488 29296 pts/1   Sl   23:53   0:00 /usr/local/jdk1.8/bin/java -Djava.util.logging.config.file=/usr/local/tomcat/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -classpath /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar -Dcatalina.base=/usr/local/tomcat -Dcatalina.home=/usr/local/tomcat -Djava.io.tmpdir=/usr/local/tomcat/temp org.apache.catalina.startup.Bootstrap start
root       3417  0.0  0.2 112668   968 pts/1    R+   23:53   0:00 grep --color=auto tomcat
[root@fxq-2 bin]# 


```

## 扩展
java容器比较 http://my.oschina.net/diedai/blog/271367==

```
现在流行的Java EE容器有很多：Tomcat、JBoss、Resin、Glassfish等等。下面对这四种Java EE容器进行了一番简单的比对。 1. Tomcat是Apache鼎力支持的Java Web应用服务器，由于它优秀的稳定性以及丰富的文档资料，广泛的使用人群，从而在开源领域受到最广泛的

                                 

现在流行的Java EE容器有很多：Tomcat、JBoss、Resin、Glassfish等等。下面对这四种Java EE容器进行了一番简单的比对。

　　1. Tomcat是Apache鼎力支持的Java Web应用服务器(注：servlet容器)，由于它优秀的稳定性以及丰富的文档资料，广泛的使用人群，从而在开源领域受到最广泛的青睐。

　　2. Jboss作为Java EE应用服务器，它不但是Servlet容器，而且是EJB容器，从而受到企业级开发人员的欢迎，从而弥补了Tomcat只是一个Servlet容器的缺憾。

　　3. Resin也仅仅是一个Servlet容器，然而由于它优秀的运行速度，使得它在轻量级Java Web领域备受喜爱，特别是在互联网Web服务领域，众多知名公司都采用其作为他们的Java Web应用服务器，譬如163、ku6等。

　　在商用应用服务器里主要有：Weblogic、Websphere,其中Weblogic我也使用过很长一段时间，当时也只用其当Servlet容器，然而就在同等条件下，在性能及易用性等方面，要比Tomcat优秀很多。

　　4.glassfish是Sun公司推出的Java EE服务器(Java EE容器)，一个比较活跃的开源社区，不断的通过社区的反馈来提高其的可用性，经过glassfish v1 glassfish v2 到今天的glassfish v3 ,它已经走向成熟。Glassfish是一个免费、开放源代码的应用服务，它实现了Java EE 5,Java EE 5 平台包括了以下最新技术：EJB 3.0、JSF 1.2、Servlet 2.5、JSP 2.1、JAX-WS 2.0、JAXB 2.0、 Java Persistence 1.0、Common Annonations 1.0、StAX 1.0等。

　　支持集群，通过内存中会话状态复制，增强了部署体系结构的可用性与可伸缩性，它对集群有着很好的支持，可以简单到通过添加机器，就可轻松的提高网站的 带负载能力，在解析能力方面，它对html的吞吐能力与apache服务器不分上下，就是tomcat所不能比的，支持目录部署，热部署，解决了 tomcat对热部署能力的缺陷。在版本方面做的更加人性化，有开发时用的简化版，专门用于部署web项目的版本，还要完全符合j2ee标准的版本。
```



==http://www.360doc.com/content/11/0618/21/16915_127901371.shtml==


```
一、tomcat
Tomcat 服务器是一个免费的开放源代码的Web 应用服务器，它是Apache 软件基金会（Apache
Software Foundation）的Jakarta 项目中的一个核心项目，由Apache、Sun 和其他一些公司及个
人共同开发而成。由于有了Sun 的参与和支持，最新的Servlet 和JSP 规范总是能在Tomcat 中得
到体现，Tomcat 5 支持最新的Servlet 2.4 和JSP 2.0 规范。因为Tomcat 技术先进、性能稳定
，而且免费，因而深受Java 爱好者的喜爱并得到了部分软件开发商的认可，成为目前比较流行的
Web 应用服务器。
Tomcat 很受广大程序员的喜欢，因为它运行时占用的系统资源小，扩展性好，支持负载平衡与邮
件服务等开发应用系统常用的功能；而且它还在不断的改进和完善中，任何一个感兴趣的程序员
都可以更改它或在其中加入新的功能。
Tomcat 是一个小型的轻量级应用服务器，在中小型系统和并发访问用户不是很多的场合下被普遍
使用，是开发和调试JSP 程序的首选。对于一个初学者来说，可以这样认为，当在一台机器上配
置好Apache 服务器，可利用它响应对HTML 页面的访问请求。实际上Tomcat 部分是Apache 服务
器的扩展，但它是独立运行的，所以当你运行tomcat 时，它实际上作为一个与Apache 独立的进
程单独运行的。
这里的诀窍是，当配置正确时，Apache 为HTML页面服务，而Tomcat 实际上运行JSP 页面和
Servlet。另外，Tomcat和IIS、Apache等Web服务器一样，具有处理HTML页面的功能，另外它还是
一个Servlet和JSP容器，独立的Servlet容器是Tomcat的默认模式。不过，Tomcat处理静态HTML的
能力不如Apache服务器。
相关连接：
http://tomcat.apache.org/ Tomcat概述
    Tomcat是一个免费的开源的Serlvet容器，它是Apache基金会的Jakarta项目中的一个核心项
目，由Apache，Sun和其它一些公司及个人共同开发而成。由于有了Sun的参与和支持，最新的
Servlet和Jsp规范总能在Tomcat中得到体现。Tomcat被JavaWorld杂志的编辑选为2001年度最具创
新的java产品，可见其在业界的地位。
    Tomcat 最新版本是4.0x.4.0x与3.x的架构不同，而是重新设计的。Tomcat4.0x中采用了新的
Servlet容器：Catalina，完整的实现了Servlet2.3和Jsp1.2规范。Tomcat提供了各种平台的版本
供下载，可以从http://jakarta.apache.org上下载其源代码版或者二进制版。由于Java的跨平台
特性，基于Java的Tomcat也具有跨平台性。
    与传统的桌面应用程序不同，Tomcat中的应用程序是一个WAR（Web Archive）文件。WAR是
Sun提出的一种Web应用程序格式，与JAR类似，也是许多文件的一个压缩包。这个包中的文件按一
定目录结构来组织：通常其根目录下包含有Html和Jsp文件或者包含这两种文件的目录，另外还会
有一个WEB-INF目录，这个目录很重要。通常在WEB-INF目录下有一个web.xml文件和一个classes
目录，web.xml是这个应用的配置文件，而classes目录下则包含编译好的Servlet类和Jsp或
Servlet所依赖的其它类（如JavaBean）。通常这些所依赖的类也可以打包成JAR放到WEB-INF下的
lib目录下，当然也可以放到系统的CLASSPATH中，但那样移植和管理起来不方便。
    在Tomcat中，应用程序的部署很简单，你只需将你的WAR放到Tomcat的webapp目录下，Tomcat
会自动检测到这个文件，并将其解压。你在浏览器中访问这个应用的Jsp时，通常第一次会很慢，
因为Tomcat要将Jsp转化为Servlet文件，然后编译。编译以后，访问将会很快。另外Tomcat也提
供了一个应用：manager，访问这个应用需要用户名和密码，用户名和密码存储在一个xml文件中
。通过这个应用，辅助于Ftp，你可以在远程通过Web部署和撤销应用。当然本地也可以。
    Tomcat不仅仅是一个Servlet容器，它也具有传统的Web服务器的功能：处理Html页面。但是
与Apache相比，它的处理静态Html的能力就不如Apache.我们可以将Tomcat和Apache集成到一块，
让Apache处理静态Html，而Tomcat处理Jsp和Servlet.这种集成只需要修改一下Apache和Tomcat的
配置文件即可。
    另外，Tomcat提供Realm支持。Realm类似于Unix里面的group.在Unix中，一个group对应着系
统的一定资源，某个group不能访问不属于它的资源。Tomcat用Realm来对不同的应用（类似系统
资源）赋给不同的用户（类似group）。没有权限的用户则不能访问这个应用。Tomcat提供三种
Realm，1：JDBCRealm，这个Realm将用户信息存在数据库里，通过JDBC获得用户信息来进行验证
。2：JNDIRealm，用户信息存在基于LDAP的服务器里，通过JNDI获取用户信息。3：MemoryRealm
，用户信息存在一个xml文件里面，上面讲的manager应用验证用户时即使用此种Realm.通过Realm
我们可以方便地对访问某个应用的客户进行验证。
    在Tomcat4中，你还可以利用Servlet2.3提供的事件监听器功能，来对你的应用或者Session
实行监听。Tomcat也提供其它的一些特征，如与SSL集成到一块，实现安全传输。还有Tomcat也提
供JNDI支持，这与那些J2EE应用服务器提供的是一致的。说到这里我们要介绍一下通常所说的应
用服务器（如WebLogic）与Tomcat有何区别。应用服务器提供更多的J2EE特征，如EJB，JMS，
JAAS等，同时也支持Jsp和Servlet.而Tomcat则功能没有那么强大，它不提供EJB等支持。但如果
与JBoss（一个开源的应用服务器）集成到一块，则可以实现J2EE的全部功能。既然应用服务器具
有Tomcat的功能，那么Tomcat有没有存在的必要呢？事实上，我们的很多中小应用不需要采用EJB
等技术，Jsp和Servlet已经足够，这时如果用应用服务器就有些浪费了。而Tomcat短小精悍，配
置方便，能满足我们的需求，这种情况下我们自然会选择Tomcat.
    基于Tomcat的开发其实主要是Jsp和Servlet的开发，开发Jsp和Servlet非常简单，你可以用
普通的文本编辑器或者IDE，然后将其打包成WAR即可。我们这里要提到另外一个工具Ant，Ant也
是Jakarta中的一个子项目，它所实现的功能类似于Unix中的make.你需要写一个build.xml文件，
然后运行Ant就可以完成xml文件中定义的工作，这个工具对于一个大的应用来说非常好，我们只
需在xml中写很少的东西就可以将其编译并打包成WAR.事实上，在很多应用服务器的发布中都包含
了Ant.另外，在Jsp1.2中，可以利用标签库实现Java代码与Html文件的分离，使Jsp的维护更方便
。
    Tomcat也可以与其它一些软件集成起来实现更多的功能。如与上面提到的JBoss集成起来开发
EJB，与Cocoon（Apache的另外一个项目）集成起来开发基于Xml的应用，与OpenJMS
    集成起来开发JMS应用，除了我们提到的这几种，可以与Tomcat集成的软件还有很多。
二、Jboss

JBoss是全世界开发者共同努力的成果，一个基于J2EE的开放源代码的应用服务器。 因为JBoss代
码遵循LGPL许可，你可以在任何商业应用中免费使用它，而不用支付费用。JBoss支持EJB 1.1和
EJB 2.0 EJB3.0的规范，它是一个管理EJB的容器和服务器。类似于Sun's J2SDK Enterprise
Edition（J2EE），JBoss的目标是一个源代码开放的J2EE环境。但是JBoss核心服务仅是提供EJB
服务器。JBoss不包括serverlers/JSP page 的WEB容器，当然可以和Tomcat或Jetty绑定使用。
JBoss还具有如下六大优点：
1、JBoss是免费的，开放源代码J2EE的实现，它通过LGPL许可证进行发布。
2、JBoss需要的内存和硬盘空间比较小。
3、安装非常简单。先解压缩JBoss打包文件再配置一些环境变量就可以了。
4、JBoss能够"热部署"，部署BEAN只是简单拷贝BEAN的JAR文件到部署路径下就可以了。如果没有
加载就加载它；如果已经加载了就卸载掉，然后LOAD这个新的。
5、JBoss与Web服务器在同一个Java虚拟机中运行，Servlet调用EJB不经过网络，从而大大提高运
行效率，提升安全性能。
6、用户可以直接实施J2EE-EAR，而不是以前分别实施EJB-JAR和Web-WAR，非常方便。
JBoss的安装和配置可以直接拷贝使用，但是要改动 %JBoss-HOME%\bin\run.bat里JAVA-HOME的设
置，改成本机JDK的目录。运行run.bat来启动JBoss
关闭JBoss：关闭JBoss的 DOS 窗口或按"CTRL + C"
JBoss的目录结构
1、bin:开始和停止JBoss的地方。
其中有两个主要的批处理文件：run.bat和shutdown.bat。要启动JBoss只要执行run.bat文件即可
；要停止JBoss必须执行shutdown.bat。
注意，shutdown.bat文件直接执行并不会自动停止JBoss，它必须要输入参数，参数意义如下：
h显示帮助信息；D设置系统属性；-停止处理选项；s停止用JNDI URL指定的远程服务；n通过制定
JMX名字来停止服务；a适配到指定的JNDI名称的远程服务；u指定用户名称；p指定用户密码；S停
止服务器；e从虚拟机退出；H暂停。
一般，我们使用S选项来停止服务器。即执行shutdown.bat -S。
2、docs:放置JBoss的例子、测试脚本和各种脚本配置文件的DTD。
3、lib:放置JBoss所需要的部分jar包文件。
4、client:放置EJB客户端运行时所需要的jar包。
5、server:放置各启动类型的服务器端EJB配置所需要的文件等。
6、backup
以80端口服务
修改这个文件： {%JBOSS_HOME%}\server\default\deploy\jbossweb-tomcat41.sar\META-
INF\jboss-service.xml
在"8080"的配置后加入以下代码
<!-- A HTTP/1.1 Connector on port 80 -->
<Connector className="org.apache.coyote.tomcat4.CoyoteConnector"
port="80" minProcessors="5" maxProcessors="100"
enableLookups="true" acceptCount="10" debug="0"
connectionTimeout="20000" useURIValidationHack="false"/>
重新启动JBoss就OK了
JBoss的启动过程
设置环境变量 JBOSS_CLASSPATH (可以自己加上安全管理器和xml解析器)
a) %JBOSS_CLASSPATH%;
b) %JAVA_HOME%/lib/tools.jar;
c) run.jar
d) ../lib/crimson.jar；
设置启动参数JAXP(xml解析器和相应工厂)
启动
设置配置信息
读取配置文件JBoss.properties，保存在系统属性中(System.properties)
设置缺省属性jboss.home和java.security.auth.login.config
创建MBeanServer.的实例:
把配置文件和补丁文件所在的目录指定给特定的远程类加载器Mlet
加载保存配置文件（mlet会自动在配置文件目录中查找）
初始化并启动MBean
配置服务ConfigurationService
加载配置文件
保存配置
服务控制ServiceControl
初始化服务程序（init方法）
启动服务程序（start方法）
在JBOSS中发布文件
1、制作JSP的war部署文件
用"jar cvf hello.war index.jsp main.jsp"的方式生成把index.jsp和main.jsp文件加入到
hello.war中。把生成的hello.war拷贝到jboss安装目录\server\default\deploy\下，部署成功
。用"http://localhost:8080/hello/"或"http://localhost:8080/hello/index.jsp"这两个地址
进行测试。
2、制作Servlet的war部署文件
A、用"javac -classpath "%CLASSPATH%;%jboss_home%
\server\default\lib\javax.servlet.jar" HelloWorld.java"的形式编译HelloWorld.java（这
是一个Servlet）得到HelloWorld.class
B、写一个web.xml配置文件
代码如下：
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE web-app
PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
"http://java.sun.com/dtd/web-app_2_3.dtd">
<web-app>
<servlet>
<servlet-name>HelloWorld</servlet-name>
<servlet-class>hello.HelloWorld</servlet-class>
</servlet>
<servlet-mapping>
<servlet-name>HelloWorld</servlet-name>
<url-pattern>/HelloWorld</url-pattern>
</servlet-mapping>
</web-app>
每个标记必须小写，否则会出错。
C、先在c:\servlet下建立一个web-inf目录，在目录中放入web.xml文件。
D、在web-inf目录下建立一个classes目录，放入编译好的HelloWorld.class文件
E、在c:\servlet下执行jar cvf hello.war *.*，很快就生成了一个名为hello.war的文件，接着
把hello.war复制到jboss的安装目录\server\default\deploy\下完成部署。
3、也可以不打包，把文件放在deploy下的同名文件夹下也可以。比如原来是制作成hello.war文
件，可以建立一个名称为hello.war的文件夹，把要发布的文件拷贝到这个目录底下也可以发布成
功。
补充
1、JBoss的默认端口是8080
3、对于NT和Win2000系统如果你要安装NT或Win2000服务的话，可以把JBoss加在系统服务中，避
免你每次需要到JBoss目录下运行run.bat命令，只是当系统启动时，自动启动JBoss服务器。
三、weblogic

WebLogic是美国bea公司出品的一个application server确切的说是一个基于j2ee架构的中间件，
webserver是用来构建网站的必要软件用来解析发布网页等功能，它是用纯java开发的。weblogic
本来不是由bea发明的，是它从别人手中买过来，然后再加工扩展。目前weblogic在世界
application server市场上占有最大的份额，其他还有象IBM的websphere，免费的tomcat、resin
等中间件。
BEA WebLogic是用于开发、集成、部署和管理大型分布式Web应用、网络应用和数据库应用的Java
应用服务器。将Java的动态功能和Java Enterprise标准的安全性引入大型网络应用的开发、集成
、部署和管理之中。
BEA WebLogic Server拥有处理关键Web应用系统问题所需的性能 、可扩展性和高可用性。
与BEA WebLogic Commerce ServerTM配合使用， BEA WebLogic Server可为部署适应性个性化电
子商务应用系统提供完善的解决方案。
BEA WebLogic Server具有开发和部署关键任务电子商务Web应用系统 所需的多种特色和优势，包
括：
1)领先的标准
对业内多种标准的全面支持，包括EJB、JSB、JMS、JDBC、XML和WML，使Web应用系统的实施更为
简单，并且保护了投资，同时也使基于标准的解决方案的开发更加简便。
2)无限的可扩展性
BEA WebLogic Server以其高扩展的架构体系闻名于业内，包括客户机连接的共享、资源pooling
以及**页和EJB组件群集。
3)快速开发
凭借对EJB和JSP的支持，以及BEA WebLogic Server 的Servlet组件架 构体系，可加速投放市场
速度。这些开放性标准与WebGain Studio配合时，可简化开发，并可发挥已有的技能，迅速部署
应用系统。
4)部署更趋灵活
BEA WebLogic Server的特点是与领先数据库、操作系统和Web服务器 紧密集成。
5)关键任务可靠性
其容错、系统管理和安全性能已经在全球数以千记的关键任务环境中得以验证。
6)体系结构
BEA WebLogic Server是专门为企业电子商务应用系统开发的。企业电子商务应用系统需要快速开
发，并要求服务器端组件具有良好的灵活性和安全性，同时还要支持关键任务所必需的扩展、性
能、和高可用性。BEA WebLogic Server简化了可移植及可扩展的应用系统的开发，并为其它应用
系统和系统提供了丰富的互操作性。
凭借其出色的群集技术，BEA WebLogic Server拥有最高水平的可扩展 性和可用性。BEA
WebLogic Server既实现了网页群集，也实现了EJB组件 群集，而且不需要任何专门的硬件或操作
系统支持。网页群集可以实现透明的复制、负载平衡以及表示内容容错，如Web购物车；组件群集
则处理复杂的复制、负载平衡和EJB组件容错，以及状态对象（如EJB实体）的恢复。
无论是网页群集，还是组件群集，对于电子商务解决方案所要求的可扩展性和可用性都是至关重
要的。共享的客户机/服务器和数据库连接以及数据缓存和EJB都增强了性能表现。这是其它Web应用系统所不具备的。
```


==j2ee、j2se、ejb、javabean、serverlet、jsp之间关系 http://bbs.csdn.net/topics/50015576==

```
Java是根（也是一门具体的语言）；

J2EE、J2SE是Java在不同领域的具体应用；

J2SE用于应用程序开发，类似Windows平台应用软件；

J2EE是个大概念，用于Web电子商务等方面；

EJB、Servlet和JavaBean的综合灵活运用能形成优秀的J2EE平台；

你可以把Jsp看作是对Servlet的进一步封装。
```


tomcat server.xml配置详解 http://blog.csdn.net/yuanxuegui2008/article/details/6056754
==tomcat常用数据库连接的方法 http://wjw7702.blog.51cto.com/5210820/1109263==


```
一、用于数据库连接的术语：
    JDBC：（Java database connectivity）是基于java数据访问技术的一个API通过客户端访问服务器的数据库，是一个面向关系型数据库并提供一种方法查询和更新数据库；
    JNDI：(Java naming and directory interface)JNDI服务提供了对应用程序命名和目录功	能的一种用java程序编写的基于API的java平台；
    DataSource：是一个通过JDBC API访问关系型数据库的java对象，当与JNDI整合并在JDNI	名称服务中注册后能更好的工作；
二、tomcat连接常用数据库的操作步骤：
（1）Tomcat配置Oracle DataSource：
1、在server.xml全局文件中定义如下内容：
<GlobalNamingResources> 
<!-- Editable user database that can also be used by 
UserDatabaseRealm to authenticate users--> 
<Resource name="jdbc/tomcat7" auth="Container" 
type="javax.sql.DataSource" 
driverClassName="oracle.jdbc.OracleDriver" 
url="jdbc:oracle:thin:@127.0.0.1:1521:test" 
description="test database for tomcat 7" 
Configuration and Deployment 
[ 46 ] 
username="admin" password="admin" maxActive="20" maxIdle="10" 
maxWait="-1"/> 
</GlobalNamingResources> 
2、http://www.oracle.com/technetwork/database/enterprise-edition/jdbc-10201-088211.html 下载Oracle JDBC驱动程序类并放在CATALINA_HOME/lib/目录下，tomcat默认只接	受.jar结尾的类，如果是zip压缩格式需要将其重名为.jar结尾，然后放到	CATALINA_HOME/lib/目录下；
3、在应用下面的WEB-INF/web.xml文件中强制定义文档类型定义，示例如下：
<resource-ref> 
<description>Oracle Datasource for tomcat </description> 
<res-ref-name>jdbc/tomcat7 </res-ref-name> 
<res-type>javax.sql.DataSource</res-type> 
<res-auth>Container</res-auth> 
</resource-ref> 
4、开发人员在代码中引用JNDI并连接到数据库；
（2）Tomcat配置mysql DataSource：
1、在server.xml全局文件中定义如下内容：
<Resource name="jdbc/tomcat7" auth="Container" 
type="javax.sql.DataSource" 
maxActive="100" maxIdle="30" maxWait="10000" 
username="tomcatuser" password="tomcat" 
driverClassName="com.mysql.jdbc.Driver" 
url="jdbc:mysql://localhost:3306/tomcat7"/> 
2、在应用下面的WEB-INF/web.xml文件中强制定义文档类型定义，示例如下：
<web-app xmlns="http://java.sun.com/xml/ns/j2ee" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee 
http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd" 
version="2.4"> 
<description>Tomcat 7 test DB</description> 
<resource-ref> 
<description>DB Connection</description> 
<res-ref-name>jdbc/tomcat7</res-ref-name> 
<res-type>javax.sql.DataSource</res-type> 
<res-auth>Container</res-auth> 
</resource-ref> 
</web-app> 
3、http://dev.mysql.com/downloads/下载MYSQL JDBC驱动程序类并放在	CATALINA_HOME/lib/目录下，tomcat默认只接受.jar结尾的类，如果是zip压缩格式需	要将其重名为.jar结尾，然后放到CATALINA_HOME/lib/目录下；
4、对连接tomcat的用户授予全部权限，格式如下：
mysql> GRANT ALL PRIVILEGES ON *.* TO tomcatuser@localhost 
IDENTIFIED BY 'tomcat7' WITH GRANT OPTION; 
mysql> create database tomcat7; 
mysql> use tomcat7; 
mysql> create table testdata ( id int not null auto_increment 
primary key,foo varchar(25), bar int); 
注：用户密码一定不要为空，否则会造成连接tomcat认证错误； 
（3）Tomcat配置Postgresql DataSource：
1、在server.xml全局文件中定义如下内容：
<Resource name="jdbc/tomcat7" auth="Container" 
type="javax.sql.DataSource" 
driverClassName="org.postgresql.Driver" 
url="jdbc:postgresql://127.0.0.1:5432/tomcat7" 
username="tomcat7" password="tomcat" maxActive="20" maxIdle="10" 
maxWait="-1"/> 
2、http://jdbc.postgresql.org/download.html下载PostgreSQL JDBC驱动类并放在 CATALINA_HOME/lib/目录下，tomcat默认只接受.jar结尾的类，如果是zip压缩格式需	要将其重名为.jar结尾，然后放到CATALINA_HOME/lib/目录下；
3、在应用下面的WEB-INF/web.xml文件中强制定义文档类型定义，示例如下：
<resource-ref> 
<description>postgreSQL Tomcat datasource </description> 
<res-ref-name>jdbc/tomcat7 </res-ref-name> 
<res-type>javax.sql.DataSource</res-type> 
<res-auth>Container</res-auth> 
</resource-ref> 
```


1 tomcat  单机多实例
http://www.ttlsa.com/tomcat/config-multi-tomcat-instance/ 


2 tomcat的jvm设置和连接数设置
http://www.cnblogs.com/bluestorm/archive/2013/04/23/3037392.html 


3 jmx监控tomcat
http://blog.csdn.net/l1028386804/article/details/51547408 


4 jvm性能调优监控工具jps/jstack/jmap/jhat/jstat
http://blog.csdn.net/wisgood/article/details/25343845 

 
http://guafei.iteye.com/blog/1815222 


5 gvm gc 相关
http://www.cnblogs.com/Mandylover/p/5208055.html 

http://blog.csdn.net/yohoph/article/details/42041729 
