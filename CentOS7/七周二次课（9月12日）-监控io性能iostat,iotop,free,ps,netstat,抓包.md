[toc]
## 10.6 监控io性能
     磁盘相关命令：
        iostat -x 磁盘使用
        iotop 磁盘使用

```

Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %uti
vda               0.00     0.00    0.00    0.00     0.00     0.00     0.00     0.00    0.00    0.00    0.00   0.00   0.0

[root@0 ~]# 

```
%uti 大,磁盘有问题。


## 10.7 free命令

```
[root@0 ~]# free
              total        used        free      shared  buff/cache   available
Mem:        1016904      231184      142732       12600      642988      612300
Swap:             0           0           0
[root@0 ~]#
```

    磁盘---->内存(cache)--->cpu
    
    cpu---> 内存（buffer）-->磁盘

    free -m 
    free -h
    free -g
    
    total =used+free+buff/cache
    avaliable :包含free和buffer/cache剩余部分,实际剩余内存
    
    
    
## 10.8 ps命令
 ps 查看进程
 
 相关解释:
 
    ps aux 
    ps elf 
    STAT 状态说明：
    D 不能中断的进程
    R run状态的进程
    S sleep状态的进程
    T 暂停的进程
    Z 僵尸进程
    <谪优先级的进程管理
    N 低优先级进程
    L 内存中被锁了内存分页
    s 主进程
    l 多线程进程
    + 前台进程

## 10.9 查看网络状态
    netstat 查看网络状态
    netstat -lnp 查看监听端口
    netstat -an
    netstat -lntp 只看tcp的,不包含socket
    
    ss -an 
    netstat  -an | awk '/^tcp/ {++sta[$NF]} END {for(key in sta) print key,"\t",sta[key]}'
    

```
[root@0 ~]# netstat  -an | awk '/^tcp/ {++sta[$NF]} END {for(key in sta) print key,"\t",sta[key]}'
LISTEN 	 4
ESTABLISHED 	 2
[root@0 ~]# 

```

## 10.10 linux下抓包
    抓包工具：tcpdump
    tcpdump -nn  -n不只是主机名 -n 端口写成数字
    tcpdump -nn -i eth0 port 22 and host 113.107.112.234
    tcpdump -nn -i eth0 -c 100 -w /tmp/1.cap 抓包并写入1.cap文件.
    tcpdump -r /tmp/1.cap 读取所抓包内容
    
    tshark -n -t a -R http.request -T fields -e "frame.time" -e"ip,src" -e "http.host" -e "http.request.method" -e "httpd.request.uri
    "
    yum install -y wireshark
 
## 扩展
tcp三次握手四次挥手 http://www.doc88.com/p-9913773324388.html 


```
http://note.youdao.com/share/?id=3c5cbfe29db3814669aad15b1f0af5aa&type=note#/
```


tshark几个用法：http://www.aminglinux.com/bbs/thread-995-1-1.html 


```
默认我们的机器上是没有安装这个工具的。如果你的linux是CentOS那么就使用yum安装
yum  install  -y  wireshark
也可以到官网下载源码 http://www.wireshark.org 
具体安装方法，请参考 http://www.qtasp.cn/wiresharkcharpt/buildingwireshark.html
以下，简单介绍这个抓包工具的应用
1. 以下的用法可以显示访问http请求的域名以及uri
tshark -n -t a -R http.request -T fields -e "frame.time" -e "ip.src" -e "http.host" -e "http.request.method" -e "http.request.uri"

2. 以下可以抓取mysql的查询
tshark -n -i eth1 -R 'mysql.query' -T fields -e "ip.src" -e "mysql.query"
另外一种方法：
tshark -i eth1 port 3307  -d tcp.port==3307,mysql -z "proto,colinfo,mysql.query,mysql.query"

3. 以下可以抓取指定类型的MySQL查询
tshark -n -i eth1 -R 'mysql matches "SELECT|INSERT|DELETE|UPDATE"' -T fields -e "ip.src" -e "mysql.query"

4. 统计http的状态
tshark -n -q -z http,stat, -z http,tree
这个命令，直到你ctrl + c 才会显示出结果

5.  tshark 增加时间标签   
tshark  -t  ad
tshark  -t  a


参考  
https://ask.wireshark.org/questions/16964/analyzing-http-protocol-using-tshark


```

