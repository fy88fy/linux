### 2.10 环境变量PATH
which rm 查看命令所在目录
> 
> ```
> [root@VM_46_188_centos fxq]# which rm
> alias rm='rm -i'
> 	/usr/bin/rm
> [root@VM_46_188_centos fxq]# 
> ```
echo $PATH

> ```
> [root@VM_46_188_centos ~]# echo $PATH
> /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/usr/local/mysql/bi
> n/:/usr/local/apache2/bin/:/usr/local/php/bin/:/root/bin[root@VM_46_188_centos ~]# 
> ```
> 
> ```
> [root@VM_46_188_centos ~]# cp /usr/bin/ls /tmp/ls2
> [root@VM_46_188_centos ~]# /tmp/ls2  
> #fxq.txt       123		     gdlogo.png
> ,	       123.zip		     gdlogo.png.1
> ,.pub	       2		     httpd_process_check.sh
> 1	       2.cap		     ip.txt
> 1.cap	       2.txt		     iptables.rules
> 1.ipt	       3.txt		     null
> 1.log.tar      4.txt		     ping_host_alive.sh
> 1.log.tar.bz2  [1-3].log	     sed.txt
> 1.log.tar.gz   \fxq.txt		     shell
> 1.log.tar1.xz  a.out		     weixin
> 1.log.xz       anaconda-ks.cfg	     wordpress-4.7.4-zh_CN.zip
> 1.log.zip      auto_install_lamp.sh  youjian.sh
> 1.txt	       baidu.png	     ~iptables_rules
> 111.txt        dir-2017-05-12
> 12	       ffff
> [root@VM_46_188_centos ~]# PATH=$PATH:/tmp/
> [root@VM_46_188_centos ~]# ls2 
> #fxq.txt       123		     gdlogo.png
> ,	       123.zip		     gdlogo.png.1
> ,.pub	       2		     httpd_process_check.sh
> 1	       2.cap		     ip.txt
> 1.cap	       2.txt		     iptables.rules
> 1.ipt	       3.txt		     null
> 1.log.tar      4.txt		     ping_host_alive.sh
> 1.log.tar.bz2  [1-3].log	     sed.txt
> 1.log.tar.gz   \fxq.txt		     shell
> 1.log.tar1.xz  a.out		     weixin
> 1.log.xz       anaconda-ks.cfg	     wordpress-4.7.4-zh_CN.zip
> 1.log.zip      auto_install_lamp.sh  youjian.sh
> 1.txt	       baidu.png	     ~iptables_rules
> 111.txt        dir-2017-05-12
> 12	       ffff
> [root@VM_46_188_centos ~]# echo $PATH 
> /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/usr/local/mysql/bi
> n/:/usr/local/apache2/bin/:/usr/local/php/bin/:/root/bin:/tmp/
> 
> 
> ```

**ls2永久生效：**

> echo "PATH=$PATH:/tmp" >> /etc/profile

### 2.11 cp命令

**功能：** 复制目录或文件。

**约定:目录均加上/**

**cp /etc/passwd /tmp/ 复制文件到到目录**
> 
**cp /etc/passwd  /tmp/passwd2**
> 
> 如果目标目录tmp/中存在passwd， 会提示是否覆盖. 
> 
> 如果目标目录tmp 中没有passwd 会直接复制passwd 到tmp/中
> 

```
[root@VM_46_188_centos ~]# cp /etc/passwd /tmp/
You have new mail in /var/spool/mail/root
[root@VM_46_188_centos ~]# ls /tmp/passwd 
/tmp/passwd
[root@VM_46_188_centos ~]# 

```


```
[root@VM_46_188_centos ~]# cp /etc/passwd /tmp/
You have new mail in /var/spool/mail/root
[root@VM_46_188_centos ~]# ls /tmp/passwd 
/tmp/passwd
[root@VM_46_188_centos ~]# cp /etc/passwd /tmp/passwd2
[root@VM_46_188_centos ~]# ls /tmp/passwd2
/tmp/passwd2
[root@VM_46_188_centos ~]# ls -l /tmp/passwd2
-rw-r--r-- 1 root root 1742 Aug  6 21:27 /tmp/passwd2
[root@VM_46_188_centos ~]# cat /tmp/passwd2
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
avahi-autoipd:x:170:170:Avahi IPv4LL Stack:/var/lib/avahi-autoipd:/sb
in/nologinsystemd-bus-proxy:x:999:997:systemd Bus Proxy:/:/sbin/nologin
systemd-network:x:998:996:systemd Network Management:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:997:995:User for polkitd:/:/sbin/nologin
abrt:x:173:173::/etc/abrt:/sbin/nologin
libstoragemgmt:x:996:994:daemon account for libstoragemgmt:/var/run/l
sm:/sbin/nologintss:x:59:59:Account used by the trousers package to sandbox the tcsd 
daemon:/dev/null:/sbin/nologinntp:x:38:38::/etc/ntp:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
chrony:x:995:993::/var/lib/chrony:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
tcpdump:x:72:72::/:/sbin/nologin
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
mysql:x:27:27:MariaDB Server:/var/lib/mysql:/sbin/nologin
fxq:x:1000:1000::/home/fxq:/bin/bash
nginx:x:994:992:Nginx web server:/var/lib/nginx:/sbin/nologin
saslauth:x:993:76:Saslauthd user:/run/saslauthd:/sbin/nologin
mailnull:x:47:47::/var/spool/mqueue:/sbin/nologin
smmsp:x:51:51::/var/spool/mqueue:/sbin/nologin
test:x:1012:1012::/home/test:/sbin/nologin
zabbix:x:1013:1014::/home/zabbix:/sbin/nologin
[root@VM_46_188_centos ~]# cp /etc/passwd /tmp/passwd2
cp: overwrite '/tmp/passwd2'? y
[root@VM_46_188_centos ~]# ls /tmp/passwd2
/tmp/passwd2
[root@VM_46_188_centos ~]# 

```


**cp -r /etc/selinux/ /tmp/fxq/  复制目录到到目录**

> 如果fxq目录存在，selinux会复制到fxq目录内
> 
> 如果fxq目录不存在,sexlinux 会复制selinux到tmp下并改名为fxq
> 

```
[root@VM_46_188_centos ~]# cp -r /etc/selinux/ /tmp/fxq/
[root@VM_46_188_centos ~]# ls /tmp/fxq/
2  ls  ls2  selinux
[root@VM_46_188_centos ~]# ls !$
ls /tmp/fxq/
2  ls  ls2  selinux
[root@VM_46_188_centos ~]# 

```
**!$  表示上次的进行的命令的最后的一个参数.**


### 2.12 mv命令
**功能：** mv 移动文件或改名

在同一目录中mv 为改名


```
[root@VM_46_188_centos fxq]# ls
2  ls  ls2  selinux
[root@VM_46_188_centos fxq]# mv ls ls3
[root@VM_46_188_centos fxq]# ls
2  ls2  ls3  selinux
[root@VM_46_188_centos fxq]#
```



不同目录中mv 为移动或改名


```
[root@VM_46_188_centos fxq]# ls
2  ls2  ls3  selinux
[root@VM_46_188_centos fxq]# mv ls2 2/
[root@VM_46_188_centos fxq]# ls
2  ls3  selinux
[root@VM_46_188_centos fxq]# ls 2
ls2
[root@VM_46_188_centos fxq]#      
[root@VM_46_188_centos fxq]# 
[root@VM_46_188_centos fxq]# ls 
2  ls3  selinux
[root@VM_46_188_centos fxq]# mv ls3 2/ls4
[root@VM_46_188_centos fxq]# ls
2  selinux
[root@VM_46_188_centos fxq]# ls 2
ls2  ls4
[root@VM_46_188_centos fxq]# 

```

**移动的是文件时**

目标是目录，移动文件到目标目录.

目录是文件时，则改名至目标目录.

**移动的是目录时**

目标目录存在时，会放到目标目录内。
目标目录不存在时，刚改名到相应目录。

### 2.13 文档查看cat/more/less/head/tail

#### cat 查看文件内容
> cat -A 显示所有字符
> cat -n /etc/passwd 显示内容带行号
> 
#### tac 倒序显示内容

#### more 分屏显示内容

> 空格向下一屏,
> 
> ctrl+ f 
> 
> ctrl+ b 向上看
> 
> 回车一行一行显示.
> 
> 
> wc -l  /etc/passwd 查看文件多少行。

#### less

> 空格向下一屏,
> 
> ctrl+ f 
> 
> ctrl+ b 向上看
> 
> 回车一行一行显示.
> 
> 箭头向上向下一行一行的
> 
> / 查找关键字 n向下 N向上
> 
> ? 从后往前查找 n向上  N向下
> 
> G 快速定们到尾行

> g 定位到首行

#### head 

> head /etc/passwd 查看文件前十行。
> 
> head -n 2 /etc/passwd查看前两行
> 
> head -n2 /etc/passwd查看前两行(另一种写法)
#### tail 

> tail /etc/passwd 查看文件后十行。
> 
> tail -n 2 /etc/passwd 查看后两行
> tail -n2 /etc/passwd 查看后两行(另一种写法)
> 
> tail -f /etc/passwd 动态查看文件变化