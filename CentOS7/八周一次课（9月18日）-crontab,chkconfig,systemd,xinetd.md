[toc]
## 10.23 linux任务计划cron
### cat /etc/crontab

```
[root@fxq-0 ~]# cat /etc/crontab 
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed

```
### crontab -e 

```
0 3 * * * root ifconfig >>/tmp/cron.log 2>>/tmp/con.log
0 3 1-10 */2 * ifconfig 
```
### 查看任务计划:
    crontab -l
### 删除任务计划
    crontab -r
### 备份 
    备份目录： /var/spool/cron/ 



## 10.24 chkconfig工具
    chkconfig --list
    chkconfig --level 3 network off
    chkconfig --level 345 network off
    chkconfig --del network
    chkconfig --add network
    
    init 或 systemd ID为1
服务目录,ls /etc/init.d/


```
[root@fxq-0 ~]# chkconfig --list

注意：该输出结果只显示 SysV 服务，并不包含原生 systemd 服务。SysV 配置数据可能被原生 systemd 配置覆盖。 
      如果您想列出 systemd 服务,请执行 'systemctl list-unit-files'。
      欲查看对特定 target 启用的服务请执行
      'systemctl list-dependencies [target]'。

netconsole     	0:关	1:关	2:关	3:关	4:关	5:关	6:关
network        	0:关	1:关	2:开	3:开	4:开	5:开	6:关
[root@fxq-0 ~]# 

```



## 10.25 systemd管理服务
    systmectl list-unit-files
    systemctl list-units --tpye=serveice
    systemctl is-enables rond 
    systemctl enable crond

```
Use -t help to see a list of allowed values.
[root@fxq-0 ~]# systemctl list-units --all --type=service
  UNIT                                   LOAD      ACTIVE   SUB     DESCRIPTION
  auditd.service                         loaded    active   running Security Auditing Service
  brandbot.service                       loaded    inactive dead    Flexible Branding Service
  chronyd.service                        loaded    active   running NTP client/server
  cpupower.service                       loaded    inactive dead    Configure CPU power related settings
  crond.service                          loaded    active   running Command Scheduler
  dbus.service                           loaded    active   running D-Bus System Message Bus
● display-manager.service                not-found inactive dead    display-manager.service
  dracut-shutdown.service                loaded    inactive dead    Restore /run/initramfs
  ebtables.service                       loaded    inactive dead    Ethernet Bridge Filtering tables
  emergency.service                      loaded    inactive dead    Emergency Shell
● exim.service                           not-found inactive dead    exim.service
  firewalld.service                      loaded    active   running firewalld - dynamic firewall daemon
  getty@tty1.service                     loaded    active   running Getty on tty1
● ip6tables.service                      not-found inactive dead    ip6tables.service
● ipset.service                          not-found inactive dead    ipset.service

```

## 10.26 unit介绍
    unit相关的命令：
    systemctl list-units  列出正在支持的命令。
    systemctl list-units --all  //列出所有，包括失败或inacdive的
    
    systemctl list-units --all --state=inactive 列出状态为active的service
    
    systemctl is-active crond.server 查看某个服务是否为active

## 10.27 target介绍
    systemctl list-unit-files --type=targt
    systemctl list-dependencies multi-user.target //查看指定target 下面有哪些unit
    systemctl get-default //查看系统默认的target
    systemctl set-default multi-user.target
    一种service 属于一种类型的unit
    多个unit 组成一个target 
    一个target 里面包含了多个serice
    cat /usr/lit/systemd/system/sshd.service//看看install 部分.
## 扩展
1. anacron http://blog.csdn.net/strikers1982/article/details/4787226

```
anacron 是干什么的呢？
简单说吧，crontab 可以指定任务在每天几点钟运行，可是如果那个钟点机器没有开，
那个任务便错过了时间在一个新的时间轮回之内不再运行了。
而 anacron 可以在每天、每周、每月（时间轮回天数可以自己指定）服务启动时便会
将所有服务置为 Ready 状态，只等时间一到，便执行任务。
说得有点别扭，一起来从配置文件入手来分析 anacron 吧。



anacron 的执行方式。

这玩意儿远看蛮简单的，可是真操作起来就没那么轻松了。

anacron 是干什么的呢？简单说吧，crontab 可以指定任务在每天几点钟运行，可是如果那个钟点机器没有开，那个任务便错过了时间在一个新的时间轮回之内不再运行了。而 anacron 可以在每天、每周、每月（时间轮回天数可以自己指定）服务启动时便会将所有服务置为 Ready 状态，只等时间一到，便执行任务，说得有点别扭，一起来从配置文件入手来分析 anacron 吧。

# /etc/anacrontab: configuration file for anacron

# See anacron(8) and anacrontab(5) for details.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

1       5       cron.daily              run-parts /etc/cron.daily
7       70      cron.weekly             run-parts /etc/cron.weekly
30      75      cron.monthly            run-parts /etc/cron.monthly

首先前两行注释，告诉你文件是做什么用的，从 man 5 anacrontab 获取配置文件帮助。

第5、6行是定义基本环境变量，保证程序可以正常运行。

最后三行是默认配置下所执行的任务，也是最重要的，任务配置部分。
分四部分：


period        delay            job-identifier        command
<轮回天数>    <轮回内的重试时间>    <任务描述>        <命令>
7           70              cron.weekly             run-parts /etc/cron.weekly

首先是轮回天数，即是指任务在多少天内执行一次，monthly 就是一个月（30天）内执行，weekly 即是在一周之内执行一次。

其 实这种说法不太好，因为它用的不是人类的日历来定启动任务的时间，而是利用天数，像"每月"，是指在"每月"执行的任务完成后的三十天内不再执行第二次， 而不是自然月的"三十天左右"，不管什么原因（比如关机了超过一个月），三十天后 anacron 启动之时该任务依然会被执行，"周"任务同理。

第 二部分 delay 是指轮回内的重试时间，这个意思有两部分，一个是 anacron 启动以后该服务 ready 暂不运行的时间（周任务的 70 delay 在 anacron 启动后70分钟内不执行，而处于 ready 状态），另一个是指如果该任务到达运行时间后却因为某种原因没有执行（比如前一个服务还没有运行完成，anacron 在 /etc/init.d 的脚本中加了一个 -s 参数，便是指在前一个任务没有完成时不执行下一个任务），依然以周任务和月任务为例，周任务在启动 anacron 后的  70 分钟执行，月任务在服务启动后 75 分钟执行，但是，如果月任务到达服务启动后 75 分钟，可是周任务运行超过5分钟依然没有完成，那月任务将会进入下一个 75 分钟的轮回，在下一个 75 分钟时再检查周任务是否完成，如果前一个任务完成了那月任务开始运行。
（这里有一个问题，如果周任务在后台死掉了，成僵尸进程了，难道月任务永远也不执行？！）

第 三部分 job-identifier 非常简单，anacron 每次启动时都会在 /var/spool/anacron 里面建立一个以 job-identifier 为文件名的文件，里面记录着任务完成的时间，如果任务是第一次运行的话那这个文件应该是空的。这里只要注意不要用不可以作为文件名的字符串便可，可能的话 文件名也不要太长。注：根据我的理解，其实就是anacron运行时，会去检查“/var/spool/anacron/这部分”文件中的内容，内容为一个日期，根据这个日期判断下面的第四部分要不要执行。 比如说这里写的是cron.daily，然后/var/spool/anacron/cron.daily文件中记录的日期为昨天的话，那anancron执行后就行执行这一行对应第四行的动作。

第四部分最为简单，仅仅是你想运行的命令，run-parts 我原以为是 anacron 配置文件的语法，后来才看到有一个 /usr/bin/run-parts，可以一次运行整个目录的可执行程序。

实战自运行任务，让我们以两种不同的方式写一个自己运行的任务，这样就更好理解了！


写一个 /etc/cron.daily/wall.cron，内容为如下：


wall anacron is running/!
pstree > /tmp/pstree-
.log然后将rm−f/var/spool/anacron/∗将所有任务置为都未完成的状态，重新启动anacron:serviceanacronrestart，可以ps−A看看，anacron已经运行在后台了。等5分钟以后（因为还有其它任务在运行，时间可能会微微多于5分钟），屏幕上将输出anacronisrunning!同时/tmp下也留下两个文件：anacron−
.ps 和 pstree-
.log，
为当时 anacron 的进程号。

还有一种写法直接写入 /etc/anacrontab，一步一步来，把 /etc/anacrontab 中的 daily, weekly, monthly 三行注释掉，然后在末尾加入一行：


1       1       anacron.test            /etc/cron.daily/wall.cron



删除 /var/spool/anacron 目录中的文件后重新启动 anacron 服务，等一分种过去之后屏幕上又会输出成功的信息：anacron is running! 

任务完成之后用 ps -A 查看运行中的进程，会发现 anacron 在运行完所有任务之后自动退出了。

这使鲁莹引出了一个问题：anacron 完成所有任务退出之后，如果不关机或者重新启动 anacron 服务，进入下一个时间轮回，由谁来启动那些任务呢？
确实是个问题，我想要不在 anacrontab 指定一个很大很大的天数，让 anacron 永远也不退出（36500 一百年够了吧。 ^_^），要不系统会每隔一个月调用一次 anacron（manpage 好像是这么写的）。


```

2. xinetd服(默认机器没有安装这个服务，需要yum install xinetd安装） http://blog.sina.com.cn/s/blog_465bbe6b010000vi.html

```
随着互联网的不断发展以及Linux系统的不断完善，以Linux为系统核心的网络服务器的比重正在逐年上升
language=javascript1.1 src="http://ad.ccw.com.cn/adshow.asp?positionID=38&js=1&innerJs=1">
。从WWW服务器到目前流行的游戏服务器，绝大多数都在采用Linux作为服务平台。正是由于各种应用的不断出现和用户群的增长，基于Linux的系统应当拥有完善的安全措施，应当足够坚固、能够抵抗来自Internet的侵袭，这也正是Linux之所以流行并且成为Internet骨干力量的主要原因。一方面，Linux为用户提供了多种优质的网络服务，包括Http、Ftp、Smtp、Pop3等；另一方面，服务的增多意味着更多的风险。每种服务本身都必然存在着某些缺陷，而这些缺陷很有可能被高明的黑客利用来对系统进行攻击。所以，提供特定服务的服务器应该尽可能开放提供服务必不可少的端口，而将与服务器服务无关的服务关闭，比如：一台作为www和ftp服务器的机器，应该只开放80和25端口，而将其他无关的服务如关掉，以减少系统漏洞。本专题将着重讲述在Linux系统中如何使用xinetd机制来管理网络应用服务。
Xinetd机制介绍
在Linux系统的早期版本中，有一种称为inetd的网络服务管理程序，也叫作“超级服务器”，就是监视一些网络请求的守护进程，其根据网络请求来调用相应的服务进程来处理连接请求。inetd.conf则是inetd的配置文件。inetd.conf文件告诉inetd监听哪些网络端口，为每个端口启动哪个服务。在任何的网络环境中使用Linux系统，第一件要做的事就是了解一下服务器到底要提供哪些服务。不需要的那些服务应该被禁止掉，这样黑客就少了一些攻击系统的机会，因为服务越多，意味着遭受攻击的风险越打。用户可以查看“/etc/inetd.conf”文件，了解一下inetd提供和开放了哪些服务，以根据实际情况进行相应的处理。而在Linux7.X的版本当中则使用xinetd（扩展的超级服务器）的概念对inetd进行了扩展和替代。因此本专题主要以xinetd为背景，来讲述如何增加和删除网络服务，从而有效地保证Linux系统安全。
xinetd的默认配置文件是/etc/xinetd.conf。其语法和/etc/inetd.conf完全不同且不兼容。它本质上是/etc/inetd.conf和/etc/hosts.allow，/etc/hosts.deny功能的组合。
系统默认使用xinetd的服务可以分为如下几类：
l         标准internet服务：http、telnet、ftp等；
l         信息服务：finger、netstat、systat；
l         邮件服务：imap、pop3、smtp；
l         RPC服务：rquotad 、rstatd、rusersd、sprayd、walld；
l         BSD服务：comsat、exec、login、ntalk、shell talk；
l         内部服务：chargen、daytime、echo等；
l         安全服务：irc；
l         其他服务：name、tftp、uucp、wu-ftp；
上述xinetd提供的服务的用途以及使用方法，用户可以查找相关的资料获得，这里不再赘述。然而，对他们有详细地了解是必不可少的，这可以帮助用户较好地确定需要或者不需要哪些网络服务功能。
下面是一个典型的/etc/xinetd.conf文件的例子：
# vi xinetd.conf
#
# Simple configuration file for xinetd
#
# Some defaults, and include /etc/xinetd.d/
defaults
{
    instances               = 60
     log_type                = SYSLOG authpriv
     log_on_success      = HOST PID
     log_on_failure      = HOST
     cps         = 25 30
}
includedir /etc/xinetd.d
从文件最后一行可以清楚地看到，/etc/xinetd.d目录是存放各项网络服务（包括http、ftp等）的核心目录，因而系统管理员需要对其中的配置文件进行熟悉和了解。
一般说来，在/etc/xinetd.d的各个网络服务配置文件中，每一项具有下列形式：
service service-name
{
Disabled             //表明是否禁用该服务
Flags                    //可重用标志
Socket_type         //TCP/IP数据流的类型，包括stream，datagram，raw等
Wait                            //是否阻塞服务，即单线程或多线程
User                            //服务进程的uid
Server                  //服务器守护进程的完整路径
log_on_failure       //登陆错误日志记录
}
其中service是必需的关键字，且属性表必须用大括号括起来。每一项都定义了由service-name定义的服务。
Service-name是任意的，但通常是标准网络服务名，也可增加其他非标准的服务，只要它们能通过网络请求激活，包括localhost自身发出的网络请求。
每一个service有很多可以使用的attribute，操作符可以是=，+=，或-=。所有属性可以使用=，其作用是分配一个或多个值，某些属性可以使用+=或－=的形式，其作用分别是将其值增加到某个现存的值表中，或将其值从现存值表中删除。
用户应该特别注意的是：每一项用户想新添加的网络服务描述既可以追加到现有的/etc/xinetd.conf中，也可以在/etc/xinetd.conf中指定的目录中分别建立单独的文件，RedHat 7.x以上的版本都建议采用后一种做法，因为这样做的可扩充性很好，管理起来也比较方便，用户只需要添加相应服务的描述信息即可追加新的网络服务。
RedHat 7.x默认的服务配置文件目录是/etc/xinetd.d，在该目录中使用如下命令可以看到许多系统提供的服务：
#cd /etc/xinetd.d
#ls
chargen      cvspserver  daytime-udp  echo-udp  ntalk  qmail-pop3  rexec   rsh    sgi_fam  telnet  time-udp  chargen-udp  daytime     echo         finger    pop3   qmail-smtp  rlogin  rsync  talk     time    wu-ftpd
然而，上述的许多服务，默认都是关闭的，看看如下文件内容：
#cat telnet
# default: off    //表明默认该服务是关闭的
# description: The telnet server serves telnet sessions; it uses \
#       unencrypted username/password pairs for authentication.
service telnet
{
        disable = yes   //表明默认该服务是关闭的
        flags           = REUSE
        socket_type     = stream       
        wait            = no
        user            = root
        server          = /usr/sbin/in.telnetd
        log_on_failure  += USERID
}
服务的开启与关闭
一般说来，用户可以使用两种办法来对网络服务进行开启与关闭。一种为通过文件直接编写进行开启与关闭；另外一种则通过用户熟悉的图形用户界面进行。下面分别进行介绍。
（1）使用/etc/xinetd.d目录下的文件进行配置
针对上面列出的关于telnet的例子，用户想要开启服务，只需要通过使用vi 编辑器改写该文件为如下内容：
service telnet
{
        disable = no   //将该域置为“no”，则表明开启该服务
        flags           = REUSE
        socket_type     = stream       
        wait            = no
        user            = root
        server          = /usr/sbin/in.telnetd
        log_on_failure  += USERID
}
然后，需要使用/etc/rc.d/init.d/xinetd restart来激活Telnet服务即可。
相对应的，如果用户想要关闭某个不需要的服务，则将上述的disable = no改为disable = yes即可，这样就修改了服务配置，并且再次使用/etc/rc.d/init.d/xinetd restart来启用最新的配置。
这种方法使用起来相对于Windows下的图形配置方法较为复杂，用户需要对其中的每个参数都有清楚地了解，不能够随意修改，所以建议高级用户或者是有经验的用户使用。
 
（2）使用图形用户界面进行配置：
用户可以在终端下键入“setup”命令来对系统提供的服务、防火墙配置、用户授权配置、网络配置、声卡配置、打印机配置等进行全方位的配置，如图1所示。
图1 配置工具示意图
用户选择其中的System services进行配置即可，将会看到如图2所示的用户界面：
图2 系统服务配置示意图
用户将会看到系统罗列出了anacron,apmd,autofs,chargen,telnet,http等包括我们上面所讲述的xinetd管理的网络服务在内的系统服务进程，用户通过选择这些进程，则可以开启相应的服务。而如果用户想关掉其中的某个服务，则取消选择，保存退出即可以完成配置。
而用户这样设置的结果，就相当于直接对/etc/xinetd.d相应网络服务的配置文件进行了改写，只不过使用起来更加直观和方便，且不需要用户具有比较熟练的Linux使用技巧。
同样需要注意的是，在配置好了相应的网络服务之后，需要执行/etc/rc.d/init.d/xinetd restart命令来对新的改动进行激活，那么就能够使用最新配置的服务了。
最后强调用户注意的是，本文给出的使用方法使用效率的高低、正确与否，极大程度上取决于具体的应用，以及用户对各项服务理解程度的不同。希望用户在配置自己的Linux服务器之前，对各种应用服务都作深入地了解，待到根据实际的应用需求确定好需要开启和哪些网络服务之后再使用xinetd机制进行配置，切忌稀里糊涂地进行配置，反而导致产生较大的漏洞和风险。
分享： 
4
喜欢
0
赠金笔赠金笔
阅读(731)┊ 评论 (0)┊	收藏(0) ┊转载(1)	┊ 喜欢▼ ┊打印┊举报
前一篇：安全管理Linux系统中的用户和组
后一篇：使用日志子系统保护Linux系统安全
评论 重要提示：警惕虚假中奖信息 [发评论]
做第一个评论者吧！ 抢沙发>>
发评论

小新小浪炮炮兵张富贵旺狗悠嘻猴酷巴熊更多>>
就不买你股市发霉陈水边裁员音乐贴你抢车位
登录名：
 密码：
 找回密码 注册	记住登录状态

 分享到微博 新   评论并转载此博文新

发评论
以上网友发言只代表其个人观点，不代表新浪网的观点或立场。
< 前一篇
安全管理Linux系统中的用户和组
后一篇 >
使用日志子系统保护Linux系统安全


```

3. systemd自定义启动脚本 http://www.jb51.net/article/100457.htm

```
.服务权限
 
systemd有系统和用户区分；系统（/user/lib/systemd/system/）、用户（/etc/lib/systemd/user/）.一般系统管理员手工创建的单元文件建议存放在/etc/systemd/system/目录下面。
 
2.创建服务文件
 



?



12345678910111213141516 

[Unit]Description=nginx - high performance web serverDocumentation=http://nginx.org/en/docs/After=network.target remote-fs.target nss-lookup.target  [Service]Type=forkingPIDFile=/run/nginx.pidExecStartPre=/usr/sbin/nginx -t -c /etc/nginx/nginx.confExecStart=/usr/sbin/nginx -c /etc/nginx/nginx.confExecReload=/bin/kill -s HUP $MAINPIDExecStop=/bin/kill -s QUIT $MAINPIDPrivateTmp=true  [Install]WantedBy=multi-user.target 

[Unit]
 
Description : 服务的简单描述
 
Documentation ： 服务文档
 
Before、After:定义启动顺序。Before=xxx.service,代表本服务在xxx.service启动之前启动。After=xxx.service,代表本服务在xxx.service之后启动。
 
Requires：这个单元启动了，它需要的单元也会被启动；它需要的单元被停止了，这个单元也停止了。
 
Wants：推荐使用。这个单元启动了，它需要的单元也会被启动；它需要的单元被停止了，对本单元没有影响。
 
[Service]
 
Type=simple（默认值）：systemd认为该服务将立即启动。服务进程不会fork。如果该服务要启动其他服务，不要使用此类型启动，除非该服务是socket激活型。
 
Type=forking：systemd认为当该服务进程fork，且父进程退出后服务启动成功。对于常规的守护进程（daemon），除非你确定此启动方式无法满足需求，使用此类型启动即可。使用此启动类型应同时指定 PIDFile=，以便systemd能够跟踪服务的主进程。
 
Type=oneshot：这一选项适用于只执行一项任务、随后立即退出的服务。可能需要同时设置 RemainAfterExit=yes 使得 systemd 在服务进程退出之后仍然认为服务处于激活状态。
 
Type=notify：与 Type=simple 相同，但约定服务会在就绪后向 systemd 发送一个信号。这一通知的实现由 libsystemd-daemon.so 提供。
 
Type=dbus：若以此方式启动，当指定的 BusName 出现在DBus系统总线上时，systemd认为服务就绪。
 
Type=idle: systemd会等待所有任务(Jobs)处理完成后，才开始执行idle类型的单元。除此之外，其他行为和Type=simple 类似。
 
PIDFile：pid文件路径
 
ExecStart：指定启动单元的命令或者脚本，ExecStartPre和ExecStartPost节指定在ExecStart之前或者之后用户自定义执行的脚本。Type=oneshot允许指定多个希望顺序执行的用户自定义命令。
 
ExecReload：指定单元停止时执行的命令或者脚本。
 
ExecStop：指定单元停止时执行的命令或者脚本。
 
PrivateTmp：True表示给服务分配独立的临时空间
 
Restart：这个选项如果被允许，服务重启的时候进程会退出，会通过systemctl命令执行清除并重启的操作。
 
RemainAfterExit：如果设置这个选择为真，服务会被认为是在激活状态，即使所以的进程已经退出，默认的值为假，这个选项只有在Type=oneshot时需要被配置。
 
[Install]
 
Alias：为单元提供一个空间分离的附加名字。
 
RequiredBy：单元被允许运行需要的一系列依赖单元，RequiredBy列表从Require获得依赖信息。
 
WantBy：单元被允许运行需要的弱依赖性单元，Wantby从Want列表获得依赖信息。
 
Also：指出和单元一起安装或者被协助的单元。
 
DefaultInstance：实例单元的限制，这个选项指定如果单元被允许运行默认的实例。
 
3.重载服务
 
systemctl enable nginx.service
 
就会在/etc/systemd/system/multi-user.target.wants/目录下新建一个/usr/lib/systemd/system/nginx.service 文件的链接。
 
4.操作服务
 



?



12345678910111213141516171819 

#启动服务$ sudo systemctl start nginx.service  #查看日志$ sudo journalctl -f -u nginx.service— Logs begin at 四 2015-06-25 17:32:20 CST. —6月 25 10:28:24 Leco.lan systemd[1]: Starting nginx – high performance web server…6月 25 10:28:24 Leco.lan nginx[7976]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok6月 25 10:28:24 Leco.lan nginx[7976]: nginx: configuration file /etc/nginx/nginx.conf test is successful6月 25 10:28:24 Leco.lan systemd[1]: Started nginx – high performance web server.  #重启$ sudo systemctl restart nginx.service  #重载$ sudo systemctl reload nginx.service  #停止$ sudo systemctl stop nginx.service 

```
