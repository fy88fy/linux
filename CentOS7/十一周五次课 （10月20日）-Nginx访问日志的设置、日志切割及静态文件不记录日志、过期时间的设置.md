[toc]
## 12.10 Nginx访问日志


### （1）查看主配置文件中日志格式配置：
 vim /usr/local/nginx/conf/nginx.conf 
 
 **日志格式:**
 //搜索log_format
```
    log_format combined_realip '$remote_addr $http_x_forwarded_for [$time_local]'
    ' $host "$request_uri" $status'
    ' "$http_referer" "$http_user_agent"';

```
![image](http://note.youdao.com/yws/api/personal/file/B25F3BD40C3546DB904FF74F3D49E902?method=download&shareKey=c880495aed5a1cf83f8ee43da8e8b3eb)
### （2）加如下面配置语句:
 除了在主配置文件nginx.conf里定义日志格式外，还需要在虚拟主机配置文件中增加
 
**==access_log /usr/local/nginx/logs/test.com/test.com_access.log combined_realip;==**
 
 这里的combined_realip就是在nginx.conf中定义的日志格式名字

### (3) 重新加载配置文件：
**==/usr/local/nginx/sbin/nginx -t==**
 
**==/usr/local/nginx/sbin/nginx -s reload==**

### （4）测试查看：
 curl -x127.0.0.1:80 www.test.com -I
 
 cat /usr/local/nginx/logs/test.com/test.com_access.log

```
[root@fxq-1 ~]# cat /usr/local/nginx/logs/test.com/test.com_access.log 
192.168.103.1 - [20/Oct/2017:21:33:16 +0800] www.test.com "/favicon.ico" 301 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
192.168.103.1 - [20/Oct/2017:21:33:17 +0800] www.test.com "/" 301 "-" "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)"
192.168.103.1 - [20/Oct/2017:21:33:35 +0800] www.test.com "/admin/admin.php" 301 "-" "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)"
127.0.0.1 - [20/Oct/2017:21:36:03 +0800] www.test.com "/" 301 "-" "curl/7.29.0"
[root@fxq-1 ~]# 

```

## 12.11 Nginx日志切割


### (1) 自定义shell 脚本

vim /usr/local/sbin/nginx_log_rotate.sh

//写入如下内容
```
    #!/bin/bash
    ## 假设nginx的日志存放路径为/usr/local/nginx/logs/test.com/
    d=`date -d "-1 day" +%Y%m%d` 
    logdir="/usr/local/nginx/logs/test.com/"
    nginx_pid="/usr/local/nginx/logs/nginx.pid"
    cd $logdir
    for log in `ls *.log`
    do
        mv $log $log-$d
    done
    /bin/kill -HUP `cat $nginx_pid`
```

### （2）加入任务计划：
 
 crontab -e 加入下面内容：
 
==**0 0 * * * /bin/bash /usr/local/sbin/nginx_log_rotate.sh**==
 
### （3）查看日志切割情况：

```
[root@fxq-1 ~]# ls /usr/local/nginx/logs/test.com/
test.com_access.log  test.com_access.log-20171019
[root@fxq-1 ~]# 
```


## 12.12 静态文件不记录日志和过期时间

### (1）编辑虚拟主机配置文件

vim /usr/local/nginx/conf/vhost/test.com.conf
加入配置如下：


```
location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
    {
          expires      7d;
          access_log off;
    }
location ~ .*\.(js|css)$
    {
          expires      12h;
          access_log off;
    }

```
### (2)测试查看：

**==/usr/local/nginx/sbin/nginx -t==**
 
**==/usr/local/nginx/sbin/nginx -s reload==**

**==cat /usr/local/nginx/logs/test.com/test.com_access.log==**
```
[root@fxq-1 ~]# cat /usr/local/nginx/logs/test.com/test.com_access.log
192.168.103.1 - [20/Oct/2017:22:18:25 +0800] www.test.com "/favicon.ico" 404 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55
.0.2883.87 Safari/537.36"192.168.103.1 - [20/Oct/2017:22:18:27 +0800] www.test.com "/admin/1.php" 404 "-" "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)"
192.168.103.1 - [20/Oct/2017:22:19:02 +0800] www.test.com "/admin/1.jpgg" 404 "-" "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)"
192.168.103.1 - [20/Oct/2017:22:19:21 +0800] www.test.com "/1.jss" 404 "-" "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)"
[root@fxq-1 ~]# 

```