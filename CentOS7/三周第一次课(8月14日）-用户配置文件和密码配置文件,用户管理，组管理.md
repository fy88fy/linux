[toc]
### 2.27linux和windows互传文件
#### 1.安装支持包：lrzsz


```
yum -y install   lrzsz
```


> putty工具 不支持lrzsz

#### 2.rz windows文件fail2ban-0.8.14.tar.gz 到linux 当前目录

![image](http://note.youdao.com/yws/api/personal/file/1921886755604983AD726EA3161561BA?method=download&shareKey=8296ecd221f98f35511eb40d66e700b3)

![image](http://note.youdao.com/yws/api/personal/file/91A4866A18AA4E90B5EC18FB6FE3581F?method=download&shareKey=13053d64a8ad2d262d07c30f078f1175)

![image](http://note.youdao.com/yws/api/personal/file/B7A6044B073D49E7982A0D6C3AA75BDA?method=download&shareKey=3b81642cacd0dea687b083c0655f0557)



#### 3.sz baidu.png 传输文件从linux 到 windows

![image](http://note.youdao.com/yws/api/personal/file/BE284BE28EA44AB4A6B5D9A31EBF5A0A?method=download&shareKey=10e3c8795eebdc65055ce98747ff4bd5)

![image](http://note.youdao.com/yws/api/personal/file/3D458920998749A5AB7C59EFB487DB8B?method=download&shareKey=7c9162d851e1c73001490134b3d098cf)


### 3.1 用户配置文件和密码配置文件

#### 1.用户配置文件

**ls /etc/passwd**


```
[root@VM_46_188_centos ~]# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
avahi-autoipd:x:170:170:Avahi IPv4LL Stack:/var/lib/avahi-autoipd:/sb
in/nologinsystemd-bus-proxy:x:999:997:systemd Bus Proxy:/:/sbin/nologin
systemd-network:x:998:996:systemd Network Management:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:997:995:User for polkitd:/:/sbin/nologin
abrt:x:173:173::/etc/abrt:/sbin/nologin
libstoragemgmt:x:996:994:daemon account for libstoragemgmt:/var/run/l
sm:/sbin/nologintss:x:59:59:Account used by the trousers package to sandbox the tcsd 
daemon:/dev/null:/sbin/nologinntp:x:38:38::/etc/ntp:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
chrony:x:995:993::/var/lib/chrony:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
tcpdump:x:72:72::/:/sbin/nologin
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
mysql:x:27:27:MariaDB Server:/var/lib/mysql:/sbin/nologin
fxq:x:1000:1000::/home/fxq:/bin/bash
nginx:x:994:992:Nginx web server:/var/lib/nginx:/sbin/nologin
saslauth:x:993:76:Saslauthd user:/run/saslauthd:/sbin/nologin
mailnull:x:47:47::/var/spool/mqueue:/sbin/nologin
smmsp:x:51:51::/var/spool/mqueue:/sbin/nologin
test:x:1012:1012::/home/test:/sbin/nologin
zabbix:x:1013:1014::/home/zabbix:/sbin/nologin
fxq1:x:1014:1015::/home/fxq1:/bin/bash
[root@VM_46_188_centos ~]# 

```
- 第一段：用户名
- 第二段：密码
- 第三段：用户UID
- 第四段：用户组ID
- 第五段：注释
- 第六段：用户家目录
- 第七段：用户的登录交互的shell  /bin/bash(可登录) /bin/noglogin(不可登录)


#### 2.密码配置文件
**ls /etc/shadow**


```
[root@VM_46_188_centos ~]# cat /etc/shadow
root:$1$QoTjnbBe$o3Xvx5L0KNpvmXMivsFnf.::0:99999:7:::
bin:*:16659:0:99999:7:::
daemon:*:16659:0:99999:7:::
adm:*:16659:0:99999:7:::
lp:*:16659:0:99999:7:::
sync:*:16659:0:99999:7:::
shutdown:*:16659:0:99999:7:::
halt:*:16659:0:99999:7:::
mail:*:16659:0:99999:7:::
operator:*:16659:0:99999:7:::
games:*:16659:0:99999:7:::
ftp:*:16659:0:99999:7:::
nobody:*:16659:0:99999:7:::
avahi-autoipd:!!:16912::::::
systemd-bus-proxy:!!:16912::::::
systemd-network:!!:16912::::::
dbus:!!:16912::::::
polkitd:!!:16912::::::
abrt:!!:16912::::::
libstoragemgmt:!!:16912::::::
tss:!!:16912::::::
ntp:!!:16912::::::
postfix:!!:16912::::::
chrony:!!:16912::::::
sshd:!!:16912::::::
tcpdump:!!:16912::::::
apache:!!:17296::::::
mysql:!!:17319::::::
fxq:$1$yMvFHPLx$IXmVlg4chjsxR6FFXtUGS1:17322:0:99999:7:::
nginx:!!:17323::::::
saslauth:!!:17344::::::
mailnull:!!:17344::::::
smmsp:!!:17344::::::
test:$1$Lv1G6wIl$gprgn2dB1dFtliHgC9fbW.:17365:0:99999:7:::
zabbix:!!:17373:0:99999:7:::
fxq1:$1$b5O76dKM$SOugJJIU7pX5AvBewRz/O/:17386:0:99999:7:::
[root@VM_46_188_centos ~]# 
```

文件中：如果用户的密码一样，出现的乱码位也是不一样的。


```
       The meanings of each field are:

       第一段:  sp_namp - pointer to null-terminated user name(用户名)

       第二段: sp_pwdp - pointer to null-terminated password

       第三段:   sp_lstchg - days since Jan 1, 1970 password was last
           changed

       第四段:  sp_min - days before which password may not be changed(到期)

       第五段  sp_max - days after which password must be changed

       第六段   sp_warn - days before password is to expire that user is
           warned of pending password expiration

       第七段   sp_inact - days after password expires that account is
           considered inactive and disabled

       第八段   sp_expire - days since Jan 1, 1970 when account will be
           disabled(在此日期前可以用)

       第九段   sp_flag - reserved for future use

```

> - 1.用户名
> - 2.密码
> - 3.上次更改密码的时间， 从1970年1月1日开始到上次更改密码过了多少天
> - 4.要过多少天后才能更改密码  0代表不限制
> - 5.密码过多少天后密码过期 ，默认99999天
> - 6.密码到期前多少天发警告。
> - 7.账号到期后多少天失效 ，失效期限
> - 8.账号生命周期，可以用多久。
> - 9.保留。



### 3.2 用户组管理

#### 1.组配置文件

**ls /etc/group**


```
[root@VM_46_188_centos ~]# cat /etc/group
root:x:0:test
bin:x:1:
daemon:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mem:x:8:
kmem:x:9:
wheel:x:10:
cdrom:x:11:
mail:x:12:postfix
man:x:15:
dialout:x:18:
floppy:x:19:
games:x:20:
tape:x:30:
video:x:39:
ftp:x:50:
lock:x:54:
audio:x:63:
nobody:x:99:
users:x:100:
avahi-autoipd:x:170:
utmp:x:22:
utempter:x:35:
ssh_keys:x:999:
input:x:998:
systemd-journal:x:190:
systemd-bus-proxy:x:997:
systemd-network:x:996:
dbus:x:81:
polkitd:x:995:
abrt:x:173:
libstoragemgmt:x:994:
tss:x:59:
ntp:x:38:
dip:x:40:
slocate:x:21:
postdrop:x:90:
postfix:x:89:
chrony:x:993:
sshd:x:74:
tcpdump:x:72:
stapusr:x:156:
stapsys:x:157:
stapdev:x:158:
apache:x:48:
mysql:x:27:
fxq:x:1000:
nginx:x:992:
saslauth:x:76:
mailnull:x:47:
smmsp:x:51:
user1:x:1012:
test:x:1013:
wireshark:x:991:
screen:x:84:
zabbix:x:1014:zabbix
fxq1:x:1015:
[root@VM_46_188_centos ~]# 
```


[root@VM_46_188_centos ~]# ls /etc/group*

/etc/group  ==**/etc/group-**==

[root@VM_46_188_centos ~]# 

**group-文件是备份**


#### 2.用户组密码配置文件
```
[root@VM_46_188_centos ~]# cat /etc/gshadow 
gshadow   gshadow-  
[root@VM_46_188_centos ~]# cat /etc/gshadow
root:::test
bin:::
daemon:::
sys:::
adm:::
tty:::
disk:::
lp:::
mem:::
kmem:::
wheel:::
cdrom:::
mail:::postfix
man:::
dialout:::
floppy:::
games:::
tape:::
video:::
ftp:::
lock:::
audio:::
nobody:::
users:::
avahi-autoipd:!::
utmp:!::
utempter:!::
ssh_keys:!::
input:!::
systemd-journal:!::
systemd-bus-proxy:!::
systemd-network:!::
dbus:!::
polkitd:!::
abrt:!::
libstoragemgmt:!::
tss:!::
ntp:!::
dip:!::
slocate:!::
postdrop:!::
postfix:!::
chrony:!::
sshd:!::
tcpdump:!::
stapusr:!::
stapsys:!::
stapdev:!::
apache:!::
mysql:!::
fxq:!::
nginx:!::
saslauth:!::
mailnull:!::
smmsp:!::
user1:!::
test:!::
wireshark:!::
screen:!::
zabbix:!::zabbix
fxq1:!::
[root@VM_46_188_centos ~]#
```
#### 3.创建组grp1：

**groupadd grp1**


```
[root@VM_46_188_centos ~]# groupadd grp1
[root@VM_46_188_centos ~]# tail -n1 /etc/group
grp1:x:1016:
[root@VM_46_188_centos ~]#
```

#### 4.创建组grp2指定组ID为990：

**groupadd -g 990 grp2**

```
[root@VM_46_188_centos ~]# groupadd -g 990 grp2
[root@VM_46_188_centos ~]# tail -n1 /etc/group
grp2:x:990:
[root@VM_46_188_centos ~]# 
```

#### 5.删除组grp2：

**groupdel grp2**

```
[root@VM_46_188_centos ~]# groupdel grp2
[root@VM_46_188_centos ~]# tail -n3 /etc/group
zabbix:x:1014:zabbix
fxq1:x:1015:
grp1:x:1016:
[root@VM_46_188_centos ~]#
```
删除组时，组内没有用户时才可以被删除.



### 3.3 用户管理

#### 1.添加用户

**useradd user1**


(adduser 也可以用)

```
[root@VM_46_188_centos ~]# useradd user2
[root@VM_46_188_centos ~]# useradd user3
[root@VM_46_188_centos ~]# tail -3 /etc/passwd
fxq1:x:1014:1015::/home/fxq1:/bin/bash
user2:x:1015:1017::/home/user2:/bin/bash
user3:x:1016:1018::/home/user3:/bin/bash
[root@VM_46_188_centos ~]# 

```
#### 2.添加并设定用户user4

(用户ID:1020,组：grp1,家目录：/home/user-444,登录shell:/sbin/nologin)

```
[root@VM_46_188_centos ~]# useradd -u 1020 -g grp1 -d /home/user-444 
-s /sbin/nologin user4
[root@VM_46_188_centos ~]# tail -4 /etc/group
fxq1:x:1015:
grp1:x:1016:
user2:x:1017:
user3:x:1018:
[root@VM_46_188_centos ~]# tail -4 /etc/passwd
fxq1:x:1014:1015::/home/fxq1:/bin/bash
user2:x:1015:1017::/home/user2:/bin/bash
user3:x:1016:1018::/home/user3:/bin/bash
user4:x:1020:1016::/home/user-444:/sbin/nologin
[root@VM_46_188_centos ~]#
```

#### 3.不创建用户家目录：

**useradd -M user5**

```
[root@VM_46_188_centos ~]# ls /home/
fxq   user-444  user02  user04  user06  user08  user2
fxq1  user01    user03  user05  user07  user09  user3
[root@VM_46_188_centos ~]# 

```

用户自定义用户ID后，用户ID和组ID都是变为一样，一起自增。user5 ID都为1021

```
[root@VM_46_188_centos ~]# useradd -M user5 
[root@VM_46_188_centos ~]# ls /home/
fxq   user-444  user02  user04  user06  user08  user2
fxq1  user01    user03  user05  user07  user09  user3
[root@VM_46_188_centos ~]# tail -3 /etc/passwd
user3:x:1016:1018::/home/user3:/bin/bash
user4:x:1020:1016::/home/user-444:/sbin/nologin
user5:x:1021:1021::/home/user5:/bin/bash
[root@VM_46_188_centos ~]# useradd user6
[root@VM_46_188_centos ~]# tail -5 /etc/passwd
user2:x:1015:1017::/home/user2:/bin/bash
user3:x:1016:1018::/home/user3:/bin/bash
user4:x:1020:1016::/home/user-444:/sbin/nologin
user5:x:1021:1021::/home/user5:/bin/bash
user6:x:1022:1022::/home/user6:/bin/bash
[root@VM_46_188_centos ~]# 

```
#### 4.删除用户：

userdel user6 删除用户user6



```
[root@VM_46_188_centos ~]# userdel user6
[root@VM_46_188_centos ~]# ls /home 
fxq   user-444  user02  user04  user06  user08  user2  user6
fxq1  user01    user03  user05  user07  user09  user3
[root@VM_46_188_centos ~]# tail -2 /etc/passwd
user4:x:1020:1016::/home/user-444:/sbin/nologin
user5:x:1021:1021::/home/user5:/bin/bash
[root@VM_46_188_centos ~]#
```
#### 5.删除用户时一同加家目录也删除
**userdel -r  user5 把用户的家目录一起删除**

再看home下刚才设置的用户家目录user-444就和用户一起被删除了。

```
[root@VM_46_188_centos ~]# userdel -r user4
[root@VM_46_188_centos ~]# ls /home/     
fxq   user01  user03  user05  user07  user09  user3
fxq1  user02  user04  user06  user08  user2   user6
[root@VM_46_188_centos ~]# 
```


