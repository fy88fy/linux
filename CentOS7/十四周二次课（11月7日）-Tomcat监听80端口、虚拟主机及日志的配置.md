[toc]
## 16.4 配置Tomcat监听80端口
### (1)停止nginx，释放80端口
    /etc/init.d/nginx stop
### (2)修改tomcat配置文件server.xml
    vim /usr/local/tomcat/conf/server.xml

    Connector port="8080" protocol="HTTP/1.1"
    修改为Connector port="80" protocol="HTTP/1.1"
### (3)重启tomcat服务
 /usr/local/tomcat/bin/shutdown.sh
 /usr/local/tomcat/bin/startup.sh
 
 
 
### (4)查看tomcat启动状况
```
 [root@fxq-2 ~]# netstat -lntp | grep java
tcp6       0      0 :::80                   :::*                    LISTEN      3725/java           
tcp6       0      0 127.0.0.1:8005          :::*                    LISTEN      3725/java           
tcp6       0      0 :::8009                 :::*                    LISTEN      3725/java           
[root@fxq-2 ~]#
```
## 16.5/16.6/16.7 配置Tomcat虚拟主机

### (1)配置mysql中的数据库zrlog
```
    create database zrlog;
    grant all on zrlog.* to 'fxq'@'127.0.0.1' identified by '123456';
```
### (2)修改配置文件
```
 vim /usr/local/tomcat/conf/server.xml
其中<Host>和</Host>之间的配置为虚拟主机配置部分，name定义域名，
appBase定义应用的目录，Java的应用通常是一个war的压缩包，你只需要将war的压缩包放到appBase目录下面即可。刚刚阿铭访问的Tomcat默认页其实就是在appBase目录下面，不过是在它子目录ROOT里。
 增加虚拟主机，编辑server.xml,在</Host>下面增加如下内容
<Host name="www.123.cn" appBase=""
    unpackWARs= "true" autoDeploy="true"
    xmlValidation="false" xmlNamespaceAware="false">
    <Context path="" docBase="/data/wwwroot/123.cn/" debug="0" reloadable="true" crossContext="true"/>
</Host>

  605  cd /usr/local/src
  611  wget http://dl.zrlog.com/release/zrlog-1.7.1-baaecb9-release.war
  615  cp zrlog-1.7.1-baaecb9-release.war /usr/local/tomcat/webapps/
  620  mv zrlog-1.7.1-baaecb9-release zrlog
  621  mkdir /data/wwwroot/123.cn
  622  mv /usr/local/tomcat/webapps/zrlog/* /data/wwwroot/123.cn/
  623  /usr/local/tomcat/bin/shutdown.sh 
  624  /usr/local/tomcat/bin/startup.sh 
  625  netstat -lntp
  626  netstat -lntp | grep java
```
### (3)测试
修改pc的host ，通过浏览器访问虚拟主机：http://www.123.cn
出一安装界面，输入数据库的信息，安装完成。


```
 docBase，这个参数用来定义网站的文件存放路径，如果不定义，默认是在appBase/ROOT下面，定义了docBase就以该目录为主了，其中appBase和docBase可以一样。在这一步操作过程中很多同学遇到过访问404的问题，其实就是docBase没有定义对。
 appBase为应用存放目录，通常是需要把war包直接放到该目录下面，它会自动解压成一个程序目录
 下面我们通过部署一个java的应用来体会appBase和docBase目录的作用
 下载zrlog wget http://dl.zrlog.com/release/zrlog-1.7.1-baaecb9-release.war
 mv zrlog-1.7.1-baaecb9-release.war /usr/local/tomcat/webapps/
 mv /usr/local/tomcat/webapps/zrlog-1.7.1-baaecb9-release /usr/local/tomcat/webapps/zrlog
 浏览器访问 ip:8080/zrlog/install/
 mv /usr/local/tomcat/webapps/zrlog/* /data/wwwroot/123.cn/



```
![image](http://note.youdao.com/yws/api/personal/file/7AF82B42394E4081AB0C53C79B72DB63?method=download&amp;shareKey=1982280cb5aa3f7cdc132172dd47ed4b)
## 16.8 Tomcat日志
### (1)Tomcat日志介绍
```
 ls /usr/local/tomcat/logs
 
 其中catalina开头的日志为Tomcat的综合日志，它记录Tomcat服务相关信息，也会记录错误日志。
 其中catalina.2017-xx-xx.log和catalina.out内容相同，前者会每天生成一个新的日志。
 host-manager和manager为管理相关的日志，其中host-manager为虚拟主机的管理日志。
 localhost和localhost_access为虚拟主机相关日志，其中带access字样的日志为访问日志，不带access字样的为默认虚拟主机的错误日志。
 访问日志默认不会生成，需要在server.xml中配置一下。
```
### (2)配置虚拟主机日志
```
具体方法是在对应虚拟主机的<Host></Host>里面加入下面的配置（假如域名为123.cn）：

<Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
         prefix="123.cn_access" suffix=".log"
         pattern="%h %l %u %t &quot;%r&quot; %s %b" />
         
prefix定义访问日志的前缀，suffix定义日志的后缀，pattern定义日志格式。新增加的虚拟主机默认并不会生成类似默认虚拟主机的那个localhost.日期.log日志，错误日志会统一记录到catalina.out中。关于Tomcat日志，你最需要关注catalina.out，当出现问题时，我们应该第一想到去查看它。

```
### (3)测试查看日志
```
[root@fxq-2 webapps]# cat ../logs/123.cn_access_.2017-11-07.log 
192.168.103.1 - - [07/Nov/2017:22:32:37 +0800] "GET /post/sort/notes HTTP/1.1" 200 8676
192.168.103.1 - - [07/Nov/2017:22:32:37 +0800] "GET /include/templates/default/css/common.css HTTP/1.1" 304 -
192.168.103.1 - - [07/Nov/2017:22:32:37 +0800] "GET /include/templates/default/css/style_2015.css HTTP/1.1" 304 -
192.168.103.1 - - [07/Nov/2017:22:32:37 +0800] "GET /include/templates/default/css/pager.css HTTP/1.1" 304 -
192.168.103.1 - - [07/Nov/2017:22:32:37 +0800] "GET /include/templates/default/css/editormd.css HTTP/1.1" 304 -
192.168.103.1 - - [07/Nov/2017:22:32:37 +0800] "GET /include/templates/default/js/lib/jquery-1.10.2.min.js HT

[root@fxq-2 logs]# ls
123.cn_access_.2017-11-07.log  host-manager.2017-11-06.log  localhost_access_.2017-11-07.log     manager.2017-11-07.log
catalina.2017-11-06.log        host-manager.2017-11-07.log  localhost_access_log.2017-11-06.txt
catalina.2017-11-07.log        localhost.2017-11-06.log     localhost_access_log.2017-11-07.txt
catalina.out                   localhost.2017-11-07.log     manager.2017-11-06.log
[root@fxq-2 logs]# 
```
## 扩展
==邱李的tomcat文档 https://www.linuser.com/forum.php?mod=forumdisplay&fid=37==

==JAR、WAR包区别 http://blog.csdn.net/lishehe/article/details/41607725==

```
最近接触这几个词较多，停下来总结总结它们的区别和联系，更好的深刻理解

        


       Jar、war、EAR、在文件结构上，三者并没有什么不同，它们都采用zip或jar档案文件压缩格式。但是它们的使用目的有所区别：

　　Jar文件（扩展名为. Jar，Java Application Archive）包含Java类的普通库、资源（resources）、辅助文件（auxiliary files）等

　　War文件（扩展名为.War,Web Application Archive）包含全部Web应用程序。在这种情形下，一个Web应用程序被定义为单独的一组文件、类和资源，用户可以对jar文件进行封装，并把它作为小型服务程序（servlet）来访问。

　　Ear文件（扩展名为.Ear,Enterprise Application Archive）包含全部企业应用程序。在这种情形下，一个企业应用程序被定义为多个jar文件、资源、类和Web应用程序的集合。

　　每一种文件（.jar, .war, .ear）只能由应用服务器（application servers）、小型服务程序容器（servlet containers）、EJB容器（EJB containers）等进行处理。





EAR文件包括整个项目，内含多个ejb module（jar文件）和web module（war文件）

 

         EAR文件的生成可以使用winrar zip压缩方式或者jar命令。

步骤：
        先打包成war和jar,并写好application.xml，放到META-INF目录下，然后 jar   cf   your_application.ear   your_war.war   your_jar.jar   META-INF/application.xml，   打包，我这假设都在当前目录下     可以用    jar   xf   your_application.ear解压  
   
 



application.xml中描述你的ear中包括的war和jar (上篇文章已经提到)
   


jboss中  的application.xml例子







[html] view plain copy

 1.<span style="font-size:18px;"><span style="font-size:18px;"><?xml version="1.0" encoding="UTF-8"?>  
2.<!DOCTYPE application PUBLIC  
3.    "-//Sun Microsystems, Inc.//DTD J2EE Application 1.3//EN"  
4.    "http://java.sun.com/dtd/application_1_3.dtd">  
5.<application>  
6.  <display-name>lishehetestear</display-name>  
7.  <module>  
8.    <java>lishehe_api-0.0.1-SNAPSHOT.jar</java>  
9.  </module>  
10.  <module>  
11.    <java>lishehe_core-0.0.1-SNAPSHOT.jar</java>  
12.  </module>  
13.  <module>  
14.    <web>  
15.      <web-uri>lishehe_war-0.0.1-SNAPSHOT.war</web-uri>  
16.      <context-root>/lishehe_war</context-root>  
17.    </web>  
18.  </module>  
19.</application></span></span>  

 

WAR的使用：




如果想生成war文件：可以使用如下命令：jar -cvf web1.war *

如果想查看web1.war中都有哪些文件，可以使用命令：jar -tf web1.war

如果想直接解压web1.war文件，可以使用命令：jar -xvf web1.war

另外，也可使用winrar软件选择zip压缩方式，并将压缩文件后缀名改为war即可压缩生成war文件；同样使用winrar软件可以强行打开war文件，或者强行解压war文件

使用jar命令与winrar软件的区别在于前者在压缩文件的同时会生成MetaINF文件夹，内包含MANIFEST.MF文件。

 

总结：

         何时使用war或者jar文件：当你的项目在没有完全竣工的时候，不适合使用war文件，因为你的类会由于调试之类的经常改，这样来回删除、创建war文件很不爽，最好是你的项目已经完成了，不改了，那么就打个war包吧，这个时候一个war文件就相当于一个web应用程序鸟；而jar文件就是把类和一些相关的资源封装到一个包中，便于程序中引用。

```

==tomcat常见配置汇总 http://blog.sina.com.cn/s/blog_4ab26bdd0100gwpk.html==

```
    1 启动内存参数的配置

　　tomcat/bin/catalina.bat 如果是linux 就是 catalina.sh

　　在rem 的后面增加如下参数

　　set JAVA_OPTS= -Xms256m -Xmx256m -XX:MaxPermSize=64m

　　2 修改Tomcat的JDK目录

　　打开tomcat/bin/catalina.bat

　　在最后一个rem后面增加

　　set JAVA_HOME=C:\Program Files\Java\jdk1.6.0

　　3 增加虚拟目录

　　/tomcat/conf/server.xml

　　第一行是以前默认存在的，第二行是新增的

　　<Context path="" docBase="ROOT" debug="0" reloadable="true"></Context>

　　<Context path="/jsp/a" reloadable="true" docBase="E:\workplace\www.java2000.net\WebContent" />

　　4 GET方式URL乱码问题解决

　　打开 tomcat/conf/server.xml

　　查找下面这部分，在最后增加一段代码就可以了。

　　<Connector port="80" maxHttpHeaderSize="8192"

　　.................

　　URIEncoding="UTF-8" useBodyEncodingForURI="true"

　　...............

　　/>

　　其中的UTF-8 请根据你的需要自己修改，比如GBK

　　5 虚拟主机配置文件

　　tomcat/conf/server.xml

　　<!-- 默认的主机 -->

　　<Host name="localhost" appBase="webapps"

　　unpackWARs="true" 

　　xmlValidation="false" xmlNamespaceAware="false">

　　<Context path="" docBase="ROOT" debug="0" reloadable="true"></Context>

　　...

　　</host>

　　<!-- 以下是新增的虚拟主机 -->

　　<Host name="www.java2000.net" appBase="webapps"

　　unpackWARs="true" 

　　xmlValidation="false" xmlNamespaceAware="false">

　　<Context path="" docBase="d:/www.java2000.net" debug="0" reloadable="true"></Context>

　　<!-- 虚拟目录 -->

　　<Context path="/count" docBase="d:/counter.java2000.net" debug="0" reloadable="true"></Context>

　　</Host>

　　<Host name="java2000.net" appBase="webapps"

　　unpackWARs="true" 

　　xmlValidation="false" xmlNamespaceAware="false">

　　<Context path="" docBase="d:/www.java2000.net" debug="0" reloadable="true"></Context>

　　<Context path="/count" docBase="d:/counter.java2000.net" debug="0" reloadable="true"></Context>

　　</Host>

　　6 数据源配置

　　比较复杂，各个版本都有所不同，请直接查看 http://java2000.net/p1906，包括tomcat5.0,tomcat5.5x,tomcat6.0的各个版本的配置方法。

　　更多关于Tomcat的使用，请看参考资料 


[编辑本段]Tomcat配置的10个技巧
　　1. 配置系统管理（Admin Web Application）

　　大多数商业化的J2EE服务器都提供一个功能强大的管理界面，且大都采用易于理解的Web应用界面。Tomcat按照自己的方式，同样提供一个成熟的管理 工具，并且丝毫不逊于那些商业化的竞争对手。Tomcat的Admin Web Application最初在4.1版本时出现，当时的功能包括管理context、data source、user和group等。当然也可以管理像初始化参数，user、group、role的多种数据库管理等。在后续的版本中，这些功能将得 到很大的扩展，但现有的功能已经非常实用了。

　　Admin Web Application被定义在自动部署文件：CATALINA_BASE/webapps/admin.xml 。

　　必须编辑这个文件，以确定Context中的docBase参数是绝对路径。也就是说， CATALINA_BASE/webapps/admin.xml 的路径是绝对路径。作为另外一种选择，也可以删除这个自动部署文件，而在server.xml文件中建立一个Admin Web Application的context，效果是一样的。不能管理Admin Web Application这个应用，换而言之，除了删除CATALINA_BASE/webapps/admin.xml ，可能什么都做不了。

　　如果使用UserDatabaseRealm（默认），将需要添加一个user以及一个role到CATALINA_BASE/conf/tomcat-users.xml 文件中。你编辑这个文件，添加一个名叫“admin”的role 到该文件中，如下：

　　<role name=“admin”/>

　　同样需要有一个用户，并且这个用户的角色是“admin”。象存在的用户那样，添加一个用户（改变密码使其更加安全）：

　　<user name=“admin” password=“deep_dark_secret” roles=“admin”/>

　　当完成这些步骤后，请重新启动Tomcat，访问http://localhost:8080/admin，将看到一个登录界面。Admin Web Application采用基于容器管理的安全机制，并采用了Jakarta Struts框架。一旦作为“admin”角色的用户登录管理界面，将能够使用这个管理界面配置Tomcat。

　　2.配置应用管理（Manager Web Application）

　　Manager Web Application让你通过一个比Admin Web Application更为简单的用户界面，执行一些简单的Web应用任务。

　　Manager Web Application被被定义在一个自动部署文件中：

　　CATALINA_BASE/webapps/manager.xml 。

　　必须编辑这个文件，以确保context的docBase参数是绝对路径，也就是说CATALINA_HOME/server/webapps/manager的绝对路径。

　　如果使用的是UserDatabaseRealm，那么需要添加一个角色和一个用户到CATALINA_BASE/conf/tomcat-users.xml文件中。接下来，编辑这个文件，添加一个名为“manager”的角色到该文件中：

　　<role name=“manager”>

　　同样需要有一个角色为“manager”的用户。像已经存在的用户那样，添加一个新用户（改变密码使其更加安全）：

　　<user name=“manager” password=“deep_dark_secret” roles=“manager”/>

　　然后重新启动Tomcat，访问http://localhost/manager/list，将看到一个很朴素的文本型管理界面，或者访问http: //localhost/manager/html/list，将看到一个HMTL的管理界面。不管是哪种方式都说明你的Manager Web Application现在已经启动了。

　　Manager application可以在没有系统管理特权的基础上，安装新的Web应用，以用于测试。如果我们有一个新的web应用位于 /home/user/hello下在，并且想把它安装到 /hello下，为了测试这个应用，可以这么做，在第一个文件框中输入“/hello”（作为访问时的path），在第二个文本框中输入“file: /home/user/hello”（作为Config URL）。

　　Manager application还允许停止、重新启动、移除以及重新部署一个web应用。停止一个应用使其无法被访问，当有用户尝试访问这个被停止的应用时，将 看到一个503的错误——“503 - This application is not currently available”。

　　移除一个web应用，只是指从Tomcat的运行拷贝中删除了该应用，如果重新启动Tomcat，被删除的应用将再次出现（也就是说，移除并不是指从硬盘上删除）。

　　3.部署一个web应用

　　有两个办法可以在系统中部署web服务。

　　1> 拷贝WAR文件或者web应用文件夹（包括该web的所有内容）到$CATALINA_BASE/webapps目录下。

　　2> 为web服务建立一个只包括context内容的XML片断文件，并把该文件放到$CATALINA_BASE/webapps目录下。这个web应用本身可以存储在硬盘上的任何地方。

　　如果有一个WAR文件，想部署它，则只需要把该文件简单的拷贝到CATALINA_BASE/webapps目录下即可，文件必须以“。war”作 为扩展名。一旦Tomcat监听到这个文件，它将（缺省的）解开该文件包作为一个子目录，并以WAR文件的文件名作为子目录的名字。接下来，Tomcat 将在内存中建立一个context，就好象在server.xml文件里建立一样。当然，其他必需的内容，将从server.xml中的 DefaultContext获得。

　　部署web应用的另一种方式是写一个Context XML片断文件，然后把该文件拷贝到CATALINA_BASE/webapps目录下。一个Context片断并非一个完整的XML文件，而只是一个 context元素，以及对该应用的相应描述。这种片断文件就像是从server.xml中切取出来的context元素一样，所以这种片断被命名为 “context片断”。

　　举个例子，如果我们想部署一个名叫MyWebApp.war的应用，该应用使用realm作为访问控制方式，我们可以使用下面这个片断：

　　<!--

　　Context fragment for deploying MyWebApp.war

　　-->

　　<Context path=“/demo” docBase=“webapps/MyWebApp.war”

　　debug=“0” privileged=“true”>

　　<Realm className=“org.apache.catalina.realm.UserDatabaseRealm”

　　resourceName=“UserDatabase”/>

　　</Context>

　　把该片断命名为“MyWebApp.xml”，然后拷贝到CATALINA_BASE/webapps目录下。

　　这种context片断提供了一种便利的方法来部署web应用，不需要编辑server.xml，除非想改变缺省的部署特性，安装一个新的web应用时不需要重启动Tomcat。

　　4.配置虚拟主机（Virtual Hosts）

　　关于server.xml中“Host”这个元素，只有在设置虚拟主机的才需要修改。虚拟主机是一种在一个web服务器上服务多个域名的机制，对每个域 名而言，都好象独享了整个主机。实际上，大多数的小型商务网站都是采用虚拟主机实现的，这主要是因为虚拟主机能直接连接到Internet并提供相应的带 宽，以保障合理的访问响应速度，另外虚拟主机还能提供一个稳定的固定IP。

　　基于名字的虚拟主机可以被建立在任何web服务器上，建立的方法就是通过在域名服务器（DNS）上建立IP地址的别名，并且告诉web服务器把去往不同域 名的请求分发到相应的网页目录。

　　在Tomcat中使用虚拟主机，需要设置DNS或主机数据。为了测试，为本地IP设置一个IP别名就足够了，接下来，你需要在server.xml中添加几行内容，如下：

　　<Server port=“8005” shutdown=“SHUTDOWN” debug=“0”>

　　<Service name=“Tomcat-Standalone”>

　　<Connector className=“org.apache.coyote.tomcat4.CoyoteConnector”

　　port=“8080” minProcessors=“5” maxProcessors=“75”

　　enableLookups=“true” redirectPort=“8443”/>

　　<Connector className=“org.apache.coyote.tomcat4.CoyoteConnector”

　　port=“8443” minProcessors=“5” maxProcessors=“75”

　　acceptCount=“10” debug=“0” scheme=“https” secure=“true”/>

　　<Factory className=“org.apache.coyote.tomcat4.CoyoteServerSocketFactory”

　　clientAuth=“false” protocol=“TLS” />

　　</Connector>

　　<Engine name=“Standalone” defaultHost=“localhost” debug=“0”>

　　<!-- This Host is the default Host -->

　　<Host name=“localhost” debug=“0” appBase=“webapps”

　　unpackWARs=“true” autoDeploy=“true”>

　　<Context path=“” docBase=“ROOT” debug=“0”/>

　　<Context path=“/orders” docBase=“/home/ian/orders” debug=“0”

　　reloadable=“true” crossContext=“true”>

　　</Context>

　　</Host>

　　<!-- This Host is the first “Virtual Host”: www.example.com -->

　　<Host name=“www.example.com” appBase=“/home/example/webapp”>

　　<Context path=“” docBase=“.”/>

　　</Host>

　　</Engine>

　　</Service>

　　</Server>

　　Tomcat的server.xml文件，在初始状态下，只包括一个虚拟主机，但是它容易被扩充到支持多个虚拟主机。在前面的例子中展示的是一个简单的 server.xml版本，其中粗体部分就是用于添加一个虚拟主机。每一个Host元素必须包括一个或多个context元素，所包含的context元 素中必须有一个是默认的context，这个默认的context的显示路径应该为空（例如，path=“”）。

　　5.配置基础验证（Basic Authentication）

　　容器管理验证方法控制着当用户访问受保护的web应用资源时，如何进行用户的身份鉴别。当一个web应用使用了Basic Authentication（BASIC参数在web.xml文件中auto-method元素中设置），而有用户访问受保护的web应用时， Tomcat将通过HTTP Basic Authentication方式，弹出一个对话框，要求用户输入用户名和密码。在这种验证方法中，所有密码将被以64位的编码方式在网络上传输。

　　注意：使用Basic Authentication通过被认为是不安全的，因为它没有强健的加密方法，除非在客户端和服务器端都使用HTTPS或者其他密码加密码方式（比如， 在一个虚拟私人网络中）。若没有额外的加密方法，网络管理员将能够截获（或滥用）用户的密码。但是，如果是刚开始使用Tomcat，或者你想在你的 web应用中测试一下基于容器的安全管理，Basic Authentication还是非常易于设置和使用的。只需要添加<security-constraint>和<login- config>两个元素到web应用的web.xml文件中，并且在CATALINA_BASE/conf/tomcat-users.xml 文件中添加适当的<role>和<user>即可，然后重新启动Tomcat。

　　6.配置单点登录（Single Sign-On）

　　一旦设置了realm和验证的方法，就需要进行实际的用户登录处理。一般说来，对用户而言登录系统是一件很麻烦的事情，必须尽量减少用户登录验证的 次数。作为缺省的情况，当用户第一次请求受保护的资源时，每一个web应用都会要求用户登录。如果运行了多个web应用，并且每个应用都需要进行单独的 用户验证，那这看起来就有点像在用户搏斗。用户们不知道怎样才能把多个分离的应用整合成一个单独的系统，所有用户也就不知道他们需要访问多少个不 同的应用，只是很迷惑，为什么总要不停的登录。

　　Tomcat 4的“single sign-on”特性允许用户在访问同一虚拟主机下所有web应用时，只需登录一次。为了使用这个功能，只需要在Host上添加一个SingleSignOn Valve元素即可，如下所示：

　　<Valve className=“org.apache.catalina.authenticator.SingleSignOn”

　　debug=“0”/>

　　在Tomcat初始安装后，server.xml的注释里面包括SingleSignOn Valve配置的例子，只需要去掉注释，即可使用。那么，任何用户只要登录过一个应用，则对于同一虚拟主机下的所有应用同样有效。

　　使用single sign-on valve有一些重要的限制：

　　1> value必须被配置和嵌套在相同的Host元素里，并且所有需要进行单点验证的web应用（必须通过context元素定义）都位于该Host下。

　　2> 包括共享用户信息的realm必须被设置在同一级Host中或者嵌套之外。

　　3> 不能被context中的realm覆盖。

　　4> 使用单点登录的web应用最好使用一个Tomcat的内置的验证方式（被定义在web.xml中的<auth-method>中），这比自定 义的验证方式强，Tomcat内置的的验证方式包括basic、digest、form和client-cert。

　　5> 如果你使用单点登录，还希望集成一个第三方的web应用到你的网站中来，并且这个新的web应用使用它自己的验证方式，而不使用容器管理安全，那你基本上 就没招了。用户每次登录原来所有应用时需要登录一次，并且在请求新的第三方应用时还得再登录一次。

　　6> 单点登录需要使用cookies。

　　7.配置用户定制目录（Customized User Directores）

　　一些站点允许个别用户在服务器上发布网页。例如，一所大学的学院可能想给每一位学生一个公共区域，或者是一个ISP希望给一些web空间给他的客户，但这又不是虚拟主机。在这种情况下，一个典型的方法就是在用户名前面加一个特殊字符（~），作为每位用户的网站，比如：

　　http://www.cs.myuniversity.edu/~username提供两种方法在主机上映射这些个人网站，主要使用一对特殊的Listener元素。Listener的className属性应该是 org.apache.catalina.startup.UserConfig，userClass属性应该是几个映射类之一。如果电脑系统是 Unix，它将有一个标准的/etc/passwd文件，该文件中的帐号能够被运行中的Tomcat很容易的读取，该文件指定了用户的主目录，使用 PasswdUserDatabase 映射类。

　　http://members.mybigisp.com/~username

　　Tomcat

　　<Listener className=“org.apache.catalina.startup.UserConfig”

　　directoryName=“public_html”

　　userClass=“org.apache.catalina.startup.PasswdUserDatabase”/>

　　web文件需要放置在像/home/users/ian/public_html 或者 /users/jbrittain/public_html一样的目录下面。当然你也可以改变public_html 到其他任何子目录下。

　　实际上，这个用户目录根本不一定需要位于用户主目录下里面。如果你没有一个密码文件，但你又想把一个用户名映射到公共的像/home一样目录的子目录里面，则可以使用HomesUserDatabase类。

　　<Listener className=“org.apache.catalina.startup.UserConfig”

　　directoryName=“public_html” homeBase=“/home”

　　userClass=“org.apache.catalina.startup.HomesUserDatabase”/>

　　这样一来，web文件就可以位于像/home/ian/public_html 或者 /home/jasonb/public_html一样的目录下。这种形式对Windows而言更加有利，你可以使用一个像c:\home这样的目录。

　　这些Listener元素，如果出现，则必须在Host元素里面，而不能在context元素里面，因为它们都用应用于Host本身。

　　8.在Tomcat中使用CGI脚本

　　Tomcat主要是作为Servlet/JSP容器，但它也有许多传统web服务器的性能。支持通用网关接口（Common Gateway Interface，即CGI）就是其中之一，CGI提供一组方法在响应浏览器请求时运行一些扩展程序。CGI之所以被称为通用，是因为它能在大多数程序 或脚本中被调用，包括：Perl，Python，awk，Unix shell scripting等，甚至包括Java。不会把一个Java应用程序当作CGI来运行，毕竟这样太过原始。一般而言，开发Servlet总 要比CGI具有更好的效率，因为当用户点击一个链接或一个按钮时，不需要从操作系统层开始进行处理。

　　Tomcat包括一个可选的CGI Servlet，允许你运行遗留下来的CGI脚本。

　　为了使Tomcat能够运行CGI，必须做的几件事：

　　1. 把servlets-cgi.renametojar （在CATALINA_HOME/server/lib/目录下）改名为servlets-cgi.jar。处理CGI的servlet应该位于Tomcat的CLASSPATH下。

　　2. 在Tomcat的CATALINA_BASE/conf/web.xml 文件中，把关于<servlet-name> CGI的那段的注释去掉（默认情况下，该段位于第241行）。

　　3. 同样，在Tomcat的CATALINA_BASE/conf/web.xml文件中，把关于对CGI进行映射的那段的注释去掉（默认情况下，该段位于第299行）。注意，这段内容指定了HTML链接到CGI脚本的访问方式。

　　4. 可以把CGI脚本放置在WEB-INF/cgi 目录下（注意，WEB-INF是一个安全的地方，你可以把一些不想被用户看见或基于安全考虑不想暴露的文件放在此处），或者也可以把CGI脚本放置在 context下的其他目录下，并为CGI Servlet调整cgiPathPrefix初始化参数。这就指定的CGI Servlet的实际位置，且不能与上一步指定的URL重名。

　　5. 重新启动Tomcat，你的CGI就可以运行了。

　　在Tomcat中，CGI程序缺省放置在WEB-INF/cgi目录下，正如前面所提示的那样，WEB-INF目录受保护的，通过客户端的浏览器无法窥探 到其中内容，所以对于放置含有密码或其他敏感信息的CGI脚本而言，这是一个非常好的地方。为了兼容其他服务器，尽管你也可以把CGI脚本保存在传统的 /cgi-bin目录，但要知道，在这些目录中的文件有可能被网上好奇的冲浪者看到。另外，在Unix中，请确定运行Tomcat的用户有执行CGI脚本 的权限。

　　　9.改变Tomcat中的JSP编译器（JSP Compiler）

　　在Tomcat 4.1（或更高版本，大概），JSP的编译由包含在Tomcat里面的Ant程序控制器直接执行。这听起来有一点点奇怪，但这正是Ant有意为之的一部 分，有一个API文档指导开发者在没有启动一个新的JVM的情况下，使用Ant。这是使用Ant进行Java开发的一大优势。另外，这也意味着你现在能够 在Ant中使用任何javac支持的编译方式，这里有一个关于Apache Ant使用手册的javac page列表。使用起来是容易的，因为你只需要在<init-param> 元素中定义一个名字叫“compiler”，并且在value中有一个支持编译的编译器名字，示例如下：

　　<servlet>

　　<servlet-name>jsp</servlet-name>

　　<servlet-class>

　　org.apache.jasper.servlet.JspServlet

　　</servlet-class>

　　<init-param>

　　<param-name>logVerbosityLevel</param-name>

　　<param-value>WARNING</param-value>

　　</init-param>

　　<init-param>

　　<param-name>compiler</param-name>

　　<param-value>jikes</param-value>

　　</init-param>

　　<load-on-startup>3</load-on-startup>

　　</servlet>

　　当然，给出的编译器必须已经安装在你的系统中，并且CLASSPATH可能需要设置，那处决于你选择的是何种编译器。

　　10.限制特定主机访问（Restricting Access to Specific Hosts）

　　有时，可能想限制对Tomcat web应用的访问，比如，希望只有指定的主机或IP地址可以访问应用。这样一来，就只有那些指定的的客户端可以访问服务的内容了。为了实现这种效 果，Tomcat提供了两个参数供你配置：RemoteHostValve 和RemoteAddrValve。

　　通过配置这两个参数，可以让你过滤来自请求的主机或IP地址，并允许或拒绝哪些主机/IP。与之类似的，在Apache的httpd文件里有对每个目录的允许/拒绝指定。

　　可以把Admin Web application设置成只允许本地访问，设置如下：

　　<Context path=“/path/to/secret_files” …>

　　<Valve className=“org.apache.catalina.valves.RemoteAddrValve”

　　allow=“127.0.0.1” deny=“”/>

　　</Context>

　　如果没有给出允许主机的指定，那么与拒绝主机匹配的主机就会被拒绝，除此之外的都是允许的。与之类似，如果没有给出拒绝主机的指定，那么与允许主机匹配的主机就会被允许，除此之外的都是拒绝的。
```

==resin安装 http://fangniuwa.blog.51cto.com/10209030/1763488/==

```
1、java的安装




jdk下载地址

http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

1.1、安装jdk-1.8



1
2
3
4
5
6
7
8
9
10
11
 

[root@fnw ~]# ls

anaconda-ks.cfg  Desktop  jdk-8u77-linux-x64.rpm 

[root@fnw ~]# rpm -ivh jdk-8u77-linux-x64.rpm 

[root@fnw ~]# java -version

openjdk version "1.8.0_65"

OpenJDK Runtime Environment (build 1.8.0_65-b17)

OpenJDK 64-Bit Server VM (build 25.65-b01, mixed mode)

[root@fnw ~]# javac

………… 略

[root@fnw ~]# java

………… 略 


1.2 jdk环境变量的配置



1
2
3
4
5
6
7
8
9
10
11
12
 

[root@fnw ~]# echo "JAVA_HOME=/usr/java/jdk1.8.0_77" >> /etc/profile

[root@fnw ~]# echo "JRE_HOME=/usr/java/jdk1.8.0_77/jre" >> /etc/profile

[root@fnw ~]# echo "PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin" >> /etc/profile

[root@fnw ~]# echo "CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib" >> /etc/profile

[root@fnw ~]# echo "export JAVA_HOME JRE_HOME PATH CLASSPATH" >> /etc/profile

[root@fnw ~]# tail -5 /etc/profile

JAVA_HOME=/usr/java/jdk1.8.0_77

JRE_HOME=/usr/java/jdk1.8.0_77/jre

PATH=/usr/lib64/qt-3.3/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin:/bin:/bin:/usr/java/jdk1.8.0_77/bin:/usr/java/jdk1.8.0_77/jre/bin

CLASSPATH=.:/usr/java/jdk1.8.0_77/lib/dt.jar:/usr/java/jdk1.8.0_77/lib/tools.jar:/usr/java/jdk1.8.0_77/jre/lib

export JAVA_HOME JRE_HOME PATH CLASSPATH

[root@fnw ~]# source /etc/profile 








2、resin的安装


2.1、下载安装resin

resin的载地址

http://caucho.com/products/resin/download



1
2
3
4
5
6
7
8
9
10
11
12
 

[root@fnw ~]# rpm -ivh resin-pro-4.0.45-1.x86_64.rpm 

warning: resin-pro-4.0.45-1.x86_64.rpm: Header V3 RSA/SHA1 Signature, key ID 6ef6d565: NOKEY

error: Failed dependencies:

openssl-devel is needed by resin-pro-4.0.45-1.x86_64

[root@fnw ~]# yum install openssl-devel -y

[root@fnw ~]# rpm -ivh resin-pro-4.0.45-1.x86_64.rpm 

warning: resin-pro-4.0.45-1.x86_64.rpm: Header V3 RSA/SHA1 Signature, key ID 6ef6d565: NOKEY

Preparing...                          ################################# [100%]

Updating / installing...

   1:resin-pro-4.0.45-1               ################################# [100%]

Resin/4.0.45 launching watchdog at 127.0.0.1:6600

Resin/4.0.45 started -server 'app-0' with watchdog at 127.0.0.1:6600 





2.2、resin环境变量的配置



1
2
3
4
 

[root@fnw resin]# echo "RESIN_HOME=/usr/local/share/resin" >> /etc/profile

[root@fnw resin]# echo "RESIN_ROOT=/var/resin" >> /etc/profile

[root@fnw resin]# echo "export RESIN_HOME RESIN_ROOT" >> /etc/profile

[root@fnw resin]# source /etc/profile 





2.3、resin主要目录简介

/etc/resin/resin.properties  # properties 配置文件，以key:value 形式出现，供resin.xml使用

/etc/resin/resin.xml         # resin主配置文件




/var/resin/                  # resin的根目录

/var/resin/webapps/          # web应用程序在resin默认部署目录

/var/log/resin/              # resin服务器日志存放目录




/usr/local/share/resin		 # resin服务器的家目录（主要存放resin的启动脚本、运行过程常用的jar包、resin的一些常用动态库文件）



1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
 

[root@fnw resin]# tree /usr/local/share/resin

/usr/local/share/resin

├── bin                                    # resin的启动脚本

│   ├── resinctl

│   ├── resinctl.in

│   ├── resin.sh

│   ├── start.bat

│   └── stop.bat

├── lib                                    # 运行过程常用的jar包

│   ├── activation.jar

│   ├── activation.LICENSE

│   ├── eclipselink-2.4.0.jar

│   ├── hibernate-validator-4.3.0.Final.jar

│   ├── javaee-16.jar

│   ├── javamail-141.jar

│   ├── javamail-14.LICENSE

│   ├── javax.faces-2.1.24.jar

│   ├── jboss-logging-3.1.0.CR2.jar

│   ├── jsf.LICENSE

│   ├── pro.jar

│   ├── resin-eclipselink.jar

│   ├── resin.jar

│   ├── validation-api-1.0.0.GA.jar

│   ├── webservices-extra-api.jar

│   ├── webservices.LICENSE

│   └── webutil.jar

└── libexec64                               # resin的一些常用动态库文件

    ├── libresin_os.so

    ├── libresin.so

    └── libresinssl.so 


	

2.4、resin的启动



1
 

[root@fnw resin]# bin/resinctl -conf /etc/resin/resin.xml start 





2.5、java进程查看命令



1
2
3
4
 

[root@fnw resin]# jps

48643 WatchdogManager

48691 Resin

48761 Jps 








3、jdk1.8和resin4.0自动化安装shell脚本



1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
 

#1、install jdk1.8.0_77

rpm -ivh jdk-8u77-linux-x64.rpm

 

#2、config jdk1.8.0_77 environment

echo "JAVA_HOME=/usr/java/jdk1.8.0_77" >> /etc/profile

echo "JRE_HOME=/usr/java/jdk1.8.0_77/jre" >> /etc/profile

echo "PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin" >> /etc/profile

echo "CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib" >> /etc/profile

echo "export JAVA_HOME JRE_HOME PATH CLASSPATH" >> /etc/profile

tail -5 /etc/profile

source /etc/profile

 

#3、install resin4.0.45

yum install openssl-devel -y

rpm -ivh resin-pro-4.0.45-1.x86_64.rpm 

 

#4、config resin4.0.45 environment

echo "RESIN_HOME=/usr/local/share/resin" >> /etc/profile

echo "RESIN_ROOT=/var/resin" >> /etc/profile

echo "export RESIN_HOME RESIN_ROOT" >> /etc/profile

source /etc/profile

 

#5、running resin 

bin/resinctl -conf /etc/resin/resin.xml start 

```