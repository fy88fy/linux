#!/bin/bash
##
while [[ true ]];do
host=$1

ping -c 1 $host &>null

if [[ $? -eq 0 ]]; then
	echo -e "\033[32m The $host is up!\033[0m"
else
	echo -e "\033[31mThe $host is down! Please check!\033[0m"
fi
done

