#!/bin/bash
#
for ip in 192.168.1.{1..254};
do 
	ping $ip -c 2 
	if [[ $? -eq 0 ]] ;
	then 
		echo "$ip is alive."
	fi
done
