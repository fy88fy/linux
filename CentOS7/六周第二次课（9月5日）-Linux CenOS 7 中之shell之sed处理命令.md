[toc]
## 9.4/9.5 sed
### sed
    sed是一个文件处理工具，本身是一个管道命令，主要是以行为单位进行处理，可以将数据行进行替换、删除、新增等特定工作.
    命令使用样例:
    mkdir sed  建立sed文件夹
    cd sed     进入sed文件夹
    cp /etc/passwd test.txt   拷贝实验文件
#### (1) 匹配查找

    sed -n '/root/'p test.txt    打印出带root关键字的行
    sed -nr '/o{2}/'p test.txt   打印出带2个o的行
    sed -nr '/root|Bus/'p test.txt   打印出带root或Bus关键字的行
    sed -n '2'p test.txt          打印出第二行
    sed -n '1,6'p test.txt    打印出1,6行
    sed -n '25,$'p test.txt    打印出从25行至文章结束
    sed -n '1,$'p test.txt     全部打印出来
    sed -n '/^1/'p test.txt    打印出以1开头的行
    sed -n 'in$'p test.txt     打印出以in结尾的行
    sed -n '/r..o/'p test.txt   打印出r与o之间有两个字符的行
    sed -n 'oo*'p test.txt      打印出一个o,或多个o的行
    sed -e '1'p -e '/111/'p -n test.txt  打印出第一行和带111关键字的行
    sed -nr -e '1'p -e '/Bus/'p -e '/(Bus)+/'p  test.txt 
    sed -nr '/111/p;/222/p,/Bus/p'  test.txt (不同的写法)
    sed -n '/bus/'Ip test.txt     打印出带bus关键字的行忽略大小写
#### (2) 删除

    sed '1'd test.txt          删除第一行
    sed '1,10'd test.txt      删除1到10行
    sed '^$'d test.txt         删除空行
    sed '/root/'d test.txt    删除带root关键字的行
#### (3) 替换  

    sed 's@root@ROOT@g' test.txt  替换root为ROOT
    sed 's/^#//g' test.txt        把开头的#删除
    sed 's/^.*$/# &/g' test.txt   把行首加上"# "
    sed 's/[a-zA-Z]//g'  test.txt  删除文中的字母
    
    sed -r '1,10s/ro+/r/g' test.txt     1到10行中的只要是r后o的都替换为r
#### (4) 插入

    sed -r 's/(.*)/aaa:\1/' test.txt    行首加上aaa:
    sed -r 's/(.*)/aaa:&/' test.txt     行首加上aaa:
#### (5) 换位

    sed -r 's/([^:]+):(.*):([^:]+)/\3:\2:\1/g' test.txt  以：分割，实现首段和尾段互换位置
    sed -r 's#([^:]+)(:.*:)(.*$)#\3\2\1#g' test.txt  以：分割，实现首段和尾段互换位置(另一种方法)
    awk -F: 'OFS=":" {print $7,$2,$3,$4,$5,$6,$1}' test.txt 以：分割，实现首段和尾段互换位置(另一种方法)
    sed -r 's#([^:]+)(:.*/)([^/$]+)#\3\2\1#g' test.txt 替换第一个单词和最后一个单词



#### (6) 更改原文件

    sed -i '1d' test.txt 改变真正的内容 

 ```
[root@0 sed]# sed -n '/root/'p test.txt 
root:x:0:0:root:/root:/bin/bash
operator:x:11:0:operator:/root:/sbin/nologin
[root@0 sed]# 

```  

```
[root@0 sed]# sed -n '/bus/'Ip test.txt 
aaaaaaaaaaBUS
systemd-bus-proxy:x:999:997:systemd Bus Proxy:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
```   

```   
[root@0 sed]# sed  's@/sbin@\\bin@g' test.txt 
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:\bin/nologin
aaaaaaaaaaBUS
daemon:x:2:2:daemon:\bin:\bin/nologin
```


    
    