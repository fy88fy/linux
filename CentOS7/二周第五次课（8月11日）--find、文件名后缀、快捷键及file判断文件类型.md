[toc]
## 2.23/2.24/2.25 find命令


locate 查找命令，从本地生成的数据库中查找文件

locate 无法查找/tmp中的文件

如果没有locate命令，安装软件包:mlocate 

```
[root@VM_46_188_centos ~]# which locate 
/usr/bin/locate
[root@VM_46_188_centos ~]# rpm -qf /usr/bin/locate
mlocate-0.26-5.el7.x86_64
[root@VM_46_188_centos ~]#
```
### 快捷键：
- ctrl + d 相当然于logout 注销
- ctrl + l 清屏
- ctrl + c 中止
- ctrl + u  删除光标前内容
- ctrl + k  删除光标后内容
- ctrl + e 定位光标到最end
- ctrl + a 定位光标到最开始
- ctrl + s 锁定屏幕
- ctrl + q 解锁屏幕

### 三个time:

> stat 2.txt  查看2.txt文件的三个time
> 
> atime:最近访问内容   cat查看  
> 
> mtime:最近更改内容 echo > /vi 
> 
> ctime 最近改动inode 信息  touch
> 
> 更改了文件内容，mtime ctime 都会变。
> 更改了文件ctime ,mtime 不会变。
> 

```
[root@VM_46_188_centos ~]# stat 2.txt
  File: '2.txt'
  Size: 0         	Blocks: 0          IO Block: 4096   regular empty file
Device: fd01h/64769d	Inode: 131211      Links: 1
Access: (0664/-rw-rw-r--)  Uid: (    0/    root)   Gid: (    0/    root)
Access: 2017-07-12 23:21:57.265143288 +0800
Modify: 2017-07-12 23:21:57.265143288 +0800
Change: 2017-07-12 23:21:57.265143288 +0800
 Birth: -
[root@VM_46_188_centos ~]#
```



**mv 更名后，ctime 会变:**
```
[root@VM_46_188_centos ~]# stat 2.txt
  File: '2.txt'
  Size: 7         	Blocks: 8          IO Block: 4096   regular f
ileDevice: fd01h/64769d	Inode: 131211      Links: 1
Access: (0664/-rw-rw-r--)  Uid: (    0/    root)   Gid: (    0/    ro
ot)Access: 2017-08-10 21:44:43.123521346 +0800
Modify: 2017-08-10 21:46:39.547587309 +0800
Change: 2017-08-10 21:46:39.649587367 +0800
 Birth: -
[root@VM_46_188_centos ~]# mv 2.txt 22.txt
[root@VM_46_188_centos ~]# stat 22.txt 
  File: '22.txt'
  Size: 7         	Blocks: 8          IO Block: 4096   regular f
ileDevice: fd01h/64769d	Inode: 131211      Links: 1
Access: (0664/-rw-rw-r--)  Uid: (    0/    root)   Gid: (    0/    ro
ot)Access: 2017-08-10 21:44:43.123521346 +0800
Modify: 2017-08-10 21:46:39.547587309 +0800
Change: 2017-08-10 21:50:32.438719259 +0800

```

### find命令：
> 1. find /etc/  -type d -name "fxq"  查找目录
> 1. find /etc/  -type f -name "fxq" 查找文件
> 1. find /etc/  -type c -name "fxq"  查找字符设备文件
> 1. find /etc/  -type b -name "fxq"  查找块文件
> 1. find /etc/  -type s -name "fxq"  查找套接字文件
> 1. find /etc/  -type l -name "fxq"  查找链接文件
> 1. find / -type f -mtime -1 查找一天内更改过内容的文件
> 1. find / -type f -mtime +1  查找大于一天更改过内容的文件
> 1. find /etc/ -type f -o mtime -1 -name "*.conf" （-o 是或者的意思）
> 1. find / -inum inode号 查找硬链接文件
> 1. find / -mmin -60  一小时内
> 1. find / -type f -mmin -120 -exec ls -l {} \\;  把所有小于2小时内的文件列出。
> 1. find / -type f -mmin -1 -exec mv {} {}.bak \\;查找出来分钟的文件更名后面加.bak
> 1. find / -size +10k -type f -exec ls -l {} \\; 列出大于10k文件
> 1. find / -size +10M -type f -exec ls -l {} \\; 列出大于10M的文件
> 1. find /etc -type f -mtime +1 -mtime -30 查找大于一天小于30天更改过内容的文件。
> 1. find . ! -name 9 -exec rm -rf {} \;查找并删除名字不是9的文件
> 
> 
**-o 或者举例：**
```
[root@VM_46_188_centos ~]# find / -name "my.cnf" -o -name "httpd.conf
"/etc/my.cnf
/backup/hszd_zabbix/bak/httpd.conf

```


## 2.26 文件名后缀
- 区分大小写
- 后缀名为好识别 .txt .gz .log .conf
- LANG=en 默认是zh_CN.UTF-8中文,要显示中文，系统一定要安装中文支持的语言包。

### file  查看文件的类型：

```
目录：
[root@VM_46_188_centos ~]# file /bin/
/bin/: directory
[root@VM_46_188_centos ~]#

二进制文件：
[root@VM_46_188_centos ~]# file /bin/cp
/bin/cp: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for GNU/Linux 2.6.32, BuildID[sha1]=9c78c9014112530b46143fe598a278c5ad792edb, 
stripped
[root@VM_46_188_centos ~]#

字符设备
[root@VM_46_188_centos ~]# file /dev/pts/0
/dev/pts/0: character special
[root@VM_46_188_centos ~]#

链接文件：
[ec2-user@ip-172-31-29-125 ~]$ ll /usr/sbin/iptables
lrwxrwxrwx. 1 root root 13 Oct 20  2016 /usr/sbin/iptables -> xtables
-multi[ec2-user@ip-172-31-29-125 ~]$ file /usr/sbin/iptables
/usr/sbin/iptables: symbolic link to `xtables-multi'
[ec2-user@ip-172-31-29-125 ~]$ 

块文件：
[ec2-user@ip-172-31-29-125 ~]$ file /dev/xvda
/dev/xvda: block special
[ec2-user@ip-172-31-29-125 ~]$

套接字文件：
/dev/log
[root@VM_46_188_centos ~]# file /data/mysql/mysql.sock
/data/mysql/mysql.sock: socket
[root@VM_46_188_centos ~]# 

[root@VM_46_188_centos ~]# file /dev/log
/dev/log: socket
[root@VM_46_188_centos ~]# 


```


