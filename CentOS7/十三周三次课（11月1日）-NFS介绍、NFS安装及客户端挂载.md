[toc]
## 14.1 NFS介绍
### (1) NFS介绍
```
 NFS是Network File System的缩写
 NFS最早由Sun公司开发，分2,3,4三个版本，2和3由Sun起草开发，4.0开始Netapp公司参与并主导开发，最新为4.1版本
 NFS数据传输基于RPC协议，RPC为Remote Procedure Call的简写。
 NFS应用场景是：A,B,C三台机器上需要保证被访问到的文件是一样的，A共享数据出来，B和C分别去挂载A共享的数据目录，从而B和C访问到的数据和A上的一致

```
### (2)NFS架构
![image](http://note.youdao.com/yws/api/personal/file/24727679CC2D4C0EB28FE54DF2AE6499?method=download&shareKey=0850daa17c4111a753544353691859a5)

### (3)NFS原理
![image](http://note.youdao.com/yws/api/personal/file/CE34EF159A134EBAB452BD41457519F6?method=download&shareKey=960775d1fc05832e96bf871b55457fe8)
## 14.2 NFS服务端安装配置
### (1)安装NFS
```
 yum install -y nfs-utils rpcbind
 
 vim /etc/exports //加入如下内容
/home/nfstestdir 192.168.103.0/24(rw,sync,all_squash,anonuid=1000,anongid=1000)
 保存配置文件后，执行如下准备操作
 
 mkdir /home/nfstestdir
 chmod 777 /home/nfstestdir
```
### (2)配置服务开机启动
```
 systemctl start rpcbind 
 systemctl start nfs
 systemctl enable rpcbind 
 systemctl enable nfs

```
```
[root@fxq-2 ~]# netstat -lntp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      1/systemd           
tcp        0      0 0.0.0.0:20048           0.0.0.0:*               LISTEN      3094/rpc.mountd     
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      2353/nginx: master  
tcp        0      0 0.0.0.0:34482           0.0.0.0:*               LISTEN      -                   
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      2081/sshd           
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      2591/master         
tcp        0      0 0.0.0.0:443             0.0.0.0:*               LISTEN      2353/nginx: master  
tcp        0      0 0.0.0.0:45404           0.0.0.0:*               LISTEN      3082/rpc.statd      
tcp        0      0 0.0.0.0:2049            0.0.0.0:*               LISTEN      -                   

```

## 14.3 NFS配置选项  
### (1)挂载参数说明
```
 rw 读写
 ro 只读
 sync 同步模式，内存数据实时写入磁盘
 async 非同步模式
 no_root_squash 客户端挂载NFS共享目录后，root用户不受约束，权限很大
 root_squash 与上面选项相对，客户端上的root用户收到约束，被限定成某个普通用户
 all_squash 客户端上所有用户在使用NFS共享目录时都被限定为一个普通用户
 anonuid/anongid 和上面几个选项搭配使用，定义被限定用户的uid和gid

```
### (2)客户端挂载：
```
 systemctl stop firewalld
 setenforce 0
 yum install -y nfs-utils
 showmount -e 192.168.103.132 //该ip为NFS服务端ip
 mount -t nfs 192.168.103.132:/home/nfstestdir /mnt
 df -h
 touch /mnt/fxq.txt
 ls -l /mnt/fxq.txt //可以看到文件的属主和属组都为1000



[root@fxq-1~]# showmount -e 192.168.103.132
Export list for 192.168.103.132:
/home/nfstestdir 192.168.103.0/24
[root@fxq-1 ~]# 

[root@fxq-1 ~]# mount -t nfs 192.168.103.132:/home/nfstestdir /mnt
mount.nfs: access denied by server while mounting 192.168.103.132:/home/nfstestdir
[root@fxq-1 ~]# mount -t nfs 192.168.103.132:/home/nfstestdir /mnt
[root@fxq-1 ~]# df -h
文件系统                          容量  已用  可用 已用% 挂载点
/dev/mapper/cl-root                28G  5.3G   23G   19% /
devtmpfs                          237M     0  237M    0% /dev
tmpfs                             248M     0  248M    0% /dev/shm
tmpfs                             248M  4.7M  243M    2% /run
tmpfs                             248M     0  248M    0% /sys/fs/cgroup
/dev/sda1                        1014M  141M  874M   14% /boot
tmpfs                              50M     0   50M    0% /run/user/0
192.168.103.132:/home/nfstestdir   28G  3.9G   24G   14% /mnt
[root@fxq-1 ~]# 



[root@fxq-1 mnt]# touch fxq.txt
[root@fxq-1 mnt]# ll
总用量 0
-rw-r--r-- 1 fxq fxq 0 11月  2 15:07 fxq.txt
[root@fxq-1 mnt]# id fxq
uid=1000(fxq) gid=1000(fxq) 组=1000(fxq),10(wheel)
[root@fxq-1 mnt]#


[root@fxq-2 ~]# ll /home/nfstestdir/
总用量 0
-rw-r--r-- 1 fxq fxq 0 11月  2 15:07 fxq.txt
[root@fxq-2 ~]# id fxq
uid=1000(fxq) gid=1000(fxq) 组=1000(fxq),10(wheel)
[root@fxq-2 ~]# 

```



