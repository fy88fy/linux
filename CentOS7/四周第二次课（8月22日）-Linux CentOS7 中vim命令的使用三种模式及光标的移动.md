[toc]
## 5.1 vim介绍
vim 是vi 的升级版本：可带颜色显示

### 1. 安装vim

    yum install -y vim-enhanced
    vim /etc/passwd

### 2. 三种模式：

    一般模式：dd p yy 
    
    编辑模式:编辑文件内容
    
    命令模式：：/ ?
    

## 5.2 vim颜色显示和移动光标
### 1. vim 可以根据文件的路径和名字显示颜色

![image](http://note.youdao.com/yws/api/personal/file/965D4E38E936493DA3B988C72C9A95E3?method=download&shareKey=4e5799e929421f2ba899499860f7d4f2)
    
    cp /etc/passwd /tmp 
    vim /tmp/passwd 查看就无颜色
    /etc 下会显示颜色，是他的特性。
    cp /etc/fstab /tmp
    vim /tmp/fstab 查看就有颜色显示.
    
    vim /tmp/passwd 加入“#/test” 就有颜色,
    给/tmp/passwd 改名为passwd.sh 就显示颜色了。
    
### 2. 配置文件：/etc/vimrc

    vim /root/.vimrc 配置自动缩进，语法高亮等功能。

    cp /etc/dnsmasq.conf /tmp/1.conf
    在第一行加入：#!/bin/bash

### 3. 一般模式下移动光标：

    h或向左箭头：向左移动。
    
    j或向下箭头:5j 向下移动5行
    
    k或向上箭头:向上移动
    
    l或向右箭头:向右移动
    
    空格：向右移动 5+空格:向右移动5个字符
    


## 5.3 vim一般模式下移动光标


    ctrl + -> :向右移动一个单词
    ctrl + b或PageUP: 向前翻页
    ctrl + f或PageDown: 向后翻页
    0或^:  移动到行首
    $:     移动到行尾
    gg:    定位到首行
    G:     定位到尾行   
    50G :  定位到50行
    
    

## 5.4 vim一般模式下复制、剪切和粘贴
    
    x: 小写的向后删除一个字符
    X：大写：向前删除一个字符
    
    dd :删除光标所在行
    10dd: 删除（包含光标所在行）10行
    
    p:(小写)在光标所在行下粘贴
    P:(大写)在光标所在行上粘贴
    
    y：复制
    nyy:复制光标下n行
    
    u ：撤销，最多50次
    ctrl + r :反撤销
    
    v: 可视化操作，选中（按自己意愿）进行操作