[toc]
## 6.5 zip压缩工具
### （1）压缩后不删除原文件：

    zip 1.txt.zip 1.txt
    zip -r selinux.zip selinux/
>     [root@0 fxq]# ls
>     1.txt  selinux  selinux.zip
>     [root@0 fxq]# zip 1.txt.zip 1.txt 
>       adding: 1.txt (stored 0%)
>     [root@0 fxq]# ls
>     1.txt  1.txt.zip  selinux  selinux.zip
>     [root@0 fxq]# 
> 
       
>     [root@0 fxq]# ls
>     selinux
>     [root@0 fxq]# zip selinux.zip selinux/
>       adding: selinux/ (stored 0%)
>     [root@0 fxq]# ls
>     selinux  selinux.zip
> 

### （2） 解压缩文件:
    unzip dir.zip
    
>     [root@0 fxq]# ls
>     selinux
>     [root@0 fxq]# zip selinux.zip selinux/
>       adding: selinux/ (stored 0%)
>     [root@0 fxq]# ls
>     selinux  selinux.zip
>     [root@0 fxq]# rm -rf selinux
>     [root@0 fxq]# ls
>     selinux.zip
>     [root@0 fxq]# unzip selinux.zip 
>     Archive:  selinux.zip
>        creating: selinux/
>     [root@0 fxq]# ls
>     selinux  selinux.zip
>     [root@0 fxq]#
### （3） zip解压到指定目录:
    unzip dir.zip -d /root/
### （4） 查看压缩包内的文件列表：
    unzip -l dir.zip
    
## 6.6 tar打包
tar打包工具:
### （1）tar打包：
    # tar -cvf selinux.tar selinux
### （2）tar 解包(文件存在时不提示覆盖)：
    # tar -xvf selinux.tar
### （3）tar 查看打包内的文件列表：
    # tar -tvf selinux.tar
### （4）打包时过滤部分目录或文件：
    # tar -cvf all.tar --exclude semanage.conf.bak selinux/
    
    
>     [root@0 fxq]# tree selinux
>     selinux
>     |-- semanage.conf.bak
>     `-- targeted
>         `-- setrans.conf.bak
>     
>     1 directory, 2 files
>     [root@0 fxq]# 
> 
>     [root@0 fxq]# tar -cvf all.tar --exclude semanage.conf.bak selinux
>     selinux/
>     selinux/targeted/
>     selinux/targeted/setrans.conf.bak
>     [root@0 fxq]# ls
>     1.txt.bz2.xz  all.tar  selinux  selinux.zip
>     [root@0 fxq]# tar -tf all.tar  
>     selinux/
>     selinux/targeted/
>     selinux/targeted/setrans.conf.bak
>     [root@0 fxq]# 

## 6.7 打包并压缩
### (1) tar打包成.gz文件:
    # tar -czvf selinux.tar.gz selinux
    
>     [root@0 fxq]# ls
>     1.txt.bz2.xz  all.tar  selinux  selinux.zip
>     [root@0 fxq]# tar -czvf selinux.tar.gz selinux
>     selinux/
>     selinux/semanage.conf.bak
>     selinux/targeted/
>     selinux/targeted/setrans.conf.bak
>     [root@0 fxq]# ls
>     1.txt.bz2.xz  all.tar  selinux  selinux.tar.gz  selinux.zip
### (2) tar打包成.bz2文件:
    # tar -cjvf selinux.tar.bz2 selinux
    
    [root@0 fxq]# tar -cjvf selinux.tar.bz2 selinux
    selinux/
    selinux/semanage.conf.bak
    selinux/targeted/
    selinux/targeted/setrans.conf.bak
    You have new mail in /var/spool/mail/root
    [root@0 fxq]# ls
    1.txt.bz2.xz  selinux          selinux.tar.gz
    all.tar       selinux.tar.bz2  selinux.zip
### (3) tar打包成.xz文件:
    # tar -cJvf selinux.tar.xz selinux

>     [root@0 fxq]# tar -cJvf selinux.tar.xz selinux
>     selinux/
>     selinux/semanage.conf.bak
>     selinux/targeted/
>     selinux/targeted/setrans.conf.bak
>     [root@0 fxq]# ls 
>     1.txt.bz2.xz  selinux          selinux.tar.gz  selinux.zip
>     all.tar       selinux.tar.bz2  selinux.tar.xz
>     [root@0 fxq]# ll
>     total 316
>     -rw-r--r-- 1 root root 285092 Aug 23 22:01 1.txt.bz2.xz
>     -rw-r--r-- 1 root root  10240 Aug 24 22:55 all.tar
>     drwxr-xr-x 3 root root   4096 Aug 14 21:43 selinux
>     -rw-r--r-- 1 root root   1736 Aug 24 23:01 selinux.tar.bz2
>     -rw-r--r-- 1 root root   1689 Aug 24 23:00 selinux.tar.gz
>     -rw-r--r-- 1 root root   1728 Aug 24 23:01 selinux.tar.xz
>     -rw-r--r-- 1 root root    166 Aug 24 22:35 selinux.zip
### (4) tar查看压缩包内文件列表:
    # tar -tf selinux.tar.xz

>     [root@0 fxq]# tar -tf selinux.tar.xz
>     selinux/
>     selinux/semanage.conf.bak
>     selinux/targeted/
>     selinux/targeted/setrans.conf.bak
>     [root@0 fxq]# 
    
看下这个帖子： http://ask.apelearn.com/question/5435

## 测试题：
```


1. gzip, bzip2 能否直接压缩目录呢？

2. 请快速写出，使用gzip和bzip2压缩和解压一个文件的命令。

3. tar 在打包的时候，如果想排除多个文件或者目录如何操作？

4. 请实验，如果不加 "-" 是否正确， 如 tar zcvf  1.tar.gz  1.txt 2.txt ?

5. 如何使用tar打包和解包 .tar.gz, .tar.bz2 的压缩包？

6. 找一个大点的文件，使用tar 分别把这个文件打成 .tar.gz和.tar.bz2 压缩包，比较一下哪个包会更小，从而得出结论，是gzip压缩效果好还是bzip2压缩效果好？

7. 使用tar打包并压缩的时候，默认压缩级别为几？ 想一想如何能够改变压缩级别呢？（提示，tar本身没有这个功能哦，可以尝试拆分打包和压缩）

```

```
1.不能
2.gzip 1.txt.gz 1.txt 
  ungzip 1.txt.gz
  gzip -c 1.txt.gz
  bzip2 1.txt.bz2 1.txt
  bunzip2 1.txt.bz2
3.tar -czvf all.tar.gz --exclude 1.txt --exclude 2.txt ./*
4.可以不加-
5.tar -zcvf all.tar.gz all/
    tar -zxvf all.tar.gz
    tar -jcvf all.tar.bz2 all/
    tar -jxvf all.tar.bz2
6.bz2比gz压缩强:
[root@0 fxq]# ll
total 2316
-rw-r--r-- 1 root root 2362256 Aug 24 23:29 1.txt
drwxr-xr-x 2 root root    4096 Aug 14 21:43 selinux
[root@0 fxq]# tar -zcvf 1.txt.tar.gz 1.txt 
1.txt
[root@0 fxq]# tar -jcvf 1.txt.tar.bz2 1.txt
1.txt
[root@0 fxq]# ll
total 3220
-rw-r--r-- 1 root root 2362256 Aug 24 23:29 1.txt
-rw-r--r-- 1 root root  288262 Aug 24 23:30 1.txt.tar.bz2
-rw-r--r-- 1 root root  624127 Aug 24 23:29 1.txt.tar.gz
drwxr-xr-x 2 root root    4096 Aug 14 21:43 selinux
[root@0 fxq]# 

7.




习题答案：
1. gzip, bzip2 能否直接压缩目录呢？
不能直接压缩目录

2. 请快速写出，使用gzip和bzip2压缩和解压一个文件的命令。
gzip 1.txt
gzip -d 1.txt.gz
bzip2 1.txt
bzip2 -d 1.txt.bz2

3. tar 在打包的时候，如果想排除多个文件或者目录如何操作？
tar cvf 123.tar --exclude a.txt --exclude b.txt 123/

4. 请实验，如果不加 "-" 是否正确， 如 tar zcvf  1.tar.gz  1.txt 2.txt ?
不加 - 一样没有问题

5. 如何使用tar打包和解包 .tar.gz, .tar.bz2 的压缩包？
tar zcvf  1.tar.gz 1
tar zxvf 1.tar.gz
tar jcvf 1.tar.bz2 1
tar jxvf 1.tar.bz2

6. 找一个大点的文件，使用tar 分别把这个文件打成 .tar.gz和.tar.bz2 压缩包，比较一下哪个包会更小，从而得出结论，是gzip压缩效果好还是bzip2压缩效果好？
理论上.tar.bz2的压缩包小一些，但个别时候，有相反的情况。但大多时候bzip2压缩效果好。

7. 使用tar打包并压缩的时候，默认压缩级别为几？ 想一想如何能够改变压缩级别呢？（提示，tar本身没有这个功能哦，可以尝试拆分打包和压缩）
tar打包压缩时，是按照gzip和bzip2的默认压缩级别来的，gzip工具默认压缩级别为6，bzip2默认压缩级别为9.
改变默认压缩级别可以这样来做，首先tar打包，然后再使用gzip或者bzip2压缩工具来压缩，压缩的时候指定压缩级别。如：  tar cvf 1.tar 123/;  gzip -2 1.tar
```
# 知识拓展
## （1）利用tar 通过网络拷贝数据

    http://www.lishiming.net/thread-152-1-2.html
    
    tar 实现不同机器间传输文件自动打包解包。
```
[root@fxq-0 ~]# mkdir test{1..10}
[root@fxq-0 ~]# ls
anaconda-ks.cfg  test1   test2  test4  test6  test8
test0            test10  test3  test5  test7  test9
[root@fxq-0 ~]# tar -zcvf - test* |ssh 192.168.42.181 "cd /tmp/;tar -zxvf -"
test0/
test1/
test10/
test2/
test3/
test4/
test5/
test6/
test7/
test8/
test9/
test0/
test1/
test10/
test2/
test3/
test4/
test5/
test6/
test7/
test8/
test9/
[root@fxq-0 ~]# ssh 192.168.42.181
Last login: Thu Aug 17 04:20:20 2017 from 192.168.42.180
[root@fxq-1 ~]# ls /tmp
test0  test10  test3  test5  test7  test9
test1  test2   test4  test6  test8
[root@fxq-1 ~]#
```
## （2）Tar打包、压缩与解压缩到指定目录的方法
    http://www.lishiming.net/thread-96-1-3.html
    
    解压到指定的目录 :-C加目录

    [root@fxq ~]# tar -zxvf /home/images.tar.gz -C /specific dir

## （3）linux下不支持解压大于4G的zip压缩包

http://www.lishiming.net/thread-1813-1-1.html

    

    1.下载：
    wget http://dl.fedoraproject.org/pub/epel/7/x86_64/p/p7zip-16.02-2.el7.x86_64.rpm
    
    2.安装：
    rpm -ivh p7zip-16.02-2.el7.x86_64.rpm
    
    3.解压命令大于4G文件：
     # 7z  x  123.zip







  
