[toc]
## 13.1 设置更改root密码
### （1）增加mysql环境变量目录。

    echo "PATH=$PATH:/usr/local/mysql/bin/" >> /etc/profile
    
    source /etc/profile
    
### （2）刚安装完mysql,密码为空时更改root密码。

    mysqladmin -u'root' password '123456'
    
    
### （3）知道mysql密码时，更改root密码。

    mysqladmin -u'root' -p'123456'  password '12345678'
    
    或
    
    登录mysql后用sql语句更改root密码:
        mysql -uroot -p
        use mysql;
        update user set password=password('123456') where user='root';

### （4） 不知道mysql密码时，更改root密码。

    1.vim /etc/mysql.cnf 
    
        在[mysqld]下面加一行
        skip-grant
        
    2.重启mysql服务：
    
        /etc/init.d/mysqld restart
    
    3.不用密码可以进入mysql
    
        mysql -uroot -p
    
    4.用sql命令更改root密码
        use mysql;
        update user set password=password('123456') where user='root';

```
 /usr/local/mysql/bin/mysql -uroot
 更改环境变量PATH，增加mysql绝对路径
 mysqladmin -uroot password '123456'
 mysql -uroot -p123456
 密码重置
 vi /etc/my.cnf//增加skip-grant
 重启mysql服务 /etc/init.d/mysqld restart
 mysql -uroot
 use mysql;
 update user set password=password('aminglinux') where user='root';

```
## 13.2 连接mysql
### (1) 连接本机

    mysql -uroot -p123456 
    
    mysql -uroot -p123456 -S/tmp/mysql.sock(只适合在本机)
    
### (2) 连接其他服务器

    mysql -uroot -p123456 -h127.0.0.1 -p3306
### (3) 脚本中使用mysql命令查询

    mysql -uroot -p123456 -e "show databases;"

    mysql -uroot -p123456 -e "use mysql;show tables;select * from user \G;"
    
    

## 13.3 mysql常用命令

### (1)查询库 show databases;
### (2)切换库 use mysql;
### (3)查看库里的表 show tables;
### (4)查看表里的字段 desc tb_name;
### (5)查看建表语句 show create table tb_name\G;
### (6)查看当前用户 select user();
```
     [root@0 ~]# mysql -uroot -pqing123. -h119.29.237.167
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 2634
    Server version: 5.5.54-log Source distribution
    
    Copyright (c) 2000, 2016, Oracle and/or its affiliates. All rights reserved.
    
    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.
    
    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
    mysql> select user();
    +---------------------+
    | user()              |
    +---------------------+
    | root@119.29.237.167 |
    +---------------------+
    1 row in set (0.00 sec)
    
    mysql> 

``` 
### (7)查看当前使用的数据库 select databsase();
```
    mysql> select database();
    +------------+
    | database() |
    +------------+
    | mysql      |
    +------------+
    1 row in set (0.00 sec)
    
    mysql> 

```
### (8)创建库 create database db1;
``` 
 
     mysql> create database tb1;
    Query OK, 1 row affected (0.32 sec)
    
    mysql> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | mysql              |
    | performance_schema |
    | tb1                |
    +--------------------+
    4 rows in set (0.00 sec)
    
    mysql>
 
```
### (9)创建表 
``` 
 use db1; create table t1(`id` int(4), `name` char(40));
 
     use db1; create table t1(`id` int(4), `name` char(40)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
     mysql> create table t1(`id` int(4), `name` char(40)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    Query OK, 0 rows affected (0.76 sec)
    
    mysql> show tables;
    +---------------+
    | Tables_in_tb1 |
    +---------------+
    | t1            |
    +---------------+
    1 row in set (0.00 sec)
    
    mysql> desc t1;
    +-------+----------+------+-----+---------+-------+
    | Field | Type     | Null | Key | Default | Extra |
    +-------+----------+------+-----+---------+-------+
    | id    | int(4)   | YES  |     | NULL    |       |
    | name  | char(40) | YES  |     | NULL    |       |
    +-------+----------+------+-----+---------+-------+
    2 rows in set (0.03 sec)
    
    mysql> show create table t1;
    +-------+---------------------------------------------------------------------------------------------------------------------+
    | Table | Create Table                                                                                                        |
    +-------+---------------------------------------------------------------------------------------------------------------------+
    | t1    | CREATE TABLE `t1` (
      `id` int(4) DEFAULT NULL,
      `name` char(40) DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 |
    +-------+---------------------------------------------------------------------------------------------------------------------+
    1 row in set (0.00 sec)


 
``` 
### (10)查看当前数据库版本 select version();
``` 
     mysql> select version();
    +-----------+
    | version() |
    +-----------+
    | 5.6.35    |
    +-----------+
    1 row in set (0.00 sec)

 
``` 
### (11)查看数据库状态 show status;
``` 
        mysql> show status;
    +-----------------------------------------------+-------------+
    | Variable_name                                 | Value       |
    +-----------------------------------------------+-------------+
    | Aborted_clients                               | 0           |
    | Aborted_connects                              | 4           |
    | Binlog_cache_disk_use                         | 0           |
    | Binlog_cache_use                              | 0           |
    | Binlog_stmt_cache_disk_use                    | 0           |
    | Binlog_stmt_cache_use                         | 0           |
    | Bytes_received                                | 1542        |
    | Bytes_sent                                    | 23805       |


```
### (12)查看各参数 show variables; 
    模糊查询：show variables like 'max_connect%';
```
 可修改my.cnf中参数：
 
  mysql> show variables like 'slow%';
+---------------------+----------------------------+
| Variable_name       | Value                      |
+---------------------+----------------------------+
| slow_launch_time    | 2                          |
| slow_query_log      | OFF                        |
| slow_query_log_file | /data/mysql/fxq-2-slow.log |
+---------------------+----------------------------+
3 rows in set (0.00 sec)

 
 
 修改参数 set global max_connect_errors=1000;
 
 修改当时生效，重启服务丢失配置
 
     mysql>  set global max_connect_errors=1000;
    Query OK, 0 rows affected (0.00 sec)
    
    mysql> show variables like 'max_connect%';
    +--------------------+-------+
    | Variable_name      | Value |
    +--------------------+-------+
    | max_connect_errors | 1000  |
    | max_connections    | 151   |
    +--------------------+-------+
    2 rows in set (0.00 sec)


    
     
```
### (13)查看队列 show processlist; show full processlist;
```
    类似于ps top命令。
    
mysql> show full processlist;
+------+--------+----------------------+--------------------+---------+------+-------+-----------------------+
| Id   | User   | Host                 | db                 | Command | Time | State | Info                  |
+------+--------+----------------------+--------------------+---------+------+-------+-----------------------+
|    9 | zabbix | localhost            | zabbix             | Sleep   |   48 |       | NULL                  |
|   10 | zabbix | localhost            | zabbix             | Sleep   |   10 |       | NULL                  |
|   11 | zabbix | localhost            | zabbix             | Sleep   |   29 |       | NULL                  |
|   12 | zabbix | localhost            | zabbix             | Sleep   |    4 |       | NULL                  |
|   18 | zabbix | localhost            | zabbix             | Sleep   |    2 |       | NULL                  |
|   19 | zabbix | localhost            | zabbix             | Sleep   |    4 |       | NULL                  |
|   24 | zabbix | localhost            | zabbix             | Sleep   |   38 |       | NULL                  |
|   27 | zabbix | localhost            | zabbix             | Sleep   |   40 |       | NULL                  |
| 2634 | root   | 119.29.237.167:45738 | NULL               | Query   |    0 | NULL  | show full processlist |
| 2668 | zabbix | 45.124.44.134:60466  | NULL               | Sleep   |   55 |       | NULL                  |
| 2669 | zabbix | 45.124.44.134:60469  | zabbix             | Sleep   |   52 |       | NULL                  |
| 2670 | zabbix | 45.124.44.134:60472  | test               | Sleep   |   48 |       | NULL                  |
| 2672 | zabbix | 45.124.44.134:60487  | information_schema | Sleep   |    2 |       | NULL                  |
+------+--------+----------------------+--------------------+---------+------+-------+-----------------------+
13 rows in set (0.00 sec)

mysql> 

```

## 扩展 
==mysql5.7 root密码更改 http://www.apelearn.com/bbs/thread-7289-1-1.html==


```
mysql5.7root有默认密码，必须重设密码后，才能进行mysql的操作，以下是设置root密码的步骤：


一、查看默认密码

[root@localhost src]# cat /root/.mysql_secret

# The random password set for the root userat Fri Jan 10 20:00:34 2014 (local time): aJqZsA2m

这里的aJqZsA2m就是生成的root随机密码啦



二、登录mysql

[root@localhost src]# mysql -u root -p

Enter password:

输入上面的密码aJqZsA2m登录，如果你没有把mysql的路径加到path里，那就用绝对路径，mysql -u root -p还可以写成mysql -uroot -paJqZsA2m


三、更改密码

mysql> SET PASSWORD  FOR 'root'@localhost = PASSWORD（'123456'）；
Query OK, 0 rows affected (0.17 sec)
至此，就成功地修改了密码。


如果出现问题，参考一下 http://www.apelearn.com/bbs/thread-10105-1-1.html  下面有更改root密码的操作。
```


==myisam 和innodb引擎对比==





http://www.pureweber.com/article/myisam-vs-innodb/

![image](http://note.youdao.com/yws/api/personal/file/DB426DA80E1C45C9B79C0A6AEAF97975?method=download&shareKey=fd688687d35d705565b03ab7b8f95d47)


==mysql 配置详解： http://blog.linuxeye.com/379.html==



```
提供一个MySQL 5.6版本适合在1GB内存VPS上的my.cnf配置文件（点击这里下载文件）：
[client]  
port = 3306  
socket = /tmp/mysql.sock  
  
[mysqld]  
port = 3306  
socket = /tmp/mysql.sock  
  
basedir = /usr/local/mysql  
datadir = /data/mysql  
pid-file = /data/mysql/mysql.pid  
user = mysql  
bind-address = 0.0.0.0  
server-id = 1 #表示是本机的序号为1,一般来讲就是master的意思  
  
skip-name-resolve  
# 禁止MySQL对外部连接进行DNS解析，使用这一选项可以消除MySQL进行DNS解析的时间。但需要注意，如果开启该选项，  
# 则所有远程主机连接授权都要使用IP地址方式，否则MySQL将无法正常处理连接请求  
  
#skip-networking  
  
back_log = 600  
# MySQL能有的连接数量。当主要MySQL线程在一个很短时间内得到非常多的连接请求，这就起作用，  
# 然后主线程花些时间(尽管很短)检查连接并且启动一个新线程。back_log值指出在MySQL暂时停止回答新请求之前的短时间内多少个请求可以被存在堆栈中。  
# 如果期望在一个短时间内有很多连接，你需要增加它。也就是说，如果MySQL的连接数据达到max_connections时，新来的请求将会被存在堆栈中，  
# 以等待某一连接释放资源，该堆栈的数量即back_log，如果等待连接的数量超过back_log，将不被授予连接资源。  
# 另外，这值（back_log）限于您的操作系统对到来的TCP/IP连接的侦听队列的大小。  
# 你的操作系统在这个队列大小上有它自己的限制（可以检查你的OS文档找出这个变量的最大值），试图设定back_log高于你的操作系统的限制将是无效的。  
  
max_connections = 1000  
# MySQL的最大连接数，如果服务器的并发连接请求量比较大，建议调高此值，以增加并行连接数量，当然这建立在机器能支撑的情况下，因为如果连接数越多，介于MySQL会为每个连接提供连接缓冲区，就会开销越多的内存，所以要适当调整该值，不能盲目提高设值。可以过'conn%'通配符查看当前状态的连接数量，以定夺该值的大小。  
  
max_connect_errors = 6000  
# 对于同一主机，如果有超出该参数值个数的中断错误连接，则该主机将被禁止连接。如需对该主机进行解禁，执行：FLUSH HOST。  
  
open_files_limit = 65535  
# MySQL打开的文件描述符限制，默认最小1024;当open_files_limit没有被配置的时候，比较max_connections*5和ulimit -n的值，哪个大用哪个，  
# 当open_file_limit被配置的时候，比较open_files_limit和max_connections*5的值，哪个大用哪个。  
  
table_open_cache = 128  
# MySQL每打开一个表，都会读入一些数据到table_open_cache缓存中，当MySQL在这个缓存中找不到相应信息时，才会去磁盘上读取。默认值64  
# 假定系统有200个并发连接，则需将此参数设置为200*N(N为每个连接所需的文件描述符数目)；  
# 当把table_open_cache设置为很大时，如果系统处理不了那么多文件描述符，那么就会出现客户端失效，连接不上  
  
max_allowed_packet = 4M  
# 接受的数据包大小；增加该变量的值十分安全，这是因为仅当需要时才会分配额外内存。例如，仅当你发出长查询或MySQLd必须返回大的结果行时MySQLd才会分配更多内存。  
# 该变量之所以取较小默认值是一种预防措施，以捕获客户端和服务器之间的错误信息包，并确保不会因偶然使用大的信息包而导致内存溢出。  
  
binlog_cache_size = 1M  
# 一个事务，在没有提交的时候，产生的日志，记录到Cache中；等到事务提交需要提交的时候，则把日志持久化到磁盘。默认binlog_cache_size大小32K  
  
max_heap_table_size = 8M  
# 定义了用户可以创建的内存表(memory table)的大小。这个值用来计算内存表的最大行数值。这个变量支持动态改变  
  
tmp_table_size = 16M  
# MySQL的heap（堆积）表缓冲大小。所有联合在一个DML指令内完成，并且大多数联合甚至可以不用临时表即可以完成。  
# 大多数临时表是基于内存的(HEAP)表。具有大的记录长度的临时表 (所有列的长度的和)或包含BLOB列的表存储在硬盘上。  
# 如果某个内部heap（堆积）表大小超过tmp_table_size，MySQL可以根据需要自动将内存中的heap表改为基于硬盘的MyISAM表。还可以通过设置tmp_table_size选项来增加临时表的大小。也就是说，如果调高该值，MySQL同时将增加heap表的大小，可达到提高联接查询速度的效果  
  
read_buffer_size = 2M  
# MySQL读入缓冲区大小。对表进行顺序扫描的请求将分配一个读入缓冲区，MySQL会为它分配一段内存缓冲区。read_buffer_size变量控制这一缓冲区的大小。  
# 如果对表的顺序扫描请求非常频繁，并且你认为频繁扫描进行得太慢，可以通过增加该变量值以及内存缓冲区大小提高其性能  
  
read_rnd_buffer_size = 8M  
# MySQL的随机读缓冲区大小。当按任意顺序读取行时(例如，按照排序顺序)，将分配一个随机读缓存区。进行排序查询时，  
# MySQL会首先扫描一遍该缓冲，以避免磁盘搜索，提高查询速度，如果需要排序大量数据，可适当调高该值。但MySQL会为每个客户连接发放该缓冲空间，所以应尽量适当设置该值，以避免内存开销过大  
  
sort_buffer_size = 8M  
# MySQL执行排序使用的缓冲大小。如果想要增加ORDER BY的速度，首先看是否可以让MySQL使用索引而不是额外的排序阶段。  
# 如果不能，可以尝试增加sort_buffer_size变量的大小  
  
join_buffer_size = 8M  
# 联合查询操作所能使用的缓冲区大小，和sort_buffer_size一样，该参数对应的分配内存也是每连接独享  
  
thread_cache_size = 8  
# 这个值（默认8）表示可以重新利用保存在缓存中线程的数量，当断开连接时如果缓存中还有空间，那么客户端的线程将被放到缓存中，  
# 如果线程重新被请求，那么请求将从缓存中读取,如果缓存中是空的或者是新的请求，那么这个线程将被重新创建,如果有很多新的线程，  
# 增加这个值可以改善系统性能.通过比较Connections和Threads_created状态的变量，可以看到这个变量的作用。(–>表示要调整的值)  
# 根据物理内存设置规则如下：  
# 1G  —> 8  
# 2G  —> 16  
# 3G  —> 32  
# 大于3G  —> 64  
  
query_cache_size = 8M  
#MySQL的查询缓冲大小（从4.0.1开始，MySQL提供了查询缓冲机制）使用查询缓冲，MySQL将SELECT语句和查询结果存放在缓冲区中，  
# 今后对于同样的SELECT语句（区分大小写），将直接从缓冲区中读取结果。根据MySQL用户手册，使用查询缓冲最多可以达到238%的效率。  
# 通过检查状态值'Qcache_%'，可以知道query_cache_size设置是否合理：如果Qcache_lowmem_prunes的值非常大，则表明经常出现缓冲不够的情况，  
# 如果Qcache_hits的值也非常大，则表明查询缓冲使用非常频繁，此时需要增加缓冲大小；如果Qcache_hits的值不大，则表明你的查询重复率很低，  
# 这种情况下使用查询缓冲反而会影响效率，那么可以考虑不用查询缓冲。此外，在SELECT语句中加入SQL_NO_CACHE可以明确表示不使用查询缓冲  
  
query_cache_limit = 2M  
#指定单个查询能够使用的缓冲区大小，默认1M  
  
key_buffer_size = 4M  
#指定用于索引的缓冲区大小，增加它可得到更好处理的索引(对所有读和多重写)，到你能负担得起那样多。如果你使它太大，  
# 系统将开始换页并且真的变慢了。对于内存在4GB左右的服务器该参数可设置为384M或512M。通过检查状态值Key_read_requests和Key_reads，  
# 可以知道key_buffer_size设置是否合理。比例key_reads/key_read_requests应该尽可能的低，  
# 至少是1:100，1:1000更好(上述状态值可以使用SHOW STATUS LIKE 'key_read%'获得)。注意：该参数值设置的过大反而会是服务器整体效率降低  
  
ft_min_word_len = 4  
# 分词词汇最小长度，默认4  
  
transaction_isolation = REPEATABLE-READ  
# MySQL支持4种事务隔离级别，他们分别是：  
# READ-UNCOMMITTED, READ-COMMITTED, REPEATABLE-READ, SERIALIZABLE.  
# 如没有指定，MySQL默认采用的是REPEATABLE-READ，ORACLE默认的是READ-COMMITTED  
  
log_bin = mysql-bin  
binlog_format = mixed  
expire_logs_days = 30 #超过30天的binlog删除  
  
log_error = /data/mysql/mysql-error.log #错误日志路径  
slow_query_log = 1  
long_query_time = 1 #慢查询时间 超过1秒则为慢查询  
slow_query_log_file = /data/mysql/mysql-slow.log  
  
performance_schema = 0  
explicit_defaults_for_timestamp  
  
#lower_case_table_names = 1 #不区分大小写  
  
skip-external-locking #MySQL选项以避免外部锁定。该选项默认开启  
  
default-storage-engine = InnoDB #默认存储引擎  
  
innodb_file_per_table = 1  
# InnoDB为独立表空间模式，每个数据库的每个表都会生成一个数据空间  
# 独立表空间优点：  
# 1．每个表都有自已独立的表空间。  
# 2．每个表的数据和索引都会存在自已的表空间中。  
# 3．可以实现单表在不同的数据库中移动。  
# 4．空间可以回收（除drop table操作处，表空不能自已回收）  
# 缺点：  
# 单表增加过大，如超过100G  
# 结论：  
# 共享表空间在Insert操作上少有优势。其它都没独立表空间表现好。当启用独立表空间时，请合理调整：innodb_open_files  
  
innodb_open_files = 500  
# 限制Innodb能打开的表的数据，如果库里的表特别多的情况，请增加这个。这个值默认是300  
  
innodb_buffer_pool_size = 64M  
# InnoDB使用一个缓冲池来保存索引和原始数据, 不像MyISAM.  
# 这里你设置越大,你在存取表里面数据时所需要的磁盘I/O越少.  
# 在一个独立使用的数据库服务器上,你可以设置这个变量到服务器物理内存大小的80%  
# 不要设置过大,否则,由于物理内存的竞争可能导致操作系统的换页颠簸.  
# 注意在32位系统上你每个进程可能被限制在 2-3.5G 用户层面内存限制,  
# 所以不要设置的太高.  
  
innodb_write_io_threads = 4  
innodb_read_io_threads = 4  
# innodb使用后台线程处理数据页上的读写 I/O(输入输出)请求,根据你的 CPU 核数来更改,默认是4  
# 注:这两个参数不支持动态改变,需要把该参数加入到my.cnf里，修改完后重启MySQL服务,允许值的范围从 1-64  
  
innodb_thread_concurrency = 0  
# 默认设置为 0,表示不限制并发数，这里推荐设置为0，更好去发挥CPU多核处理能力，提高并发量  
  
innodb_purge_threads = 1  
# InnoDB中的清除操作是一类定期回收无用数据的操作。在之前的几个版本中，清除操作是主线程的一部分，这意味着运行时它可能会堵塞其它的数据库操作。  
# 从MySQL5.5.X版本开始，该操作运行于独立的线程中,并支持更多的并发数。用户可通过设置innodb_purge_threads配置参数来选择清除操作是否使用单  
# 独线程,默认情况下参数设置为0(不使用单独线程),设置为 1 时表示使用单独的清除线程。建议为1  
  
innodb_flush_log_at_trx_commit = 2  
# 0：如果innodb_flush_log_at_trx_commit的值为0,log buffer每秒就会被刷写日志文件到磁盘，提交事务的时候不做任何操作（执行是由mysql的master thread线程来执行的。  
# 主线程中每秒会将重做日志缓冲写入磁盘的重做日志文件(REDO LOG)中。不论事务是否已经提交）默认的日志文件是ib_logfile0,ib_logfile1  
# 1：当设为默认值1的时候，每次提交事务的时候，都会将log buffer刷写到日志。  
# 2：如果设为2,每次提交事务都会写日志，但并不会执行刷的操作。每秒定时会刷到日志文件。要注意的是，并不能保证100%每秒一定都会刷到磁盘，这要取决于进程的调度。  
# 每次事务提交的时候将数据写入事务日志，而这里的写入仅是调用了文件系统的写入操作，而文件系统是有 缓存的，所以这个写入并不能保证数据已经写入到物理磁盘  
# 默认值1是为了保证完整的ACID。当然，你可以将这个配置项设为1以外的值来换取更高的性能，但是在系统崩溃的时候，你将会丢失1秒的数据。  
# 设为0的话，mysqld进程崩溃的时候，就会丢失最后1秒的事务。设为2,只有在操作系统崩溃或者断电的时候才会丢失最后1秒的数据。InnoDB在做恢复的时候会忽略这个值。  
# 总结  
# 设为1当然是最安全的，但性能页是最差的（相对其他两个参数而言，但不是不能接受）。如果对数据一致性和完整性要求不高，完全可以设为2，如果只最求性能，例如高并发写的日志服务器，设为0来获得更高性能  
  
innodb_log_buffer_size = 2M  
# 此参数确定些日志文件所用的内存大小，以M为单位。缓冲区更大能提高性能，但意外的故障将会丢失数据。MySQL开发人员建议设置为1－8M之间  
  
innodb_log_file_size = 32M  
# 此参数确定数据日志文件的大小，更大的设置可以提高性能，但也会增加恢复故障数据库所需的时间  
  
innodb_log_files_in_group = 3  
# 为提高性能，MySQL可以以循环方式将日志文件写到多个文件。推荐设置为3  
  
innodb_max_dirty_pages_pct = 90  
# innodb主线程刷新缓存池中的数据，使脏数据比例小于90%  
  
innodb_lock_wait_timeout = 120   
# InnoDB事务在被回滚之前可以等待一个锁定的超时秒数。InnoDB在它自己的锁定表中自动检测事务死锁并且回滚事务。InnoDB用LOCK TABLES语句注意到锁定设置。默认值是50秒  
  
bulk_insert_buffer_size = 8M  
# 批量插入缓存大小， 这个参数是针对MyISAM存储引擎来说的。适用于在一次性插入100-1000+条记录时， 提高效率。默认值是8M。可以针对数据量的大小，翻倍增加。  
  
myisam_sort_buffer_size = 8M  
# MyISAM设置恢复表之时使用的缓冲区的尺寸，当在REPAIR TABLE或用CREATE INDEX创建索引或ALTER TABLE过程中排序 MyISAM索引分配的缓冲区  
  
myisam_max_sort_file_size = 10G  
# 如果临时文件会变得超过索引，不要使用快速排序索引方法来创建一个索引。注释：这个参数以字节的形式给出  
  
myisam_repair_threads = 1  
# 如果该值大于1，在Repair by sorting过程中并行创建MyISAM表索引(每个索引在自己的线程内)    
  
interactive_timeout = 28800  
# 服务器关闭交互式连接前等待活动的秒数。交互式客户端定义为在mysql_real_connect()中使用CLIENT_INTERACTIVE选项的客户端。默认值：28800秒（8小时）  
  
wait_timeout = 28800  
# 服务器关闭非交互连接之前等待活动的秒数。在线程启动时，根据全局wait_timeout值或全局interactive_timeout值初始化会话wait_timeout值，  
# 取决于客户端类型(由mysql_real_connect()的连接选项CLIENT_INTERACTIVE定义)。参数默认值：28800秒（8小时）  
# MySQL服务器所支持的最大连接数是有上限的，因为每个连接的建立都会消耗内存，因此我们希望客户端在连接到MySQL Server处理完相应的操作后，  
# 应该断开连接并释放占用的内存。如果你的MySQL Server有大量的闲置连接，他们不仅会白白消耗内存，而且如果连接一直在累加而不断开，  
# 最终肯定会达到MySQL Server的连接上限数，这会报'too many connections'的错误。对于wait_timeout的值设定，应该根据系统的运行情况来判断。  
# 在系统运行一段时间后，可以通过show processlist命令查看当前系统的连接状态，如果发现有大量的sleep状态的连接进程，则说明该参数设置的过大，  
# 可以进行适当的调整小些。要同时设置interactive_timeout和wait_timeout才会生效。  
  
[mysqldump]  
quick  
max_allowed_packet = 16M #服务器发送和接受的最大包长度  
  
[myisamchk]  
key_buffer_size = 8M  
sort_buffer_size = 8M  
read_buffer = 4M  
write_buffer = 4M  
```

==mysql调优： http://www.aminglinux.com/bbs/thread-5758-1-1.html==

```
MySQL调优可以从几个方面来做：
1. 架构层：
做从库，实现读写分离；

2.系统层次：
增加内存；
给磁盘做raid0或者raid5以增加磁盘的读写速度；
可以重新挂载磁盘，并加上noatime参数，这样可以减少磁盘的i/o;

3. MySQL本身调优：
(1) 如果未配置主从同步，可以把bin-log功能关闭，减少磁盘i/o
(2) 在my.cnf中加上skip-name-resolve,这样可以避免由于解析主机名延迟造成mysql执行慢
(3) 调整几个关键的buffer和cache。调整的依据，主要根据数据库的状态来调试。如何调优可以参考5.

4. 应用层次：
查看慢查询日志，根据慢查询日志优化程序中的SQL语句，比如增加索引

5. 调整几个关键的buffer和cache

1) key_buffer_size  首先可以根据系统的内存大小设定它，大概的一个参考值：1G以下内存设定128M；2G/256M; 4G/384M;8G/1024M；16G/2048M.这个值可以通过检查状态值Key_read_requests和 Key_reads,可以知道key_buffer_size设置是否合理。比例key_reads / key_read_requests应该尽可能的低，至少是1:100，1:1000更好(上述状态值可以使用SHOW STATUS LIKE ‘key_read%’获得)。注意：该参数值设置的过大反而会是服务器整体效率降低!


2) table_open_cache 打开一个表的时候，会临时把表里面的数据放到这部分内存中，一般设置成1024就够了，它的大小我们可以通过这样的方法来衡量： 如果你发现 open_tables等于table_cache，并且opened_tables在不断增长，那么你就需要增加table_cache的值了(上述状态值可以使用SHOW STATUS LIKE ‘Open%tables’获得)。注意，不能盲目地把table_cache设置成很大的值。如果设置得太高，可能会造成文件描述符不足，从而造成性能不稳定或者连接失败。


3) sort_buffer_size 查询排序时所能使用的缓冲区大小,该参数对应的分配内存是每连接独占!如果有100个连接，那么实际分配的总共排序缓冲区大小为100 × 4 = 400MB。所以，对于内存在4GB左右的服务器推荐设置为4-8M。


4) read_buffer_size 读查询操作所能使用的缓冲区大小。和sort_buffer_size一样，该参数对应的分配内存也是每连接独享!


5) join_buffer_size 联合查询操作所能使用的缓冲区大小，和sort_buffer_size一样，该参数对应的分配内存也是每连接独享!


6) myisam_sort_buffer_size 这个缓冲区主要用于修复表过程中排序索引使用的内存或者是建立索引时排序索引用到的内存大小，一般4G内存给64M即可。


7) query_cache_size MySQL查询操作缓冲区的大小，通过以下做法调整：SHOW STATUS LIKE ‘Qcache%’; 如果Qcache_lowmem_prunes该参数记录有多少条查询因为内存不足而被移除出查询缓存。通过这个值，用户可以适当的调整缓存大小。如果该值非常大，则表明经常出现缓冲不够的情况，需要增加缓存大小;Qcache_free_memory:查询缓存的内存大小，通过这个参数可以很清晰的知道当前系统的查询内存是否够用，是多了，还是不够用，我们可以根据实际情况做出调整。一般情况下4G内存设置64M足够了。


8) thread_cache_size 表示可以重新利用保存在缓存中线程的数，参考如下值：1G  —> 8 2G  —> 16 3G  —> 32  >3G  —> 64
除此之外，还有几个比较关键的参数：


9) thread_concurrency 这个值设置为cpu核数的2倍即可


10) wait_timeout 表示空闲的连接超时时间，默认是28800s，这个参数是和interactive_timeout一起使用的，也就是说要想让wait_timeout 生效，必须同时设置interactive_timeout，建议他们两个都设置为10


11) max_connect_errors 是一个MySQL中与安全有关的计数器值，它负责阻止过多尝试失败的客户端以防止暴力破解密码的情况。与性能并无太大关系。为了避免一些错误我们一般都设置比较大，比如说10000 


12) max_connections 最大的连接数，根据业务请求量适当调整，设置500足够


13) max_user_connections 是指同一个账号能够同时连接到mysql服务的最大连接数。设置为0表示不限制。通常我们设置为100足够 
```



==同学分享的亲身mysql调优经历： http://www.apelearn.com/bbs/thread-11281-1-1.html==

```
先介绍下服务器架构及配置8核8G，10M带宽Centos6.5 64

Nginx   1.8.1PHP      5.3.29Mysql    5.5.42


一电商网站后台查询订单时 经常php超时，导致php报错以下是排查过程

1、php执行超时，首先我们想到的就是php.ini文件中max_execution_time =  #把默认的值调整了下

2、然后在后台执行订单查询php不报错了，但是查询耗时较长，用时65s.  而且一些表成锁死状态碎片比较多，本人对mysql数据库优化不是很了解，于是请教了铭哥下，铭哥给出的答复是：一般mysql调优主要是根据慢查询日志去优化sql语句，my.cnf里面没啥可调的。 下面就是分析mysql慢日志,调整参数3、mysql参数优化，主要调整的参数如下。根据机器性能来调整，如果你对参数不是很了解，建议不要盲目的调

mysql优化帖子  http://ask.apelearn.com/question/5758

把一些配置文件修改好后重启相关服务，由原来的65s变成了十几秒。效果还是不是很理想，查看了下mysql默认引擎为MyISAM，决定把引擎改为Innodb

1、导出shop数据库的表结构mysqldump -d -uxxx -p shop > shop_table.sql其中-d参数表示不导出数据，只导出表结构

2、替换shop_table.sql里的MyISAM为INNODBsed -i 's/MyISAM/INNODB/g' shop_table.sql3、新建数据库shop_new,并导入表结构mysql > create database shop_new;mysql -uroot -p shop_new < shop_table.sql可以通过show table status来检查表引擎是否为INNODB。

4、导出shop的数据mysqldump -t -uroot -p shop > shop_data.sql其中-t参数表示只导数据，不导表结构

5、导入数据到shop_newmysql -uroot -p shop_new < shop_data.sql

6、 首先开启慢日志，修改/etc/my.cnf  增加以下两段配置，保存重启mysql

vim /etc/my.cnf
long_query_time = 2
log_slow_queries = /data/mysql/slow.log
service mysqld restart   ##重启mysql服务
7、查看慢日志来定位mysql哪条语句执行慢，然后建立索引，优化sql执行语句。

<font color="Red">tail -n20 /data/mysql/slow.log   #查看20行</font>
# Time: 160303 12:12:38
# User@Host: root[root] @  [10.165.34.182]
# Query_time: 10.145685  Lock_time: 0.000395 Rows_sent: 1  Rows_examined: 24306970
use shop;
SET timestamp=1456978358;
SELECT COUNT(*) FROM `shop`.`ecs_order_info` o LEFT JOIN`shop`.`ecs_users` u ON o.user_id = u.user_id LEFT JOIN `shop`.`ecs_affiliate_log` a ON o.order_id = a.order_id WHERE o.user_id > 0 AND (u.parent_id > 0 AND o.is_separate = 0 OR o.is_separate > 0);
# Time: 160303 12:12:44
# User@Host: root[root] @  [10.165.34.182]
# Query_time: 6.073441  Lock_time: 0.000152 Rows_sent: 15  Rows_examined: 24314767
SET timestamp=1456978364;
SELECT o.*, a.log_id, a.user_id as suid,  a.user_name as auser, a.money, a.point, a.separate_type,u.parent_id as up FROM `shop`.`ecs_order_info` o LEFT JOIN`shop`.`ecs_users` u ON o.user_id = u.user_id LEFT JOIN `shop`.`ecs_affiliate_log` a ON o.order_id = a.order_id WHERE o.user_id > 0 AND (u.parent_id > 0 AND o.is_separate = 0 OR o.is_separate > 0)  ORDER BY order_id DESC LIMIT 0,15;
通过慢日志发现其中有几个表查询耗时较长，下面就是把这个查询慢的表建立索引
用到的软件 NAvicat，对查询慢的表进行设计，增加索引

0160304191200.png

根据 explain  的解释，查看下  索引是否建立，一般都是 这样调整 就行。

QQ图片20160304202041.png
修改完后重启mysql 服务，查询时间从65s,缩短到 0.017407 秒

QQ截图20160304202346.png
参考了大量的网络资料，头一次搞优化。优化完后很有成就感，算是一次新的挑战

QQ图片20160304181535.png 0160304191200.png QQ图片20160304202041.png QQ截图20160304202346.png
```