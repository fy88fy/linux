[toc]
## 11.28 限定某个目录禁止解析php

### (1)修改配置文件
```
 核心配置文件内容
    <Directory /data/wwwroot/www.123.com/upload>
        php_admin_flag engine off
        <FilesMatch (.*)\.php(.*)>
        Order allow,deny
        Deny from all
        </FilesMatch>
    </Directory>
``` 


### (2)重新加载配置文件：
```
/usr/local/apache2.4/bin/apachectl -t
/usr/local/apache2.4/bin/apachectl graceful
```
curl测试时直接返回了php源代码，并未解析
```
```
## 11.29 限制user_agent
### (1)修改配置文件
```

 user_agent可以理解为浏览器标识
 核心配置文件内容
   <IfModule mod_rewrite.c>
        RewriteEngine on
        RewriteCond %{HTTP_USER_AGENT}  .*curl.* [NC,OR]
        RewriteCond %{HTTP_USER_AGENT}  .*baidu.com.* [NC]
        RewriteRule  .*  -  [F]
    </IfModule>
```    
### (2)重新加载配置文件：
```
/usr/local/apache2.4/bin/apachectl -t
/usr/local/apache2.4/bin/apachectl graceful
```
### (3)测试
 curl -A "123123" 指定user_agent
 查看日志

```
```
## 11.30/11.31 php相关配置
 查看网站所在路径的PHPinfo()是找到php.ini最准。
eval :一句话木马用的函数
```
 查看php配置文件位置
 /usr/local/php/bin/php -i|grep -i "loaded configuration file" 
 date.timezone = Asia/Chongqing 
 
 disable_functions=
eval,assert,popen,passthru,escapeshellarg,escapeshellcmd,passthru,exec,system,chroot,scandir,chgrp,chown,escapeshellcmd,escapeshellarg,shell_exec,proc_get_status,ini_alter,ini_restore,dl,pfsockopen,openlog,syslog,readlink,symlink,leak,popepassthru,stream_socket_server,popen,proc_open,proc_close,phpinfo 

 error_log = /tmp/php_errors.log, 
 log_errors = On,
 display_errors = Off,
 error_reporting = E_ALL & ~E_NOTICE
 
 open_basedir = //php.ini针对所有站点
 
 编辑虚拟主机配置文件：
 vim httpd-vhost.conf
 php_admin_value open_basedir = "/data/wwwroot/111.com:/tmp/"

```

## 扩展
apache开启压缩 http://ask.apelearn.com/question/5528
```
这里的压缩并不是对网站的图片压缩，而是对普通的静态文件，诸如html, js, css 等元素压缩。不要小看这个压缩功能，如果一个网站的请求量很大的话，这样可以节省海量带宽，在我国带宽资源非常昂贵，所以小小的一个压缩功能可以为企业节省不少的成本呢！下面就来看看如何配置它？

首先，需要看一下我们的apache是否支持压缩功能。 
/usr/local/apache2/bin/apachectl -l
看看是否有mod_deflate
如果这里没有，那继续看一下 
ls /usr/local/apache2/modules/
下面有没有 mod_deflate.so 这个文件

如果这里也没有，那说明你的apache不支持压缩，需要重编译一下，或者扩展形式安装，或者重新编译apache, 需要在编译的时候，加上  --enable-deflate=shared  

好，如果你的apache有了deflate这个模块支持，也就支持了压缩功能。

下面该配置httpd.conf 了。
在httpd.conf 中增加 ：

LoadModule deflate_module modules/mod_deflate.so

然后再增加如下配置：


DeflateCompressionLevel 5
AddOutputFilterByType DEFLATE text/html text/plain text/xml 
AddOutputFilter DEFLATE js css


其中DeflateCompressionLevel  是指压缩程度的等级，从1到9，9是最高等级。
```


apache2.2到2.4配置文件变更 http://ask.apelearn.com/question/7292


```
参考： http://www.dotblogs.com.tw/maple ... e24_httpd_conf.aspx

1.  访问控制
2.2 的时候
Order deny,allow
Deny from all
在 2.4 需要改成
Require all denied

常用的配置有：
Require all denied   
Require all granted   
Require host xxx.com   
Require ip 192.168.1 192.168.2   
Require local

2. RewriteLogLevel  变为：logLevel
如，LogLevel warn rewrite: warn

3. Namevirtualhost 被移除

4. 网站压缩，除了使用mod_deflate，还要mod_filter
使用ssl，除了使用mod_ssl，还需要mod_socache_shmcb

```


apache options参数 http://ask.apelearn.com/question/1051
```
指令控制了在特定目录中将使用哪些服务器特性。Options属性有一个非常特别的功能： 如果你没有用“+”或者“-”来增加或者减少一个功能的时候，每个之前定义的Options的所有功能都会被取消， 直到你又为它指定一些功能。所以options属性在整体设置和虚拟主机设置的是不相关的， 互相不起作用，因为他们在特定的范围内被重载了。  如果要在虚拟主机里面使用在整体设置中的Options的设置， 那么就不要在虚拟主机设置中指定Options属性。如果要增加或者减少功能， 那么用“+”或者“-”符号来实  Options  指令控制了在特定目录中将使用哪些服务器特性。  可选项能设置为  None  ，在这种情况下，将不启用任何额外特性。或设置为以下选项中的一个或多个：  
All 除MultiViews之外的所有特性。这是默认设置。 
ExecCGI 允许执行CGI脚本. 
FollowSymLinks 服务器会在此目录中使用符号连接。  注意：即便服务器会使用符号连接，但它不会改变用于匹配配置段的路径名。 如果此配置位于配置段中，则此设置会被忽略。
Includes 允许服务器端包含。
IncludesNOEXEC 允许服务器端包含，但禁用#exec命令和#exec CGI。但仍可以从ScriptAliase目录使用#include 虚拟CGI脚本。 
Indexes 如果一个映射到目录的URL被请求，而此目录中又没有DirectoryIndex（例如：index.html），那么服务器会返回一个格式化后的目录 列表。 
MultiViews 允许内容协商的多重视图。 
SymLinksIfOwnerMatch 服务器仅在符号连接与其目的目录或文件拥有者具有同样的用户id时才使用它。  注意：如果此配置出现在配置段中，此选项将被忽略。  一般来说，如果一个目录被多次设置了  Options  ，则最特殊的一个会被完全接受，而各个可选项的设定彼此并不融合。然而，如果所有施用于  Options  指令的可选项前都加有+或-符号，此可选项将被合并。所有前面加有+号的可选项将强制覆盖当前可选项设置，而所有前面有-号的可选项将强制从当前可选项设置中去除。  
比如说，没有任何+和-符号：
  Options Indexes FollowSymLinks
  
  Options Includes  
  
则只有  Includes  设置到/web/docs/spec目录上。
然而如果第二个  Options  指令使用了+和-符号：
  Options Indexes FollowSymLinks
  
  Options +Includes -Indexes
  
那么就会有  FollowSymLinks  和  Includes  设置到/web/docs/spec目录上。
```


apache禁止trace或track防止xss http://ask.apelearn.com/question/1045

```
TRACE和TRACK是用来调试web服务器连接的HTTP方式。
支持该方式的服务器存在跨站脚本漏洞，通常在描述各种浏览器缺陷的时候，把"Cross-Site-Tracing"简称为XST。
攻击者可以利用此漏洞欺骗合法用户并得到他们的私人信息。

禁用trace可以使用rewrite功能来实现
RewriteEngine On
RewriteCondi %{REQUEST_METHOD} ^TRACE
RewriteRule .* - [F]

或者还可以直接在apache的配置文件中配置相应参数
TraceEnable off
```

apache 配置https 支持ssl http://ask.apelearn.com/question/1029

```
1. 安装openssl 
apache2.0 建议安装0.9版本，我曾经试过2.0.59 对openssl-1.0编译不过去
下载Openssl：http://www.openssl.org/source/
       tar -zxf openssl-0.9.8k.tar.gz    //解压安装包   
       cd openssl-0.9.8k                 //进入已经解压的安装包   
       ./config                          //配置安装。推荐使用默认配置   
       make && make install              //编译及安装   
openssl默认将被安装到/usr/local/ssl 


2. 让apache支持ssl，编译的时候，要指定ssl支持。
静态或者动态
静态方法即  --enable-ssl=static --with-ssl=/usr/local/ssl
动态方法  --enable-ssl=shared --with-ssl=/usr/local/ssl
其中第二种方法会在module/ 目录下生成 mod_ssl.so 模块，而静态不会有，当然第二种方法也需要在httpd.conf 中加入
LoadModule ssl_module modules/mod_ssl.so   

3. 1    创建私钥  
在创建证书请求之前，您需要首先生成服务器证书私钥文件。  
cd /usr/local/ssl/bin                    //进入openssl安装目录  
openssl genrsa -out server.key 2048      //运行openssl命令，生成2048位长的私钥server.key文件。如果您需要对 server.key 添加保护密码，请使用 -des3 扩展命令。Windows环境下不支持加密格式私钥，Linux环境下使用加密格式私钥时，每次重启Apache都需要您输入该私钥密码（例：openssl genrsa -des3 -out server.key 2048）。 
cp server.key   /usr/local/apache/conf/ssl.key/
  
3.2    生成证书请求（CSR）文件   
openssl req -new -key server.key -out certreq.csr   
Country Name：                           //您所在国家的ISO标准代号，中国为CN   
State or Province Name：                 //您单位所在地省/自治区/直辖市   
Locality Name：                          //您单位所在地的市/县/区   
Organization Name：                      //您单位/机构/企业合法的名称   
Organizational Unit Name：               //部门名称   
Common Name：                            //通用名，例如：www.itrus.com.cn。此项必须与您访问提供SSL服务的服务器时所应用的域名完全匹配。   
Email Address：                          //您的邮件地址，不必输入，直接回车跳过   
"extra"attributes                        //以下信息不必输入，回车跳过直到命令执行完毕。 
   
3.3    备份私钥并提交证书请求   
请将证书请求文件certreq.csr提交给天威诚信，并备份保存证书私钥文件server.key，等待证书的签发。服务器证书密钥对必须配对使用，私钥文件丢失将导致证书不可用。 

4.安装证书
4.1 获取服务器证书中级CA证书   
为保障服务器证书在客户端的兼容性，服务器证书需要安装两张中级CA证书(不同品牌证书，可能只有一张中级证书)。   
从邮件中获取中级CA证书：   
将证书签发邮件中的从BEGIN到 END结束的两张中级CA证书内容（包括“-----BEGIN CERTIFICATE-----”和“-----END CERTIFICATE-----”）粘贴到同一个记事本等文本编辑器中，中间用回车换行分隔。修改文件扩展名，保存为conf/ssl.crt/intermediatebundle.crt文件(如果只有一张中级证书，则只需要保存并安装一张中级证书)。   
4.2 获取EV服务器证书   
将证书签发邮件中的从BEGIN到 END结束的服务器证书内容（包括“-----BEGIN CERTIFICATE-----”和“-----END CERTIFICATE-----”） 粘贴到记事本等文本编辑器中，保存为ssl.crt/server.crt文件 
   
4.3 apache的配置 2.0的配置
httpd.conf 中增加
Listen  443
NameVirtualHost *:443

    DocumentRoot "/data/web/www"
    ServerName aaa.com:443
    ErrorLog "logs/error.log"
    CustomLog "logs/access.log" combined
     
        SSLEngine on
        SSLCertificateFile /usr/local/apache/conf/ssl.crt/server.crt
        SSLCertificateKeyFile /usr/local/apache/conf/ssl.key/server.key
        SSLCertificateChainFile /usr/local/apache/conf/ssl.crt/intermediatebundle.crt
```
