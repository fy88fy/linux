#!/bin/bash
#date 2017-07-05
#Authors is Fengxiaoqing
# The shell for close the iptables 

OS=`cat /etc/redhat-release |awk -F. '{print $1}' |awk '{print $NF}'`

##centos 6
centos6_iptables_close (){
if  (( $OS == 6  )) ; then
		##close selinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
selinux_s=`getenforce`
if [ $selinux_s == "enforcing" ]
then
 	  	 setenforce 0
fi

##close iptables
iptables-save > /etc/sysconfig/iptables_`date +%s`
iptables -F
service iptables save

fi
}

##centOS 7 
centos7_iptables_close(){
if (( $OS == 7  )) ; then
	##close selinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
selinux_s=`getenforce`
if [ $selinux_s == "enforcing" ]
then
 	  	 setenforce 0
fi

iptables-save > /etc/sysconfig/iptables_`date +%s`
iptables  -F
systemctl stop  firewalld.service
fi
} 

centos6_iptables_close
centos7_iptables_close
