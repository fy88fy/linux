[toc]
## 10.15 iptables filter表案例
![image](http://note.youdao.com/yws/api/personal/file/D02B01DB9813435CB52462AFD1401ABB?method=download&shareKey=f6f3de9135b27ec24519b832c746890e)

### (1)小案例

客户端只允许访问80 21端口,192.168.42网段允许22端口登录服务器.

vim /usr/local/sbin/iptables.sh

```
#!/bin/bash
ipt="/usr/sbin/iptables"
$ipt -F
$ipt -P INPUT DROP
$ipt -P OUTPUT ACCEPT
$ipt -P FORWARD ACCEPT
$ipt -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
$ipt -A INPUT -s 192.168.42.0/24 -p tcp --dport 22 -j ACCEPT
$ipt -A INPUT -p tcp --dport 80 -j ACCEPT
$ipt -A INPUT -p tcp --dport 21 -j ACCEPT

```

### (2)服务器禁ping

```
iptables -A INPUT -p icmp --icmp-type 8 -j DROP
```


## 10.16/10.17/10.18 iptables nat表应用
### nat 表应用
需求：两台linux服务器,fxq-0可以上网，fxq-1不能上网，通fxq-0的设置，实现为fxq-1提上网功能，并做端口映射，把fxq-1的22端口，映射到fxq-0的外网区域，实现外部能够通过1122端口 ssh连接到fxq-1服务器。

#### 配置好基础环境
```
fxq-0(两块网卡):  外网IP:192.168.42.180/24 (正常上网)
                  内网IP:192.168.100.1/24(不设置网关)
        
fxq-1(一块网卡):内网IP:192.168.100.100/24
                网关：192.168.100.1
                DNS1:8.8.8.8
```

    用命令行临时配置ip: ifconfig ens37 192.168.100.1/24
    
    实现两台服务器能够互相ping通.192.168.100.1<--->192.168.100.100   

#### 配置路由转发功能
实现:fxq-1通过fxq-0上网

fxq-0机器操作如下:

    echo "1" > /proc/sys/net/ipv4/ip_forward
    iptables -t nat -A POSTROUTING -s 192.168.100.0/24 -o ens33 -j MASQUERADE
    
fxq-0机器操作如下：

    配置网关为192.168.100.1 DNS:8.8.8.8
    route add default gw 192.168.100.1
    
测试：fxq-0机器是否能上网：
    
    ping 192.168.42.180 
    ping www.qq.com 
    
#### 配置端口映射
    
fxq-0机器操作如下：

    echo "1" /proc/sys/net/ipv4/ip_forward 打开路由转发功能
    iptables -t nat -A PREROUTING --d 192.168.133.130 -P TCP --dport 1122 -j DNAT --to 192.168.100.100:22
    iptables -t nas _A PORSTROUTING -s 192.168.100.100. -j SNAT --to 192.168.42.180:22

fxq-1机器操作如下：：

    命令行加网关：

    route add default gw 192.168.100.1
测试：fxq机器 ssh 192.168.42.180:1122

## 扩展
1. iptables应用在一个网段 http://www.aminglinux.com/bbs/thread-177-1-1.html
```    
iptables -I INPUT -m iprange --src-range 61.4.176.0-61.4.191.255 -j DROP
```
2. sant,dnat,masquerade http://www.aminglinux.com/bbs/thread-7255-1-1.html
```
DNAT（Destination Network Address Translation,目的地址转换) 通常被叫做目的映谢。而SNAT（Source Network Address Translation，源地址转换）通常被叫做源映谢。

这是我们在设置Linux网关或者防火墙时经常要用来的两种方式。以前对这两个都解释得不太清楚，现在我在这里解释一下。首先，我们要了解一下IP包的结构，如下图所
```
![image](http://note.youdao.com/yws/api/personal/file/DF6A3B5A68554B099200A5F8E659799D?method=download&shareKey=2593ae5a80a668ec261afacd8b782758)
```
在任何一个IP数据包中，都会有Source IP Address与Destination IP Address这两个字段，数据包所经过的路由器也是根据这两个字段是判定数据包是由什么地方发过来的，它要将数据包发到什么地方去。而iptables的DNAT与SNAT就是根据这个原理，对Source IP Address与Destination IP Address进行修改。然后，我们再看看数据包在iptables中要经过的链（chain）：  
```
![image](http://note.youdao.com/yws/api/personal/file/2681B9D06F0F4587BFDE78B789EF0215?method=download&shareKey=6e9428e6e8e47cfaee87313a9a205b39)
```
图中正菱形的区域是对数据包进行判定转发的地方。在这里，系统会根据IP数据包中的destination ip address中的IP地址对数据包进行分发。如果destination ip adress是本机地址，数据将会被转交给INPUT链。如果不是本机地址，则交给FORWARD链检测。

这也就是说，我们要做的DNAT要在进入这个菱形转发区域之前，也就是在PREROUTING链中做，比如我们要把访问202.103.96.112的访问转发到192.168.0.112上：

iptables -t nat -A PREROUTING -d 202.103.96.112 -j DNAT --to-destination 192.168.0.112

这个转换过程当中，其实就是将已经达到这台Linux网关（防火墙）上的数据包上的destination ip address从202.103.96.112修改为192.168.0.112然后交给系统路由进行转发。

而SNAT自然是要在数据包流出这台机器之前的最后一个链也就是POSTROUTING链来进行操作

iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -j SNAT --to-source 58.20.51.66

这个语句就是告诉系统把即将要流出本机的数据的source ip address修改成为58.20.51.66。这样，数据包在达到目的机器以后，目的机器会将包返回到58.20.51.66也就是本机。如果不做这个操作，那么你的数据包在传递的过程中，reply的包肯定会丢失。

假如当前系统用的是ADSL/3G/4G动态拨号方式，那么每次拨号，出口IP都会改变，SNAT就会有局限性。

iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -o eth0 -j MASQUERADE

重点在那个『 MASQUERADE 』！这个设定值就是『IP伪装成为封包出去(-o)的那块装置上的IP』！不管现在eth0的出口获得了怎样的动态ip，MASQUERADE会自动读取eth0现在的ip地址然后做SNAT出去，这样就实现了很好的动态SNAT地址转换。
```


3. iptables限制syn速率 http://www.aminglinux.com/bbs/thread-985-1-1.html

```
原理，每5s内tcp三次握手大于20次的属于不正常访问。
iptables -A INPUT -s ! 192.168.0.0/255.255.255.0 -d 192.168.0.101 -p tcp -m tcp --dport 80 -m state --state NEW -m recent --set --name httpuser --rsource
iptables -A INPUT -m recent --update --seconds 5 --hitcount 20 --name httpuser --rsource -j DROP

其中192.168.0.0/255.255.255.0 为不受限制的网段， 192.168.0.101  为本机IP。
该iptables策略，可有效预防syn攻击，也可以有效防止机器人发垃圾帖。
```
