[toc]
## 3.4 usermod命令
### 1. usermod更改用户的属性
> 
> usermod -u 111 user1 更改用户user1的UID为111
> 
> usermod -g grp2 user2  更改user2的组为user2
> 
> usermod -d /home/test user3 更改user3的家目录为/home/test/
> 
> usermod -s /sbin/nologin user2 更改user2登录的shell 为不能登录

### 2. id user1

查看用户UID,GID,扩展组等信息

一个用户可以属于多个组，用扩展组：
> 
> usermod -G grp2 user1  增加user1的扩展组为grp2
> 

```
[root@VM_46_188_centos ~]# usermod -G grp1,fxq user2
[root@VM_46_188_centos ~]# id user2
uid=1015(user2) gid=1017(user2) groups=1017(user2),1000(fxq),1016(grp
1)[root@VM_46_188_centos ~]# 
```


## 3.5 用户密码管理
### 1. 更改用户密码：

passwd root 更改root密码

passwd fxq  更改fxq用户密码

查看密码文件真实存储文件/etc/shadow
```
[root@VM_46_188_centos ~]# cat /etc/shadow
root:$1$QoTjnbBe$o3Xvx5L0KNpvmXMivsFnf.::0:99999:7:::
bin:*:16659:0:99999:7:::
daemon:*:16659:0:99999:7:::
adm:*:16659:0:99999:7:::
lp:*:16659:0:99999:7:::
sync:*:16659:0:99999:7:::
shutdown:*:16659:0:99999:7:::
halt:*:16659:0:99999:7:::
mail:*:16659:0:99999:7:::
operator:*:16659:0:99999:7:::
games:*:16659:0:99999:7:::
ftp:*:16659:0:99999:7:::
nobody:*:16659:0:99999:7:::
avahi-autoipd:!!:16912::::::
systemd-bus-proxy:!!:16912::::::
systemd-network:!!:16912::::::
dbus:!!:16912::::::
polkitd:!!:16912::::::
abrt:!!:16912::::::
libstoragemgmt:!!:16912::::::
tss:!!:16912::::::
ntp:!!:16912::::::
postfix:!!:16912::::::
chrony:!!:16912::::::
sshd:!!:16912::::::
tcpdump:!!:16912::::::
apache:!!:17296::::::
mysql:!!:17319::::::
fxq:$1$yMvFHPLx$IXmVlg4chjsxR6FFXtUGS1:17322:0:99999:7:::
nginx:!!:17323::::::
saslauth:!!:17344::::::
mailnull:!!:17344::::::
smmsp:!!:17344::::::
test:$1$Lv1G6wIl$gprgn2dB1dFtliHgC9fbW.:17365:0:99999:7:::
zabbix:!!:17373:0:99999:7:::
fxq1:$1$b5O76dKM$SOugJJIU7pX5AvBewRz/O/:17386:0:99999:7:::
user2:!!:17391:0:99999:7:::
user3:!!:17391:0:99999:7:::
[root@VM_46_188_centos ~]# 

```
### 2. 锁定用户和解锁用户
> 
> 密码处是! * 都是不能登录的。
> 
> passwd -l user2  把用户密码前加上！！
> 
> passwd -u user2 解锁用户密码
> 
> usermod -L user2 也可以锁定用户user2
> 
> usermod -U user2 解锁用户user2
> 

### 3. 脚本中一条命令更改用户user2的密码：

passwd --stdin user2 更改用户密码

```
[root@VM_46_188_centos ~]# passwd --stdin user2
Changing password for user user2.
123456
passwd: all authentication tokens updated successfully.
[root@VM_46_188_centos ~]# 
```



- **echo "qing123." | passwd --stdin user2**

```
[root@VM_46_188_centos ~]# echo "qing123." | passwd --stdin user2
Changing password for user user2.
passwd: all authentication tokens updated successfully.
[root@VM_46_188_centos ~]# 
```

echo -e "123\naaa"  实现\n换行的用法

- **echo -e "qing123.\nqing123." | paswd user2**


```
[root@VM_46_188_centos ~]# echo -e "qing123.\nqing123." | passwd user
2Changing password for user user2.
New password: Retype new password: passwd: all authentication tokens 
updated successfully.[root@VM_46_188_centos ~]#
```
### 4. 密码的安全设定：

- 大于10位
- 包含数字、大小写字母、特殊符号
- 不能设置有规律性
- 不能包含名字、生日等信息


## 3.6 mkpasswd命令

mkpasswd生成随时密码工具：（make passwd）

如果没有mkpasswd命令，安装expect包

**yum install expect**

**选项：**

-l 长度位数

-s 特殊符号位数

```
[root@VM_46_188_centos ~]# mkpasswd 
40otte@YZ
[root@VM_46_188_centos ~]# mkpasswd -l 12
yme0qzt#3XFe
[root@VM_46_188_centos ~]# mkpasswd -l 12 -s 3
g\fn%HA2nw7*
[root@VM_46_188_centos ~]# mkpasswd -l 12 -s 0
8oaKipqcNfw3
[root@VM_46_188_centos ~]# 

```


mkpasswd -l 12 -s 0  长度12位,不用特殊符号



