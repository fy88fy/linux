[toc]
## 5.5 进入编辑模式
    i:在光标前插入
    I:定位到行首插入
    o:在光标所在行下插入
    O:在光标所在行上插入
    a:在光标后插入
    A:在行尾插入
## 5.6 vim命令模式
    /word 从光标处向后查询word
    n:向下查
    N:向上查
    
    ?word 从光标处向前查询word
    n:向上查
    N:向下查
    
    :1,100s/word1/word2/g  替换1到100行内word1 为word2
    
    :1,$s/dnsmasq/fxq/g 没有g时候只替换行中第一个
    
    :1,$s@dnsmasq@fxq@g
    
    :w
    :w!
    :q
    :wq 保存
    :x 保存 如果文件只打开了。wq 只改变了atime.x使用起来不加atmie
    :q!
    :wq!
    
    
    :set nu   显示行号
    :set nonu 不显示行号
    :nohl  不显示搜索高亮
    
    
## 5.7 vim实践

![image](http://note.youdao.com/yws/api/personal/file/76F7F7EF25C24289A26E739FE8B6AE29?method=download&shareKey=d3a48e1b78b2b6ca0723ba07270e292b)


![image](http://note.youdao.com/yws/api/personal/file/WEBe2ec31a0512e836bd04f40199a0dd138?method=download&shareKey=6e2985caed0a62262886ccdf6fa1999d)

## vim扩展：

**vim编辑器里面一些不为人知的操作**

> 1.vim编辑器的替换模式与可视模式
> 在一般模式下按键盘上的r和R进入替换模式。如果按小r那么这时候就进入了替换模式，你下一个输入的字符会把你当前光标所在处的字符替换，然后自动退出替换模式。如果你按的是大R那么你下面输入的所有字符会把后面的字符依次替换，直到按退出替换模式。
> 
> 在一般模式下按键盘上的v和V进入可视模式。如果是按小v那么这时候就时入了视图模式，这时候你移动光标会把你光标所在处到光标结尾处的所有字符选中，这时候可以进行复制，删除等操作。如果是按大V同样也是进入了视图模式，这时候移动光标会把光标所在行到光标结尾的行的所有内容选中，也可以进行复制，删除等操作。
> 
> 注意：使用在一般模式使用“ctrl+v”组合键可以进入块操作模式
> 在这个模式下和可视模式差不多，但是选择的内容不同，大家可实际操作看看
> 2.删除从光标所在处到行尾字符
> 
> 在一般模式下输入大写“Ｄ”或者输入“d$”
> 
> 3.删除从光标所在处到行首字符
> 
> 
> 在一般模式下输入大写“d^”
> 
> 
> 4.删除从光标所在行到文件末尾行
> 
> 
> 在一般模式下输入大写“dG”
> 
> 
> 5.删除指定范围内所有行
> 
> 例：删除10到15行的所有内容
> 在一般模式下输入“:10,15d”回车
> 
> 6.把正在编辑的文件另存为新文件
> 
> 例：把正在编辑的文件另存为到“/root/”下面并保存为1.txt
> 在一般模式下输入“:w /root/1.txt”
> 例：把正在编辑的文件的第10行到第15行另存为1.txt并保存到root目录下在一般模式下输入“:10,15 w /root/1.txt”
> 
> 7.把其它文件的内容导入到正在编辑的文件的光标所在处
> 
> 例：把“/root/1.txt” 文件的内容，导入到下在编辑的文件的第10行下面
> 首先在一般模式下按“10G”把光标定位到第10行
> 然后按“o”小写，当前行的下面另起一行，并进入插入模式
> 最后按键盘上的返回到一般模式，再输入“:r /root/1.txt”回车
> 
> 8.正在编辑文件时，不退出文件仍可以运行linux命令
> 
> 列：我下在编辑一个文件，但这时候我想查看“/root/1.txt” 文件的内容，但是我不想退出我正在编辑的文件，那么我们可以这样
> 在编辑模式下输入“:! cat /root/1.txt”
> 
> 9.把命令的执行结果导入到正在编辑的文件的光标所在处
> 
> 这题我们可以结合上面两题，在一般模式下输入“:r ! cat /root/1.txt”
> 
> 10.查找替换的功能使用
> 
> 例：在10到15行的行首增加“#”
> 在一般模式下输入“:10,15s/^/#/”
> 例：在10到15行的行首去掉“#”
> 在一般模式下输入“:10,15s/^#//”
> 例：在10到15行的行首增加“//”
> 在一般模式下输入“:10,15s/^/\/\//”或者“:10,15s@^@//@”或者“:10,15s#^#//#”
> 
> 
> 注意：在上面所有命令的最后面都可以加g或者c一起配合使用，g的意思是行中出现的所有指定字符都替换，但是如果加了g那么前面就不能出现位置定义字符，反之前面出现的位置定义字符，那么后面就不可以出现g。在后面加c可以跟用户交互，在查找到符合命令的字符提示用户是否替换，需要用户确认，否则不需要确认
> 
> 11.把输入的指定字符替换为指定的字符
> 
> 例：在编辑一个文档的时候，我要频繁的输入“abcdefghijklmnopqrstuvwxyz”这样的连续字符串，这时候我想只输入一个或者一串指定字符就可以替换为刚才的字符，比如我指定输入“aming”系统就会自动把“aming”替换成“abcdefghijklmnopqrstuvwxyz”
> 在一般模式下输入“:ab aming abcdefghijklmnopqrstuvwxyz”然后回车，再进入编辑模式，当你输入“aming”的时候就会发现自动替换成了“abcdefghijklmnopqrstuvwxyz”
> 
> 12.快捷键的定义
> 
> 例：我想在一般模式下按键盘上的ctrl+b快捷键，会自动在光标所在行的行首插入“#”号，然后自动退出到一般模式
> 在一般模式下输入“:map ctrl+v ctrl+b I # ”然后回车，这时候在一般模式按键盘上的ctrl+b的时候就会在光标所在的行首插入“#”号了
> 
> 注意：命令中ctrl+v和ctrl+b是键盘上的组合键，不是输入进去的字符，是需要按的组合键，其中第一个ctrl+v就照按，第二个ctrl+b是要定义的快捷键，根据自己需要的设置按。然后“I”的意思就是一般模式下的“I”进入插入模式并将光标移动到行首，然后接着输入“#”号，后面“”的意思是退出编辑模式
> 
> 13.同进编辑两个文件或者
> 
> 例：我现正在编辑１.txt文件，然后我想再打开root目录下的2.txt同时编辑，并把窗口上下水平分隔，一起显示
> 在一般模式下输入“:new /root/2.txt”
> 
> 例：我现正在编辑１.txt文件，然后我想再打开root目录下的2.txt同时编辑，并把窗口左右垂直分隔，一起显示
> 在一般模式下输入“:vsplit /root/2.txt”
> 
> 注意：在一般模式下按“ctrl+w”组合键，再按左右，或者上下方向键，可以在不同窗口之间切换如果在一般模式下输入“:only”那么只保留当前正在编辑的窗口，其它全关闭
> 
> 
> 15.在vim查找关键字时不区分大小写
> 
> 在一般模式下输入“:set ic”
> 如果想取消就输入“:set noic”
> 
> 16.如何把文件设置成只读文件，只有强制保存时才能保存
> 
> 在一般模式下输入“:set readonly”
> 
> 17.把文件恢复到打开时的状态
> 
> 在一般模式下输入“:e!”
> 
> 18.配置文件的使用
> 以上那么多操作，像设置忽略大小写，设定快捷键，设定自动替换，等一些操作，当电脑重启后就没有了。这时候我们可以把这些命令写入配置文件，这样电脑重启后还是可以使用，我们有两种方法
> 第一种：所有用户都统一
> 修改“/etc/vimrc”文件，在末尾加入需要设置的命令，就是我红色标注的部分
> 第二种：只对当前用户
> 修改用户家目录下的“.vimrc”文件，注意有个点，这是隐藏文件，一般用户家下没有，需要自己手工创建





> 
```
分享几个vim里常用的快捷键

1，注释所有的行：ctrl+v j/k+ shift +i
2，查看光标当前的man手册 shift+k
3，d+f+字符
4，选中面积 v+t+字符
5，在vim里面删除光标后的所有字符 shift+d
6，删除当前光标上的单词 d+i+w
7，选中当前光标上的单词 v+i+w
```

**vim 快速删除指定的一段字符**
```


因为不是一行，所以用dd不行，但用x去删除的话，又太慢。今天从网上发现一种特别快速删除的方法。
那就是使用da，如何使用，请看下面的例子。
比如，我的1.txt内容如下：

11111111111111111111111111
222222222222222222222222222222B3NzaC1yc2EAAAABIwAAAQEAv5oJvuIdaaVUsDOA2FbfnL0K2GbTc05Yg6TGM+8SNleI6bU5MhAy2uP5J4yCrMu43911hEJ2uh1UPycWX1O4xpEgUm8TGIs1HoQySnukv3g121uOLACRj37qqL9j4RRhrUxhunAW3alLSGIV0mxFD0ApyycFoLA/1I3hU7Yyx7tdripwz0FeHHhT3Qjfe9yC8Z6Ptq7cvBPXBBvc/G8pXVq3bnGMtj9Ifmbh7NnTvfHnEZGacf2MR4FSy0MMuNL0k3X5sBlsyP9/rXY9CPOh73eKUhZQoK3uWjwuDRp/dqrxgWDVeg0NZ+0t130pKu/LSREothWoVBu54rrtUUIdb3Sq0xsW4x9EhKGJJHPvBrbGbiDPTKBUaHdQEfmQQPAWeeX1hMC7lCunnfgTzf39Pv/2VpXz2l8NH2Jem0nrS48A6sf4eFz5VIakoRySMQu/6mY4s9aU3arbX+JvUE9s2/7D+JdqJlINtQqRU4V92LQq3BJaSMmKiwnPSytxDtARI3+8I2XXqFCJ5bBY7e3333333333333333333333
44444444444444444444444444444
我现在想删除22222222222和33333333333333333之间的字符只需要这样做：
1.  把1.txt的内容改成：
11111111111111111111111111
222222222222222222222222222222{B3NzaC1yc2EAAAABIwAAAQEAv5oJvuIdaaVUsDOA2FbfnL0K2GbTc05Yg6TGM+8SNleI6bU5MhAy2uP5J4yCrMu43911hEJ2uh1UPycWX1O4xpEgUm8TGIs1HoQySnukv3g121uOLACRj37qqL9j4RRhrUxhunAW3alLSGIV0mxFD0ApyycFoLA/1I3hU7Yyx7tdripwz0FeHHhT3Qjfe9yC8Z6Ptq7cvBPXBBvc/G8pXVq3bnGMtj9Ifmbh7NnTvfHnEZGacf2MR4FSy0MMuNL0k3X5sBlsyP9/rXY9CPOh73eKUhZQoK3uWjwuDRp/dqrxgWDVeg0NZ+0t130pKu/LSREothWoVBu54rrtUUIdb3Sq0xsW4x9EhKGJJHPvBrbGbiDPTKBUaHdQEfmQQPAWeeX1hMC7lCunnfgTzf39Pv/2VpXz2l8NH2Jem0nrS48A6sf4eFz5VIakoRySMQu/6mY4s9aU3arbX+JvUE9s2/7D+JdqJlINtQqRU4V92LQq3BJaSMmKiwnPSytxDtARI3+8I2XXqFCJ5bBY7e}3333333333333333333333
44444444444444444444444444444
注意，2222 和{ 之间没有换行。
2.  把光标移动到第一个{，也就是最后一个2后，然后输入da{ 即可把{}内的字符全部删除。

另外，除了可以使用{ 外，还可以使用 ", ', (  等成对的特殊符号。

其实vim还有一个比较常用的那就是v了，用v和d来删除也挺方便的：
打开一个文本后，直接按v，然后移动光标可以选中文本，当选中完你想要的文本后，直接按d，就删除了。
```

**vim 粘贴代码自动缩进导致全乱了**
```

用vim打开一个空白文档，然后把已经复制的代码给粘贴进来，发现它有自动缩进功能，最终导致粘贴的文本一行比一行靠右，看起来乱成一团。比较快的解决办法是，在粘贴文档前，在命令行模式下，输入
:set noai nosi
然后按'i' 进入编辑模式，再粘贴已经复制的代码内容，这样就不会自动缩进了。

有时候，这样的方法不好用，可以尝试这种：

:set paste
```

**客户端putty, xshell连接linux中vim的小键盘问题**

```


回复收藏
 分享 
在putty上用vi的时候，开NumLock时按小键盘上的数字键并不能输入数字，而是出现一个字母然后换行（实际上是命令模式上对应上下左右的键）。解决方法

选项Terminal->Features里，找到Disable application keypad mode，选上就可以了



在xmanager 4 中的xshell也有小键盘问题 解决方法为

修改session 属性 -> 终端->VT模式->初始数字键盘模式

选择 设置为普通。
```


**Linux用vim/vi给文件加密和解密**

```
文件加密

一、利用 vim/vi 加密：
优点：加密后，如果不知道密码，就看不到明文，包括root用户也看不了；
缺点：很明显让别人知道加密了，容易让别人把加密的文件破坏掉，包括内容破坏和删除；

vi编辑器相信大家都很熟悉了吧，vi里有一个命令是给文件加密的，举个例子吧：
1）首先在root主目录/root/下建立一个实验文件text.txt：
[root@www ~]#  vim/vi  text.txt
2）进到编辑模式，输入完内容后按ESC，然后输入:X（注意是大写的X），回车；
3）这时系统提示让你输入密码，2次，如下所示：
输入密码: *******
请再输入一次: *******
4） 保存后退出，现在这个文件已经加密了；
5）  用cat或more查看文件内容，显示为乱码；用 vim/vi 重新编辑这个文件，会提示输入密码，如果输入的密码不正确，同样会显示为乱码！
注意：文件加密后，千万别忘了密码！


二、解密用vi加密的文件（前提是你知道加密的密码）：
1）用 vim/vi 打开文件如text.txt，要输入正确的密码，然后在编辑时，将密码设置为空，方法是输入下面的命令：
：set key=
然后直接回车，保存文件后，文件已经解密了。
2） 或者这样也行：
在正确打开文件后用 “:X” 指令，然后给一个空密码也可以。保存用“wq!”保存。
两种方法实际上效果是一样的
```
