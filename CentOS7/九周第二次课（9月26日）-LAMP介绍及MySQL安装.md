[toc]
## 11.1 LAMP架构介绍
     LAMP架构介绍：
    1.LAMP:linux+ Apache(httpd)+MySQL+PHP
    2.PHP网站（Google、淘宝、百度等）
    3.三个角色可以在一台机器上，也可以分开（httpd和PHP要在一起）.
## 11.2 MySQL、MariaDB介绍
    1.mysql->sun收购(2008,10亿$)->oracle收购（2009年,74亿$）

    2.官网：www.mysql.com
    3. mysql 5.6变化比较大   mysql5.7性能提升
        
    4. mariadb

        mariadb 5.5 ---mysql 5.5
        
        mariadb 10.0----mysql 5.6

**版本介绍:**

    community 社区版
    Enterprise企业版
    GA（通用版,生产环境中用）
    DMR开发里程碑发布版
    RC发行候选版
    Beta开发测试版
    Alpha内部测试版

## 11.3/11.4/11.5 MySQL安装
### 1.安装包类型
    （1）rpm 
    （2）源码
    （3）二进制免编译
### 2.安装
    (1) cd /usr/local/src/
    
    (2) wget 对应的包
        5.6_32位二进制包：wget http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.35-linux-glibc2.5-i686.tar.gz
        5.6_64位二进制包：wget http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.35-linux-glibc2.5-x86_64.tar.gz
    
    (3) tar -zxvf mysql-5.6.35-linux-glibc2.5-i686.tar.gz
    
    (4) cp -R  mysql-5.6.35-linux-glibc2.5-i686 /usr/local/mysql
    
    (5) cd /usr/local/mysql/
    
    (6) useradd mysql
    
    (7) mkdir /data/
    
    (8) ./scripts/mysql_install_db --user=mysql --datadir=/data/mysql
    
        查询提示少的包，通过yum进行查找:yum list | grep -i perl | grep -i dumper
    
    (9) cp support-files/my-default.cnf /etc/my.cnf
    
    (10) vi /etc/my.cnf
        改为:
        datadir=/data/mysql
        socket=/tmp/mysql.sock

**安装报错1：**

./scripts/mysql_install_db --user=mysql --datadir=/data/mysql
        后报错：
    
```
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# ./scripts/mysql_install_db --user=mysql --da
tadir=/data/mysql/FATAL ERROR: please install the following Perl modules before executing ./scripts/mysql_ins
tall_db:Data::Dumper

```
     解决方法：yum install perl-Data-Dumper
    
**安装报错2：**
```
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# ./scripts/mysql_install_db --user=mysql --da
tadir=/data/mysql/Installing MySQL system tables..../bin/mysqld: error while loading shared libraries: libaio
.so.1: cannot open shared object file: No such file or directory[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# 

```
    解决方法: yum install libaio
  
```
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# ls /etc/my.cnf
/etc/my.cnf
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# rpm -qf /etc/my.cnf
mariadb-libs-5.5.52-1.el7.i686
[root@fxq-0 mysql-5.6.35-linux-glibc2.5-i686]# 

```
    (11) cp support-files/mysql.server /etc/init.d/mysqld
    
    (12) vi /etc/init.d/mysqld
        
        加入下面两行的配置:
        basedir=/usr/local/mysql
        datadir=/data/mysql
    
    (13) service mysqld start
        /etc/init.d/mysqld start
        /usr/local/mysql/bin/mysqld_safe --datadir=/data/mysql --pid-file=/data/mysql/fxq-0.pid &
        /usr/local/mysql/bin/mysqld_safe --defaults-file=/etc/my.cnf --user=mysql --datadir=/data/mysql &
    
**启动报错：**
```
service mysqld start
/etc/init.d/mysqld: line 256: my_print_defaults: command not found
/etc/init.d/mysqld: line 276: cd: /usr/local/mysql: No such file or directory
Starting MySQL ERROR! Couldn't find MySQL server (/usr/local/mysql/bin/mysqld_safe)
```
    (没有执行：cp -R  mysql-5.6.35-linux-glibc2.5-i686 /usr/local/mysql)

**杀死进程经验**：

    killall 先停止读写，比kill -9 靠谱
    killall之后，如果等了一段时间进程还在。说明还有数据库在写入硬盘，不可强行杀死，否则可能会造成数据丢失。




## 扩展
mysql5.5源码编译安装 http://www.aminglinux.com/bbs/thread-1059-1-1.html

```
首先安装必要的库
yum -y install gcc*
###### 安装 MYSQL ######
首先安装camke 
一、支持YUM，则
yum install -y cmake
二、也可以源码安装
cd /usr/local/src
#下载cmake
wget http://www.cmake.org/files/v2.8/cmake-2.8.7.tar.gz
tar zxvf cmake-2.8.7.tar.gz
cd cmake-2.8.7
#安装cmake
./configure
make
make install
安装 MYSQL
官网下载 MYSQL5.5版本 linux下源码包
http://dev.mysql.com/downloads/
安装
groupadd mysql
useradd -g mysql mysql
tar zxvf mysql-5.2.25.tar.gz
cd mysql-5.2.25
#cmake  .              //默认情况下安装,安装目录为/usr/local/mysql  数据目录为/usr/local/mysql/data
#也可以指定参数安装，如指定UTF8，数据引擎等
#具体参照http://dev.mysql.com/doc/refman/ ... ration-options.html
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql -DMYSQL_DATADIR=/mysql/data -DDEFAULT_CHARSET=utf8  -DDEFAULT_COLLATION=utf8_general_ci -DWITH_EXTRA_CHARSETS:STRING=all -DWITH_DEBUG=0 -DWITH_SSL=yes -DWITH_READLINE=1 -DENABLED_LOCAL_INFILE=1
make &amp;&amp; make install
cd /usr/local/mysql
chown -R mysql:mysql  /usr/local/mysql
./scripts/mysql_install_db --user=mysql  -datadir=/mysql/data
#此处如不指定datadir，到启动时会报错
chown -R root .
chown -R mysql data
cp support-files/my-medium.cnf /etc/my.cnf
bin/mysqld_safe --user=mysql &amp;
# Next command is optional
cp support-files/mysql.server /etc/init.d/mysqld
chmod +x /etc/init.d/mysqld
/etc/init.d/mysqld start
到此，安装完成
```



mysql5.7二进制包安装（变化较大） http://www.apelearn.com/bbs/thread-10105-1-1.html

```
1. 下载包 

wget   http://mirrors.sohu.com/mysql/MySQL-5.7/mysql-5.7.12-linux-glibc2.5-x86_64.tar.gz 

wget http://mirrors.sohu.com/mysql/MySQL-5.7/mysql-5.7.17-linux-glibc2.5-i686.tar.gz

若该链接失效，请到r.aminglinux.com 找最新的下载地址。

2. 解压 

tar  xxvf mysql-5.7.12-linux-glibc2.5-x86_64.tar.gz

mv  mysql-5.7.12-linux-glibc2.5-x86_64  /usr/local/mysql

3. 初始化

useradd -M -s /sbin/nologin  mysql 

mkdir -p /data/mysql

chown mysql /data/mysql

cd /usr/local/mysql

./bin/mysqld  --initialize --user=mysql --datadir=/data/mysql

注意，这一步最后一行会有一个提示

[Note] A temporary password is generated for root@localhost: B*s1i(*,kXwg

最后面的字符串为root密码。

./bin/mysql_ssl_rsa_setup --datadir=/data/mysql

4. 拷贝配置文件和启动脚本

cp support-files/my-default.cnf  /etc/my.cnf  

vim /etc/my.cnf //编辑或者修改

basedir = /usr/local/mysql

datadir = /data/mysql

port = 3306

socket = /tmp/mysql.sock

cp support-files/mysql.server /etc/init.d/mysqld

vi /etc/init.d/mysqld   //编辑或者修改

basedir=/usr/local/mysql

datadir=/data/mysql

5. 启动服务

/etc/init.d/mysqld start

6. 设置root密码

使用初始化密码登录

/usr/local/mysql/bin/mysql -uroot -p'B*s1i(*,kXwg'  //进入后直接设置密码

mysql>set password = password('mypass');   //一定要设置一下新密码

退出来，再使用新的密码登录就可以了

还有一种情况，就是不知道初始化密码

vi /etc/my.cnf

在[mysqld]下面增加一行

skip-grant-tables

重启  /etc/init.d/mysqld restart

/usr/local/mysql/bin/mysql -uroot 

mysql> update user set authentication_string=password('123333') where user='root';

退出来后，更改my.cnf，去掉刚加的 skip-grant-tables

重启 /etc/init.d/mysqld restart

此时就可以使用新的密码了。
```